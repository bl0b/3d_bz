# ewwww....
# alias the clang-format tool because it isnt in PATH - it is sometimes problematic to add visual studio tools to path,
# because they can be incompatible (x86 vs x64 compiler/linkers, for example).
$cf = "C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\Common7\IDE\VC\vcpackages\clang-format.exe"

$dirs = "src", "test"
$exts = "cl", "h", "cpp", "hpp", "comp", "frag", "vert", "geom"

Foreach($dir in $dirs) { 
Foreach ($ext in $exts) {


  cd $dir
  Get-ChildItem -Path . -Recurse -Include *.$ext | foreach { 
    # todo: don't do this in third_party!
    if (-Not ($_.FullName -like '*src\engine\third_party\*')) {
      &$cf -i -style=file $_.FullName 
	}
    
  }
  cd ..

}
}
