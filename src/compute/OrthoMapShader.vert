#version 450 core

// x and y coordinates on original depth image - defines direction of
layout(location = 0) in vec4 pos;

// depth camera intrinsics,
uniform mat4 pos_tform;
uniform vec2 min_coords;
uniform vec2 max_coords;
uniform float max_depth;

// if rendering to custom viewport...
uniform bool custom_viewport;
uniform vec2 custom_viewport_min;
uniform vec2 custom_viewport_max;
uniform vec2 full_screen_dims;
out vec2 vert_orig_ndc;

out noperspective float vert_z_pos;
out vec4 vert_plane_pos;

void main() {

  vec4 new_pos = pos_tform * pos;

  if (custom_viewport) {
    // new_pos.x =

    // float pct on screen = new_pos.x / full_screen_dims.x

    // new_pos.x = custom_viewport_min.x + (new_pos.x / full_screen_dims.x) * (custom_viewport_max.x
    // - custom_viewport_min.x); new_pos.y = custom_viewport_min.y + (new_pos.y /
    // full_screen_dims.y) * (custom_viewport_max.y - custom_viewport_min.y);
  }

  gl_Position = vec4(
      (2 * (new_pos.x - min_coords.x) / (max_coords.x - min_coords.x)) - 1,
      (2 * (new_pos.y - min_coords.y) / (max_coords.y - min_coords.y)) - 1,
      // just for some depth mask
      new_pos.z / max_depth,
      1);

  vert_orig_ndc = gl_Position.xy;

  if (custom_viewport) {
    // mapped from 0 to 1 instead..
    vec2 start_out_pixel = ((gl_Position.xy + 1.) / 2.);
    vec2 end_out_pixel =
        (custom_viewport_min / full_screen_dims) +
        (start_out_pixel * (custom_viewport_max - custom_viewport_min) / full_screen_dims);
    gl_Position.xy = (2. * end_out_pixel) - 1;
  }

  vert_plane_pos = pos;
  vert_z_pos = new_pos.z;
}