#pragma once
#include "ComputeLib.hpp"

namespace ComputeLib {

class DiffDepthImages : protected ComputeBase {
#include <generated/DiffDepthImages_cl_signature.hpp>

  Engine::Gpu::Buff<unsigned int> num_depth_diffs_gpu;
  void get_depth_diffs(
      Engine::Gpu::Tex<Engine::Gpu::TF::R16UI>& orig_img,
      Engine::Gpu::Tex<Engine::Gpu::TF::R16UI>& rendered_img,
      Engine::Gpu::Tex<Engine::Gpu::TF::R16UI>& rendered_img_mesh_ids,
      unsigned int* num_depth_diffs,
      Engine::Gpu::Buff<ivec4>& depth_diffs) {

    num_depth_diffs_gpu.set_storage(1);
    num_depth_diffs_gpu.cu_memset(0);

    const auto& d = orig_img.get_dims();

    const int minif_factor = 2;

    // 2 entries per coord!
    depth_diffs.set_storage((d.x * d.y * 2) / (minif_factor * minif_factor));

    kernel_DiffDepthImages_get_depth_diffs(
        d / minif_factor,
        {8, 8},
        orig_img,
        rendered_img,
        rendered_img_mesh_ids,
        num_depth_diffs_gpu,
        depth_diffs,
        minif_factor);

    num_depth_diffs_gpu.cu_copy_to(num_depth_diffs, 1);
  }

  Engine::Gpu::Buff<unsigned int> num_edge_diffs_gpu;
  void get_edge_diffs(
      Engine::Gpu::Tex<Engine::Gpu::TF::R16UI>& orig_plane_img,
      Engine::Gpu::Tex<Engine::Gpu::TF::R16UI>& rendered_img,
      Engine::Gpu::Tex<Engine::Gpu::TF::R16UI>& rendered_img_mesh_ids,
      unsigned int* num_edge_diffs,
      Engine::Gpu::Buff<ivec4>& edge_diffs) {

    num_edge_diffs_gpu.set_storage(1);
    num_edge_diffs_gpu.cu_memset(0);

    const auto& d = orig_plane_img.get_dims();

    edge_diffs.set_storage(d.x * d.y);

    kernel_DiffDepthImages_get_edge_diffs(
        d,
        {8, 8},
        orig_plane_img,
        rendered_img,
        rendered_img_mesh_ids,
        num_edge_diffs_gpu,
        edge_diffs);

    num_edge_diffs_gpu.cu_copy_to(num_edge_diffs, 1);
  }

  Engine::Gpu::Buff<unsigned int> __local_empty_count;
  Engine::Gpu::Buff<ivec4> __local_empties;
  void find_nearest_empty_pixels(
      Engine::Gpu::Tex<Engine::Gpu::TF::R16UI>& orig_img,
      unsigned int num_to_check,
      Engine::Gpu::Buff<ivec4>& check_coords,
      Engine::Gpu::Buff<ivec4>& nearest_empty_coords) {

    const unsigned int block = 6;

    if (num_to_check) {
      nearest_empty_coords.set_storage(check_coords.size());

      __local_empty_count.set_storage(check_coords.size());
      __local_empty_count.cu_memset(0);
      __local_empties.set_storage(check_coords.size() * block * block);

      kernel_DiffDepthImages_find_nearest_empty_pixels(
          ivec3{num_to_check, block, block},
          ivec3{1, block, block},
          orig_img,
          num_to_check,
          check_coords,
          nearest_empty_coords,
          __local_empty_count,
          __local_empties);
    }
  }

  void convert_depth_img_pairs_to_plane_space_vectors(
      rs2_intrinsics intrinsics,
      Engine::Math::Plane p,
      unsigned int num_coords,
      Engine::Gpu::Buff<ivec4>& start_coords,
      Engine::Gpu::Buff<ivec4>& nearest_coords,
      Engine::Gpu::Buff<vec4>& start_coords_out,
      Engine::Gpu::Buff<vec4>& nearest_coords_out) {

    // double entries per coord pair!
    start_coords_out.set_storage(start_coords.size());
    nearest_coords_out.set_storage(start_coords.size());

    if (num_coords) {

      kernel_DiffDepthImages_deproj_coords(
          num_coords,
          32,
          intrinsics,
          num_coords,
          start_coords,
          nearest_coords,
          start_coords_out,
          nearest_coords_out);
    }
  }
};

} // namespace ComputeLib