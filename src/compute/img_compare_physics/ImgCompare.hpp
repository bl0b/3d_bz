#pragma once
//#include "ComputeLib.hpp"

#include <cassert>

namespace ComputeLib {

using namespace Engine::Gpu;

class ImgCompare : protected ComputeBase {
#include <generated/ImgCompare_cl_signature.hpp>

  // for a given set of intrinsics, generates a texture of normalized ray directions for each pixel
  // in an image made by a camera with the given intrinsics.
  void gen_ray_dirs(Tex<TF::RGBA32F>& ray_dirs_out, rs2_intrinsics i) {
    ray_dirs_out.set_storage({i.width, i.height});
    kernel_ImgCompare_gen_ray_dirs({i.width, i.height}, {8, 8}, ray_dirs_out, i);
  }

  Buff<unsigned int> __num_edge_pixels;
  Tex<TF::R16UI> __empty_img_mesh_ids;
  void find_edge_pixels(
      Tex<TF::R16UI>& depth_img,
      unsigned int* num_edge_pixels,
      Buff<ivec4>& edge_pixels_out,
      Tex<TF::RGBA8>& debug_out,
      Tex<TF::R16UI>* depth_img_mesh_ids = nullptr) {

    const auto d = depth_img.get_dims();
    edge_pixels_out.set_storage(d.x * d.y);
    __num_edge_pixels.set_storage(1);
    __num_edge_pixels.cu_memset(0);

    bool write_mesh_ids = depth_img_mesh_ids != nullptr;
    Tex<TF::R16UI>& mesh_ids = write_mesh_ids ? *depth_img_mesh_ids : __empty_img_mesh_ids;

    kernel_ImgCompare_find_edge_pixels(
        d,
        ivec2{8, 8},
        depth_img,
        __num_edge_pixels,
        edge_pixels_out,
        (int)(write_mesh_ids ? 1 : 0),
        mesh_ids,
        debug_out);

    __num_edge_pixels.cu_copy_to(num_edge_pixels, 1);
  }

  Buff<int> __local_dir_counts;
  void find_edge_line_normals(
      Tex<TF::R16UI>& depth_img,
      unsigned int num_edge_pixels,
      Buff<ivec4>& edge_pixels_in,
      Buff<vec4>& normals_out, // 2d normalized directions in image! still working in 2d here
      Tex<TF::RGBA8>& debug_out) {

    if (!num_edge_pixels) { printf("no pixels!\n"); }

    normals_out.set_storage(edge_pixels_in.size());
    // 2 entries for each coord = x and y!
    __local_dir_counts.set_storage(edge_pixels_in.size() * 2);
    __local_dir_counts.cu_memset(0);

    kernel_ImgCompare_find_edge_line_normals(
        {num_edge_pixels, 9},
        {1, 9},
        depth_img,
        num_edge_pixels,
        edge_pixels_in,
        __local_dir_counts,
        normals_out,
        debug_out);
  }

  Buff<unsigned int> __num_coords;
  void find_overlapping_coords(
      unsigned int num_coords,
      Buff<ivec4>& coords,
      Tex<TF::R16UI>& depth_img,
      unsigned int* num_overlapping_coords,
      Buff<ivec4>& overlapping_coords,
      Tex<TF::R16UI>* depth_img_mesh_ids = nullptr) {

    overlapping_coords.set_storage(num_coords);
    __num_coords.set_storage(1);
    __num_coords.cu_memset(0);

    bool write_mesh_ids = depth_img_mesh_ids != nullptr;
    Tex<TF::R16UI>& mesh_ids = write_mesh_ids ? *depth_img_mesh_ids : __empty_img_mesh_ids;

    kernel_ImgCompare_find_overlapping_coords(
        num_coords,
        32,
        num_coords,
        coords,
        depth_img,
        (int)(write_mesh_ids ? 1 : 0),
        mesh_ids,
        __num_coords,
        overlapping_coords);

    __num_coords.cu_copy_to(num_overlapping_coords, 1);
  }

  void generate_edge_diff_impulses_out(
      uint num_overlap_coords,
      Buff<ivec4>& overlap_img_coords,
      Buff<vec4>& overlap_edge_normals,     // 2d normals on depth img
      Tex<TF::RGBA32F>& rendered_img_vtxes, // NON-perspective-projected rasterization of 3d
                                            // positions matching depth img
      rs2_intrinsics cam_intrinsics,
      mat4 rendered_img_vtxes_tform, // (need to convert to world space from camera space though)
      // vec4 obj_world_pos, // impulses out are in world space, relative to xyz position of object.
      Buff<mat4>& obj_world_positions,
      Buff<vec4>& impulses_out) {

    // each impulse: direction & application
    impulses_out.set_storage(2 * num_overlap_coords);

    kernel_ImgCompare_generate_impulses_out(
        num_overlap_coords,
        32,
        num_overlap_coords,
        overlap_img_coords,
        overlap_edge_normals,
        rendered_img_vtxes,
        cam_intrinsics,
        rendered_img_vtxes_tform,
        glm::inverse(rendered_img_vtxes_tform),
        obj_world_positions,
        impulses_out);
  }

  Buff<unsigned int> __num_points;
  void get_points_minif(
      Tex<TF::R16UI>& depth_img_1,
      Tex<TF::R16UI>& depth_img_2,
      int minif,
      unsigned int* num_points,
      Buff<ivec4>& points) {

    const auto out_d = depth_img_1.get_dims() / minif;
    points.set_storage(out_d.x * out_d.y);
    __num_points.set_storage(1);
    __num_points.cu_memset(0);

    kernel_ImgCompare_get_points_minified(
        out_d, {8, 8}, depth_img_1, depth_img_2, minif, __num_points, points);

    __num_points.cu_copy_to(num_points, 1);
  }

  void find_points_diffs_impulses(
      unsigned int num_points,
      Buff<ivec4>& coords,
      Tex<TF::RGBA32F>& points_1,
      Tex<TF::RGBA32F>& points_2,
      Tex<TF::R16UI>& points_2_mesh_ids,
      mat4 cam_to_world,
      Buff<mat4>& obj_world_positions,
      // vec4 obj_world_pos,
      Buff<vec4>& impulses_out) {

    impulses_out.set_storage(num_points * 2);

    kernel_ImgCompare_find_point_diffs_impulses(
        num_points,
        32,
        num_points,
        coords,
        points_1,
        points_2,
        points_2_mesh_ids,
        cam_to_world,
        obj_world_positions,
        impulses_out);
  }

  Buff<unsigned int> __edge_violation_count;
  Buff<unsigned int> __diff_count;
  Buff<unsigned int> __sum_diff;
  void compute_img_difference(Tex<TF::R16UI>& img1, Tex<TF::R16UI>& img2, float* img_cost) {

    __edge_violation_count.set_storage(1);
    __edge_violation_count.cu_memset(0);
    __diff_count.set_storage(1);
    __diff_count.cu_memset(0);
    __sum_diff.set_storage(1);
    __sum_diff.cu_memset(0);

    kernel_ImgCompare_compute_img_diff(
        img1.get_dims(), {8, 8}, img1, img2, __edge_violation_count, __diff_count, __sum_diff);

    unsigned int edge_violation_count;
    __edge_violation_count.cu_copy_to(&edge_violation_count, 1);
    unsigned int diff_count;
    __diff_count.cu_copy_to(&diff_count, 1);
    unsigned int sum_diff;
    __sum_diff.cu_copy_to(&sum_diff, 1);

    float avg_diff = diff_count ? (sum_diff * 1.f) / diff_count : 0;
    float edge_err = edge_violation_count * 1.f;

    printf("avg diff: %f -- edge_err: %f\n", avg_diff, edge_err);
  }

  void find_edge_pixels_2(
      Tex<TF::R16UI>& depth_img,
      unsigned int* num_edge_pixels,
      Buff<ivec4>& edge_pixels_out,
      Tex<TF::RGBA8>& debug_out) {

    const auto d = depth_img.get_dims();
    edge_pixels_out.set_storage(d.x * d.y);
    __num_edge_pixels.set_storage(1);
    __num_edge_pixels.cu_memset(0);

    // bool write_mesh_ids = depth_img_mesh_ids != nullptr;
    // Tex<TF::R16UI>& mesh_ids = write_mesh_ids ? *depth_img_mesh_ids : __empty_img_mesh_ids;

    kernel_ImgCompare_find_edge_pixels_2(
        d, ivec2{8, 8}, depth_img, __num_edge_pixels, edge_pixels_out, debug_out);

    __num_edge_pixels.cu_copy_to(num_edge_pixels, 1);
  }

  void draw_debug(
      Tex<TF::RGBA8>& debug_img,
      Buff<vec4>& coords,
      unsigned int num_to_draw,
      unsigned int start_coord,
      unsigned int total_coords) {
    kernel_ImgCompare_draw_debug(
        num_to_draw, 32, debug_img, coords, num_to_draw, start_coord, total_coords);
  }

  Buff<unsigned int> _all_costs2;
  void gen_costs2(
      Tex<TF::RG16UI>& src_img_split,
      Tex<TF::RG16UI>& rendered_img, // x channel
      Buff<vec4>& vp_buff,
      unsigned int num_vps,
      vector<float>& costs,
      unsigned int mismatch_scale,
      rs2_intrinsics intrinsics,
      mat4 cam_to_world,
      OrthoSampler2D ortho_stencil_sampler,
      Tex<TF::R16UI>& ortho_stencil) {

    _all_costs2.set_storage(num_vps);
    _all_costs2.cu_memset(0);

    kernel_ImgCompare_gen_costs2(
        {num_vps, src_img_split.get_dims().x},
        {1, 32},
        src_img_split,
        rendered_img,
        vp_buff,
        num_vps,
        _all_costs2,
        mismatch_scale,
        intrinsics,
        cam_to_world,
        ortho_stencil_sampler,
        ortho_stencil);

    vector <unsigned int> costs_uint(num_vps);
    _all_costs2.cu_copy_to(costs_uint.data(), num_vps);

    costs.clear();
    const int num_pixels = src_img_split.get_dims().x * src_img_split.get_dims().y;
    for (auto c : costs_uint) {
      costs.emplace_back(c * 1.f / num_pixels);
    }

  }

  Buff<unsigned int> _all_costs; //, _all_counts;//, _count;
  // Buff<ivec4> coords_with_bbox;
  void gen_all_costs(
      Tex<TF::R16UI>& src_img,
      Tex<TF::R16UI>& mesh_ids,
      unsigned int num_mesh_ids,
      Tex<TF::R16UI>& rendered_img,
      vector<float>& costs,
      Buff<vec4>& vp_buff,
      unsigned int num_vps) {

    _count.set_storage(1);
    _count.cu_memset(0);

    _counts.set_storage(num_mesh_ids);
    _counts.cu_memset(0);

    const ivec2 d = mesh_ids.get_dims();
    coords_with_bbox.set_storage(d.x * d.y);

    kernel_ImgCompare_gen_coords_in_bbox(d, {8, 8}, mesh_ids, _count, _counts, coords_with_bbox);
    unsigned int num_coords;
    _count.cu_copy_to(&num_coords, 1);

    vector<unsigned int> counts(num_mesh_ids);
    _counts.cu_copy_to(counts.data(), num_mesh_ids);

    // 2d array!
    _all_costs.set_storage(num_mesh_ids * num_vps);
    _all_costs.cu_memset(0);

    unsigned int mismatch_scale = 50; // add to cost in case of src/rendered presence mismatch
    kernel_ImgCompare_gen_all_costs(
        ivec2{num_vps, num_coords},
        ivec2{1, 32},
        num_vps,
        vp_buff,
        num_coords,
        coords_with_bbox,
        num_mesh_ids,
        src_img,
        rendered_img,
        _all_costs,
        mismatch_scale);

    vector<unsigned int> raw_costs(num_mesh_ids * num_vps);
    _all_costs.cu_copy_to(raw_costs.data(), num_mesh_ids * num_vps);

    costs.resize(num_mesh_ids * num_vps);

    for (int i = 0; i < raw_costs.size(); i++) {
      // avg!
      costs[i] = raw_costs[i] * 1.f / counts[i % num_mesh_ids];
    }
  }

  Buff<unsigned int> _count, _counts, _costs;
  Buff<ivec4> coords_with_bbox;
  void gen_costs(
      Tex<TF::R16UI>& src_img,
      Tex<TF::R16UI>& mesh_ids,
      unsigned int num_mesh_ids,
      Tex<TF::R16UI>& rendered_img,
      vector<float>& costs,
      Buff<vec4>& vp_buff,
      unsigned int vp_id) {

    _costs.set_storage(num_mesh_ids);
    _costs.cu_memset(0);

    _counts.set_storage(num_mesh_ids);
    _counts.cu_memset(0);

    _count.set_storage(1);
    _count.cu_memset(0);

    const ivec2 d = mesh_ids.get_dims();

    coords_with_bbox.set_storage(d.x * d.y);

    kernel_ImgCompare_gen_coords_in_bbox(d, {8, 8}, mesh_ids, _count, _counts, coords_with_bbox);
    unsigned int num_coords;
    _count.cu_copy_to(&num_coords, 1);

    vector<unsigned int> counts(num_mesh_ids);
    _counts.cu_copy_to(counts.data(), num_mesh_ids);

    unsigned int mismatch_scale = 50; // add to cost in case of src/rendered presence mismatch
    kernel_ImgCompare_gen_costs(
        num_coords,
        32,
        coords_with_bbox,
        num_coords,
        src_img,
        rendered_img,
        _costs,
        mismatch_scale,
        vp_buff,
        vp_id);

    vector<unsigned int> raw_costs(num_mesh_ids);
    _costs.cu_copy_to(raw_costs.data(), num_mesh_ids);

    costs.resize(num_mesh_ids);

    for (int i = 0; i < costs.size(); i++) {
      // avg!
      costs[i] = raw_costs[i] * 1.f / counts[i];
    }
  }
};

} // namespace ComputeLib