#pragma once

#include <engine/Engine.h>

template <std::size_t size, class T>
std::string __get_vec_name() {
  char b[10];
  sprintf(b, "%svec%d", std::is_integral<T>::value ? "i" : "", size);
  return std::string(b);
}

namespace glm {

template <class Ar, std::size_t size, class T, glm::precision q>
std::string save_minimal(Ar const&, glm::vec<size, T, q> const& v) {
  std::string s_out = __get_vec_name<size, T>();
  s_out += "{";
  for (int i = 0; i < size; i++) {
    s_out += std::to_string((&v.x)[i]);
    if (i < size - 1) { s_out += ", "; }
  }
  s_out += "}";
  return s_out;
}

template <class Ar, std::size_t size, class T, glm::precision q>
void load_minimal(Ar const&, glm::vec<size, T, q>& v, std::string const& s) {

  std::string vec_name = __get_vec_name<size, T>();
  if (s.find(vec_name, 0) != 0) {
    printf("error parsing GLM vector type json\n");
    throw std::invalid_argument("errrr");
  }

  int start = vec_name.size() + 1;
  int end;
  vector<string> fs;
  for (int i = 0; i < size - 1; i++) {
    end = s.find(", ", start);
    fs.emplace_back(s.substr(start, end - start));
    start = end + 1;
  }
  fs.emplace_back(s.substr(start, s.size() - 1 - start));

  for (int i = 0; i < size; i++) {
    const auto& _s = fs[i];
    if (std::is_integral<T>::value) {
      (&v.x)[i] = std::stoi(_s);
    } else {
      (&v.x)[i] = std::stof(_s);
    }
  }
}

} // namespace glm