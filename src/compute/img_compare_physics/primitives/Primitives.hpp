#pragma once

#include <engine/Engine.h>

class Primitives {
public:
  static void make_cube(InstancedMesh& m, vec3 scale) {

    vector<vec4> vtxes{{-0.5, -0.5, 0.5, 1.0},
                       {0.5, -0.5, 0.5, 1.0},
                       {0.5, 0.5, 0.5, 1.0},
                       {-0.5, 0.5, 0.5, 1.0},
                       {-0.5, -0.5, -0.5, 1.0},
                       {0.5, -0.5, -0.5, 1.0},
                       {0.5, 0.5, -0.5, 1.0},
                       {-0.5, 0.5, -0.5, 1.0}};

    vector<unsigned int> idxes{0, 1, 2, 2, 3, 0, 1, 5, 6, 6, 2, 1, 7, 6, 5, 5, 4, 7,
                               4, 0, 3, 3, 7, 4, 4, 5, 1, 1, 0, 4, 3, 2, 6, 6, 7, 3};

    vector<vec4> vtxes_scaled;
    for (auto& v : vtxes) {
      vtxes_scaled.emplace_back(glm::scale(scale) * v);
    }

    m.positions.set_data(vtxes_scaled);
    m.num_triangles = 12;
    m.idxes.set_data(idxes);
  }

  static void make_sphere(InstancedMesh& m, vec3 scale, mat4 orientation, const int sections) {

    const auto n3 = [](float a, float b, float c) -> vec4 {
      return vec4{glm::normalize(vec3{a, b, c}), 1.f};
    };

    const auto nv = [](vec3 v) -> vec4 { return vec4{glm::normalize(v), 1.f}; };

    const auto refine =
        [nv](vector<vec4>& pts, vector<unsigned int>& idxes_in, vector<unsigned int>& idxes_out) {
          //vector<unsigned int> idxes_2;
          idxes_out.clear();

          const int num_t_0 = idxes_in.size() / 3;

          for (int i = 0; i < num_t_0; i++) {
            vector<unsigned int> tri = {idxes_in[i * 3], idxes_in[i * 3 + 1], idxes_in[i * 3 + 2]};
            vector<vec4> tri_v = {pts[tri[0]], pts[tri[1]], pts[tri[2]]};

            vec4 ab_mid = nv((tri_v[0].xyz() + tri_v[1].xyz()) / 2.f);
            vec4 bc_mid = nv((tri_v[1].xyz() + tri_v[2].xyz()) / 2.f);
            vec4 ca_mid = nv((tri_v[2].xyz() + tri_v[0].xyz()) / 2.f);

            unsigned int a_i = tri[0];
            unsigned int b_i = tri[1];
            unsigned int c_i = tri[2];
            unsigned int ab_mid_i = pts.size();
            unsigned int bc_mid_i = pts.size() + 1;
            unsigned int ca_mid_i = pts.size() + 2;

            pts.push_back(ab_mid);
            pts.push_back(bc_mid);
            pts.push_back(ca_mid);

            idxes_out.push_back(a_i);
            idxes_out.push_back(ab_mid_i);
            idxes_out.push_back(ca_mid_i);

            idxes_out.push_back(b_i);
            idxes_out.push_back(bc_mid_i);
            idxes_out.push_back(ab_mid_i);

            idxes_out.push_back(c_i);
            idxes_out.push_back(ca_mid_i);
            idxes_out.push_back(bc_mid_i);

            idxes_out.push_back(ab_mid_i);
            idxes_out.push_back(bc_mid_i);
            idxes_out.push_back(ca_mid_i);
          }
        };

    // create 12 vertices of a icosahedron
    const float t = (1.0f + sqrt(5.0f)) / 2.0f;

    vector<vec4> pts = {n3(-1, t, 0.),
                        n3(1, t, 0.),
                        n3(-1, -t, 0.),
                        n3(1, -t, 0.),
                        n3(0, -1, t),
                        n3(0, 1, t),
                        n3(0, -1, -t),
                        n3(0, 1, -t),
                        n3(t, 0, -1.),
                        n3(t, 0, 1.),
                        n3(-t, 0, -1.),
                        n3(-t, 0, 1.)};

    vector<unsigned int> idxes = {
        // 5 faces around point 0
        0,
        11,
        5,
        0,
        5,
        1,
        0,
        1,
        7,
        0,
        7,
        10,
        0,
        10,
        11,
        // 5 adjacent faces
        1,
        5,
        9,
        5,
        11,
        4,
        11,
        10,
        2,
        10,
        7,
        6,
        7,
        1,
        8,
        // 5 faces around point 3
        3,
        9,
        4,
        3,
        4,
        2,
        3,
        2,
        6,
        3,
        6,
        8,
        3,
        8,
        9,
        // 5 adjacent faces
        4,
        9,
        5,
        2,
        4,
        11,
        6,
        2,
        10,
        8,
        6,
        7,
        9,
        8,
        1,
    };

    vector<unsigned int> idxes_2;
    refine(pts, idxes, idxes_2);

    //vector<unsigned int> idxes_3;
    //refine(pts, idxes_2, idxes_3);

    m.positions.set_data(pts);
    m.idxes.set_data(idxes_2);
    m.num_triangles = idxes_2.size() / 3;
  }

  static void make_cylinder(
      InstancedMesh& m,
      vec3 scale,
      mat4 orientation,
      const int sections,
      vec2 scale2 = vec2{1., 1.}) {

    // scale2 = relative scaling for +x section...

    // provide scaling option for front and back!
    m.num_local_tforms = 2;

    vector<vec4> cylinder_vtxes(2 + (sections * 2));
    cylinder_vtxes[0] = {-0.5, 0., 0, 1.};
    cylinder_vtxes[sections + 1] = {0.5, 0., 0., 1.};

    vector<unsigned int> cylinder_idxes;

    vector<unsigned int> local_tform_ids(cylinder_vtxes.size());
    local_tform_ids[0] = 0;
    local_tform_ids[1] = 1;

    const int start_0 = 0;
    const int offset_0 = 1;

    const int start_1 = 1 + sections;
    const int offset_1 = 2 + sections;

    for (int s = 0; s < sections; s++) {
      const float theta = (s * 2.f / sections) * (float)(M_PI);
      vec2 dir = vec2{cos(theta), sin(theta)} * 0.5f;
      cylinder_vtxes[1 + s] = vec4{-0.5, dir, 1.f};
      cylinder_vtxes[2 + s + sections] = vec4{0.5, scale2 * dir, 1.f};

      local_tform_ids[1 + s] = 0;
      local_tform_ids[2 + s + sections] = 1;
      // local_tform_ids.push_ba

      const int next_s = ((s + 1) % sections);

      cylinder_idxes.push_back(start_0);
      cylinder_idxes.push_back(offset_0 + s);
      cylinder_idxes.push_back(offset_0 + next_s);

      cylinder_idxes.push_back(start_1);
      cylinder_idxes.push_back(offset_1 + s);
      cylinder_idxes.push_back(offset_1 + next_s);

      cylinder_idxes.push_back(offset_0 + s);
      cylinder_idxes.push_back(offset_1 + s);
      cylinder_idxes.push_back(offset_0 + next_s);

      cylinder_idxes.push_back(offset_1 + s);
      cylinder_idxes.push_back(offset_0 + next_s);
      cylinder_idxes.push_back(offset_1 + next_s);
    }

    vector<vec4> cylinder_vtxes_baked;
    for (auto& v : cylinder_vtxes) {
      cylinder_vtxes_baked.emplace_back(glm::scale(scale) * orientation * v);
    }

    m.local_tform_map.set_data(local_tform_ids);
    m.positions.set_data(cylinder_vtxes_baked);
    m.num_triangles = cylinder_idxes.size() / 3;
    m.idxes.set_data(cylinder_idxes);
  }
};