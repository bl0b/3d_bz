// #py_include <RsTypes.cl>
// #py_include <cglm.cl>
// #py_include <OrthoSampler2D.cl>

__kernel void gen_ray_dirs(__write_only image2d_t /*TF::RGBA32F*/ ray_dirs, rs2_intrinsics i) {
  const int2 c = (int2){get_global_id(0), get_global_id(1)};
  const int2 i_c = get_image_dim(ray_dirs);

  if (c.x < i_c.x && c.y < i_c.y) {

    // deproject point with depth 1
    float3 dir = (float3){(c.x - i.ppx) / i.fx, (c.y - i.ppy) / i.fy, 1.};

    glm_vec3_normalize(&dir);

    write_imagef(ray_dirs, c, (float4){dir.x, dir.y, dir.z, 0.});
  }
}

__kernel void find_edge_pixels(
    __read_only image2d_t /*TF::R16UI*/ depth_img,
    __global uint* num_coords_out,
    __global int4* coords_out,
    int write_mesh_ids,
    __read_only image2d_t /*TF::R16UI*/ mesh_ids,
    __write_only image2d_t /*TF::RGBA8*/ debug_out) {

  // write_mesh_ids basically a bool!

  const int2 c = (int2){get_global_id(0), get_global_id(1)};
  const int2 i_c = get_image_dim(depth_img);

  if (c.x < i_c.x && c.y < i_c.y) {
    uint val = read_imageui(depth_img, c).x;

    bool write_out = false;

    uint mesh_id = 0;
    if (!val) {
      for (int i = 0; i < 8; i++) {
        int _i = i < 4 ? i : i + 1;
        int2 c2 = (int2){c.x - 1 + _i % 3, c.y - 1 + _i / 3};
        val = read_imageui(depth_img, c2).x;
        if (val) {
          write_out = true;
          // just find first mesh id!
          if (write_mesh_ids) { mesh_id = read_imageui(mesh_ids, c2).x; }
          break;
        }
      }
    }

    if (write_out) {
      const uint out_id = atomic_add(num_coords_out, 1);
      coords_out[out_id] = (int4){c.x, c.y, mesh_id, 0.};

      float4 debug_color = (float4){1., 1., 1., 1.};
      if (write_mesh_ids) {
        if (mesh_id == 1) {
          debug_color.x = .3;
          debug_color.y = .3;
          debug_color.z = .9;
        } else if (mesh_id == 2) {
          debug_color.x = 0.0;
          debug_color.y = 0.0;
          debug_color.z = 0.0;
        }
      }

      write_imagef(debug_out, c, debug_color);
    }
  }
}

// edge line normals: pointing outwards away from object!
__kernel void find_edge_line_normals(
    __read_only image2d_t /*TF::R16UI*/ depth_img,
    uint num_coords,
    __global int4* coords,
    __global int* __local_coord_counts,
    __global float4* normals,
    __write_only image2d_t /*TF::RGBA8*/ debug_out) {

  const int i = get_global_id(0);
  const int j = get_global_id(1);

  const int dx_map[9] = {1, 0, -1, 2, 0, -2, 1, 0, -1};
  const int dy_map[9] = {1, 2, 1, 0, 0, 0, -1, -2, -1};

  if (i < num_coords && j < 9) {

    const int4 c = coords[i];
    const int2 c_guess = (int2){c.x - 1 + (j % 3), c.y - 1 + (j / 3)};
    const uint val = read_imageui(depth_img, c_guess).x;
    if (val) {

      const int dx = dx_map[j];
      const int dy = dy_map[j];

      const uint local_idx = i * 2;
      atomic_add(__local_coord_counts + local_idx, dx);
      atomic_add(__local_coord_counts + local_idx + 1, dy);
    }
  }

  barrier(CLK_GLOBAL_MEM_FENCE);

  if (i < num_coords && j == 0) {

    const int dx = __local_coord_counts[i * 2];
    const int dy = __local_coord_counts[i * 2 + 1];

    float3 d = (float3){dx * -1.f, dy * -1.f, 0.};
    glm_vec3_normalize(&d);

    normals[i] = (float4){d.x, d.y, 0., 0.};

    // not necessary, just for debug interest!
    const int4 c = coords[i];
    const int2 c_write = (int2){c.x, c.y};
    // const int2 c_write = (int2){c.x - dx, c.y - dy};

    const int2 image_dim = get_image_dim(debug_out);
    if (c_write.x > 0 && c_write.x < image_dim.x && c_write.y > 0 && c_write.y < image_dim.y) {
      write_imagef(debug_out, c_write, (float4){0., 0., 0., 1.});
    }
  }
}

__kernel void find_overlapping_coords(
    uint num_coords,
    __global int4* coords,
    __read_only image2d_t /*TF::R16UI*/ depth_img,
    int write_mesh_ids,
    __read_only image2d_t /*TF::R16UI*/ mesh_ids,
    __global uint* num_overlapping_coords,
    __global int4* overlapping_coords) {

  const int i = get_global_id(0);
  if (i < num_coords) {
    const int4 c = coords[i];
    const uint val = read_imageui(depth_img, (int2){c.x, c.y}).x;
    if (val) {
      if (write_mesh_ids) {
        uint mesh_id = read_imageui(mesh_ids, (int2){c.x, c.y}).x;
        c.z = (int)mesh_id;
      }
      uint overlap_id = atomic_add(num_overlapping_coords, 1);
      overlapping_coords[overlap_id] = c;
    }
  }
}

__kernel void generate_impulses_out(
    uint num_coords,
    __global int4* coords,
    __global float4* coord_edge_normals,
    __read_only image2d_t /*TF::RGBA32F*/ vtx_positions,
    rs2_intrinsics intrin,
    _mat4 camera_to_world,
    _mat4 world_to_camera,
    __global _mat4* obj_world_positions,
    __global float4* impulses_out) {

  const int i = get_global_id(0);
  if (i < num_coords) {
    const int4 c = coords[i];
    const int mesh_id = c.z;
    // mesh ids 1-idxed to distinguish from empty pixel!
    _mat4 obj_in_world = obj_world_positions[mesh_id - 1];

    // impulse relative position (world space):
    // rasterized position from camera -> position in world space -> relative position in world
    // space from object
    float4 vtx_cam_position = read_imagef(vtx_positions, (int2){c.x, c.y});

    // first get object center of mass in world coords
    float4 _origin = (float4){0., 0., 0., 1.};
    float4 obj_world_pos;
    glm_mat4_mulv(&obj_in_world, &_origin, &obj_world_pos);

    // then get object center of mass in cam coords
    float4 obj_cam_pos;
    glm_mat4_mulv(&world_to_camera, &obj_world_pos, &obj_cam_pos);

    // reset z position of impulse, to avoid spinning around wrong axis.
    // TODO: trace along ray coming out of depth cam to correct x/y values a bit as well
    /*
    vtx_cam_position.z = obj_cam_pos.z;
     */

    // convert corrected coords to world space
    float4 vw;
    glm_mat4_mulv(&camera_to_world, &vtx_cam_position, &vw);

    const float4 ow = obj_world_pos;
    float4 vrel = (float4){vw.x - ow.x, vw.y - ow.y, vw.z - ow.z, mesh_id * 1.f};
    impulses_out[i * 2] = vrel;

    // impulse direction (world space):
    float3 ray_1 = (float3){(c.x - intrin.ppx) / intrin.fx, (c.y - intrin.ppy) / intrin.fy, 1.};
    const float4 coord_edge_normal = coord_edge_normals[i];
    float3 ray_2 = (float3){(c.x + coord_edge_normal.x - intrin.ppx) / intrin.fx,
                            (c.y + coord_edge_normal.y - intrin.ppy) / intrin.fy,
                            1.};

    float4 dir_c = (float4){ray_1.x - ray_2.x, ray_1.y - ray_2.y, 0., 1.};
    float4 dir_w;
    glm_mat4_mulv(&camera_to_world, &dir_c, &dir_w);

    float4 origin_c = (float4){0., 0., 0., 1.};
    float4 origin_w;
    glm_mat4_mulv(&camera_to_world, &origin_c, &origin_w);

    float3 dir = (float3){dir_w.x - origin_w.x, dir_w.y - origin_w.y, dir_w.z - origin_w.z};
    glm_vec3_normalize(&dir);
    impulses_out[i * 2 + 1] = (float4){dir.x, dir.y, dir.z, 0.};
  }
}

__kernel void get_points_minified(
    __read_only image2d_t /*TF::R16UI*/ depth_img_1,
    __read_only image2d_t /*TF::R16UI*/ depth_img_2,
    int minif,
    __global uint* num_points_out,
    __global int4* coords_out) {

  const int x = minif * get_global_id(0);
  const int y = minif * get_global_id(1);

  const int2 dim = get_image_dim(depth_img_1);

  if (x < dim.x && y < dim.y) {
    const uint val1 = read_imageui(depth_img_1, (int2){x, y}).x;
    if (val1) {
      const uint val2 = read_imageui(depth_img_2, (int2){x, y}).x;
      if (val2 && val1 != val2) {
        const uint out_id = atomic_add(num_points_out, 1);
        coords_out[out_id] = (int4){x, y, 0, 0};
      }
    }
  }
}

float4 convert_dir_coords(_mat4* m, float4* v) {
  float4 vc;
  glm_mat4_mulv(m, v, &vc);

  float4 o = (float4){0., 0., 0., 1.};
  float4 oc;
  glm_mat4_mulv(m, &o, &oc);

  return (float4){vc.x - o.x, vc.y - o.y, vc.z - o.z, 0.};
}

__kernel void find_point_diffs_impulses(
    uint num_points,
    __global int4* coords,
    __read_only image2d_t /*TF::RGBA32F*/ vtxes_1,
    __read_only image2d_t /*TF::RGBA32F*/ vtxes_2,
    __read_only image2d_t /*TF::R16UI*/ vtxes_2_mesh_ids,
    _mat4 cam_to_world,
    __global _mat4* obj_world_positions,
    __global float4* impulses_out) {

  const int i = get_global_id(0);
  if (i < num_points) {

    const int4 _c = coords[i];
    const int2 c = (int2){_c.x, _c.y};

    uint mesh_id = read_imageui(vtxes_2_mesh_ids, c).x;

    float4 _origin = (float4){0., 0., 0., 1.};
    // mesh ids 1-idxed to distinguish from empty pixel!
    _mat4 obj_in_world = obj_world_positions[mesh_id - 1];
    float4 obj_world_pos;
    glm_mat4_mulv(&obj_in_world, &_origin, &obj_world_pos);

    float4 v1 = read_imagef(vtxes_1, c);
    float4 v2 = read_imagef(vtxes_2, c);

    float4 v1_w;
    glm_mat4_mulv(&cam_to_world, &v1, &v1_w);

    float4 v2_w;
    glm_mat4_mulv(&cam_to_world, &v2, &v2_w);

    float4 v2_w_rel =
        (float4){v2_w.x - obj_world_pos.x, v2_w.y - obj_world_pos.y, v2_w.z - obj_world_pos.z, 0.};
    // relative pos! v2: guess img!

    v2_w_rel.w = 1.f * mesh_id;
    // v2_w.w = 1.f * mesh_id;

    impulses_out[i * 2] = v2_w_rel;

    float4 diff = (float4){v1_w.x - v2_w.x, v1_w.y - v2_w.y, v1_w.z - v2_w.z, 0};
    impulses_out[i * 2 + 1] = diff;
  }
}

__kernel void compute_img_diff(
    __read_only image2d_t /*TF::R16UI*/ img_1,
    __read_only image2d_t /*TF::R16UI*/ img_2,
    __global uint* edge_violations,
    __global uint* diff_count,
    __global uint* sum_diff) {

  const int2 c = (int2){get_global_id(0), get_global_id(1)};
  const int2 dim = get_image_dim(img_1);

  if (c.x < dim.x && c.y < dim.y) {

    const uint val1 = read_imageui(img_1, c).x;
    const uint val2 = read_imageui(img_2, c).x;

    if ((val1 && !val2) || (!val1 && val2)) {
      atomic_add(edge_violations, 1);
    } else if (val1 && val2) {
      uint diff = val1 > val2 ? val1 - val2 : val2 - val1;
      if (diff) {
        atomic_add(diff_count, 1);
        atomic_add(sum_diff, diff);
      }
    }
  }
}

__kernel void find_edge_pixels_2(
    __read_only image2d_t /*TF::R16UI*/ depth_img,
    __global uint* num_coords_out,
    __global int4* coords_out,
    __write_only image2d_t /*TF::RGBA8*/ debug_out) {

  const int2 c = (int2){get_global_id(0), get_global_id(1)};
  const int2 i_c = get_image_dim(depth_img);

  if (c.x < i_c.x && c.y < i_c.y) {
    uint val = read_imageui(depth_img, c).x;
    bool write_out = false;
    if (val) {
      for (int i = 0; i < 4; i++) {
        int2 c2 = (int2){c.x + (i == 1 ? -1 : 0) + (i == 2 ? 1 : 0),
                         c.y + (i == 0 ? -1 : 0) + (i == 3 ? 1 : 0)};
        val = read_imageui(depth_img, c2).x;
        if (!val) {
          write_out = true;
          break;
        }
      }
    }

    if (write_out) {
      const uint out_id = atomic_add(num_coords_out, 1);
      coords_out[out_id] = (int4){c.x, c.y, 0, 0.};
    }
  }
}

__kernel void draw_debug(
    __write_only image2d_t /*TF::RGBA8*/ img,
    __global float4* coords,
    uint num_to_draw,
    uint coord_offset,
    uint total_coords) {
  const int c_idx = get_global_id(0);
  const int half_side = num_to_draw / 2;
  if (c_idx < num_to_draw) {

    const int change = c_idx > half_side ? -(c_idx - half_side) : c_idx;
    const int idx = (total_coords + coord_offset + change) % total_coords;

    const float4 c_f = coords[idx];
    // const float4 c_f = coords[(c_idx + coord_offset) % total_coords];
    const int2 c = (int2){c_f.x, c_f.y};
    const float4 color = (float4){0., 0., 1., 1};
    write_imagef(img, c, color);
  }
}

__kernel void gen_coords_in_bbox(
    __read_only image2d_t /*TF::R16UI*/ mesh_ids,
    __global uint* count,
    __global uint* counts,
    __global int4* coords_out) {

  const int2 c = (int2){get_global_id(0), get_global_id(1)};
  const int2 d = get_image_dim(mesh_ids);

  if (c.x < d.x && c.y < d.y) {
    uint mesh_id = read_imageui(mesh_ids, c).x;
    if (mesh_id) {
      uint current = atomic_add(count, 1);
      coords_out[current] = (int4){c.x, c.y, mesh_id, 0};
      atomic_add(counts + mesh_id - 1, 1);
    }
  }
}

__kernel void gen_all_costs(
    uint num_viewports,
    __global float4* viewports,
    uint num_coords,
    __global int4* coords,
    uint num_mesh_ids,
    __read_only image2d_t /*TF::R16UI*/ img0,
    __read_only image2d_t /*TF::R16UI*/ img1,
    __global uint* costs,
    uint mismatch_scale) {

  const uint vp_id = get_global_id(0);
  const uint coord_id = get_global_id(1);

  if (vp_id < num_viewports && coord_id < num_coords) {
    const float4 vp = viewports[vp_id];

    //    if (vp_id == 30) {
    //      printf("vp 30!\n");
    //    }

    const int4 _coord = coords[coord_id];
    const int2 coord = (int2){_coord.x, _coord.y};
    const int mesh_id = _coord.z;

    uint v0 = read_imageui(img0, coord).x;
    int2 vp_origin = (int2){(int)vp.x, (int)vp.y};

    uint v1 = read_imageui(img1, vp_origin + coord).x;

    const int cost_offset = (vp_id * num_mesh_ids) + mesh_id - 1;

    if (!v0 && !v1) {
      // no values! don't do anything!
    } else if (v0 && v1) {
      // both values!
      uint diff = abs((int)v0 - (int)v1);
      atomic_add(costs + cost_offset, diff);
    } else if (v0) {
      // mismatch! value in original image, no value in rendered image!
      // this isn't that bad
      atomic_add(costs + cost_offset, mismatch_scale);
    } else {
      // mismatch! value in rendered image, no value in real! this should not be!
      atomic_add(costs + cost_offset, mismatch_scale * 4);
    }
  }
}

__kernel void gen_costs2(
    __read_only image2d_t /*TF::RG16UI*/ img0_split,
    __read_only image2d_t /*TF::RG16UI*/ img1,
    __global float4* viewports,
    uint num_viewports,
    __global uint* costs,
    uint mismatch_scale,
    rs2_intrinsics i,
    _mat4 camera_to_world,
    OrthoSampler2D ps_sampler,
    __read_only image2d_t /*TF::R16UI*/ plane_stencil) {

  const uint vp_id = get_global_id(0);
  if (vp_id < num_viewports) {
    const float4 vp = viewports[vp_id];
    const int2 vp_origin = (int2){(int)vp.x, (int)vp.y};

    int2 img0dim = get_image_dim(img0_split);
    const uint img_x = get_global_id(1);
    if (img_x < img0dim.x) {

      uint diff_sum = 0;

      for (uint img_y = 0; img_y < img0dim.y; img_y++) {

        int2 c = (int2){img_x, img_y};
        const uint2 _v0 = read_imageui(img0_split, c).xy;
        uint v0 = _v0.x;
        uint v0_not = _v0.y;
        uint v1 = read_imageui(img1, vp_origin + c).x;

        if (v1 && v0_not) { diff_sum += mismatch_scale; }

        if (!v1 && v0) { diff_sum += mismatch_scale; }

        if (v0 && v1) {
          uint diff = abs((int)v0 - (int)v1);
          diff_sum += diff;
        }

        // in rendered

        /*

        if (!v0 && !v1) {
          // no values! don't do anything!
        } else if (v0 && v1) {
          // both values!
          uint diff = abs((int)v0 - (int)v1);
          diff_sum += diff;
        } else if (v0) {
          // mismatch! value in original image, no value in rendered image!
          diff_sum += mismatch_scale;
        } else {
          // mismatch! value in rendered image, no value in real!

          // xy = c;
          // z = v1;
          // -> world-pos;
          // -> plane-pos;
          // -> compare plane xy to ortho map stencil
          // -> if inside stencil, only then do cost!

          const float d = (float)v1;
          float4 pos_camspace =
              (float4){d * ((float)c.x - i.ppx) / i.fx, d * ((float)c.y - i.ppy) / i.fy, d, 1.f};

          float4 pos_planespace;
          glm_mat4_mulv(&camera_to_world, &pos_camspace, &pos_planespace);

          float2 pos_stencil_img = // mapped 0-1
              (((float2){pos_planespace.xy} - ps_sampler.min_coords) /
               (ps_sampler.max_coords - ps_sampler.min_coords));

          int2 coords_stencil_img = (int2){(int)round(pos_stencil_img.x * ps_sampler.img_dims.x),
                                           (int)round(pos_stencil_img.y * ps_sampler.img_dims.y)};

          uint stencil_val = read_imageui(plane_stencil, coords_stencil_img).x;

          if (stencil_val) {
            diff_sum += mismatch_scale;
          }
        }

        */
      }

      if (diff_sum) { atomic_add(costs + vp_id, diff_sum); }
    }
  }
}

__kernel void gen_costs(
    __global int4* coords,
    uint num_coords,
    __read_only image2d_t /*TF::R16UI*/ img0,
    __read_only image2d_t /*TF::R16UI*/ img1,
    __global uint* costs,
    uint mismatch_scale,
    __global float4* viewports,
    uint viewport_id) {

  const uint i = get_global_id(0);

  if (i < num_coords) {

    int4 _c = coords[i];
    int2 c = (int2){_c.x, _c.y};
    int mesh_id = _c.z;

    uint v0 = read_imageui(img0, c).x;

    // img1 may live inside a viewport in larger image!
    float4 vp = viewports[viewport_id];
    int2 vp_origin = (int2){(int)vp.x, (int)vp.y};
    uint v1 = read_imageui(img1, vp_origin + c).x;

    if (!v0 && !v1) {
      // no values! don't do anything!
    } else if (v0 && v1) {
      // both values!
      uint diff = abs((int)v0 - (int)v1);
      atomic_add(costs + mesh_id - 1, diff);
    } else if (v0) {
      // mismatch! value in original image, no value in rendered image!
      // this isn't that bad
      atomic_add(costs + mesh_id - 1, mismatch_scale);
    } else {
      // mismatch! value in rendered image, no value in real! this should not be!
      atomic_add(costs + mesh_id - 1, mismatch_scale * 4);
    }
  }
}
