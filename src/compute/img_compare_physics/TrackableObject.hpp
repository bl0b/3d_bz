#pragma once

#include <engine/Engine.h>
#include <stdio.h>

#include "TrackableJson.hpp"
#include "primitives/Primitives.hpp"

#include <cereal/cereal.hpp>
#include <cereal/archives/json.hpp>
#include <cereal/types/string.hpp>
#include <cereal/types/vector.hpp>

class CerealPosition {
public:
  vec3 trans;
  vec3 rot;

  mat4 get_mat4() {
    return glm::translate(trans) * glm::rotate(rot.x, vec3{1., 0., 0.}) *
           glm::rotate(rot.y, vec3{0., 1., 0.}) * glm::rotate(rot.z, vec3{0., 0., 1.});
  }

private:
  friend class cereal::access;
  template <class Ar>
  void serialize(Ar& ar) {
    ar(CEREAL_NVP(trans));
    ar(CEREAL_NVP(rot));
  }
};

class CerealBodyMesh {
public:
  string mesh_type;

private:
  friend class cereal::access;
  template <class Ar>
  void serialize(Ar& ar) {
    ar(CEREAL_NVP(mesh_type));
  }
};

class CerealBody {
public:
  vec3 scale;
  CerealPosition initial_position;
  float mass;
  vector<CerealBodyMesh> meshes;

private:
  friend class cereal::access;
  template <class Archive>
  void serialize(Archive& archive) {
    archive(CEREAL_NVP(scale));
    archive(CEREAL_NVP(mass));
    archive(CEREAL_NVP(initial_position));
    archive(CEREAL_NVP(meshes));
  }
};

struct CerealConstraint {
  int body_id_0;
  int body_id_1;
  // world space coordinates of joint at initialization time
  CerealPosition initial_position;
  vec3 angle_min;
  vec3 angle_max;

private:
  friend class cereal::access;
  template <class Archive>
  void serialize(Archive& a) {
    a(CEREAL_NVP(body_id_0));
    a(CEREAL_NVP(body_id_1));
    a(CEREAL_NVP(initial_position));
    a(CEREAL_NVP(angle_min));
    a(CEREAL_NVP(angle_max));
  }
};

struct CerealBodiesConfig {
  string name;
  vector<string> values;
  vector<CerealBody> bodies;
  vector<CerealConstraint> joints;

  template <class Archive>
  void serialize(Archive& archive) {
    archive(CEREAL_NVP(name));
    archive(CEREAL_NVP(values));
    archive(CEREAL_NVP(bodies));
    archive(CEREAL_NVP(joints));
  }
};

class TrackableObject {

public:
  InstancedMeshGroup m;

  // mesh showing underlying physics models in use
  InstancedMeshGroup physics_outline_mesh;

  vector<PhysicsObject> bodies;

  vector<unsigned int> m_id_to_body_idx;

  // world holding the object!
  PhysicsCtx physics_ctx;

  void init() {

    physics_ctx.m_dynamics_world->setGravity({0., 0., 0.});

    const string test_filename = "C:/Users/Carson/Desktop/test2.json";

    std::ifstream is(test_filename);

    CerealBodiesConfig test_config_back;
    {
      cereal::JSONInputArchive test_json(is);
//      test_json(test_config_back);
    }

    const int num_configs = test_config_back.bodies.size();
    bodies.resize(num_configs);

    physics_outline_mesh.meshes.resize(num_configs);
    physics_outline_mesh.num_meshes.resize(num_configs, 1);

    for (int i = 0; i < test_config_back.bodies.size(); i++) {
      auto& b_in = test_config_back.bodies[i];
      auto& b = bodies[i];
      b.make(b_in.scale / 2.f, b_in.mass, b_in.initial_position.get_mat4());
      b.body->setActivationState(DISABLE_DEACTIVATION);
      physics_ctx.m_dynamics_world->addRigidBody(b.body);

      auto& physics_mesh = physics_outline_mesh.meshes[i];
      Primitives::make_cube(physics_mesh, b_in.scale);

      m.num_meshes.push_back(b_in.meshes.size());

      for (int j = 0; j < b_in.meshes.size(); j++) {
        m.meshes.emplace_back();
        auto& _m = m.meshes[m.meshes.size() - 1];
        m_id_to_body_idx.push_back(i);

        auto const& t = b_in.meshes[j].mesh_type;
        if (t == "cube") {

          Primitives::make_cube(_m, b_in.scale);

        } else if (t == "cylinder") {

          const int num_sections = 9;
          Primitives::make_cylinder(_m, b_in.scale, mat4{1.f}, num_sections);

        } else {
          printf("unknown mesh type!!\n");
        }
      }
    }

    for (auto& c_in : test_config_back.joints) {

      const int b0 = c_in.body_id_0;
      const int b1 = c_in.body_id_1;

      mat4 b0_pos = test_config_back.bodies[b0].initial_position.get_mat4();
      mat4 b1_pos = test_config_back.bodies[b1].initial_position.get_mat4();

      mat4 j_pos = c_in.initial_position.get_mat4();

      mat4 j_pos_b0 = glm::inverse(b0_pos) * j_pos;
      mat4 j_pos_b1 = glm::inverse(b1_pos) * j_pos;

      auto* c = new btGeneric6DofSpring2Constraint(
          *bodies[b0].body, *bodies[b1].body, toBt(j_pos_b0), toBt(j_pos_b1));

      c->setAngularLowerLimit(toBt(c_in.angle_min));
      c->setAngularUpperLimit(toBt(c_in.angle_max));

      physics_ctx.m_dynamics_world->addConstraint(c, true);
    }
  }

  // draw control UI for allowing user to move the 'real' object around the scene
  // vector<bool> pressed;
  void draw_control_ui() {

    ImGui::GetIO().KeyRepeatDelay = 0.;

    const float push_force = 15.;
    const float t_f = 32.;
    ImVec2 b_s{20., 20};
    ImGui::Begin("cube controls!");

    const vector<float> force_scalars{push_force, t_f};
    const vector<string> change_types{"position", "rotation"};
    const vector<function<void(int, vec3)>> change_fns{
        [&](int i, vec3 f) {
          bodies[i].body->applyForce({f.x, f.y, f.z}, {0., 0., 0.});
        },
        [&](int i, vec3 f) {
          bodies[i].body->applyTorque({f.x, f.y, f.z});
        }};
    const vector<string> var_names{"x", "y", "z"};
    const vector<string> direction_names{"+", "-"};

    for (int i = 0; i < bodies.size(); i++) {
      ImGui::PushID(i);
      ImGui::Text("Body %d", i);
      ImGui::Indent(10.);

      for (int t = 0; t < 2; t++) {
        ImGui::PushID(t);
        ImGui::Text("%s:\n", change_types[t].c_str());
        for (int j = 0; j < 3; j++) {
          ImGui::PushID(j);
          for (int k = 0; k < 2; k++) {
            ImGui::PushID(k);
            ImGui::SameLine();

            char* buff[16];
            sprintf((char*)buff, "%s%s", var_names[j].c_str(), direction_names[k].c_str());

            if (ImGui::ButtonEx((char*)buff, b_s, ImGuiButtonFlags_Repeat)) {
              vec3 force{0., 0., 0.};
              const float f = force_scalars[t];
              *(&force.x + j) = k ? f : -f;
              change_fns[t](i, force);
            }
            ImGui::PopID();
          }
          ImGui::PopID();
        }
        ImGui::PopID();
      }
      ImGui::PopID();
      ImGui::Text("-");
      ImGui::Unindent(10.);
    }

    ImGui::End();
  }

  void write_cube_mesh_positions() {
    vector<mat4> m_tforms;
    for (auto& body_idx : m_id_to_body_idx) {
      m_tforms.emplace_back(bodies[body_idx].get_current_tform());
    }
    m.mesh_tforms.set_data(m_tforms);

    vector<mat4> bb_tforms;
    for (auto& body : bodies) {
      bb_tforms.emplace_back(body.get_current_tform());
    }
    physics_outline_mesh.mesh_tforms.set_data(bb_tforms);
  }

  void draw_preview(ShaderInstanced& cam, Tex<TF::RGBA8>& img, vec4 color, bool clear = true) {

    GL::Renderer::enable(GL::Renderer::Feature::DepthTest);
    write_cube_mesh_positions();

    cam.fbo.bind();
    cam.set_target(nullptr, nullptr, &img, nullptr);

    if (clear) {
      GL::Renderer::setClearColor(0x000000_rgbf);
      cam.fbo.clear(GL::FramebufferClear::Color | GL::FramebufferClear::Depth);
    }
    GL::Renderer::setPolygonMode(GL::Renderer::PolygonMode::Line);

    cam.render_mode = ShaderInstanced::ColorRenderMode::FULL_COLOR;

    // in dimmer color, draw physics box outlines
    cam.color_out = color * 0.7f;
    cam.draw_instanced_no_viewport(physics_outline_mesh, 1, 0);

    cam.color_out = color;
    cam.draw_instanced_no_viewport(m, 1, 0);

    cam.clear_target();
    GL::defaultFramebuffer.bind();
  }

  void draw_img(
      ShaderInstanced& cam,
      // mesh IDs optional param...
      // if drawing a fake input image for example, this shouldn't be a thing..
      Tex<TF::R16UI>* mesh_ids,
      Tex<TF::R16UI>& depth,
      Tex<TF::RGBA32F>& vtxes) {

    GL::Renderer::enable(GL::Renderer::Feature::DepthTest);
    GL::Renderer::setClearColor(0x000000_rgbf);
    GL::Renderer::setPolygonMode(GL::Renderer::PolygonMode::Fill);

    cam.fbo.bind();
    cam.set_target(mesh_ids, &depth, nullptr, &vtxes);

    cam.fbo.clear(GL::FramebufferClear::Color | GL::FramebufferClear::Depth);

    cam.draw_instanced_no_viewport(m, 1, 0);

    cam.clear_target();
  }

  float physics_damping = 0.5;
  void damp_physics_bodies() {
    for (auto& b : bodies) {
      b.body->setAngularVelocity(b.body->getAngularVelocity() * physics_damping);
      b.body->setLinearVelocity(b.body->getLinearVelocity() * physics_damping);
    }
  }

  void
  apply_generated_impulses(const int num_impulses, Buff<vec4>& impulses, const float force_scale) {
    vector<vec4> impulses_cpu(num_impulses * 2);
    impulses.cu_copy_to(impulses_cpu.data(), num_impulses * 2);
    for (int i = 0; i < num_impulses; i++) {
      const vec4 rel_pos = impulses_cpu[i * 2];
      const vec4 dir = impulses_cpu[i * 2 + 1];
      const int mesh_id = (int)(round(rel_pos.w));
      const int body_idx = m_id_to_body_idx[mesh_id - 1];
      bodies[body_idx].body->applyForce(
          btVector3{dir.x, dir.y, dir.z} * force_scale, {rel_pos.x, rel_pos.y, rel_pos.z});
    }
  }
};
