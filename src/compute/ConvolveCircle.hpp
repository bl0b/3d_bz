#pragma once
#include "ComputeLib.hpp"

namespace ComputeLib {

using namespace Engine::Gpu;

// Converts an image of incoming depth values to an image of 3d positions.
class ConvolveCircle : protected ComputeBase {
#include <generated/ConvolveCircle_cl_signature.hpp>
public:
  void
  run(Engine::Gpu::Tex<Engine::Gpu::TF::R16UI>& depth_img_ortho,
      OrthoSampler2D depth_img_sampler,
      Engine::Gpu::Buff<vec4>& convolve_out,
      int num_thetas,
      float dist,
      int num_samples,
      vec2 step,
      unsigned int* num_rays_out,
      vector<vec4>& rays_out) {

    vec2 total_img_size = depth_img_sampler.max_coords - depth_img_sampler.min_coords;
    ivec2 num_steps{total_img_size / step};

    const int out_num_els = num_steps.x * num_steps.y * num_thetas * num_samples;
    convolve_out.set_storage(out_num_els);
    convolve_out.cu_memset(0);

    kernel_ConvolveCircle_convolve_circle(
        ivec3{num_steps, num_thetas},
        {1, 1, num_thetas},
        depth_img_ortho,
        depth_img_sampler,
        convolve_out,
        num_thetas,
        dist,
        num_samples,
        step);

    temp_1.set_storage(num_steps.x * num_steps.y * num_thetas);
    temp_1.cu_memset(0);

    kernel_ConvolveCircle_trace_rays(
        ivec3{num_steps, num_thetas},
        {1, 1, num_thetas},
        convolve_out,
        num_thetas,
        num_samples,
        depth_img_sampler,
        temp_1);

    full_ray_count.set_storage(1);
    full_rays.set_storage(num_steps.x * num_steps.y * num_thetas / 2);
    ray_ids.set_storage(num_steps.x * num_steps.y * num_thetas / 2);
    full_ray_count.cu_memset(0);

    kernel_ConvolveCircle_trace_rays_2(
        ivec3{num_steps, num_thetas / 2},
        {1, 1, num_thetas / 2},
        temp_1,
        depth_img_sampler,
        num_thetas,
        num_samples,
        step,
        full_ray_count,
        full_rays,
        ray_ids);

    full_ray_count.cu_copy_to(num_rays_out, 1);

    kernel_ConvolveCircle_rays_linear_regression(
        *num_rays_out,
        num_samples,
        convolve_out,
        full_ray_count,
        ray_ids,
        full_rays,
        num_thetas,
        2 * num_samples,
        num_samples,
        num_steps);

    if (rays_out.size() < *num_rays_out) { rays_out.resize(*num_rays_out); }
    full_rays.cu_copy_to(rays_out.data(), *num_rays_out);

    // perform very simple linear regression on data,
    // determine squared error, filter by a threshold.
    // only relatively 'flat' sections ..
  };

  void make_convolve_grid(
      const unsigned int num_coords,
      Buff<ivec4>& coord_ids,
      Buff<vec4>& coords,
      int num_thetas,
      Tex<TF::R16UI>& depth_img_ortho,
      OrthoSampler2D depth_img_sampler,
      // still need convovle_out to look up x/y coords
      ivec2 num_samples,
      vec2 sample_ray_dist,
      Buff<vec4>& convolve_grid_out) {

    if (num_coords) {
      convolve_grid_out.set_storage(num_coords * num_samples.x * num_samples.y);
      kernel_ConvolveCircle_make_convolve_grid(
          ivec3{num_coords, num_samples},
          ivec3{1, 8, 8},
          num_coords,
          coord_ids,
          coords,
          num_thetas,
          depth_img_ortho,
          depth_img_sampler,
          num_samples,
          sample_ray_dist,
          convolve_grid_out);
    }
  }

  void combine_two_convolve_outs(
      Engine::Gpu::Buff<vec4>& c0, Engine::Gpu::Buff<vec4>& c1, Engine::Gpu::Buff<vec4>& c_out) {

    if (c0.size() != c1.size()) { printf("error: sizes must match convolve!\n"); }

    c_out.set_storage(c0.size());

    kernel_ConvolveCircle_convert_to_plane_object_depth(c0.size(), 32, c0.size(), c0, c1, c_out);
  }

  void trace_rays_3(
      Engine::Gpu::Buff<vec4>& convolve_combined,
      int num_convolve_coords,
      int num_thetas,
      int num_samples,
      Engine::Gpu::Buff<ivec4>& ray_lengths) {

    ray_lengths.set_storage(num_convolve_coords * num_thetas);

    kernel_ConvolveCircle_trace_rays_3(
        {num_convolve_coords, num_thetas},
        {1, 16},
        convolve_combined,
        num_convolve_coords,
        num_thetas,
        num_samples,
        ray_lengths);
  }

  Engine::Gpu::Buff<unsigned int> num_matching_rays_gpu;
  void find_short_pairs_of_opposite_thetas(
      Engine::Gpu::Buff<ivec4>& ray_lengths,
      int num_convolve_coords,
      int num_thetas,
      int num_samples,
      float min_ray_length_pct,
      Engine::Gpu::Buff<ivec4>& matching_rays, // identified by {coord_id, first_theta_id, 0, 0}
      unsigned int* num_matching_rays) {

    matching_rays.set_storage(ray_lengths.size() * 2);
    num_matching_rays_gpu.set_storage(1);
    num_matching_rays_gpu.cu_memset(0);

    int min_ray_length = (int)round(min_ray_length_pct * num_samples);

    kernel_ConvolveCircle_find_pairs_of_short_rays(
        num_convolve_coords,
        16,
        num_convolve_coords,
        num_thetas,
        num_samples,
        min_ray_length,
        ray_lengths,
        num_matching_rays_gpu,
        matching_rays);

    num_matching_rays_gpu.cu_copy_to(num_matching_rays, 1);

    //
  }

  Engine::Gpu::Buff<unsigned int> coords_count;
  void generate_coords_for_convolution(
      Engine::Gpu::Tex<Engine::Gpu::TF::R16UI>& depth_img,
      Engine::Gpu::Buff<vec4>& coords_for_convolution,
      int minification,
      unsigned int* num_coords,
      // for debuggin
      Engine::Gpu::Tex<Engine::Gpu::TF::RGBA8>& depth_img_rgba) {

    int max_num_coords =
        depth_img.get_dims().x * depth_img.get_dims().y / (minification * minification);

    coords_for_convolution.set_storage(max_num_coords);

    coords_count.set_storage(1);
    coords_count.cu_memset(0);

    const int block_size = 8;

    kernel_ConvolveCircle_generate_invocation_coords(
        depth_img.get_dims() / minification,
        {block_size, block_size},
        depth_img,
        coords_for_convolution,
        coords_count,
        minification,
        depth_img_rgba);

    coords_count.cu_copy_to(num_coords, 1);
  }

  void convolve_coords_list(
      Engine::Gpu::Buff<vec4>& coords_for_convolution,
      unsigned int num_coords,
      Engine::Gpu::Tex<Engine::Gpu::TF::R16UI>& depth_img,
      OrthoSampler2D depth_img_sampler,
      int num_thetas,
      int num_samples,
      float dist, // pixels!
      Engine::Gpu::Buff<vec4>& convolve_out) {

    const int num_els_out = num_coords * num_thetas * num_samples;

    if (num_els_out) {
      convolve_out.set_storage(num_els_out);

      kernel_ConvolveCircle_convolve_coords_list(
          ivec3{num_coords, num_thetas, 1},
          ivec3{1, 1, num_samples},
          num_coords,
          coords_for_convolution,
          depth_img,
          depth_img_sampler,
          num_thetas,
          num_samples,
          dist,
          convolve_out);
    }
  }

  void search_rays(
      Buff<vec4>& coords_for_convolution,
      unsigned int num_coords,
      int num_thetas,
      int num_samples,
      int ray_min_length,
      Buff<vec4>& convolve_result,
      Buff<ivec4>& rays_out, // {coord idx, theta idx, 0, 0}
      unsigned int* num_rays_out) {

    rays_out.set_storage(num_coords * num_thetas);
    full_ray_count.cu_memset(0);

    kernel_ConvolveCircle_search_rays(
        num_coords,
        num_thetas,
        coords_for_convolution,
        num_coords,
        num_thetas,
        num_samples,
        ray_min_length,
        convolve_result,
        rays_out,
        full_ray_count);

    full_ray_count.cu_copy_to(num_rays_out, 1);
  }

  void analyze_rays(
      Buff<vec4>& coords_for_convolution,
      unsigned int num_coords,
      int num_thetas,
      int num_samples,
      Buff<vec4>& convolve_result,
      Buff<ivec4>& ray_info_out) {

    ray_info_out.set_storage(num_coords * num_thetas);

    kernel_ConvolveCircle_analyze_rays(
        num_coords,
        num_thetas,
        coords_for_convolution,
        num_coords,
        num_thetas,
        num_samples,
        convolve_result,
        ray_info_out);
  }

  // search for long section with rest mostly short
  void find_matching_ray_length_patterns(
      unsigned int num_coords,
      int num_thetas,
      int num_samples,
      Buff<ivec4>&
          ray_info_in, // one per every coord/theta combo.  {coord_id, theta_id, ray_length, 1}
      Buff<ivec4>& matching_coords_out, // one per every matching coord, after looking at every
                                        // theta {coord_id, theta, 0, 0}
      unsigned int* num_matching_coords_out) {

    matching_coords_out.set_storage(num_coords);

    coords_count.set_storage(1);
    coords_count.cu_memset(0);

    kernel_ConvolveCircle_find_matching_ray_length_patterns(
        num_coords,
        8,
        num_coords,
        num_thetas,
        num_samples,
        ray_info_in,
        matching_coords_out,
        coords_count);

    coords_count.cu_copy_to(num_matching_coords_out, 1);
  }

private:
  // hold temp buffer containing length of each ray
  Engine::Gpu::Buff<unsigned int> temp_1;
  Engine::Gpu::Buff<unsigned int> full_ray_count;//{vector<unsigned int>{0}};
  Engine::Gpu::Buff<vec4> full_rays;
  Engine::Gpu::Buff<ivec4> ray_ids;
};

} // namespace ComputeLib
