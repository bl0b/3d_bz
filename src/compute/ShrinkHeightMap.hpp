#pragma once
#include "ComputeLib.hpp"

namespace ComputeLib {

class ShrinkHeightMap : protected ComputeBase {
#include <generated/ShrinkHeightMap_cl_signature.hpp>

  void
  run(Engine::Gpu::Tex<Engine::Gpu::TF::R16UI>& orig,
      Engine::Gpu::Tex<Engine::Gpu::TF::R16UI>& in,
      Engine::Gpu::Tex<Engine::Gpu::TF::R16UI>& out) {

    out.set_storage(in.get_dims());

    const int block_size = 4;

    const int _TRUE = 1;
    const int _FALSE = 0;

    cl_program.invoke("shrink_height_map")
        .dims((in.get_dims() / block_size) + 1, {block_size, block_size})
        .arg(orig)
        .arg(in)
        .arg(out)
        .arg(_TRUE)
        .call();

    cl_program.invoke("shrink_height_map")
        .dims((in.get_dims() / block_size) + 1, {block_size, block_size})
        .arg(orig)
        .arg(out)
        .arg(in)
        .arg(_TRUE)
        .call();

    cl_program.invoke("shrink_height_map")
        .dims((in.get_dims() / block_size) + 1, {block_size, block_size})
        .arg(orig)
        .arg(in)
        .arg(out)
        .arg(_TRUE)
        .call();

    cl_program.invoke("shrink_height_map")
        .dims((in.get_dims() / block_size) + 1, {block_size, block_size})
        .arg(orig)
        .arg(out)
        .arg(in)
        .arg(_TRUE)
        .call();

    cl_program.invoke("shrink_height_map")
        .dims((in.get_dims() / block_size) + 1, {block_size, block_size})
        .arg(orig)
        .arg(in)
        .arg(out)
        .arg(_FALSE)
        .call();

    cl_program.invoke("shrink_height_map")
        .dims((in.get_dims() / block_size) + 1, {block_size, block_size})
        .arg(orig)
        .arg(out)
        .arg(in)
        .arg(_FALSE)
        .call();

    cl_program.invoke("shrink_height_map")
        .dims((in.get_dims() / block_size) + 1, {block_size, block_size})
        .arg(orig)
        .arg(in)
        .arg(out)
        .arg(_FALSE)
        .call();
  }
};

} // namespace ComputeLib
