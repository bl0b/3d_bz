#pragma once
#include "ComputeLib.hpp"

namespace ComputeLib {

class MapDepth : protected ComputeBase {
#include <generated/MapDepth_cl_signature.hpp>

  void run(Engine::Gpu::Tex<TF::R16UI>& depth_img, Engine::Gpu::Tex<TF::RGBA8>& depth_img_rgba) {
    const ivec2 dims = depth_img.get_dims();
    depth_img_rgba.set_storage(dims);
    const int block_size = 8;

    kernel_MapDepth_map_depth(
        (dims / block_size) + 1, {block_size, block_size}, depth_img, depth_img_rgba, dims);
  }

  void
  run_split(Engine::Gpu::Tex<TF::RG16UI>& in, Engine::Gpu::Tex<TF::RGBA8>& out) {
    const ivec2 dims = in.get_dims();
    out.set_storage(dims);
    const int block_size = 8;

    kernel_MapDepth_map_depth_split(
        (dims / block_size) + 1, {block_size, block_size}, in, out, dims);
  }

  //void run_depth(
    //Engine::Gpu
  //)

  void run_depth(
    Engine::Gpu::Tex<TF::RG16UI>& img, Engine::Gpu::Tex<TF::RGBA8>& img_rgba, unsigned int channel = 0) {

    const ivec2 dims = img.get_dims();
    img_rgba.set_storage(dims);

    const int block_size = 8;

    kernel_MapDepth_map_depth2(
      (dims / block_size) + 1, { block_size, block_size }, img, img_rgba, dims, channel);
  }

  void run_meshid(
      Engine::Gpu::Tex<TF::RG16UI>& mesh_id_img, Engine::Gpu::Tex<TF::RGBA8>& mesh_id_img_rgba, unsigned int channel = 1) {

    const ivec2 dims = mesh_id_img.get_dims();
    mesh_id_img_rgba.set_storage(dims);

    const int block_size = 8;

    kernel_MapDepth_map_depth_meshid(
        (dims / block_size) + 1, {block_size, block_size}, mesh_id_img, mesh_id_img_rgba, dims, channel);
  }
};

} // namespace ComputeLib
