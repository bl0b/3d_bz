#pragma once
#include "ComputeLib.hpp"

namespace ComputeLib {

// Converts an image of incoming depth values to an image of 3d positions.
class DeprojectDepth : protected ComputeBase {
#include <generated/DeprojectDepth_cl_signature.hpp>

  void
  run(Engine::Gpu::Tex<TF::R16UI>& depth_img,
      Engine::Gpu::Tex<TF::RGBA32F>& vtxes,
      const rs2_intrinsics& intrin,
      const int minif,
      const mat4 transform,
      rs2_intrinsics* intrin_out = nullptr) {

    // Verify intrinsics distortion model :
    // Cannot deproject from a forward-distorted image
    assert(intrin.model != RS2_DISTORTION_MODIFIED_BROWN_CONRADY);
    // Cannot deproject to an ftheta image
    assert(intrin.model != RS2_DISTORTION_FTHETA);
    // this one can be deprojected, just haven't copied from realsense lib yet
    assert(intrin.model != RS2_DISTORTION_INVERSE_BROWN_CONRADY);

    const ivec2 in_dims = depth_img.get_dims();
    const ivec2 out_dims = in_dims / minif;
    const int block_dim = 32;

    // out dims cannot be 0!
    assert(out_dims.x > 0 && out_dims.y > 0);

    vtxes.set_storage(out_dims);

    kernel_DeprojectDepth_deproject_depth(
        (out_dims / block_dim) + 1,
        {block_dim, block_dim},
        depth_img,
        vtxes,
        minif,
        vec2{intrin.ppx, intrin.ppy},
        vec2{intrin.fx, intrin.fy},
        transform);

    if (intrin_out != nullptr) { *intrin_out = make_new_intrinsics(intrin, minif); }
  }

  void run_split(
    // assume ch0 for now! (r)
      Engine::Gpu::Tex<TF::RG16UI>& depth_img,
      Engine::Gpu::Tex<TF::RGBA32F>& vtxes,
      const rs2_intrinsics& intrin,
      const int minif,
      const mat4 transform,
      rs2_intrinsics* intrin_out = nullptr) {

    // Verify intrinsics distortion model :
    // Cannot deproject from a forward-distorted image
    assert(intrin.model != RS2_DISTORTION_MODIFIED_BROWN_CONRADY);
    // Cannot deproject to an ftheta image
    assert(intrin.model != RS2_DISTORTION_FTHETA);
    // this one can be deprojected, just haven't copied from realsense lib yet
    assert(intrin.model != RS2_DISTORTION_INVERSE_BROWN_CONRADY);

    const ivec2 in_dims = depth_img.get_dims();
    const ivec2 out_dims = in_dims / minif;
    const int block_dim = 32;

    // out dims cannot be 0!
    assert(out_dims.x > 0 && out_dims.y > 0);

    vtxes.set_storage(out_dims);

    kernel_DeprojectDepth_deproject_depth_split(
        (out_dims / block_dim) + 1,
        {block_dim, block_dim},
        depth_img,
        vtxes,
        minif,
        vec2{intrin.ppx, intrin.ppy},
        vec2{intrin.fx, intrin.fy},
        transform);

    if (intrin_out != nullptr) { *intrin_out = make_new_intrinsics(intrin, minif); }
  }

  void tform_points(
      Engine::Gpu::Tex<TF::RGBA32F>& vtxes_in,
      Engine::Gpu::Tex<TF::RGBA32F>& vtxes_out,
      const mat4 transform) {

    vtxes_out.set_storage(vtxes_in.get_dims());

    kernel_DeprojectDepth_tform_points(vtxes_in.get_dims(), {8, 8}, vtxes_in, vtxes_out, transform);
  }

  void tform_points(Engine::Gpu::Buff<vec4>& vtxes, const mat4 transform) {

    kernel_DeprojectDepth_tform_points_buff(vtxes.size(), 32, vtxes, vtxes.size(), transform);
  }

  rs2_intrinsics make_new_intrinsics(const rs2_intrinsics& i, int minif) {
    rs2_intrinsics o;
    o.fx = i.fx / minif;
    o.fy = i.fy / minif;
    o.width = i.width / minif;
    o.height = i.height / minif;
    o.ppx = i.ppx / minif;
    o.ppy = i.ppy / minif;
    o.model = i.model;
    return o;
  }
};

} // namespace ComputeLib
