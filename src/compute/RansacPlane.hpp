#pragma once
#include "ComputeLib.hpp"

#include <cstdlib>

namespace ComputeLib {

class RansacPlane : protected ComputeBase {
#include <generated/RansacPlane_cl_signature.hpp>

private:
  void __pick_random_3(vector<vec4>& pts_out, vector<vec4>& all_pts) {
    pts_out.resize(3);
    for (int i = 0; i < 3; i++) {
      pts_out[i] = all_pts[rand() % all_pts.size()];
    }
  }

  bool __validate_pts(vector<vec4>& pts) {
    if (!pts.size()) { return false; }
    for (int i = 0; i < pts.size(); i++) {
      if (pts[i].w < 1.f) return false;
    }
    return true;
  }

public:
  Engine::Gpu::Buff<int> num_inliers;
  void
  run(Engine::Gpu::Tex<Engine::Gpu::TF::RGBA32F>& pts,
      Engine::Math::Plane* p,
      float inclusion_z_threshold,
      int num_iterations) {

    // pick 3 random points
    // generate plane from points
    // check against rest of data
    //   determine: #inliers, err of inliers
    // determine: #of inliers in

    vector<vec4> pts_cpu;
    pts.cu_copy_to(pts_cpu);

    int max_inliers_cpu = 0;
    Engine::Math::Plane best_plane;
    // int num_iterations = 10;
    for (int i = 0; i < num_iterations; i++) {

      vector<vec4> random_pts;
      while (!__validate_pts(random_pts)) {
        __pick_random_3(random_pts, pts_cpu);
      }

      Engine::Math::Plane temp_p;
      temp_p.generate(random_pts[0], random_pts[1], random_pts[2]);

      if (temp_p.normal.z < 0.f) {
        temp_p.generate({temp_p.normal.xy(), -temp_p.normal.z}, temp_p.center);
      }

      num_inliers.set_storage(1);
      num_inliers.cu_memset(0);

      kernel_RansacPlane_plane_ransac_check(
          pts.get_dims(), glm::ivec2{16, 16}, pts, temp_p, num_inliers, inclusion_z_threshold);

      int inliers_cpu = 0;
      num_inliers.cu_copy_to(&inliers_cpu, 1);

      if (inliers_cpu > max_inliers_cpu) {
        max_inliers_cpu = inliers_cpu;
        best_plane = temp_p;
      }
    }

    *p = best_plane;
  }

  // 2 entries, x & y
  Engine::Gpu::Buff<float> xy_sum;
  void get_xy_avg_of_inliers(
      Engine::Gpu::Tex<Engine::Gpu::TF::RGBA32F>& pts,
      float z_threshold,
      vec2* xy_avg,
      vec2* xy_min,
      vec2* xy_max) {

    num_inliers.set_storage(1);
    num_inliers.cu_memset(0);

    // sum (x/y), min (x/y), max (x/y)
    {
      vector<float> z{0, 0, 0, 0, 0, 0};
      xy_sum.set_data(z);
    }
    kernel_RansacPlane_get_xy_avg_of_inliers(
        pts.get_dims(), {16, 16}, pts, z_threshold, xy_sum, num_inliers);

    vector<float> xy_sum_cpu;
    xy_sum.cu_copy_to(xy_sum_cpu);

    int num_inliers_cpu;
    num_inliers.cu_copy_to(&num_inliers_cpu, 1);

    xy_avg->x = xy_sum_cpu[0] / num_inliers_cpu;
    xy_avg->y = xy_sum_cpu[1] / num_inliers_cpu;
    *xy_min = {xy_sum_cpu[2], xy_sum_cpu[3]};
    *xy_max = {xy_sum_cpu[4], xy_sum_cpu[5]};
  }
};

class RansacPlane_Pipeline {
private:
  RansacPlane ransac_plane;
  DeprojectDepth deproject_depth;
  FilterPoints filter_points;
  GenTriangles gen_triangles;
  MapDepth map_depth;

  DepthCameraMesh<Engine::Scene::MatrixPosition> gen_mesh;
  OrthoMapShader ortho_cam;

  // temporary buffer..
  Engine::Gpu::Tex<Engine::Gpu::TF::RGBA32F> depth_vtxes_tform;
  Engine::Gpu::Tex<Engine::Gpu::TF::RGBA32F> depth_vtxes_tform_2;
  Engine::Gpu::Tex<Engine::Gpu::TF::RGBA32F> depth_vtxes_tform_filtered;

public:
  bool plane_set = false;
  Engine::Math::Plane p;

  // ortho maps of plane: points & binary stencils
  OrthoSampler2D ortho_sampler;
  Engine::Gpu::Tex<Engine::Gpu::TF::RGBA32F> ortho_map_points;
  Engine::Gpu::Tex<Engine::Gpu::TF::R16UI> ortho_map_binary;
  Engine::Gpu::Tex<Engine::Gpu::TF::RGBA8> ortho_map_binary_rgba;

  void find_calibration(
      Engine::Gpu::Tex<Engine::Gpu::TF::R16UI>& depth_img,
      rs2_intrinsics intrinsics,
      int minif_factor,
      float z_threshold,
      int num_iterations,
      ivec2 ortho_map_dims) {

    Tex<TF::RGBA32F> vtxes;
    deproject_depth.run(depth_img, vtxes, intrinsics, minif_factor, mat4{1.f});

    find_calibration(vtxes, z_threshold, num_iterations, ortho_map_dims);

  }

  void find_calibration(
      Engine::Gpu::Tex<Engine::Gpu::TF::RGBA32F>& depth_vtxes,
      float z_threshold,
      int num_iterations,
      ivec2 ortho_map_dims) {

    ortho_map_points.set_storage(ortho_map_dims);
    ortho_map_binary.set_storage(ortho_map_dims);

    ransac_plane.run(depth_vtxes, &p, z_threshold, num_iterations);

    deproject_depth.tform_points(depth_vtxes, depth_vtxes_tform, p.to_plane_coords);

    vec2 xy_avg;
    ransac_plane.get_xy_avg_of_inliers(
        depth_vtxes_tform,
        z_threshold,
        &xy_avg,
        &ortho_sampler.min_coords,
        &ortho_sampler.max_coords);

    ortho_sampler.min_coords -= xy_avg;
    ortho_sampler.max_coords -= xy_avg;
    ortho_sampler.img_dims = ortho_map_points.get_dims();

    // // // //
    mat4 translate_correction = glm::translate(vec3{xy_avg * -1.f, 0});
    vec4 new_center = p.from_plane_coords * vec4{xy_avg, 0., 1};
    p.generate(p.normal, new_center);

    // bake in translation again
    deproject_depth.tform_points(depth_vtxes_tform, depth_vtxes_tform_2, translate_correction);

    // filter points by z coordinates
    filter_points.by_z_value(
        depth_vtxes_tform_2,
        depth_vtxes_tform_filtered,
        FilterPoints::FilterBy::OUTSIDE,
        -z_threshold,
        z_threshold);

    // draw ortho map with z values!
    depth_vtxes_tform_filtered.cu_copy_to(gen_mesh.positions);
    gen_triangles.run(depth_vtxes_tform_filtered, gen_mesh.idxes, &gen_mesh.num_triangles);

    ortho_cam.fbo.bind();

    // z value: observed z value of surface
    // w value: 1 or 0, depending on if pixel was there

    ortho_cam.set_target_texture(ortho_map_points);

    GL::Renderer::setClearColor(0x00000000_rgbaf);
    GL::Renderer::enable(GL::Renderer::Feature::DepthTest);
    GL::Renderer::setPolygonMode(GL::Renderer::PolygonMode::Fill);

    ortho_cam.draw_height = true;
    ortho_cam.set_uniforms(ortho_sampler.min_coords, ortho_sampler.max_coords);
    ortho_cam.fbo.clear(GL::FramebufferClear::Color | GL::FramebufferClear::Depth);
    gen_mesh.get_mesh().draw(ortho_cam);

    ortho_cam.clear_target_texture();

    ortho_cam.set_target_texture(ortho_map_binary);

    ortho_cam.draw_height = false;
    ortho_cam.set_uniforms(ortho_sampler.min_coords, ortho_sampler.max_coords);
    ortho_cam.fbo.clear(GL::FramebufferClear::Color | GL::FramebufferClear::Depth);
    gen_mesh.get_mesh().draw(ortho_cam);

    ortho_cam.clear_target_texture();

    map_depth.run(ortho_map_binary, ortho_map_binary_rgba);

    GL::defaultFramebuffer.bind();

    plane_set = true;
  }
};

} // namespace ComputeLib