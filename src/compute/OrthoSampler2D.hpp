#pragma once

#include <engine/Engine.h>

#include <algorithm>

namespace ComputeLib {

class OrthoSampler2D {
public:
  vec2 min_coords;
  vec2 max_coords;
  ivec2 img_dims;

  // host functions can be defined here
  void set_min_and_max(vector<vec4>& p) {
    const auto x_comp = [](const vec4& v1, const vec4& v2) { return v1.x < v2.x; };
    const auto y_comp = [](const vec4& v1, const vec4& v2) { return v1.y < v2.y; };
    const float min_x = (*std::min_element(p.begin(), p.end(), x_comp)).x;
    const float max_x = (*std::max_element(p.begin(), p.end(), x_comp)).x;
    const float min_y = (*std::min_element(p.begin(), p.end(), y_comp)).y;
    const float max_y = (*std::max_element(p.begin(), p.end(), y_comp)).y;

    min_coords = {min_x, min_y};
    max_coords = {max_x, max_y};
  }

  // just simple 2d pixels
  template <Engine::Gpu::TF t>
  static OrthoSampler2D from_tex(Engine::Gpu::Tex<t>& tex) {
    OrthoSampler2D o;
    o.img_dims = tex.get_dims();
    o.min_coords = {0, 0};
    o.max_coords = tex.get_dims();
    return o;
  }
};

} // namespace ComputeLib
