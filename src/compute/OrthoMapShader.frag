#version 450 core

in noperspective float geom_z_pos;

in vec2 frag_orig_ndc;

uniform bool draw_height;
uniform bool custom_viewport;

layout(location = 0) out uint StencilOut;
layout(location = 1) out vec4 StencilOut_f;

void main() {

  if (custom_viewport) {
    if (frag_orig_ndc.x < -1. || frag_orig_ndc.x > 1. || frag_orig_ndc.y < -1. ||
        frag_orig_ndc.y > 1.) {
      discard;
      return;
    }
  }

  if (draw_height) {
    StencilOut_f = vec4(0, 0, geom_z_pos, 1);
    StencilOut = uint(abs(geom_z_pos));
  } else {
    StencilOut_f = vec4(0, 0, 0, 1);
    StencilOut = uint(1);
  }
}
