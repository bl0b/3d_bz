#pragma once

// primitives for talking to GPU
#include <engine/Engine.h>

// librealsense
#include <librealsense2/rs.hpp>
#include <librealsense2/rsutil.h>

#include <assert.h>
#include <functional>
#include <string>
#include <vector>

using TF = Engine::Gpu::TF;

#include "ComputeBase.hpp"

#include "MapDepth.hpp"
#include "DeprojectDepth.hpp"
#include "CompareDepthImages.hpp"

#include "OrthoSampler2D.hpp"
#include "OrthoMapShader.hpp"

#include "MeshGroups.hpp"

#include "GenTriangles.hpp"
#include "FitRegionsToPlanes.hpp"
#include "MakeSimplifiedStencilMesh.hpp"
#include "FilterPoints.hpp"

#include "BoundingBoxHeightMapsLib.hpp"

#include "ShrinkHeightMap.hpp"

#include "ConvolveCircle.hpp"
#include "RansacPlane.hpp"

#include "DiffDepthImages.hpp"

#include "img_compare_physics/ImgCompare.hpp"
