#pragma once

namespace ComputeLib {

class MakeSimplifiedStencilMesh : protected ComputeBase {
#include <generated/MakeSimplifiedStencilMesh_cl_signature.hpp>

  void
  run(Engine::Gpu::Tex<TF::R16UI>& stencil,
      OrthoSampler2D stencil_sampler,
      // not actually part of mesh, but used later for determining distance from edge of mesh
      Engine::Gpu::Buff<glm::vec4>& out_border,
      DepthCameraMesh<Engine::Scene::MatrixPosition>& out_mesh,
      const int num_rays,
      const int num_sections_per_ray,
      float auto_shrinkage,
      const rs2_intrinsics depth_cam_intrin,
      const rs2_intrinsics rgba_cam_intrin,
      const rs2_extrinsics depth_to_rgba_extrin,
      const float depth_units,
      Engine::Math::Plane cam_to_surface_plane) {

    const int num_vtxes = (num_rays * num_sections_per_ray) + 1;
    const int num_triangles = num_rays * ((2 * num_sections_per_ray) - 1);

    // allocate buffers, configure mesh.
    out_border.set_storage(num_rays);
    out_mesh.positions.set_storage(num_vtxes);
    out_mesh.uv_coords.set_storage(num_vtxes);
    out_mesh.idxes.set_storage(num_triangles * 3);
    out_mesh.set_render_mode(DepthCameraMesh<Engine::Scene::MatrixPosition>::RenderMode::Texture);
    out_mesh.num_triangles = num_rays * ((2 * num_sections_per_ray) - 1);

    cl_program.invoke("make_simplified_stencil_mesh")
        .dims(num_rays, 1)
        .arg(stencil)
        .arg(stencil_sampler)
        .arg(out_border)
        .arg(out_mesh.positions)
        .arg(out_mesh.uv_coords)
        .arg(out_mesh.idxes)
        .arg(auto_shrinkage)
        .arg(depth_cam_intrin)
        .arg(rgba_cam_intrin)
        .arg(depth_to_rgba_extrin)
        .arg(depth_units)
        .arg(cam_to_surface_plane.from_plane_coords)
        .arg(num_sections_per_ray)
        .call();
  }

  void filter_vtxes_by_distance_to_border(
      Engine::Gpu::Buff<glm::vec4>& border,
      Engine::Gpu::Tex<TF::RGBA32F>& vtx_pos_in,
      Engine::Gpu::Tex<TF::RGBA32F>& vtx_pos_out,
      rs2_intrinsics intrin,
      glm::mat4 to_plane) {

    ivec2 dims = vtx_pos_in.get_dims();
    vtx_pos_out.set_storage(dims);

    const float thresh = 100.f;
    size_t shared_mem_size = border.size() * sizeof(float);
    vector<Engine::Math::Plane> viewport_planes = make_viewport_planes(intrin, to_plane);

    cl_program.invoke("filter_vtxes_by_distance_to_border")
        .dims(dims, {border.size(), 1})
        .arg_local(shared_mem_size)
        .arg(vtx_pos_in)
        .arg(vtx_pos_out)
        .arg(border)
        .arg(thresh)
        .arg(viewport_planes[0])
        .arg(viewport_planes[1])
        .arg(viewport_planes[2])
        .arg(viewport_planes[3])
        .call();
  }

  static vector<Engine::Math::Plane>
  make_viewport_planes(const rs2_intrinsics intrin, const mat4 to_plane) {

    // Given the intrinsics and the calculated plane, determine on-plane coords of 4 corners of
    // depth camera viewport, to determine maximum possible plane size

    // a strip on the left of the incoming depth image (for d415 at least) seems to never have data
    // need to treat the end of that zone as edge of viewport - ideally this value would be
    // automatically calibrated
    const float dead_zone = 0;
    // const float dead_zone = 75;

    vector<vec2> corner_coords = {{dead_zone, 0},
                                  {dead_zone, intrin.height},
                                  {intrin.width, intrin.height},
                                  {intrin.width, 0}};

    vec4 viewport_rays[4];
    for (int i = 0; i < 4; i++) {
      // simple deprojection of point at z = 1
      vec2& coord = corner_coords[i];
      viewport_rays[i] =
          to_plane *
          vec4{normalize(
                   vec3((coord.x - intrin.ppx) / intrin.fx, (coord.y - intrin.ppy) / intrin.fy, 1)),
               1.};
    }

    vector<Engine::Math::Plane> viewport_planes(4);
    vec4 viewport_origin = to_plane * vec4{0, 0, 0, 1};

    for (int i = 0; i < 4; i++) {
      viewport_planes[i].generate(
          viewport_origin.xyz(), viewport_rays[i].xyz(), viewport_rays[(i + 1) % 4].xyz());
    }

    return move(viewport_planes);
  }
};

} // namespace ComputeLib
