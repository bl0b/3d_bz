#pragma once
#include "ComputeLib.hpp"
#include <queue>

namespace ComputeLib {

// holds info about mesh groups..
class MeshGroups : public ComputeBase {
public:
  struct GroupInfo {
    uint32_t group_num;
    uint32_t num_els;
    uint32_t normalized_group_num;
    float _padding0[1];

    // calculated bounding box info!
    vec4 group_center{0, 0, 0, 0};

    float group_rot = 0; // radians!
    float _padding1[3];

    vec2 group_dims{1, 1};
    float _padding2[2];

    uint32_t els_offset;    // if group_num == 0, then this is num groups
    uint32_t els_populated; // running tally on how many else have been populated!
    float _padding3[2];

    vec4 avg_pos; // average position of all points in group..
    vec4 traced_pos;
    vec4 sampled_pos;
  };

  DepthCameraMesh<Engine::Scene::MatrixPosition> bounding_box_mesh;

  vector<GroupInfo> groups_info_cpu;
  Engine::Gpu::Buff<GroupInfo> groups_info;

  ivec2 groups_map_dims;
  Engine::Gpu::Buff<uint32_t> groups_map;
  Engine::Gpu::Buff<uint32_t> groups_map_last;

  // std vector too slow for debug build, so just use a memory block
  uint32_t* groups_map_cpu = nullptr;
  int groups_map_cpu_length = 0;

  // memory used in initial group generation
  bool* seen = nullptr;
  int seen_length = 0;

  MeshGroups() {

    size_t fi_size = sizeof(GroupInfo);
    size_t dsd_sz = sizeof(vec4);

    vector<u8vec4> tex_colors(4, {100, 100, 100, 50});
    bounding_box_mesh.tex.set_data({2, 2}, tex_colors.data());

    {
      vector<vec4> uvs{
          {0.5, 0.5, 0, 0},
          {0, 0, 0, 0},
          {1, 0, 0, 0},
          {1, 1, 0, 0},
          {0, 1, 0, 0},
      };
      bounding_box_mesh.uv_coords.set_data(uvs);
    }

    bounding_box_mesh.set_render_mode(
        DepthCameraMesh<Engine::Scene::MatrixPosition>::RenderMode::Texture);

    {vector<uint32_t> idxes{0, 1, 2, 0, 2, 3, 0, 3, 4, 0, 4, 1};
      bounding_box_mesh.idxes.set_data(idxes);
      bounding_box_mesh.num_triangles = 4;
      vector<vec4> vtxes{
          vec4{0, 0, 0, 1},
          vec4{-0.5, -0.5, 0, 1},
          vec4{0.5, -0.5, 0, 1},
          vec4{0.5, 0.5, 0, 1},
          vec4{-0.5, 0.5, 0, 1}};
      bounding_box_mesh.positions.set_data(vtxes);
    }

    init();
  }

  void init() {

#include <generated/cglm_cl.hpp>
#include <generated/MeshGroups_cl.hpp>
#include <generated/OrthoSampler2D_cl.hpp>
#include <generated/atomics_cl.hpp>
    cl_program.init(
        {cglm_cl, OrthoSampler2D_cl, atomics_cl, MeshGroups_cl},
        {"remap_to_normalized_group_numbers",
         "remap_by_previous_group_numbers",
         "draw_vtx_colors",
         "reset_bbox_temp",
         "find_bounding_boxes",
         "normalize_bounding_boxes",
         "get_all_positions_of_groups"});
  }

  ~MeshGroups() {
    if (seen != nullptr) delete[] seen;
    if (groups_map_cpu != nullptr) delete[] groups_map_cpu;
  }

  template <class T> //, class T2>
  void render_bounding_boxes(MeshCamera<T>& cam) {

    Magnum::GL::Renderer::setPolygonMode(Magnum::GL::Renderer::PolygonMode::Fill);
    for (int i = 0; i < groups_info_cpu.size(); i++) {
      const auto& info = groups_info_cpu[i];
      // draw calculated bounding box for each group
      const mat4 t_form = translate(vec3{info.group_center.x, info.group_center.y, -1}) *
                          rotate(-info.group_rot, vec3{0, 0, 1}) * scale(vec3{info.group_dims, 1});
      bounding_box_mesh.position.set(t_form);
      cam.draw(bounding_box_mesh);
    }
  }

  void make_new_from_height_map(
      uint16_t* height_map,
      ivec2 height_map_dims,
      const float min_group_size,
      const int max_num_groups) {

    groups_map_dims = height_map_dims;
    const int groups_map_num_els = groups_map_dims.x * groups_map_dims.y;

    // generate the height map on the CPU
    run(height_map, height_map_dims);

    // copy output height map back to GPU
    groups_map.cu_copy_from(groups_map_cpu, groups_map_num_els);

    // remove groups that are too small
    remove_small_groups(min_group_size);

    // reconcile resulting groups with previously resolved groups, changing group numbers if
    // necessary
    map_groups_to_previous_frame(max_num_groups);
  }

  void remove_small_groups(const float min_group_size) {

    // convert int cutoff beforehand to avoid excessive float math
    const int min_group_num_els = (int)round(min_group_size * groups_map.size());

    int norm_group_num = 1;
    for (auto& g : groups_info_cpu) {
      g.normalized_group_num = g.num_els > min_group_num_els ? norm_group_num++ : 0;
      g.group_num = g.normalized_group_num;
    }

    // truly exhorbitant amount of storage for this, but this is in fact the theoretical maximum
    // number of groups, if it was a perfect checkerboard pattern
    groups_info.set_storage(groups_map.size() / 2);
    // but only copy the actual number of groups to front of buffer
    // there shouldn't be very many groups even though there could be
    // so these should be quick calls.
    groups_info.cu_copy_from(groups_info_cpu.data(), groups_info_cpu.size());

    remap_to_normalized_group_numbers(groups_info, groups_map);

    auto res = std::remove_if(
        groups_info_cpu.begin(), groups_info_cpu.end(), [&](MeshGroups::GroupInfo& info) {
          return info.num_els < min_group_num_els;
        });
    groups_info_cpu.erase(res, groups_info_cpu.end());

    groups_info.cu_copy_from(groups_info_cpu.data(), groups_info_cpu.size());
  }

  void map_groups_to_previous_frame(const int max_num_groups) {

    // make sure previous frame is at least allocated on first call.
    groups_map_last.set_storage(groups_map.size());

    // after small groups have been removed, attempt to switch group numbers if they better
    // match previous group numbers
    remap_by_previous_group_numbers(
        max_num_groups, groups_info_cpu, groups_info, groups_map, groups_map_last);

    remap_to_normalized_group_numbers(groups_info, groups_map);

    groups_map.cu_copy_to(groups_map_last);
  }

  void run(uint16_t* height_map, ivec2 height_map_dims) {

    groups_info_cpu.clear();

    const int num_els = height_map_dims.x * height_map_dims.y;

    if (groups_map_cpu_length != num_els) {
      if (groups_map_cpu != nullptr) delete[] groups_map_cpu;
      groups_map_cpu = new uint32_t[num_els];
      groups_map_cpu_length = 0;
    }
    std::fill(groups_map_cpu, groups_map_cpu + num_els, 0);

    if (seen_length != num_els) {
      seen_length = num_els;
      if (seen != nullptr) delete[] seen;
      seen = new bool[num_els];
    }
    std::fill(seen, seen + num_els, false);

    int current_group_number = 1;

    auto get_idx = [](ivec2 coords, ivec2 dims) -> uint32_t {
      return coords.x + coords.y * dims.x;
    };
    auto get_coords = [](int idx, ivec2 dims) -> ivec2 {
      return ivec2{idx % dims.x, idx / dims.x};
    };

    vector<ivec2> neighbor_dirs{{-1, 0}, {1, 0}, {0, -1}, {0, 1}};

    // todo: if this was just raw memory, would this be faster in debug mode?
    queue<uint32_t> frontier;

    for (int y = 0; y < height_map_dims.y; y++) {
      for (int x = 0; x < height_map_dims.x; x++) {

        int idx = get_idx({x, y}, height_map_dims);
        if (seen[idx]) continue;
        uint16_t h = height_map[idx];
        if (!h) {
          seen[idx] = true;
          continue;
        }

        frontier.push(idx);

        int num_els_in_group = 0;

        while (!frontier.empty()) {
          idx = frontier.front();
          frontier.pop();
          if (seen[idx]) continue;

          num_els_in_group++;

          seen[idx] = true;
          h = height_map[idx];
          if (!h) continue;

          groups_map_cpu[idx] = current_group_number;

          for (auto dir : neighbor_dirs) {
            ivec2 new_coord = get_coords(idx, height_map_dims) + dir;
            if (new_coord.x >= 0 && new_coord.y >= 0 && new_coord.x < height_map_dims.x &&
                new_coord.y < height_map_dims.y) {
              int new_idx = get_idx(new_coord, height_map_dims);
              if (!seen[new_idx]) { frontier.push(new_idx); }
            }
          }
        }

        GroupInfo inf;
        inf.group_num = current_group_number++;
        inf.num_els = num_els_in_group;

        groups_info_cpu.push_back(inf);
      }
    }
  }

  void color_vtxes(
      Engine::Gpu::Buff<vec4>& colors,
      Engine::Gpu::Buff<vec4>& positions,
      OrthoSampler2D group_sampler) {
    const int num_els = positions.size();
    const int block_length = 16;
    cl_program.invoke("draw_vtx_colors")
        .dims((num_els / block_length) + 1, block_length)
        .arg(num_els)
        .arg(groups_map)
        .arg(positions)
        .arg(colors)
        .arg(group_sampler)
        .call();
  }

  void remap_to_normalized_group_numbers(
      Engine::Gpu::Buff<MeshGroups::GroupInfo>& mesh_groups_info,
      Engine::Gpu::Buff<uint32_t>& groups) {

    const int num_els = groups.size();
    const int block_length = 16;

    cl_program.invoke("remap_to_normalized_group_numbers")
        .dims((num_els / block_length) + 1, block_length)
        .arg(mesh_groups_info)
        .arg(groups)
        .arg(num_els)
        .call();
  }

  int next_group_counter = 0;
  set<uint32_t> registered_group_ids;
  Engine::Gpu::Buff<uint32_t> group_matches_with_previous;
  void remap_by_previous_group_numbers(
      const int max_num_groups,
      vector<MeshGroups::GroupInfo>& groups_info_cpu,
      Engine::Gpu::Buff<MeshGroups::GroupInfo>& groups_info,
      Engine::Gpu::Buff<uint32_t>& groups,
      Engine::Gpu::Buff<uint32_t>& groups_old) {

    // this isn't meant to be run on more than 1 or 2..
    const int num_groups = groups_info_cpu.size();
    if (num_groups > max_num_groups) { throw new invalid_argument("too many groups!!"); }

    group_matches_with_previous.set_storage(max_num_groups * max_num_groups);
    group_matches_with_previous.cu_memset(0);

    const int num_els = groups.size();
    const int block_length = 16;

    cl_program.invoke("remap_by_previous_group_numbers")
        .dims((num_els / block_length) + 1, block_length)
        .arg(groups_info)
        .arg(groups)
        .arg(num_els)
        .arg(groups_old)
        .arg(group_matches_with_previous)
        .arg(max_num_groups)
        .call();

    vector<uint32_t> group_matches_cpu;
    group_matches_with_previous.cu_copy_to(group_matches_cpu);

    for (int i = 0; i < num_groups; i++) {

      groups_info_cpu[i].normalized_group_num = next_group_counter + 1;

      do {
        next_group_counter = (next_group_counter + 1) % max_num_groups;
      } while (registered_group_ids.count(next_group_counter + 1));

      bool matched = false;
      unordered_set<int> matches;
      for (int j = 0; j < max_num_groups && !matched; j++) {
        int num_matches = group_matches_cpu[j + i * max_num_groups];
        if (((num_matches * 1.f) / groups_info_cpu[i].num_els) > 0.3) {
          bool other_matches = false;
          for (int k = 0; k < num_groups; k++) {
            if (k == i) continue;
            num_matches = group_matches_cpu[j + k * max_num_groups];
            if (((num_matches * 1.f) / groups_info_cpu[k].num_els) > 0.3) { other_matches = true; }
          }

          if (!other_matches) {
            if (groups_info_cpu[i].normalized_group_num != j + 1) {
              groups_info_cpu[i].normalized_group_num = j + 1;
            }
            matched = true;
          }
        }
      }
    }

    registered_group_ids.clear();
    for (int i = 0; i < num_groups; i++) {
      registered_group_ids.insert(groups_info_cpu[i].normalized_group_num);
    }

    groups_info.cu_copy_from(groups_info_cpu.data(), num_groups);
  }

  Engine::Gpu::Buff<vec4> positions;
  Engine::Gpu::Buff<uint32_t> _all_groups;
  Engine::Gpu::Buff<uint32_t> pos_count;

  Engine::Gpu::Buff<vec4> bounding_box_temp;
  void find_groups_bounding_boxes(
      const int max_num_groups,
      // copy results here too, for cpu side processing..
      vector<MeshGroups::GroupInfo>& groups_info_cpu,
      Engine::Gpu::Buff<MeshGroups::GroupInfo>& groups_info,
      Engine::Gpu::Buff<uint32_t>& groups,
      OrthoSampler2D groups_sampler) {

    const int num_angle_checks = 45;

    // rows: angle check num
    // cols: group num
    bounding_box_temp.set_storage(max_num_groups * num_angle_checks);

    // theoretically could be full.. hopefully will never get close
    positions.set_storage(groups_sampler.img_dims.x * groups_sampler.img_dims.y);
    _all_groups.set_storage(groups_sampler.img_dims.x * groups_sampler.img_dims.y);

    pos_count.set_storage(1);
    pos_count.cu_memset(0);

    const int block_size = 8;
    cl_program.invoke("get_all_positions_of_groups")
        .dims((groups_sampler.img_dims / block_size) + 1, {block_size, block_size})
        .arg(groups_sampler)
        .arg(groups)
        .arg(positions)
        .arg(_all_groups)
        .arg(pos_count)
        .call();

    uint32_t p_c;
    pos_count.cu_copy_to(&p_c, 1);

    if (p_c) {

      cl_program.invoke("reset_bbox_temp")
          .dims(max_num_groups * num_angle_checks, 1)
          .arg(bounding_box_temp)
          .call();

      ivec2 grid_dims{p_c, 1};
      // ivec2 grid_dims{(p_c / 1) + 1, 1};
      ivec2 block_dims{1, num_angle_checks};

      cl_program.invoke("find_bounding_boxes")
          .dims(grid_dims, block_dims)
          .arg(max_num_groups)
          .arg(groups_info)
          .arg(positions)
          .arg(_all_groups)
          .arg(p_c)
          .arg(bounding_box_temp)
          .call();
    }

    if (groups_info_cpu.size()) {
      cl_program.invoke("normalize_bounding_boxes")
          .dims(groups_info_cpu.size(), 1)
          .arg(groups_info)
          .arg(num_angle_checks)
          .arg(bounding_box_temp)
          .call();
    }

    groups_info.cu_copy_to(groups_info_cpu.data(), groups_info_cpu.size());
  }
};

} // namespace ComputeLib
