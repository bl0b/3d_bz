#pragma once

namespace ComputeLib {

// Divides image into regions, attempts to find a plane that matches each region
class FitRegionsToPlanes : protected ComputeBase {
public:
  struct PlaneFit {
    vec4 center;
    vec4 normal;
    float percent_sampled;
    float sum_error;

    // GPU alignment..
    float padding[6];
  };

#include <generated/FitRegionsToPlanes_cl_signature.hpp>

  void
  run(Engine::Gpu::Tex<TF::RGBA32F>& in_points,
      const int region_side_length,
      Engine::Gpu::Buff<PlaneFit>& planes,
      ivec2* planes_dims) {
    // don't round up: must be full regions to get sampled
    *planes_dims = in_points.get_dims() / region_side_length;
    planes.set_storage(planes_dims->x * planes_dims->y);
    const size_t shared_mem_size = sizeof(vec4) * (region_side_length)*2;
    cl_program.invoke("fit_regions_to_planes")
        .dims(*planes_dims, ivec2{region_side_length, 2})
        .arg_local(shared_mem_size)
        .arg(planes)
        .arg(in_points)
        .call();
  }

  /*
  static void to_rgba(
      Engine::Gpu::Buff<PlaneFit>& planes,
      const int region_side_length,
      Engine::Gpu::Tex<TF::RGBA8>& out_rgba,
      const float pct_measured_thresh,
      const float error_thresh);*/

  void to_vtx_colors(
      Engine::Gpu::Buff<vec4>& colors,
      Engine::Gpu::Buff<vec4>& positions,
      const mat4 to_surface,
      const ivec2 vtx_grid_dims,
      const float plane_thresh) {
    colors.set_storage(vtx_grid_dims.x * vtx_grid_dims.y);
    const int block_size = 8;
    cl_program.invoke("to_vtx_colors")
        .dims((vtx_grid_dims / block_size) + 1, {block_size, block_size})
        .arg(colors)
        .arg(positions)
        .arg(to_surface)
        .arg(vtx_grid_dims)
        .arg(plane_thresh)
        .call();
  }

  void to_vtx_colors_2(
      Engine::Gpu::Buff<vec4>& colors,
      Engine::Gpu::Buff<vec4>& positions,
      const mat4 to_surface,
      const ivec2 vtx_grid_dims,
      const float plane_thresh,
      Engine::Gpu::Tex<Engine::Gpu::TF::RGBA32F>& plane_map,
      Engine::Gpu::Tex<Engine::Gpu::TF::R16UI>& plane_map_binary,
      OrthoSampler2D ortho_sampler) {
    colors.set_storage(vtx_grid_dims.x * vtx_grid_dims.y);
    const int block_size = 8;
    cl_program.invoke("to_vtx_colors_2")
        .dims((vtx_grid_dims / block_size) + 1, {block_size, block_size})
        .arg(colors)
        .arg(positions)
        .arg(to_surface)
        .arg(vtx_grid_dims)
        .arg(plane_thresh)
        .arg(plane_map)
        .arg(plane_map_binary)
        .arg(ortho_sampler)
        .call();
  }

  static void find_largest_contiguous_flat_region(
      Engine::Gpu::Buff<PlaneFit>& planes_gpu,
      const ivec2& planes_dims,
      const float pct_measured_thresh,
      const float error_thresh,
      float* pct_planes_matched,
      Engine::Math::Plane* best_fit_plane) {

    vector<FitRegionsToPlanes::PlaneFit> planes;
    planes_gpu.cu_copy_to(planes);

    // Helper lambdas..
    const auto get_coord_on_planes_grid = [&](const int idx) {
      return ivec2{idx % planes_dims.x, idx / planes_dims.x};
    };
    const auto get_idx_on_planes_grid = [&](const ivec2 c) { return c.x + c.y * planes_dims.x; };

    // Find idxes of all fitted planes that fit their data well (i.e. the data is flat)
    unordered_set<int> all_flat_plane_idxes;

    queue<int> flat_plane_idxes_to_check;

    for (int i = 0; i < planes.size(); i++) {
      const auto& plane = planes[i];
      if (plane.percent_sampled > pct_measured_thresh && plane.sum_error < error_thresh) {
        all_flat_plane_idxes.insert(i);
        flat_plane_idxes_to_check.push(i);
      }
    }

    unordered_set<int> seen_flat_plane_idxes;

    vector<unordered_set<int>> contiguous_flat_planes;

    while (!flat_plane_idxes_to_check.empty()) {
      const int plane_idx = flat_plane_idxes_to_check.front();
      flat_plane_idxes_to_check.pop();

      if (seen_flat_plane_idxes.count(plane_idx)) { continue; }

      unordered_set<int> a;

      queue<int> planes_to_check;
      planes_to_check.push(plane_idx);

      while (!planes_to_check.empty()) {
        const int current_idx = planes_to_check.front();
        planes_to_check.pop();

        if (!seen_flat_plane_idxes.count(current_idx)) {
          a.insert(current_idx);
          seen_flat_plane_idxes.insert(current_idx);

          ivec2 orig_coords = get_coord_on_planes_grid(current_idx);

          // simple encoding of how to get immediate neighbors of plane!
          const vector<ivec2> coord_diff = {{-1, 0}, {1, 0}, {0, -1}, {0, 1}};

          for (const auto& diff : coord_diff) {
            const ivec2 n = orig_coords + diff;
            if (n.x >= 0 && n.x < planes_dims.x && n.y >= 0 && n.y < planes_dims.y) {
              const int n_idx = get_idx_on_planes_grid(n);
              if (all_flat_plane_idxes.count(n_idx)) { planes_to_check.push(n_idx); }
            }
          }
        }
      }

      contiguous_flat_planes.push_back(move(a));
    }

    int biggest_flat_planes_idx = -1;

    for (int i = 0; i < contiguous_flat_planes.size(); i++) {
      if (biggest_flat_planes_idx == -1 ||
          contiguous_flat_planes[i].size() >
              contiguous_flat_planes[biggest_flat_planes_idx].size()) {
        biggest_flat_planes_idx = i;
      }
    }

    if (biggest_flat_planes_idx > -1) {
      unordered_set<int>& contiguous_plane = contiguous_flat_planes[biggest_flat_planes_idx];

      vec3 avg_pos{0., 0., 0.};
      vec3 avg_norm{0., 0., 0.};
      for (const int p : contiguous_plane) {
        avg_pos += planes[p].center.xyz();
        avg_norm += planes[p].normal.xyz();
      }

      avg_pos = avg_pos * (1.f / contiguous_plane.size());
      avg_norm = normalize(avg_norm);

      // Write outputs: plane of best fit, and % of planes used to generate plane of best fit
      best_fit_plane->generate(avg_norm, avg_pos);

      const float pct_flat_planes =
          (contiguous_plane.size() * 1.f) / (planes_dims.x * planes_dims.y);
      *pct_planes_matched = pct_flat_planes;
    } else {
      // If there wasn't a single flat plane..
      *pct_planes_matched = 0.;
    }
  }
};

} // namespace ComputeLib