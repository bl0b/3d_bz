#pragma once

#include <engine/Engine.h>

namespace ComputeLib {

// shader for drawing a top-down orthographic projection, looking along the z axis.
// can either render 0/1 for whether a fragment is present at the given pixel, uint for the height
// above 0 each fragment is, or float for real number change from 0.
class OrthoMapShader : public Engine::Gpu::ShaderBase {
public:
  // must use magnum vector type here!
  // typedef GL::Attribute<0, Magnum::Vector4> PositionAttribute;

  Magnum::GL::Framebuffer fbo{Magnum::NoCreate};
  Magnum::GL::Renderbuffer depth_stencil{Magnum::NoCreate};

  float max_depth = 50000.f;

  OrthoMapShader() : Engine::Gpu::ShaderBase() {

#include <generated/OrthoMapShader_vert.hpp>
#include <generated/OrthoMapShaderGroupFilter_geom.hpp>
#include <generated/OrthoMapShader_frag.hpp>

    load(OrthoMapShader_vert);
    load(OrthoMapShaderGroupFilter_geom);
    load(OrthoMapShader_frag);
    finish();

    fbo = move(GL::Framebuffer({{0, 0}, {0, 0}}));
    fbo.mapForDraw(
        {{0, GL::Framebuffer::ColorAttachment(0)}, {1, GL::Framebuffer::ColorAttachment(1)}});
  }

  // todo: make this part of abstract shader program - add option to pick buffer to render too
  void set_target_texture(Engine::Gpu::Tex<Engine::Gpu::TF::R16UI>& tex) {
    set_target_texture(tex, {0, 0}, tex.get_dims());
  }

  void set_target_texture(
      Engine::Gpu::Tex<Engine::Gpu::TF::R16UI>& tex, ivec2 viewport_min, ivec2 viewport_max) {

    depth_stencil.setStorage(
        RenderbufferFormat::DepthStencil, {tex.get_dims().x, tex.get_dims().y});
    fbo.setViewport({{viewport_min.x, viewport_min.y}, {viewport_max.x, viewport_max.y}});
    fbo.attachRenderbuffer(GL::Framebuffer::BufferAttachment::DepthStencil, depth_stencil);

    fbo.attachTexture(GL::Framebuffer::ColorAttachment(0), tex.get(), 0);
    Engine::Gpu::GlUtil::verify_framebuffer_complete(fbo);
  }

  void set_target_texture(Engine::Gpu::Tex<Engine::Gpu::TF::RGBA32F>& tex) {
    depth_stencil.setStorage(
        RenderbufferFormat::DepthStencil, {tex.get_dims().x, tex.get_dims().y});
    fbo.setViewport({{0, 0}, {tex.get_dims().x, tex.get_dims().y}});
    fbo.attachRenderbuffer(GL::Framebuffer::BufferAttachment::DepthStencil, depth_stencil);

    fbo.attachTexture(GL::Framebuffer::ColorAttachment(1), tex.get(), 0);
    Engine::Gpu::GlUtil::verify_framebuffer_complete(fbo);
  }

  void clear_target_texture() {
    fbo.detach(GL::Framebuffer::ColorAttachment(0));
    fbo.detach(GL::Framebuffer::ColorAttachment(1));
  }

  void set_uniforms(const vec2& min_coords, const vec2& max_coords, mat4 pos_tform = mat4{1.f}) {
    // set_uniform
    set_uniform<2>("min_coords", min_coords);
    set_uniform<2>("max_coords", max_coords);
    set_uniform("draw_height", draw_height);
    set_uniform("pos_tform", pos_tform);
    set_uniform("filter_by_group", false);
    set_uniform("max_depth", max_depth);
    set_uniform("custom_viewport", false);
  }

  void set_uniforms_custom_viewport(
      const vec2& min_coords,
      const vec2& max_coords,
      mat4 pos_tform,
      vec2 min_vp,
      vec2 max_vp,
      vec2 full_dims) {
    // set_uniform
    set_uniform<2>("min_coords", min_coords);
    set_uniform<2>("max_coords", max_coords);
    set_uniform("draw_height", draw_height);
    set_uniform("pos_tform", pos_tform);
    set_uniform("filter_by_group", false);
    set_uniform("max_depth", max_depth);

    set_uniform("custom_viewport", true);
    set_uniform<2>("custom_viewport_min", min_vp);
    set_uniform<2>("custom_viewport_max", max_vp);
    set_uniform<2>("full_screen_dims", full_dims);
  }

  void set_uniforms(
      const vec2& min_coords,
      const vec2& max_coords,
      const mat4 pos_tform,
      OrthoSampler2D& groups_sampler,
      Engine::Gpu::Tex<Engine::Gpu::TF::R32UI>& groups,
      const uint group_number) {

    set_uniform<2>("min_coords", min_coords);
    set_uniform<2>("max_coords", max_coords);
    set_uniform("draw_height", draw_height);
    set_uniform("pos_tform", pos_tform);
    set_uniform("max_depth", max_depth);

    set_uniform("custom_viewport", false);
    set_uniform("filter_by_group", true);
    set_uniform("ex_group_num", group_number);
    set_uniform("groups_sampler_min", groups_sampler.min_coords);
    set_uniform("groups_sampler_max", groups_sampler.max_coords);
    set_uniform("groups_sampler_dims", groups_sampler.img_dims);
    groups.bind(2, Magnum::GL::ImageAccess::ReadOnly);
  }

  bool draw_height = false;
};

} // namespace ComputeLib