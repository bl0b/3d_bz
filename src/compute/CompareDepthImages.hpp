#pragma once
#include "ComputeLib.hpp"

namespace ComputeLib {

class CompareDepthImages : protected ComputeBase {
public:
  enum CompareMode {
    // determines percent of pixels that match exactly
    EXACT_MATCH = 1,
    // determines average difference between each pixel
    AVG_DIFF = 2
  };

#include <generated/CompareDepthImages_cl_signature.hpp>

  Engine::Gpu::Buff<vec4> rendered_row_costs;
  void compare_generated_binary_images_to_original_depth(
      Engine::Gpu::Tex<TF::R16UI>& original_obj,
      Engine::Gpu::Tex<TF::R16UI>& original_plane,
      Engine::Gpu::Tex<TF::R16UI>& rendered_images,
      const int num_images,
      const ivec2 rendered_max_grid,
      Engine::Gpu::Buff<vec4>& rendered_costs) {

    if (!num_images) { return; }

    // one entry per image per row
    rendered_row_costs.set_storage(num_images * original_obj.get_dims().y);
    rendered_costs.set_storage(num_images);
    rendered_row_costs.cu_memset(0);
    rendered_costs.cu_memset(0);

    const int num_per_img_grid = 1024;

    kernel_CompareDepthImages_compare_generated_binary_images_to_original_depth(
        {num_per_img_grid, original_obj.get_dims().y},
        {1, 32},
        num_per_img_grid,
        original_obj,
        original_plane,
        rendered_images,
        num_images,
        rendered_max_grid,
        rendered_row_costs);

    kernel_CompareDepthImages_sum_row_costs(
        num_images, 32, rendered_row_costs, num_images, original_obj.get_dims().y, rendered_costs);
  }

  Engine::Gpu::Buff<unsigned int> diff_sum;
  void
  run(Engine::Gpu::Tex<TF::R16UI>& img_1,
      Engine::Gpu::Tex<TF::R16UI>& img_2,
      Engine::Gpu::Tex<TF::R16UI>& diff_img,
      float* output_val,
      CompareDepthImages::CompareMode mode) {

    ivec2 img_1_dims = img_1.get_dims();
    ivec2 img_2_dims = img_2.get_dims();

    if (img_1_dims != img_2_dims) {
      throw new invalid_argument("Depth images to compare must have same dimensions!");
    }

    const ivec2 dims = img_1_dims;

    diff_img.set_storage(dims);

    // clear sum buffer
    unsigned int diff_sum_cpu = 0;
    diff_sum.cu_copy_from(&diff_sum_cpu, 1);

    const int block_size = 8;

    kernel_CompareDepthImages_compare_depth_images(
        (dims / block_size) + 1,
        ivec2{block_size, block_size},
        img_1,
        img_2,
        diff_img,
        diff_sum,
        mode);

    diff_sum.cu_copy_to(&diff_sum_cpu);

    unsigned int num_els = dims.x * dims.y;

    switch (mode) {
    case CompareDepthImages::CompareMode::EXACT_MATCH:
      *output_val = (1.f * (num_els - diff_sum_cpu)) / num_els;
      break;
    case CompareDepthImages::CompareMode::AVG_DIFF:
      *output_val = (1.f * (diff_sum_cpu)) / num_els;
      break;
    }
  }
};

} // namespace ComputeLib
