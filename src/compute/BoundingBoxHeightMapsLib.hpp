#pragma once
#include "ComputeLib.hpp"

namespace ComputeLib {

class BoundingBoxHeightMapsLib : protected ComputeBase {
#include <generated/BoundingBoxHeightMapsLib_cl_signature.hpp>

  void make_heights_list(
      Engine::Gpu::Buff<uint32_t>& height_list,
      Engine::Gpu::Tex<Engine::Gpu::TF::R16UI>& height_map,
      bool trace_x) {

    // operating under assumption here that dims are square
    if (height_map.get_dims().x != height_map.get_dims().y) {
      throw invalid_argument("dims must match!");
    }

    const int dim = height_map.get_dims().x;

    height_list.set_storage(dim);
    height_list.cu_memset(0);

    //
    // trace x to int!!
    //

    int trace_x_int = (int)trace_x;

    const int block_dim = 32;

    kernel_BoundingBoxHeightMapsLib_make_heights_list(
        dim, block_dim, height_map, height_list, trace_x_int, dim);
  }

  Engine::Gpu::Buff<vec4> avg_pos_buff;
  void get_average_pos(
      vec4* avg_pos,
      Engine::Gpu::Tex<Engine::Gpu::TF::R16UI>& height_map,
      vec2 min_coords,
      vec2 max_coords,
      vec2 range_x,
      vec2 range_y) {

    // just storing one average position;
    avg_pos_buff.set_storage(1);

    // operating under assumption here that dims are square
    if (height_map.get_dims().x != height_map.get_dims().y) {
      throw invalid_argument("dims must match!");
    }

    OrthoSampler2D height_map_sampler;
    height_map_sampler.min_coords = min_coords;
    height_map_sampler.max_coords = max_coords;
    height_map_sampler.img_dims = height_map.get_dims();

    avg_pos_buff.cu_memset(0);

    const int num_samples = 48;

    kernel_BoundingBoxHeightMapsLib_get_average_pos(
        {num_samples, num_samples},
        {1, 1},
        height_map,
        height_map_sampler,
        range_x,
        range_y,
        avg_pos_buff);

    avg_pos_buff.cu_copy_to(avg_pos);
  }
};

} // namespace ComputeLib
