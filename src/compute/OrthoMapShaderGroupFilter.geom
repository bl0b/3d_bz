#version 430 core

layout(triangles) in;
layout(triangle_strip, max_vertices = 3) out;

in noperspective float vert_z_pos[];
in vec4 vert_plane_pos[];
out noperspective float geom_z_pos;

in vec2 vert_orig_ndc[];
out vec2 frag_orig_ndc;

uniform layout(binding = 2, r32ui) readonly uimage2D groups_img;

uniform bool filter_by_group;
uniform uint ex_group_num;

uniform vec2 groups_sampler_min;
uniform vec2 groups_sampler_max;
uniform ivec2 groups_sampler_dims;

ivec2 get_img_coords(vec2 coords) {
  vec2 c_norm = (coords - groups_sampler_min) / (groups_sampler_max - groups_sampler_min);
  ivec2 c_img =
      ivec2(int((c_norm.x * groups_sampler_dims.x)), int((c_norm.y * groups_sampler_dims.y)));

  if (c_img.x < 0 || c_img.x >= groups_sampler_dims.x || c_img.y < 0 ||
      c_img.y >= groups_sampler_dims.y) {
    return ivec2(-1, -1);
  }
  return c_img;
}

void main() {

  bool emit_vertex = true;

  if (filter_by_group) {
    ivec2 img_coords =
        get_img_coords(((vert_plane_pos[0] + vert_plane_pos[1] + vert_plane_pos[2]) / 3).xy);
    if (img_coords.x < 0) {
      emit_vertex = false;
    } else {
      const uint group = imageLoad(groups_img, img_coords).r;
      if (group != ex_group_num) { emit_vertex = false; }
    }
  }

  if (emit_vertex) {
    gl_Position = gl_in[0].gl_Position;
    geom_z_pos = vert_z_pos[0];
    frag_orig_ndc = vert_orig_ndc[0];
    EmitVertex();

    gl_Position = gl_in[1].gl_Position;
    geom_z_pos = vert_z_pos[1];
    frag_orig_ndc = vert_orig_ndc[1];
    EmitVertex();

    gl_Position = gl_in[2].gl_Position;
    geom_z_pos = vert_z_pos[2];
    frag_orig_ndc = vert_orig_ndc[2];
    EmitVertex();

    EndPrimitive();
  }
}