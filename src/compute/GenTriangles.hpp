#pragma once

using namespace glm;
using namespace std;

namespace ComputeLib {

// Fills a buffer with triangle idxes and a count, given a set of incoming points.
class GenTriangles : protected ComputeBase {
#include <generated/GenTriangles_cl_signature.hpp>

  Engine::Gpu::Buff<unsigned int> num_triangles_temp_buff{1};

  // Performs no filtering, except for points at (0, 0, 0, 0)
  void
  run(Engine::Gpu::Tex<TF::RGBA32F>& in_points,
      Engine::Gpu::Buff<unsigned int>& out_triangle_idxes,
      unsigned int* out_num_triangles) {

    unsigned int ZERO = 0;
    num_triangles_temp_buff.cu_copy_from(&ZERO, 1);

    const int block_dim = 8;
    const ivec2 vtxes_dims = in_points.get_dims();
    const int max_triangles = 2 * (vtxes_dims.x - 1) * (vtxes_dims.y - 1);
    out_triangle_idxes.set_storage(3 * max_triangles, GL::BufferUsage::DynamicDraw);

    kernel_GenTriangles_gen_triangles(
        (vtxes_dims / block_dim) + 1,
        {block_dim, block_dim},
        in_points,
        out_triangle_idxes,
        num_triangles_temp_buff,
        vtxes_dims);

    num_triangles_temp_buff.cu_copy_to(out_num_triangles);
  }

  //#ifdef BUILD_GL_COMPUTE
  // Engine::Gpu::ShaderBase* compute_shader = nullptr;
  //#endif

  void filter_by_group(
      Engine::Gpu::Buff<unsigned int>& in_triangle_idxes,
      const unsigned int in_num_triangles,
      Engine::Gpu::Buff<unsigned int>& out_triangle_idxes,
      unsigned int* out_num_triangles,
      Engine::Gpu::Buff<vec4>& mesh_vtx_positions,
      Engine::Gpu::Buff<uint32_t>& groups,
      OrthoSampler2D groups_sampler,
      const uint32_t group_num) {

    num_triangles_temp_buff.set_storage(1);
    num_triangles_temp_buff.cu_memset(0);

    const int block_dim = 16;

    kernel_GenTriangles_filter_by_group(
        (in_num_triangles / block_dim) + 1,
        block_dim,
        in_triangle_idxes,
        in_num_triangles,
        out_triangle_idxes,
        num_triangles_temp_buff,
        mesh_vtx_positions,
        groups,
        groups_sampler,
        group_num);

    num_triangles_temp_buff.cu_copy_to(out_num_triangles, 1);
  }
};

} // namespace ComputeLib