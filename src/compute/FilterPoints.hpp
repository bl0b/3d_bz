#pragma once

using TF = Engine::Gpu::TF;

using namespace glm;
using namespace std;

namespace ComputeLib {

class FilterPoints : protected ComputeBase {
public:
  enum FilterBy : int {
    // INSIDE,
    OUTSIDE = 1,
    // LESS_THAN,
    MORE_THAN = 2
  };

#include <generated/FilterPoints_cl_signature.hpp>

  void by_z_value(
      Engine::Gpu::Tex<TF::RGBA32F>& points_in,
      Engine::Gpu::Tex<TF::RGBA32F>& points_out,
      const FilterPoints::FilterBy filter_by,
      const float v0,
      const float v1) {
    const int block_size = 8;
    points_out.set_storage(points_in.get_dims());
    cl_program.invoke("by_z_value")
        .dims((points_out.get_dims() / block_size) + 1, {block_size, block_size})
        .arg(points_in)
        .arg(points_out)
        .arg(filter_by)
        .arg(v0)
        .arg(v1)
        .call();
  }

  void by_z_value_and_plane_map(
      Engine::Gpu::Tex<TF::RGBA32F>& points_in,
      Engine::Gpu::Tex<TF::RGBA32F>& points_out,
      Engine::Gpu::Tex<TF::RGBA32F>& plane_map,
      Engine::Gpu::Tex<TF::R16UI>& plane_map_binary,
      OrthoSampler2D plane_map_sampler,
      const float threshold) {

    const int block_size = 8;
    points_out.set_storage(points_in.get_dims());
    cl_program.invoke("by_z_value_and_plane_map")
        .dims((points_out.get_dims() / block_size) + 1, {block_size, block_size})
        .arg(points_in)
        .arg(points_out)
        .arg(plane_map)
        .arg(plane_map_binary)
        .arg(plane_map_sampler)
        .arg(threshold)
        .call();
  }

  void divide_original_2_single(
    // in
    Engine::Gpu::Tex<TF::R16UI>& img_in,
    mat4 to_world,
    rs2_intrinsics i_in,
    const unsigned int minif,
    // out
    Engine::Gpu::Tex<TF::RG16UI>& split_out,
    rs2_intrinsics* i_out,
    // params..
    Engine::Gpu::Tex<TF::RGBA32F>& plane_map,
    OrthoSampler2D plane_map_sampler,
    const float threshold) {

    const ivec2 d_out = img_in.get_dims() / (int)minif;
    split_out.set_storage(d_out);

    const int block_size = 8;
    kernel_FilterPoints_divide_original_2_single(
      (d_out / block_size) + 1,
      { block_size, block_size },
      img_in,
      to_world,
      i_in,
      minif,
      split_out,
      plane_map,
      plane_map_sampler,
      threshold);

    i_out->fx = i_in.fx / minif;
    i_out->fy = i_in.fy / minif;
    i_out->width = d_out.x;
    i_out->height = d_out.y;
    i_out->ppx = i_in.ppx / minif;
    i_out->ppy = i_in.ppy / minif;
    i_out->model = i_in.model;
  }

  void by_stencil(
      Engine::Gpu::Tex<TF::R16UI>& img_in,
      Engine::Gpu::Tex<TF::R16UI>& stencil,
      Engine::Gpu::Tex<TF::R16UI>& img_out) {

    img_out.set_storage(img_in.get_dims());

    kernel_FilterPoints_by_stencil(img_in.get_dims(), {8, 8}, img_in, stencil, img_out);
  }

  // Remove all points not inside the stencil
  void by_plane_stencil(
      Engine::Gpu::Tex<TF::RGBA32F>& points_in,
      Engine::Gpu::Tex<TF::RGBA32F>& points_out,
      Engine::Gpu::Tex<TF::R16UI>& stencil,
      OrthoSampler2D stencil_sampler) {
    points_out.set_storage(points_in.get_dims());
    const int block_size = 8;
    cl_program.invoke("by_plane_stencil")
        .dims((points_in.get_dims() / block_size) + 1, {block_size, block_size})
        .arg(points_in)
        .arg(points_out)
        .arg(stencil)
        .arg(stencil_sampler)
        .call();
  }
};

} // namespace ComputeLib
