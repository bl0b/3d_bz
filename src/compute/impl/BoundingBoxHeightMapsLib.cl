// #py_include <atomics.cl>
// #py_include <OrthoSampler2D.cl>

__kernel void get_average_pos(
    __read_only image2d_t /*TF::R16UI*/ height_map,
    OrthoSampler2D height_map_sampler,
    float2 range_min,
    float2 range_max,
    __global float4* avg_pos) {

  const int x = get_global_id(0);
  const int y = get_global_id(1);

  const float2 norm_coords =
      (float2){(x * 1.f) / get_global_size(0), (y * 1.f) / get_global_size(1)};

  const float2 img_real_coords = range_min + (norm_coords * (range_max - range_min));

  const int2 img_coords = get_img_coords(height_map_sampler, img_real_coords);

  if (img_coords.x > -1 && img_coords.y > -1) {

    const uint h = read_imageui(height_map, img_coords).x;

    if (h) {

      __global float* avg_x = (__global float*)avg_pos;
      __global float* avg_y = avg_x + 1;
      // skip z!
      __global float* avg_w = avg_x + 3;

      atomic_addf(avg_x, img_real_coords.x);
      atomic_addf(avg_y, img_real_coords.y);
      atomic_addf(avg_w, 1.);
    }
  }
}

__kernel void make_heights_list(
    __read_only image2d_t /*TF::R16UI*/ height_map,
    __global uint* max_heights,
    int trace_x,
    int col_dim) {

  // for each row, get max value!
  const int row_idx = get_group_id(0);

  const int col_start_idx = get_local_id(0);
  const int col_stride = get_local_size(0);

  for (int col_idx = col_start_idx; col_idx < col_dim; col_idx += col_stride) {

    const uint h =
        read_imageui(height_map, (int2){trace_x ? row_idx : col_idx, trace_x ? col_idx : row_idx})
            .x;
    if (h) { atomic_max(max_heights + row_idx, h); }
  }
}