// #py_include <cglm.cl>

__kernel void map_depth(
    __read_only image2d_t /*TF::R16UI*/ points_in,
    __write_only image2d_t /*TF::RGBA8*/ color_out,
    int2 dims) {

  const int x = get_global_id(0);
  const int y = get_global_id(1);

  if (x < dims.x && y < dims.y) {

    const sampler_t in_sampler =
        CLK_NORMALIZED_COORDS_FALSE | CLK_ADDRESS_NONE | CLK_FILTER_NEAREST;
    uint depth = read_imageui(points_in, in_sampler, (int2){x, y}).x;
    bool a = depth > 0;

    // clang-format off
    float4 out_color = a ? (float4){
          0,
          (700 + (depth % 700)) * 1.f / 1500.f,
          0.5,
          1} : (float4){0, 0, 0, 1};
    // clang-format on

    write_imagef(color_out, (int2){x, y}, out_color);
  }
}

__kernel void map_depth_split(
    __read_only image2d_t /*TF::RG16UI*/ in, __write_only image2d_t /*TF::RGBA8*/ out, int2 dims) {

  const int x = get_global_id(0);
  const int y = get_global_id(1);

  if (x < dims.x && y < dims.y) {

    const uint2 vals = read_imageui(in, (int2){x, y}).xy;
    uint ch0 = vals.x;
    bool a = ch0 > 0;

    uint ch1 = vals.y;
    bool b = ch1 > 0;

    // clang-format off
    float4 out_color = (a || b) ? (float4) {
        ch1 ? ((700 + (ch1 % 700)) * 1.f / 1500.f) : 0,
        ch0 ? ((700 + (ch0 % 700)) * 1.f / 1500.f) : 0,
        0.5,
        1
    } : (float4) { 0, 0, 0, 1 };
    // clang-format on

    write_imagef(out, (int2){x, y}, out_color);
  }
}

__kernel void map_depth2(
    __read_only image2d_t /*TF::RG16UI*/ in,
    __write_only image2d_t /*TF::RGBA8*/ out,
    int2 dims,
    uint channel) {

  const int x = get_global_id(0);
  const int y = get_global_id(1);

  if (x < dims.x && y < dims.y) {

    const uint4 v = read_imageui(in, (int2){x, y});
    uint d;
    switch (channel) {
    case 0:
      d = v.x;
      break;
    case 1:
      d = v.y;
      break;
    }

    // clang-format off
    float4 out_color = (d) ? (float4) {
        0,
        ((700 + (d % 700)) * 1.f / 1500.f),
        0.5,
        1
    } : (float4) { 0, 0, 0, 1 };
    // clang-format on

    write_imagef(out, (int2){x, y}, out_color);
  }
}

__kernel void map_depth_meshid(
    __read_only image2d_t /*TF::RG16UI*/ mesh_ids_in,
    __write_only image2d_t /*TF::RGBA8*/ color_out,
    int2 dims,
    uint channel) {

  const int x = get_global_id(0);
  const int y = get_global_id(1);

  if (x < dims.x && y < dims.y) {

    const uint4 v = read_imageui(mesh_ids_in, (int2){x, y});
    uint mesh_id;
    switch (channel) {
    case 0:
      mesh_id = v.x;
      break;
    case 1:
      mesh_id = v.y;
      break;
    }

    float4 out_color = (float4){0, 0, 0, 1};
    if (mesh_id) {
      switch (mesh_id) {
      case 1:
        out_color.x = 1.;
        break;
      case 2:
        out_color.y = 1.;
        break;
      case 3:
        out_color.z = 1.;
        break;
      case 4:
        out_color.x = 1.;
        out_color.y = 1.;
        break;
      case 5:
        out_color.y = 1.;
        out_color.z = 1.;
        break;
      case 6:
        out_color.x = 1.;
        out_color.z = 1.;
        break;
      case 7:
        out_color.x = .3;
        out_color.y = .7;
        out_color.z = .1;
        break;
      default:
        // unidentified mesh-id!
        out_color = (float4){1., 1., 1., 1.};
      }
    }

    write_imagef(color_out, (int2){x, y}, out_color);
  }
}