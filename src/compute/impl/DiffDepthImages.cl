// #py_include <cglm.cl>
// #py_include <Plane.cl>
// #py_include <RsTypes.cl>

/*
struct {
  int4 coord; // {coord.x, coord.y, mesh_id, 0}
  float4 depth; // {depth (img 1), depth (img 2), 0, 0}
} DepthDiff;
*/

__kernel void get_depth_diffs(
    __read_only image2d_t /*TF::R16UI*/ orig_obj_img,
    __read_only image2d_t /*TF::R16UI*/ rendered_img,
    __read_only image2d_t /*TF::R16UI*/ rendered_img_mesh_ids,
    __global uint* num_depth_diffs,
    __global int4* depth_diffs,
    int minif_factor) {

  const int2 c = (int2){get_global_id(0) * minif_factor, get_global_id(1) * minif_factor};
  const int2 dims = get_image_dim(orig_obj_img);
  if (c.x < dims.x && c.y < dims.y) {

    uint orig_val = read_imageui(orig_obj_img, c).x;
    uint rendered_val = read_imageui(rendered_img, c).x;

    if (orig_val && rendered_val && orig_val != rendered_val &&
        abs((int)orig_val - (int)rendered_val) > 10) {

      uint depth_diff_id = atomic_add(num_depth_diffs, 1);
      uint rendered_val_mesh_id = read_imageui(rendered_img_mesh_ids, c).x;
      depth_diffs[depth_diff_id * 2] = (int4){c.x, c.y, rendered_val_mesh_id, 0};
      depth_diffs[depth_diff_id * 2 + 1] = (int4){(int)orig_val, (int)rendered_val, 0, 0};
    }
  }
}

__kernel void get_edge_diffs(
    __read_only image2d_t /*TF::R16UI*/ orig_plane_img,
    __read_only image2d_t /*TF::R16UI*/ rendered_img,
    __read_only image2d_t /*TF::R16UI*/ rendered_img_mesh_ids,
    __global uint* num_edge_diffs,
    __global int4* edge_diffs) {

  const int2 c = (int2){get_global_id(0), get_global_id(1)};
  const int2 dims = get_image_dim(orig_plane_img);
  if (c.x < dims.x && c.y < dims.y) {

    uint orig_val = read_imageui(orig_plane_img, c).x;
    uint rendered_val = read_imageui(rendered_img, c).x;

    // there is a fragment being guessed to be on top of the plane!
    // need to push it out of the way!
    if (orig_val && rendered_val) {

      uint edge_diff_id = atomic_add(num_edge_diffs, 1);

      uint rendered_val_mesh_id = read_imageui(rendered_img_mesh_ids, c).x;

      edge_diffs[edge_diff_id] = (int4){c.x, c.y, rendered_val_mesh_id, 0};
    }
  }
}

__kernel void find_nearest_empty_pixels(
    __read_only image2d_t /*TF::R16UI*/ orig_img,
    uint num_to_check,
    __global int4* coords_to_check,
    __global int4* nearest_empty_coords,
    __global uint* found_empty_count,
    __global int4* found_empties) {

  const int i = get_global_id(0);
  const int j = get_global_id(1);
  const int k = get_global_id(2);

  const int max_j = get_global_size(1);
  const int max_k = get_global_size(2);

  const int max_empties = max_j * max_k;

  if (i < num_to_check) {

    int4 coord = coords_to_check[i];

    int2 coord_offset = (int2){j - (max_j / 2), k - (max_k / 2)};
    int2 coord_check = (int2){coord.x, coord.y} + coord_offset;

    uint orig_val = read_imageui(orig_img, coord_check).x;

    if (!orig_val) {
      int pixel_dist_sq = coord_offset.x * coord_offset.x + coord_offset.y * coord_offset.y;
      uint empty_idx = atomic_add(found_empty_count + i, 1);
      found_empties[max_empties * i + empty_idx] =
          (int4){coord_check.x, coord_check.y, pixel_dist_sq, 0};
    }
  }

  barrier(CLK_GLOBAL_MEM_FENCE);

  if (i < num_to_check && j == 0 && k == 0) {
    const uint empties_found = found_empty_count[i];

    if (empties_found) {

      int closest_dist_sq;
      int2 closest_dist_sq_coords;

      for (int q = 0; q < empties_found; q++) {
        const int4 found_empty = found_empties[max_empties * i + q];
        const int dist_sq = found_empty.z;
        if (q == 0 || dist_sq < closest_dist_sq) {
          closest_dist_sq = dist_sq;
          closest_dist_sq_coords = (int2){found_empty.x, found_empty.y};
        }
      }

      nearest_empty_coords[i] = (int4){closest_dist_sq_coords.x, closest_dist_sq_coords.y, 0, 69};
    }
  }
}

// really int2!!
float3 deproj(rs2_intrinsics* i, int4 coord, float depth) {
  return (float3){depth * (coord.x - i->ppx) / i->fx, depth * (coord.y - i->ppy) / i->fy, depth};
}

__kernel void deproj_coords(
    rs2_intrinsics intrinsics,
    uint num_coords,
    __global int4* coords_a,
    __global int4* coords_b,
    __global float4* coords_a_out,
    __global float4* coords_b_out) {

  const int i = get_global_id(0);
  if (i < num_coords) {
    float3 a = deproj(&intrinsics, coords_a[i], 1.f);
    glm_vec3_normalize(&a);
    float3 b = deproj(&intrinsics, coords_b[i], 1.f);
    glm_vec3_normalize(&b);
    coords_a_out[i] = (float4){a.x, a.y, a.z, 1.};
    coords_b_out[i] = (float4){b.x, b.y, b.z, 1.};
  }
}