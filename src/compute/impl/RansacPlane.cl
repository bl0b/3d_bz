// #py_include <cglm.cl>
// #py_include <Plane.cl>
// #py_include <atomics.cl>

__kernel void plane_ransac_check(
    __read_only image2d_t /*TF::RGBA32F*/ pts,
    Plane plane,
    __global int* num_inliers,
    float z_threshold) {

  const int2 coord = (int2){get_global_id(0), get_global_id(1)};

  int2 img_dims = get_image_dim(pts);

  const sampler_t in_sampler = CLK_NORMALIZED_COORDS_FALSE | CLK_ADDRESS_NONE | CLK_FILTER_NEAREST;

  if (coord.x < img_dims.x && coord.y < img_dims.y) {

    float4 position = read_imagef(pts, coord);
    if (position.w > 0.f) {
      float4 new_pos;
      glm_mat4_mulv(&plane.to_plane_coords, &position, &new_pos);

      if (fabs(new_pos.z) < z_threshold) { atomic_add(num_inliers, 1); }
    }
  }
}

__kernel void get_xy_avg_of_inliers(
    __read_only image2d_t /*TF::RGBA32F*/ pts,
    float z_threshold,
    __global float* xy_avg,
    __global int* num_inliers) {

  const int2 coord = (int2){get_global_id(0), get_global_id(1)};
  int2 img_dims = get_image_dim(pts);
  const sampler_t in_sampler = CLK_NORMALIZED_COORDS_FALSE | CLK_ADDRESS_NONE | CLK_FILTER_NEAREST;

  if (coord.x < img_dims.x && coord.y < img_dims.y) {
    float4 position = read_imagef(pts, coord);
    if (position.w > 0.f) {
      if (position.z < z_threshold && position.z > -z_threshold) {
        atomic_addf(xy_avg, position.x);
        atomic_addf(xy_avg + 1, position.y);
        atomic_add(num_inliers, 1);

        atomic_minf(xy_avg + 2, position.x);
        atomic_minf(xy_avg + 3, position.y);

        atomic_maxf(xy_avg + 4, position.x);
        atomic_maxf(xy_avg + 5, position.y);
      }
    }
  }
}
