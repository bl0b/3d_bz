// #py_include <cglm.cl>
// #py_include <OrthoSampler2D.cl>
// #py_include <atomics.cl>

typedef struct {
  uint group_num;
  uint num_els;
  uint normalized_group_num;
  float _padding0[1];

  // calculated bounding box info!
  float4 group_center;

  float group_rot;    // radians!
  float _padding1[3]; //

  float2 group_dims;
  float _padding2[2];

  uint els_offset;    // if group_num == 0, then this is num groups
  uint els_populated; // running tally on how many else have been populated!
  float _padding3[2];

  float4 avg_pos; // average position of all points in group..
  float4 traced_pos;
  float4 sampled_pos;
} GroupInfo;

__kernel void remap_to_normalized_group_numbers(
    __global uchar* _mesh_groups_info, __global uint* groups, int num_els) {

  __global GroupInfo* mesh_groups_info = (__global GroupInfo*)(_mesh_groups_info);

  const int idx = get_global_id(0);
  if (idx < num_els) {

    const int orig_group_num = groups[idx];
    if (orig_group_num) {
      const GroupInfo group_info = mesh_groups_info[orig_group_num - 1];
      const int new_group_num = group_info.normalized_group_num;
      groups[idx] = new_group_num;
    }
  }
}

// matches: max_num_groups sized square array. cols (y) are new groups. rows (x) are old groups, num
// matches seen
__kernel void remap_by_previous_group_numbers(
    __global GroupInfo* groups_info,
    __global uint* groups,
    int num_els,
    __global uint* groups_old,

    __global uint* matches,
    int max_num_groups) {

  const int idx = get_global_id(0);
  if (idx < num_els) {
    const int new_group_num = groups[idx];
    const int old_group_num = groups_old[idx];
    if (new_group_num && old_group_num) {

      atomic_inc(matches + ((old_group_num - 1) + ((new_group_num - 1) * max_num_groups)));
    }
  }
}

// assignes a color for each vertex based on which group it is part of
inline float4 get_vtx_color(uint group_num) {
  switch (group_num) {
  case 0:
    // not part of group: clear to white-ish color
    return (float4){1., 0.8, 0.8, 0.7};
    // else: hard-code colors for each group.
    // TODO: make these colors better looking (not just pure red, etc.)
  case 1:
    return (float4){1., 0., 0., 0.7};
  case 2:
    return (float4){1., 1., 0., 0.7};
  case 3:
    return (float4){0., 1., 1., 0.7};
  case 4:
    return (float4){0., 0., 1., 0.7};
  case 5:
    return (float4){1., 0., 1., 0.7};
  case 6:
    return (float4){0., 1., 0., 0.7};
  case 7:
    return (float4){0.2, .6, 0., 0.7};
  case 8:
    return (float4){0.7, 1., 0.2, 0.7};
  case 9:
    return (float4){0.9, .5, 0.5, 0.7};
  }
}

__kernel void draw_vtx_colors(
    int total_els,
    __global uint* groups,
    __global float4* vtx_positions,
    __global float4* vtx_colors,
    OrthoSampler2D groups_sampler) {

  const int idx = get_global_id(0);
  if (idx < total_els) {
    float4 pos = vtx_positions[idx];

    // only need to do anything if it is a real position
    if (pos.w > 0) {
      int group_idx = get_img_idx(groups_sampler, (float2){pos.x, pos.y});
      uint group_num = group_idx > -1 ? groups[group_idx] : 0;
      vtx_colors[idx] = get_vtx_color(group_num);
    }
  }
}

__kernel void get_all_positions_of_groups(
    OrthoSampler2D groups_sampler,
    __global uint* groups_grid,
    __global float4* all_positions,
    __global uint* all_groups,
    __global uint* pos_count) {

  const int2 group_coords = (int2){get_global_id(0), get_global_id(1)};
  if (group_coords.x < groups_sampler.img_dims.x && group_coords.y < groups_sampler.img_dims.y) {
    const uint group = groups_grid[group_coords.x + group_coords.y * groups_sampler.img_dims.x];
    if (group) {

      // check neighboring pixels: if has all 4 direct neighbors, then we conclude
      // it is not an 'edge'. Only edges can contribute to the min/max, so that's
      // all we need!

      const int2 neighbors[4] = {{-1, 0}, {1, 0}, {0, -1}, {0, 1}};
      bool empty_found = false;

      for (int i = 0; i < 4; i++) {
        if (!empty_found) {
          const int2 c = group_coords + neighbors[i];
          if (c.x < 0 || c.y < 0 || c.x >= groups_sampler.img_dims.x ||
              c.y >= groups_sampler.img_dims.y) {
            empty_found = true;
          } else {
            const int c_idx = c.x + c.y * groups_sampler.img_dims.x;
            if (!groups_grid[c_idx]) { empty_found = true; }
          }
        }
      }

      if (empty_found) {

        float2 pos = get_world_coords(groups_sampler, group_coords);
        int pos_idx = atomic_inc(pos_count);
        all_positions[pos_idx] = (float4){pos.x, pos.y, 0., 1.};
        all_groups[pos_idx] = group;
      }
    }
  }
}

__kernel void find_bounding_boxes(
    int max_num_groups,
    __global GroupInfo* groups_info,
    __global float4* positions,
    __global uint* all_groups,
    uint num_positions,
    __global float4* bbox_temp) {

  const int idx = get_global_id(0);

  const int rot_num = get_global_id(1);
  const int num_rotations = get_global_size(1);
  const float theta = (M_PI * 0.5f * rot_num) / num_rotations;

  if (idx < num_positions) {

    float4 _pos = positions[idx];

    uint group = all_groups[idx];

    float2 rotated_pos = (float2){_pos.x * cos(theta) - _pos.y * sin(theta),
                                  _pos.x * sin(theta) + _pos.y * cos(theta)};

    __global float4* bbox_temp_idx = bbox_temp + (rot_num + ((group - 1) * num_rotations));
    __global float* bb_min_x = ((__global float*)bbox_temp_idx);     // x
    __global float* bb_min_y = ((__global float*)bbox_temp_idx) + 1; // y
    __global float* bb_max_x = ((__global float*)bbox_temp_idx) + 2; // z
    __global float* bb_max_y = ((__global float*)bbox_temp_idx) + 3; // w
    atomic_minf(bb_min_x, rotated_pos.x);
    atomic_maxf(bb_max_x, rotated_pos.x);

    atomic_minf(bb_min_y, rotated_pos.y);
    atomic_maxf(bb_max_y, rotated_pos.y);
  }
}

// stupid use of GPU functionality...
__kernel void normalize_bounding_boxes(
    __global GroupInfo* groups_info, int num_rot_checks, __global float4* bbox_temp) {
  const int idx = get_global_id(0);
  __global GroupInfo* info = groups_info + idx;

  int min_seen = -1;
  float4 best_min_max;
  float min_area = -1;
  for (int i = 0; i < num_rot_checks; i++) {
    float4 min_max = bbox_temp[((info->normalized_group_num - 1) * num_rot_checks) + i];
    float2 range = (float2){min_max.z - min_max.x, min_max.w - min_max.y};
    float area = range.x * range.y;
    if (min_seen < 0 || area < min_area) {
      min_seen = i;
      min_area = area;
      best_min_max = min_max;
    }
  }

  const float theta = info->group_rot = (M_PI * 0.5f * min_seen) / num_rot_checks;

  float2 group_bb_min = (float2){best_min_max.x, best_min_max.y};
  float2 group_bb_max = (float2){best_min_max.z, best_min_max.w};

  float2 bb_min_reverted = (float2){group_bb_min.x * cos(-theta) - group_bb_min.y * sin(-theta),
                                    group_bb_min.x * sin(-theta) + group_bb_min.y * cos(-theta)};

  float2 bb_max_reverted = (float2){group_bb_max.x * cos(-theta) - group_bb_max.y * sin(-theta),
                                    group_bb_max.x * sin(-theta) + group_bb_max.y * cos(-theta)};

  info->group_dims = group_bb_max - group_bb_min;

  float2 center = (bb_max_reverted + bb_min_reverted) / 2.f;
  info->group_center = (float4){center.x, center.y, 0., 1.};

  info->group_rot = theta;
}

// 1st 2 elements an atomic min, so use max val for comparison, same for 2nd 2 elements.
__kernel void reset_bbox_temp(__global float4* bbox_temp) {
  const float MAX_VAL = 10000000.;
  const int idx = get_global_id(0);
  bbox_temp[idx] = (float4){MAX_VAL, MAX_VAL, -MAX_VAL, -MAX_VAL};
}