// #py_include <OrthoSampler2D.cl>
// #py_include <atomics.cl>

// just as long as a value is present, then the invocation coord is accepted
__kernel void generate_invocation_coords(
    __read_only image2d_t /*TF::R16UI*/ depth_img,
    __global float4* invocation_coords_out,
    __global uint* num_invocation_coords,
    int minification,
    __write_only image2d_t /*TF::RGBA8*/ depth_img_rgba) {

  uint x = get_global_id(0);
  uint y = get_global_id(1);

  uint num_x = get_global_size(0);
  uint num_y = get_global_size(1);

  if (x < num_x && y < num_y) {

    int2 coords = (int2){x * minification, y * minification};
    uint value = read_imageui(depth_img, coords).x;
    if (value) {

      uint invocation_num = atomic_add(num_invocation_coords, 1);
      invocation_coords_out[invocation_num] =
          (float4){x * minification * 1.f, y * minification * 1.f, 0, 0};

      write_imagef(
          depth_img_rgba, (int2){x * minification, y * minification}, (float4){0.0, 0.8, 0.8, 1.0});
    }
  }
}

__kernel void make_convolve_grid(
    uint num_coords,
    __global int4* coord_ids,
    __global float4* coords,
    int num_thetas,
    __read_only image2d_t /*TF::R16UI*/ depth_img,
    OrthoSampler2D depth_sampler,
    int2 num_samples,
    float2 sample_ray_dist,
    __global float4* convolve_out) {

  int coord_id_id = get_global_id(0);
  int2 sample_id = (int2){get_global_id(1), get_global_id(2)};

  if (coord_id_id < num_coords && sample_id.x < num_samples.x && sample_id.y < num_samples.y) {

    const int4 coord_id_info = coord_ids[coord_id_id];
    const int coord_id = coord_id_info.x;
    const int theta_id = coord_id_info.y;
    const float theta = theta_id * M_PI * 2.f / num_thetas;

    const float4 coord = coords[coord_id]; // only look at x/y

    float2 theta_dir = (float2){cos(theta), sin(theta)};
    float2 theta_orthogonal_dir = (float2){cos(theta + (M_PI / 2.f)), sin(theta + (M_PI / 2.f))};

    // sample id 1: how far along theta dir!
    float step_along_theta = (1.f * sample_ray_dist.x) / num_samples.x;
    float step_against_theta = (1.f * sample_ray_dist.y) / num_samples.y;

    int sample_id_y_adjusted = sample_id.y - (num_samples.y / 2);

    float2 sample_coord =
        (float2){coord.x + (theta_dir.x * step_along_theta * sample_id.x) +
                     (theta_orthogonal_dir.x * step_against_theta * sample_id_y_adjusted),
                 coord.y + (theta_dir.y * step_along_theta * sample_id.x) +
                     (theta_orthogonal_dir.y * step_against_theta * sample_id_y_adjusted)};

    const sampler_t in_sampler =
        CLK_NORMALIZED_COORDS_FALSE | CLK_ADDRESS_NONE | CLK_FILTER_NEAREST;

    uint sampled_value = read_imageui(depth_img, get_img_coords(depth_sampler, sample_coord)).x;

    const int out_idx =
        coord_id_id * (num_samples.x * num_samples.y) + sample_id.x * num_samples.y + sample_id.y;

    convolve_out[out_idx] = (float4){sample_coord.x, sample_coord.y, sampled_value * 1.f, 1.f};
  }
}

__kernel void convert_to_plane_object_depth(
    int num_entries,
    __global float4* object_convolved,
    __global float4* plane_convolved,
    __global float4* plane_object_convolved_combined) {

  uint entry_id = get_global_id(0);

  if (entry_id < num_entries) {
    float obj_depth = object_convolved[entry_id].z;
    float plane_depth = plane_convolved[entry_id].z;

    if (obj_depth && plane_depth) { printf("both value? shouldn't happen..\n"); }

    plane_object_convolved_combined[entry_id] = (float4){obj_depth, plane_depth, 0., 0.};
  }
}

// convolved combined:
__kernel void trace_rays_3(
    __global float4* convolved_combined,
    int num_coords,
    int num_thetas,
    int num_samples,
    __global int4* ray_lengths) {

  uint coord_id = get_global_id(0);
  uint theta_id = get_global_id(1);

  if (coord_id < num_coords && theta_id < num_thetas) {

    const int step_memory_size = num_thetas * num_samples;
    const int start_idx = coord_id * step_memory_size + (theta_id * num_samples);

    bool counting = true;
    uint count = 0;
    bool min_obj_seen = false;
    float min_obj_val_seen;
    int min_obj_id_seen;

    for (int i = 0; i < num_samples; i++) {
      const float4 val = convolved_combined[start_idx + i];
      const float obj_depth = val.x;
      const float plane_depth = val.y;

      if (counting) {

        if (obj_depth > 0.) {
          if (!min_obj_seen) {
            min_obj_seen = true;
            min_obj_val_seen = obj_depth;
            min_obj_id_seen = count;
          } else if (obj_depth < min_obj_val_seen) {
            min_obj_val_seen = obj_depth;
            min_obj_id_seen = count;
          }

          count++;

          // only when encountering a piece of non-occluded plane does the count really stop!
        } else if (plane_depth > 0.) {
          count++;
          counting = false;
        } else {
          count++;
        }
      }
    }

    const int ray_group_memory_size = num_thetas;
    const int ray_group_start_idx = coord_id * ray_group_memory_size + theta_id;

    // { ray length, min val seen on ray, step # of min val seen
    ray_lengths[ray_group_start_idx] = (int4){count, min_obj_val_seen, min_obj_id_seen, 0};
  }
}

__kernel void find_pairs_of_short_rays(
    int num_coords,
    int num_thetas,
    int max_ray_length,
    int min_ray_length,
    __global int4* ray_lengths,
    __global uint* num_matching_rays,
    __global int4* matching_rays) {

  uint coord_id = get_global_id(0);
  // uint theta_id = get_global_id(1);

  if (coord_id < num_coords) { // && theta_id < (num_thetas / 2)) {

    int match_count = 0;
    bool match_found = false;
    uint shortest_found = 0;
    int shortest_theta_found;
    uint shortest_rl1;
    uint shortest_rl2;
    int shortest_step;

    const int half_thetas = num_thetas / 2;
    for (int i = 0; i < half_thetas; i++) {

      const int r1 = (coord_id * num_thetas) + i;
      const int r2 = r1 + (num_thetas / 2);

      const uint rl1 = ray_lengths[r1].x;
      const uint rl2 = ray_lengths[r2].x;

      // if (rl1 > min_ray_length && rl2 > min_ray_length) {

      if (rl1 > min_ray_length && rl1 < max_ray_length && rl2 > min_ray_length &&
          rl2 < max_ray_length) {

        match_count++;

        const uint full_ray_length = rl1 + rl2;

        if (!match_found) {
          match_found = true;
          shortest_found = full_ray_length;
          shortest_theta_found = i;
          shortest_rl1 = rl1;
          shortest_rl2 = rl2;
          shortest_step =
              ray_lengths[r1].y < ray_lengths[r2].y ? ray_lengths[r1].z : -ray_lengths[r2].z;

        } else if (full_ray_length < shortest_found) {
          shortest_found = full_ray_length;
          shortest_theta_found = i;
          shortest_rl1 = rl1;
          shortest_rl2 = rl2;
          shortest_step =
              ray_lengths[r1].y < ray_lengths[r2].y ? ray_lengths[r1].z : -ray_lengths[r2].z;
        }
      }
    }

    if (match_found && match_count > 3) {
      uint match_id = atomic_add(num_matching_rays, 1);
      // each match takes up 2!
      matching_rays[match_id * 2] =
          (int4){coord_id, shortest_theta_found, shortest_rl1, shortest_rl2};
      matching_rays[match_id * 2 + 1] = (int4){shortest_step, 0, 0, 69};
    }
  }
}

__kernel void convolve_coords_list(
    int num_coords,
    __global float4* coords,
    __read_only image2d_t /*TF::R16UI*/ depth_img,
    OrthoSampler2D depth_sampler,
    int num_thetas,
    int num_samples,
    float dist,
    __global float4* convolve_out) {

  uint coord_id = get_global_id(0);
  uint theta_id = get_global_id(1);
  uint sample_id = get_global_id(2);

  if (coord_id < num_coords) {

    const int num_els_per_coord = num_thetas * num_samples;

    // starting coord..
    const float4 coord = coords[coord_id];

    const sampler_t in_sampler =
        CLK_NORMALIZED_COORDS_FALSE | CLK_ADDRESS_NONE | CLK_FILTER_NEAREST;

    const float theta = (M_PI * 2.f * theta_id / num_thetas);
    const float sample_step = (dist / (num_samples - 1)) * sample_id;

    float2 new_pos =
        (float2){coord.x + sample_step * cos(theta), coord.y + sample_step * sin(theta)};

    uint sampled_value = read_imageui(depth_img, get_img_coords(depth_sampler, new_pos)).x;

    const int out_idx = (num_els_per_coord * coord_id) + (num_samples * theta_id) + sample_id;

    if (sampled_value > 0) {
      convolve_out[out_idx] = (float4){new_pos.x, new_pos.y, sampled_value * 1.f, 1.f};
    } else {
      convolve_out[out_idx] = (float4){0, 0, 0, 0};
    }
    //}
  }
}

__kernel void analyze_rays(
    __global float4* coords,
    uint num_coords,
    int num_thetas,
    int num_samples,
    __global float4* convolve_result,
    __global int4* info_out_result) {

  uint coord_id = get_group_id(0);
  uint theta_id = get_local_id(0);

  if (coord_id < num_coords && theta_id < num_thetas) {

    const uint convolve_result_idx =
        (coord_id * num_samples * num_thetas) + (theta_id * num_samples);

    int count = 0;
    bool counting = true;

    for (int i = 0; i < num_samples && counting; i++) {
      float4 sample_result = convolve_result[convolve_result_idx + i];
      if (sample_result.w > 0.f) {
        count++;
      } else {
        counting = false;
      }
    }

    info_out_result[(coord_id * num_thetas) + theta_id] = (int4){coord_id, theta_id, count, 1};
  }
}

__kernel void find_matching_ray_length_patterns(
    int num_coords,
    int num_thetas,
    int num_samples,
    __global int4* ray_info_in,
    __global int4* match_coords_out,
    __global uint* coord_match_id

) {

  uint coord_id = get_global_id(0);

  if (coord_id < num_coords) {

    int count_min = 0;
    int count_max = 0;
    int total_count = 0;

    int theta_sum = 0;

    const float min_ray_pct = .2;

    const int min_length = round(min_ray_pct * num_samples);

    // bool full_section_seen = false;
    // bool in_full_section = false;

    // bool looking = true;

    bool in_full_section = false;
    // bool full_section_seen = false;
    int num_full_sections = 0;
    bool start_in_full = false;

    int start_full_i;
    int end_full_i;

    bool empty = false;

    for (int i = 0; i < num_thetas; i++) {
      int4 ray_length_info = ray_info_in[coord_id * num_thetas + i];
      int ray_length = ray_length_info.z;
      if (ray_length >= min_length) {
        total_count++;
        if (ray_length == num_samples) {
          if (i == 0) { start_in_full = true; }
          if (!in_full_section) {
            in_full_section = true;
            start_full_i = i;
            num_full_sections++;
          }

          end_full_i = i;

          count_max++;

        } else {

          if (in_full_section) { in_full_section = false; }

          count_min++;
        }
      } else {
        empty = true;
      }
    }

    // wrapped around..
    if (in_full_section && start_in_full) { empty = true; }

    int avg = (start_full_i + end_full_i) / 2;

    const float pct_max = 1.f * count_max / num_thetas;
    const float pct_min = 1.f * count_min / num_thetas;

    if (!empty && num_full_sections == 1 && pct_max > 0.05 && pct_max < 0.4 && count_min > 0.6) {
      uint match_id = atomic_add(coord_match_id, 1);
      match_coords_out[match_id] = (int4){coord_id, avg, 0, 0};
    }
    // ray_info_in
  }
}

__kernel void search_rays(
    __global float4* coords,
    uint num_coords,
    int num_thetas,
    int num_samples,
    int ray_min_length,
    __global float4* convolve_result,
    __global int4* rays_out,
    __global uint* num_rays_out) {

  uint coord_id = get_group_id(0);
  uint theta_id = get_local_id(0);

  // int min_samples = num_samples / 3;

  if (coord_id < num_coords && theta_id < num_thetas) {

    const uint convolve_result_idx =
        (coord_id * num_samples * num_thetas) + (theta_id * num_samples);

    // float max_val = -1;
    float last_val = convolve_result[convolve_result_idx].z;

    bool still_counting = true;
    bool is_ray = true;

    for (int i = 1; i < num_samples && still_counting; i++) {
      float4 sample_result = convolve_result[convolve_result_idx + i];

      bool is_sample = sample_result.w > 0.f;
      if (!is_sample) {
        is_ray = false;
        still_counting = false;
      }

      /*
  if (!is_sample) {
    still_counting = false;
    is_ray = (i >= ray_min_length);
  }
  if (sample_result.z < last_val) {
    still_counting = false;
  } else {
    last_val = sample_result.z;
  }

      */
    }

    if (is_ray) {
      uint ray_out_id = atomic_add(num_rays_out, 1);
      rays_out[ray_out_id] = (int4){coord_id, theta_id, 0, 0};

      // count num in w!
      atomic_addf((__global float*)(coords + coord_id) + 3, 1.f);
    }
  }
}

__kernel void convolve_circle(
    __read_only image2d_t /*TF::R16UI*/ ortho_img,
    OrthoSampler2D ortho_sampler,
    __global float4* convolve_out,
    int num_thetas,
    float dist,
    int num_samples,
    float2 step) {

  uint num_steps_x = get_global_size(0);
  uint num_steps_y = get_global_size(1);

  uint x = get_global_id(0);
  uint y = get_global_id(1);
  uint z = get_global_id(2);

  if (x < num_steps_x && y < num_steps_y && z < num_thetas) {

    const int step_memory_size = num_thetas * num_samples;
    const int step_idx = y * num_steps_x + x;
    const int start_out_idx = step_idx * step_memory_size + z * num_samples;

    const sampler_t in_sampler =
        CLK_NORMALIZED_COORDS_FALSE | CLK_ADDRESS_NONE | CLK_FILTER_NEAREST;

    // coordinates at center of sampling circle
    float2 start_coords = ortho_sampler.min_coords;
    start_coords.x = start_coords.x + step.x * x;
    start_coords.y = start_coords.y + step.y * y;

    float theta = (2.f * z * M_PI) / num_thetas;
    const float sample_step = dist / num_samples;

    for (int i = 0; i < num_samples; i++) {
      float2 new_pos = (float2){start_coords.x + (sample_step * i * cos(theta)),
                                start_coords.y + (sample_step * i * sin(theta))};

      int2 coord = get_img_coords(ortho_sampler, new_pos);

      uint sampled_value = read_imageui(ortho_img, in_sampler, coord).x;
      if (sampled_value > 0) {
        convolve_out[start_out_idx + i] = (float4){new_pos.x, new_pos.y, sampled_value * 1.f, 1.f};
      }
    }
  }
}

__kernel void trace_rays(
    __global float4* convolved_rays,
    int num_thetas,
    int num_samples,
    OrthoSampler2D ortho_sampler,
    __global uint* ray_lengths) {

  uint num_steps_x = get_global_size(0);
  uint num_steps_y = get_global_size(1);

  uint x = get_global_id(0);
  uint y = get_global_id(1);
  uint z = get_global_id(2);

  if (x < num_steps_x && y < num_steps_y && z < num_thetas) {

    const int step_memory_size = num_thetas * num_samples;
    const int step_idx = y * num_steps_x + x;
    const int start_idx = step_idx * step_memory_size + z * num_samples;

    bool counting = true;
    uint count = 0;

    for (int i = 0; i < num_samples && counting; i++) {
      float4 sampled_pos = convolved_rays[start_idx + i];
      if (counting && sampled_pos.w > 0) {
        count++;
      } else {
        counting = false;
      }
    }

    const int ray_group_memory_size = num_thetas;
    const int ray_group_start_idx = step_idx * ray_group_memory_size + z;

    ray_lengths[ray_group_start_idx] = count;
  }
}

__kernel void trace_rays_2(
    __global uint* ray_lengths,
    OrthoSampler2D sampler,
    int num_thetas,
    int num_samples,
    float2 step,
    __global uint* full_ray_count,
    __global float4* full_rays,
    __global int4* ray_ids

) {

  uint num_steps_x = get_global_size(0);
  uint num_steps_y = get_global_size(1);

  uint x = get_global_id(0);
  uint y = get_global_id(1);
  uint z = get_global_id(2);

  if (x < num_steps_x && y < num_steps_y && z < num_thetas / 2) {

    const int step_idx = y * num_steps_x + x;

    const uint ray_length_1 = ray_lengths[step_idx * num_thetas + z];
    const uint ray_length_2 = ray_lengths[step_idx * num_thetas + (z + num_thetas / 2)];

    const uint full_ray_length = ray_length_1 + ray_length_2;

    // if ray is full in both directions, record it!
    if (full_ray_length == num_samples * 2) {

      const float x_coord = sampler.min_coords.x + (step.x * x);
      const float y_coord = sampler.min_coords.y + (step.y * y);

      const float theta = (2.f * z * M_PI) / num_thetas;

      uint match_num = atomic_inc(full_ray_count);
      full_rays[match_num] = (float4){x_coord, y_coord, theta, 0.};
      ray_ids[match_num] = (int4){x, y, z, 0};
    }
  }
}

__kernel void rays_linear_regression(
    __global float4* convolved_rays,
    __global uint* num_rays,
    __global int4* ray_ids,
    __global float4* ray_positions,

    int num_thetas,
    __local float* local_z_vals,
    int num_samples,
    int2 convolved_dims) {

  const int x = get_global_id(0);
  if (x < *num_rays) {
    int4 ray_id = ray_ids[x];
    int r_x = ray_id.x;
    int r_y = ray_id.y;
    int r_z = ray_id.z;

    const int step_memory_size = num_thetas * num_samples;
    const int step_idx = r_y * convolved_dims.x + r_x;
    const int ray_1_idx = step_idx * step_memory_size + r_z * num_samples;

    // ray_1 only one direction of ray that extends both directions from the origin
    const int ray_2_idx = ray_1_idx + (num_thetas * num_samples / 2);

    __global float4* ray_1 = convolved_rays + ray_1_idx;
    __global float4* ray_2 = convolved_rays + ray_2_idx;

    float sum_x = 0.f;
    float sum_y = 0.f;
    float sum_xx = 0.f;
    float sum_xy = 0.f;

    const float n = num_samples * 2.f;

    for (int i = 0; i < num_samples; i++) {
      {
        float val = convolved_rays[ray_1_idx + (num_samples - 1) - i].z;
        float _x = i * 1.f;
        float _y = val;
        sum_x += _x;
        sum_y += _y;
        sum_xx += (_x * _x);
        sum_xy += (_x * _y);

        if (r_x == 22 && r_y == 42 && r_z == 3) {
          //        printf("%f1 ", val);
        }
      }
      {
        float val = convolved_rays[ray_2_idx + i].z;
        float _x = (num_samples + i) * 1.f;
        float _y = val;
        sum_x += _x;
        sum_y += _y;
        sum_xx += (_x * _x);
        sum_xy += (_x * _y);
        if (r_x == 22 && r_y == 42 && r_z == 3) {
          // printf("%f1\n", val);
        }
      }

      // local_z_vals[num_samples + i] = convolved_rays[ray_2_idx + i].z;
    }

    //
    if (r_x == 22 && r_y == 42 && r_z == 3) {

      // printf("ray_1_idx: %d\n", ray_1_idx);
      // printf("ray_2_idx: %d\n", ray_2_idx);

      // for (int i = 0; i < num_samples; i++) {
      // printf("%f1 - %f1\n", convolved_rays[ray_1_idx + i].z, convolved_rays[ray_2_idx + i].z);
      //  printf("%f1 - %f1\n", local_z_vals[i], local_z_vals[i + num_samples]);
      //}

      // printf("\n");
    }

    //

    // local_z_vals now populated with a full ray. the center 2 entries are the same!
    // this throws off a bit of the 'straight line fit' math, but worry about it later

    /*

for (int i = 0; i < num_samples * 2; i++) {
  float _x = i * 1.f;
  float _y = local_z_vals[i];
  sum_x += _x;
  sum_y += _y;
  sum_xx += (_x * _x);
  sum_xy += (_x * _y);
}

    */

    float m = ((n * sum_xy) - (sum_x * sum_y)) / ((n * sum_xx) - (sum_x * sum_x));

    float b = (sum_y - (m * sum_x)) / n;

    float sq_err = 0.f;

    for (int i = 0; i < num_samples; i++) {
      float err = ((m * i) + b) - convolved_rays[ray_1_idx + (num_samples - 1) - i].z;
      sq_err += (err * err);
      err = ((m * (i + num_samples)) + b) - convolved_rays[ray_2_idx + i].z;
      sq_err += (err * err);
    }

    ray_positions[x].w = sq_err;
  }
}
