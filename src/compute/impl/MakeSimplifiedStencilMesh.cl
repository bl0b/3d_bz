// #py_include <cglm.cl>
// #py_include <OrthoSampler2D.cl>
// #py_include <RsTypes.cl>
// #py_include <Plane.cl>

float2 get_uv(
    const float2 plane_coords,
    const _mat4 from_plane_coords,
    const rs2_intrinsics depth_cam_intrin,
    const rs2_intrinsics rgba_cam_intrin,
    const rs2_extrinsics depth_to_rgba_extrin,
    const float depth_units) {

  const float* r = depth_to_rgba_extrin.rotation;
  const float* t = depth_to_rgba_extrin.translation;

  float4 coord = (float4){plane_coords.x, plane_coords.y, 0, 1};
  glm_mat4_mulv(&from_plane_coords, &coord, &coord);
  coord.x *= depth_units;
  coord.y *= depth_units;
  coord.z *= depth_units;

  // copied directly from from rsutil function..
  float4 rgba_cam_coord = (float4){r[0] * coord.x + r[3] * coord.y + r[6] * coord.z + t[0],
                                   r[1] * coord.x + r[4] * coord.y + r[7] * coord.z + t[1],
                                   r[2] * coord.x + r[5] * coord.y + r[8] * coord.z + t[2],
                                   1};

  // Approximate pixel coordinates of point in depth image
  const float2 pixel_coords = (float2){
      ((rgba_cam_coord.x / rgba_cam_coord.z) * rgba_cam_intrin.fx) + rgba_cam_intrin.ppx,
      ((rgba_cam_coord.y / rgba_cam_coord.z) * rgba_cam_intrin.fy) + rgba_cam_intrin.ppy,
  };

  // Normalize pixel coordinates from 0-1, for UV mapping
  const float2 pixel_coords_normalized =
      (float2){pixel_coords.x / rgba_cam_intrin.width, pixel_coords.y / rgba_cam_intrin.height};

  return pixel_coords_normalized;
}

__kernel void make_simplified_stencil_mesh(
    __read_only image2d_t /*TF::R16UI*/ stencil,
    OrthoSampler2D stencil_sampler,
    __global float4* out_border,
    __global float4* out_mesh_pos,
    __global float4* out_mesh_uv,
    __global uint* out_mesh_idx,
    float auto_shrinkage,
    rs2_intrinsics depth_cam_intrin,
    rs2_intrinsics rgba_cam_intrin,
    rs2_extrinsics depth_to_rgba_extrin,
    float depth_units,
    _mat4 from_plane_coords,
    int num_sections_per_ray) {

  const int i = get_group_id(0);
  const int num_rays = get_num_groups(0);
  const float theta = (M_PI * 2.f / num_rays) * i;

  const float2 ray_dir = (float2){cos(theta), sin(theta)};

  if (i == 0) {
    // set origin as first point if first thread
    out_mesh_pos[0] = (float4){0., 0., 0., 1.};

    const float2 uv_coord = get_uv(
        (float2){0., 0.},
        from_plane_coords,
        depth_cam_intrin,
        rgba_cam_intrin,
        depth_to_rgba_extrin,
        depth_units);
    // 2nd two elements in UV buffer don't matter!
    out_mesh_uv[0] = (float4){uv_coord.x, uv_coord.y, 0., 0.};
  }

  const float small_distance_inc = 20.;
  const int num_small_dist_checks = 15;
  const float distance_inc = 100.;
  // can start a decent amount away from center..
  float current_distance = 500.;
  float2 coords;
  int2 stencil_coords;
  uint is_value = 1;

  bool still_looking = true;
  while (still_looking) {

    current_distance += distance_inc;
    coords = ray_dir * current_distance;

    int2 check_coords = get_img_coords(stencil_sampler, coords);
    is_value = read_imageui(stencil, check_coords).x;

    if (!is_value) {

      // if having struck a pixel that is deemed 'not' part of the grid, then check ahead on the
      // same ray. if most of the pixels ahead are also not part of the grid, declare the tracing
      // to be done. otherwise continue
      int num_off_plane = 0;
      for (int small_check = 0; small_check < num_small_dist_checks; small_check++) {

        coords = ray_dir * (current_distance + (small_check * small_distance_inc));

        check_coords = get_img_coords(stencil_sampler, coords);
        uint a = read_imageui(stencil, check_coords).x;
        if (!a) num_off_plane++;
      }

      const float pct_off_plane = (1.f * num_off_plane) / num_small_dist_checks;
      if (pct_off_plane > 0.8f) { still_looking = false; }
    }
  }

  coords = ray_dir * (current_distance - auto_shrinkage);

  const int start_vtx_idx = (i * num_sections_per_ray);
  const int triangles_per_ray = (2 * num_sections_per_ray) - 1;
  const int start_tri_num = i * triangles_per_ray; //(i * ((2 * num_sections_per_ray) - 1));

  const int num_vtxes = num_rays * num_sections_per_ray;

  for (int s = 0; s < num_sections_per_ray; s++) {

    const float2 plane_coord = coords * ((s + 1) / (1.f * num_sections_per_ray));
    out_mesh_pos[start_vtx_idx + s + 1] = (float4){plane_coord.x, plane_coord.y, 0, 1};

    const float2 uv_coord = get_uv(
        plane_coord,
        from_plane_coords,
        depth_cam_intrin,
        rgba_cam_intrin,
        depth_to_rgba_extrin,
        depth_units);
    out_mesh_uv[start_vtx_idx + s + 1] = (float4){uv_coord.x, uv_coord.y, 0, 0};

    if (s == 0) {
      const int start_idx = start_tri_num * 3;
      out_mesh_idx[(start_tri_num * 3)] = 0;
      out_mesh_idx[(start_tri_num * 3) + 1] = start_vtx_idx + 1;
      out_mesh_idx[(start_tri_num * 3) + 2] =
          ((start_vtx_idx + num_sections_per_ray) % (num_vtxes)) + 1;

    } else {
      const int start_idx = (start_tri_num + (s * 2) - 1) * 3;
      out_mesh_idx[start_idx] = start_vtx_idx + s;
      out_mesh_idx[start_idx + 1] = start_vtx_idx + s + 1;
      out_mesh_idx[start_idx + 2] =
          ((start_vtx_idx + num_sections_per_ray) % num_vtxes) + s; //) % num_vtxes) + 1;

      out_mesh_idx[start_idx + 3] = ((start_vtx_idx + num_sections_per_ray) % num_vtxes) + s;
      out_mesh_idx[start_idx + 4] = ((start_vtx_idx + num_sections_per_ray) % num_vtxes) + s + 1;
      out_mesh_idx[start_idx + 5] = start_vtx_idx + s + 1;
    }

    if (s == num_sections_per_ray - 1) {
      out_border[i] = (float4){plane_coord.x, plane_coord.y, 0, 1};
    }
  }
}

inline float distance2(float2 a, float2 b) { return pow(a.x - b.x, 2) + pow(a.y - b.y, 2); }

__kernel void filter_vtxes_by_distance_to_border(
    __local float* dist,
    __read_only image2d_t /*TF::RGBA32F*/ vtxes_obj_in,
    __write_only image2d_t /*TF::RGBA32F*/ vtxes_obj_out,
    __global float4* border,
    float threshold,
    Plane viewport_plane_0,
    Plane viewport_plane_1,
    Plane viewport_plane_2,
    Plane viewport_plane_3) {

  Plane viewport_planes[4] = {
      viewport_plane_0, viewport_plane_1, viewport_plane_2, viewport_plane_3};

  const int ray = get_local_id(0);

  const int2 coords = (int2){get_group_id(0), get_group_id(1)};

  const int next_ray = (ray + 1) % get_local_size(0);

  float4 orig_pos = read_imagef(vtxes_obj_in, coords);

  bool is_pos = orig_pos.w > 0.;

  if (is_pos) {

    float2 vtx_pos = (float2){orig_pos.x, orig_pos.y};
    float2 p0 = (float2){border[ray].x, border[ray].y};
    float2 p1 = (float2){border[next_ray].x, border[next_ray].y};

    float vtx_to_line_dist_sq;

    float line_dist_sq = distance2(p0, p1);
    if (line_dist_sq == 0) {
      // this means line consists of 2 identical points.. this probably shouldn't happen, because we
      // are generating these points to be a polygon
      vtx_to_line_dist_sq = distance2(vtx_pos, p0);
    } else {
      float t =
          ((vtx_pos.x - p0.x) * (p1.x - p0.x) + (vtx_pos.y - p0.y) * (p1.y - p0.y)) / line_dist_sq;

      // clamp between 0 and 1..
      t = t < 0 ? 0 : t;
      t = t > 1 ? 1 : t;

      vtx_to_line_dist_sq = distance2(vtx_pos, p0 + (t * (p1 - p0)));
    }

    dist[ray] = sqrt(vtx_to_line_dist_sq);
  }

  barrier(CLK_LOCAL_MEM_FENCE);

  if (ray == 0) {

    if (is_pos) {

      float dist_from_viewport_limit = -1;
      for (int v = 0; v < 4; v++) {
        Plane p = viewport_planes[v];

        const float dist_from_viewport_plane = fabs(
            (orig_pos.x * p.equation.x) + (orig_pos.y * p.equation.y) +
            (orig_pos.z * p.equation.z) + p.equation.w);

        // float dist_from_viewport_plane = p.distance_from_point(orig_pos.xyz());
        if (dist_from_viewport_limit < 0 || dist_from_viewport_plane < dist_from_viewport_limit) {
          dist_from_viewport_limit = dist_from_viewport_plane;
        }
      }

      float min_seen = -1;
      for (int i = 0; i < get_local_size(0); i++) {
        if (min_seen < 0 || dist[i] < min_seen) { min_seen = dist[i]; }
      }

      bool on_edge = min_seen < threshold || dist_from_viewport_limit < threshold;

      // remove vtxes on edge of visible area!
      if (on_edge) {
        write_imagef(vtxes_obj_out, coords, (float4){0., 0., 0., 0.});
      } else {
        write_imagef(vtxes_obj_out, coords, orig_pos);
      }
    } else {
      write_imagef(vtxes_obj_out, coords, (float4){0, 0, 0, 0});
    }
  }
};
