// #py_include <cglm.cl>

__kernel void deproject_depth(
    __read_only image2d_t /*TF::R16UI*/ pts_in,
    __write_only image2d_t /*TF::RGBA32F*/ pts_out,
    int minif,
    float2 i_pp,
    float2 i_f,
    _mat4 tform) {

  unsigned int x = get_global_id(0);
  unsigned int y = get_global_id(1);

  int2 vtx_img_dims = get_image_dim(pts_out);

  if (x < vtx_img_dims.x && y < vtx_img_dims.y) {

    int2 fetch_coord;
    fetch_coord.x = x * minif;
    fetch_coord.y = y * minif;
    uint depth = read_imageui(pts_in, fetch_coord).x;

    int2 coord;
    coord.x = x;
    coord.y = y;

    float4 v = (float4){depth * (fetch_coord.x - i_pp.x) / i_f.x,
                        depth * (fetch_coord.y - i_pp.y) / i_f.y,
                        depth,
                        (depth > 0) ? 1. : 0.};

    if (depth) { glm_mat4_mulv(&tform, &v, &v); }

    write_imagef(pts_out, coord, v);
  }
}



__kernel void deproject_depth_split(
  __read_only image2d_t /*TF::RG16UI*/ pts_in,
  __write_only image2d_t /*TF::RGBA32F*/ pts_out,
  int minif,
  float2 i_pp,
  float2 i_f,
  _mat4 tform) {

  unsigned int x = get_global_id(0);
  unsigned int y = get_global_id(1);

  int2 vtx_img_dims = get_image_dim(pts_out);

  if (x < vtx_img_dims.x && y < vtx_img_dims.y) {

    int2 fetch_coord = (int2){ x, y } * minif;
    //fetch_coord.x = x * minif;
    //fetch_coord.y = y * minif;
    uint depth = read_imageui(pts_in, fetch_coord).x;

    int2 coord;
    coord.x = x;
    coord.y = y;

    float4 v = (float4){ depth * (fetch_coord.x - i_pp.x) / i_f.x,
                        depth * (fetch_coord.y - i_pp.y) / i_f.y,
                        depth,
                        (depth > 0) ? 1. : 0. };

    if (depth) { glm_mat4_mulv(&tform, &v, &v); }

    write_imagef(pts_out, coord, v);
  }
}


__kernel void tform_points(
    __read_only image2d_t /*TF::RGBA32F*/ pts_in,
    __write_only image2d_t /*TF::RGBA32F*/ pts_out,
    _mat4 tform) {

  unsigned int x = get_global_id(0);
  unsigned int y = get_global_id(1);

  int2 dims = get_image_dim(pts_out);

  if (x < dims.x && y < dims.y) {
    int2 coord = (int2){x, y};
    float4 pt_in = read_imagef(pts_in, coord);
    if (pt_in.w > 0.) {
      float4 pt_out;
      glm_mat4_mulv(&tform, &pt_in, &pt_out);
      write_imagef(pts_out, coord, pt_out);
    } else {
      write_imagef(pts_out, coord, (float4){0, 0, 0, 0});
    }
  }
}

__kernel void tform_points_buff(__global float4* pts, int num_pts, _mat4 tform) {

  unsigned int i = get_global_id(0);

  if (i < num_pts) {
    // int2 coord = (int2){x, y};
    float4 pt_in = pts[i];
    if (pt_in.w > 0.) {
      float4 pt_out;
      glm_mat4_mulv(&tform, &pt_in, &pt_out);
      pts[i] = pt_out;
    } else {
      pts[i] = (float4){0, 0, 0, 0};
    }
  }
}