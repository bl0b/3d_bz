// https://stackoverflow.com/questions/18950732/atomic-max-for-floats-in-opencl
// https://streamhpc.com/blog/2016-02-09/atomic-operations-for-floats-in-opencl-improved/

// Function to perform the atomic max
inline void atomic_maxf(volatile __global float* source, const float operand) {
  union {
    unsigned int intVal;
    float floatVal;
  } newVal;
  union {
    unsigned int intVal;
    float floatVal;
  } prevVal;
  do {
    prevVal.floatVal = *source;
    newVal.floatVal = max(prevVal.floatVal, operand);
  } while (atomic_cmpxchg((volatile __global unsigned int*)source, prevVal.intVal, newVal.intVal) !=
           prevVal.intVal);
}

// Function to perform the atomic min
inline void atomic_minf(volatile __global float* source, const float operand) {
  union {
    unsigned int intVal;
    float floatVal;
  } newVal;
  union {
    unsigned int intVal;
    float floatVal;
  } prevVal;
  do {
    prevVal.floatVal = *source;
    newVal.floatVal = min(prevVal.floatVal, operand);
  } while (atomic_cmpxchg((volatile __global unsigned int*)source, prevVal.intVal, newVal.intVal) !=
           prevVal.intVal);
}

inline void atomic_addf(volatile __global float* addr, float val) {
  union {
    unsigned int u32;
    float f32;
  } next, expected, current;
  current.f32 = *addr;
  do {
    expected.f32 = current.f32;
    next.f32 = expected.f32 + val;
    current.u32 = atomic_cmpxchg((volatile __global unsigned int*)addr, expected.u32, next.u32);
  } while (current.u32 != expected.u32);
}
