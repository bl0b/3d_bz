// Copied directly from intel realsense code!

typedef enum rs2_distortion {
  RS2_DISTORTION_NONE, /**< Rectilinear images. No distortion compensation required. */
  RS2_DISTORTION_MODIFIED_BROWN_CONRADY, /**< Equivalent to Brown-Conrady distortion, except that
                                            tangential distortion is applied to radially distorted
                                            points */
  RS2_DISTORTION_INVERSE_BROWN_CONRADY,  /**< Equivalent to Brown-Conrady distortion, except
                                            undistorts image instead of distorting it */
  RS2_DISTORTION_FTHETA,                 /**< F-Theta fish-eye distortion model */
  RS2_DISTORTION_BROWN_CONRADY,          /**< Unmodified Brown-Conrady distortion model */
  RS2_DISTORTION_KANNALA_BRANDT4,        /**< Four parameter Kannala Brandt distortion model */
  RS2_DISTORTION_COUNT /**< Number of enumeration values. Not a valid input: intended to be used in
                          for-loops. */
} rs2_distortion;

typedef struct rs2_intrinsics {
  int width;  /**< Width of the image in pixels */
  int height; /**< Height of the image in pixels */
  float ppx;  /**< Horizontal coordinate of the principal point of the image, as a pixel offset from
                 the left edge */
  float ppy;  /**< Vertical coordinate of the principal point of the image, as a pixel offset from
                 the top edge */
  float fx;   /**< Focal length of the image plane, as a multiple of pixel width */
  float fy;   /**< Focal length of the image plane, as a multiple of pixel height */
  rs2_distortion model; /**< Distortion model of the image */
  float coeffs[5];      /**< Distortion coefficients */
} rs2_intrinsics;

typedef struct rs2_extrinsics {
  float rotation[9];    /**< Column-major 3x3 rotation matrix */
  float translation[3]; /**< Three-element translation vector, in meters */
} rs2_extrinsics;

// utils!!

void rs2_pixel_to_point(rs2_intrinsics* i, const int2* c, uint d, float4* out) {

  if (d) {
    *out = (float4){
        d * (c->x - i->ppx) / i->fx,
        d * (c->y - i->ppy) / i->fy,
        d * 1.,
        1
    };

  } else {
    *out = (float4){0, 0, 0, 0};
  }
}