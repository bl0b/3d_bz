typedef struct OrthoSampler2D {
  float2 min_coords;
  float2 max_coords;
  int2 img_dims;
} OrthoSampler2D;

inline int2 get_img_coords(OrthoSampler2D s, float2 coords) {
  float2 c_norm = (coords - s.min_coords) / (s.max_coords - s.min_coords);
  if (c_norm.x < 0 || c_norm.x >= s.img_dims.x || c_norm.y < 0 || c_norm.y >= s.img_dims.y) {
    return (int2){-1, -1};
  }
  int2 c_img = (int2){(int)round(c_norm.x * s.img_dims.x), (int)round(c_norm.y * s.img_dims.y)};
  return c_img;
}

inline int get_img_idx(OrthoSampler2D s, float2 coords) {
  int2 c = get_img_coords(s, coords);
  return (c.x > -1) ? c.x + c.y * s.img_dims.x : -1;
}

// given coordinates on image, get cooresponding world positions
inline float2 get_world_coords(OrthoSampler2D s, int2 img_coords) {
  const float2 norm = (float2){1.f * img_coords.x, 1.f * img_coords.y} /
                      (float2){1.f * s.img_dims.x, 1.f * s.img_dims.y};
  const float2 dir = s.max_coords - s.min_coords;
  return s.min_coords + (dir * norm);
}