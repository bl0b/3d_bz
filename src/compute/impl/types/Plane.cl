typedef struct {
  // plane normal (same as z axis)
  float4 normal;

  // a point on the plane defining the center
  float4 center;

  // 3 axes defining plane as coordinate system
  float4 x_axis;
  float4 y_axis;
  float4 z_axis;

  // transformation matrices converting plane to/from coordinate system defined by plane
  _mat4 to_plane_coords, from_plane_coords;

  // variables in ax + by + cz + d = 0 equation defining plane
  float4 equation;

} Plane;
