typedef enum {
  EXACT_MATCH = 1,
  AVG_DIFF = 2,
} CompareMode;

__kernel void compare_generated_binary_images_to_original_depth(
    int num_per_image_thread,
    __read_only image2d_t /*TF::R16UI*/ original_obj,
    __read_only image2d_t /*TF::R16UI*/ original_plane,
    __read_only image2d_t /*TF::R16UI*/ rendered_images,
    int num_images,
    int2 rendered_max_grid,
    __global float4* rendered_row_costs) {

  const int image_thread_id = get_global_id(0);
  const int row_id = get_global_id(1);

  const int2 original_dims = get_image_dim(original_obj);

  // printf("image thread id: %i\n", image_thread_id);
  if (row_id < original_dims.y) {

    int image_id;
    for (int i = 0; (image_id = ((i * num_per_image_thread) + image_thread_id)) < num_images; i++) {

      float num_found = 0.;

      // const int image_id = (i * num_per_image_thread) + image_thread_id;
      const int image_start_x = (image_id % rendered_max_grid.x) * original_dims.x;
      // summing all values for y here!
      const int image_y = (image_id / rendered_max_grid.x) * original_dims.y + row_id;

      for (int j = 0; j < original_dims.x; j++) {

        const uint rendered_val =
            read_imageui(rendered_images, (int2){image_start_x + j, image_y}).x;
        const uint orig_val = read_imageui(original_obj, (int2){j, row_id}).x;

        const uint plane_val = read_imageui(original_plane, (int2){j, row_id}).x;

        if (rendered_val && orig_val) { num_found += 1.; }
        if (rendered_val && plane_val) { num_found -= 0.2; }
      }

      const int row_costs_idx = (image_id * original_dims.y) + row_id;
      rendered_row_costs[row_costs_idx] = (float4){num_found, 0., 0., 69.};

      /*
      if (row_costs_idx >= num_images * original_dims.y) {
        printf(
            "overflow! idx: %i, num images: %i, origy: %i, imgid: %i\n",
            row_costs_idx,
            num_images,
            original_dims.y,
            image_id);
      }
      */
    }
  }
}

__kernel void sum_row_costs(
    __global float4* row_costs,
    int num_images,
    int num_rows_per_image,
    __global float4* sum_costs) {

  int i = get_global_id(0);
  if (i < num_images) {
    const uint start_cost_idx = i * num_rows_per_image;
    float sum_cost = 0.;
    for (int j = 0; j < num_rows_per_image; j++) {
      sum_cost += row_costs[start_cost_idx + j].x;
    }
    sum_costs[i] = (float4){sum_cost, 0., 0., 69};
  }
}

__kernel void compare_depth_images(
    __read_only image2d_t /*TF::R16UI*/ img_1,
    __read_only image2d_t /*TF::R16UI*/ img_2,
    __write_only image2d_t /*TF::R16UI*/ diff_img,
    __global uint* diff_sum,
    CompareMode mode) {

  const int2 coord = (int2){get_global_id(0), get_global_id(1)};
  const int2 img_dims = get_image_dim(img_1);

  if (coord.x < img_dims.x & coord.y < img_dims.y) {

    const sampler_t in_sampler =
        CLK_NORMALIZED_COORDS_FALSE | CLK_ADDRESS_NONE | CLK_FILTER_NEAREST;

    uint img_1_val = read_imageui(img_1, in_sampler, coord).x;
    uint img_2_val = read_imageui(img_2, in_sampler, coord).x;

    const bool values_match = img_1_val == img_2_val;

    switch (mode) {
    case EXACT_MATCH:
      if (!values_match) {
        atomic_inc(diff_sum);
        write_imageui(diff_img, coord, (uint4){1, 0, 0, 0});
      } else {
        write_imageui(diff_img, coord, (uint4){0, 0, 0, 0});
      }
      break;
    case AVG_DIFF:
      if (!values_match) {
        const uint diff =
            (uint)(img_1_val > img_2_val ? img_1_val - img_2_val : img_2_val - img_1_val);
        atomic_add(diff_sum, diff);
        write_imageui(diff_img, coord, (uint4){diff, 0, 0, 0});
      } else {
        write_imageui(diff_img, coord, (uint4){0, 0, 0, 0});
      }
      break;
    }
  }
}