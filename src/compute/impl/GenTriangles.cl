// #py_include <cglm.cl>
// #py_include <OrthoSampler2D.cl>

__kernel void gen_triangles(
    __read_only image2d_t /*TF::RGBA32F*/ pts_in,
    __global uint* idxes_out,
    __global uint* num_triangles,
    int2 vtxes_dims) {

  uint2 vtx_pixel = {get_global_id(0), get_global_id(1)};

  if (vtx_pixel.x < vtxes_dims.x - 1 && vtx_pixel.y < vtxes_dims.y - 1) {

    const uint2 pixels[4] = {
        vtx_pixel, vtx_pixel + (uint2){1, 0}, vtx_pixel + (uint2){1, 1}, vtx_pixel + (uint2){0, 1}};

    const float4 positions[4] = {
        read_imagef(pts_in, (int2){pixels[0].x, pixels[0].y}),
        read_imagef(pts_in, (int2){pixels[1].x, pixels[1].y}),
        read_imagef(pts_in, (int2){pixels[2].x, pixels[2].y}),
        read_imagef(pts_in, (int2){pixels[3].x, pixels[3].y}),
    };

    if (positions[0].w > 0. && positions[1].w > 0. && positions[2].w > 0. && positions[3].w > 0.) {

      const uint idxes[4] = {
          pixels[0].x + vtxes_dims.x * pixels[0].y,
          pixels[1].x + vtxes_dims.x * pixels[1].y,
          pixels[2].x + vtxes_dims.x * pixels[2].y,
          pixels[3].x + vtxes_dims.x * pixels[3].y,
      };

      uint tri_num = atomic_inc(num_triangles);
      idxes_out[tri_num * 3] = idxes[0];
      idxes_out[tri_num * 3 + 1] = idxes[1];
      idxes_out[tri_num * 3 + 2] = idxes[2];

      tri_num = atomic_inc(num_triangles);
      idxes_out[tri_num * 3] = idxes[0];
      idxes_out[tri_num * 3 + 1] = idxes[2];
      idxes_out[tri_num * 3 + 2] = idxes[3];
    }
  }
}

__kernel void filter_by_group(
    __global uint* in_triangles,
    uint in_num_triangles,
    __global uint* out_triangles,
    __global uint* out_num_triangles,
    __global float4* vtx_positions,
    __global uint* groups,
    OrthoSampler2D groups_sampler,
    uint group_num) {

  const int in_tri_num = get_global_id(0); // threadIdx.x + blockIdx.x * blockDim.x;

  if (in_tri_num < in_num_triangles) {

    __global uint* tri = in_triangles + (in_tri_num * 3);

    float4 tri_avg_pos =
        (vtx_positions[tri[0]] + vtx_positions[tri[1]] + vtx_positions[tri[2]]) / 3.f;

    // const int group_idx = groups_sampler.get_idx(tri_avg_pos.xy());
    const int group_idx = get_img_idx(groups_sampler, (float2){tri_avg_pos.x, tri_avg_pos.y});
    const uint group = groups[group_idx];

    if (group == group_num) {
      const int new_tri_num = atomic_add(out_num_triangles, 1);
      __global uint* tri_out = out_triangles + (new_tri_num * 3);

      tri_out[0] = tri[0];
      tri_out[1] = tri[1];
      tri_out[2] = tri[2];
    }
  }
}