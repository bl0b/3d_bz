// #py_include <cglm.cl>
// #py_include <OrthoSampler2D.cl>
// #py_include <RsTypes.cl>

typedef enum {
  OUTSIDE = 1,
  // LESS_THAN,
  MORE_THAN = 2
} FilterBy;

__kernel void by_z_value(
    __read_only image2d_t /*TF::RGBA32F*/ points_in,
    __write_only image2d_t /*TF::RGBA32F*/ points_out,
    FilterBy filter_by,
    float v0,
    float v1) {

  const int2 coord = (int2){get_global_id(0), get_global_id(1)};
  const int2 img_dims = get_image_dim(points_in);

  if (coord.x < img_dims.x & coord.y < img_dims.y) {

    float4 position = read_imagef(points_in, coord);
    bool filter_out = false;
    if (position.w > 0) {
      switch (filter_by) {
      case OUTSIDE:
        // Outside: remove if point is below first value or above second value
        filter_out = position.z < v0 || position.z > v1;
        break;
      case MORE_THAN:
        filter_out = position.z > v0;
        break;
      }
    } else {
      filter_out = true;
    }

    write_imagef(points_out, coord, filter_out ? (float4){0., 0., 0., 0.} : position);
  }
}

__kernel void by_z_value_and_plane_map(
    __read_only image2d_t /*TF::RGBA32F*/ points_in,
    __write_only image2d_t /*TF::RGBA32F*/ points_out,
    __read_only image2d_t /*TF::RGBA32F*/ plane_map_in,
    __read_only image2d_t /*TF::R16UI*/ plane_map_in_binary,
    OrthoSampler2D ortho_sampler,
    float threshold) {

  const int2 coord = (int2){get_global_id(0), get_global_id(1)};
  const int2 img_dims = get_image_dim(points_in);

  if (coord.x < img_dims.x & coord.y < img_dims.y) {

    float4 position = read_imagef(points_in, coord);

    // expected plane height at that x/y
    uint on_plane =
        read_imageui(
            plane_map_in_binary, get_img_coords(ortho_sampler, (float2){position.x, position.y}))
            .x;

    bool filter_out = false;

    if (!on_plane) {
      filter_out = true;
    } else {

      float plane_z =
          read_imagef(plane_map_in, get_img_coords(ortho_sampler, (float2){position.x, position.y}))
              .z;

      float corrected_z = position.z - plane_z;
      if (corrected_z > -threshold) { filter_out = true; }
    }
    write_imagef(points_out, coord, filter_out ? (float4){0., 0., 0., 0.} : position);
  }
}

__kernel void divide_original_2(
    __read_only image2d_t /*TF::R16UI*/ img_in,
    _mat4 to_world,
    rs2_intrinsics i,
    uint minif,
    __write_only image2d_t /*TF::R16UI*/ obj_out,
    __write_only image2d_t /*TF::R16UI*/ plane_out,
    __read_only image2d_t /*TF::RGBA32F*/ plane_map_in,
    OrthoSampler2D ortho_sampler,
    float threshold) {

  const int2 c_out = (int2){get_global_id(0), get_global_id(1)};
  const int2 d_out = get_image_dim(obj_out);

  if (c_out.x < d_out.x & c_out.y < d_out.y) {

    const int2 c_in = c_out * (int)minif;
    const uint depth_in = read_imageui(img_in, c_in).x;
    if (!depth_in) return;

    const float4 cam_pos = (float4){
        depth_in * (c_in.x - i.ppx) / i.fx, depth_in * (c_in.y - i.ppy) / i.fy, depth_in, 1.};
    float4 world_pos;
    glm_mat4_mulv(&to_world, &cam_pos, &world_pos);

    const float4 plane_map_pos = read_imagef(
        plane_map_in, get_img_coords(ortho_sampler, (float2){world_pos.x, world_pos.y}));
    if (!(plane_map_pos.w > 0.f)) return;

    float plane_z = plane_map_pos.z;
    float corrected_z = world_pos.z - plane_z;
    const bool above_plane = corrected_z <= -threshold;

    above_plane ? write_imageui(obj_out, c_out, depth_in)
                : write_imageui(plane_out, c_out, depth_in);
  }
}

__kernel void divide_original_2_single(
    __read_only image2d_t /*TF::R16UI*/ img_in,
    _mat4 to_world,
    rs2_intrinsics i,
    uint minif,
    __write_only image2d_t /*TF::RG16UI*/ split_out,
    __read_only image2d_t /*TF::RGBA32F*/ plane_map_in,
    OrthoSampler2D ortho_sampler,
    float threshold) {

  const int2 c_out = (int2){get_global_id(0), get_global_id(1)};
  const int2 d_out = get_image_dim(split_out);

  if (c_out.x < d_out.x & c_out.y < d_out.y) {

    const int2 c_in = c_out * (int)minif;
    const uint depth_in = read_imageui(img_in, c_in).x;
    uint4 o = (uint4){0, 0, 0, 0};
    if (depth_in) {

      const float4 cam_pos = (float4){
          depth_in * (c_in.x - i.ppx) / i.fx, depth_in * (c_in.y - i.ppy) / i.fy, depth_in, 1.};
      float4 world_pos;
      glm_mat4_mulv(&to_world, &cam_pos, &world_pos);

      const float4 plane_map_pos = read_imagef(
          plane_map_in, get_img_coords(ortho_sampler, (float2){world_pos.x, world_pos.y}));
      if (plane_map_pos.w > 0.f) {
        float plane_z = plane_map_pos.z;
        float corrected_z = world_pos.z - plane_z;
        const bool above_plane = corrected_z <= -threshold;

        if (above_plane)
          o.x = depth_in;
        else
          o.y = depth_in;
      }
    }

    write_imageui(split_out, c_out, o);
  }
}

__kernel void divide_original(
    __read_only image2d_t /*TF::RGBA32F*/ points_in,
    _mat4 deprojected_to_camera,
    __write_only image2d_t /*TF::R16UI*/ object_points_out,
    __write_only image2d_t /*TF::R16UI*/ plane_points_out,
    __read_only image2d_t /*TF::RGBA32F*/ plane_map_in,
    OrthoSampler2D ortho_sampler,
    float threshold) {

  const int2 coord = (int2){get_global_id(0), get_global_id(1)};
  const int2 img_dims = get_image_dim(points_in);

  if (coord.x < img_dims.x & coord.y < img_dims.y) {

    const float4 p = read_imagef(points_in, coord);
    if (!(p.w > 0.f)) return;

    // expected plane height the given x/y, or 0000 if no point present!
    const float4 plane_map_pos =
        read_imagef(plane_map_in, get_img_coords(ortho_sampler, (float2){p.x, p.y}));
    if (!(plane_map_pos.w > 0.f)) return;

    float plane_z = plane_map_pos.z;
    float corrected_z = p.z - plane_z;
    bool above_plane = true;
    if (corrected_z > -threshold) { above_plane = false; }

    float4 cam_pos;
    glm_mat4_mulv(&deprojected_to_camera, &p, &cam_pos);
    const uint original_depth_value = (uint)round(cam_pos.z);

    above_plane ? write_imageui(object_points_out, coord, original_depth_value)
                : write_imageui(plane_points_out, coord, original_depth_value);
  }
}

__kernel void by_stencil(
    __read_only image2d_t /*TF::R16UI*/ img_in,
    __read_only image2d_t /*TF::R16UI*/ stencil,
    __write_only image2d_t /*TF::R16UI*/ img_out) {

  const int2 coords = (int2){get_global_id(0), get_global_id(1)};

  const int2 points_dims = get_image_dim(img_in);

  if (coords.x < points_dims.x && coords.y < points_dims.y) {
    const uint is_val = read_imageui(stencil, coords).x;
    if (is_val) {
      const uint val = read_imageui(img_in, coords).x;
      write_imageui(img_out, coords, (uint4){val, 0, 0, 0});
    }
  }
}

__kernel void by_plane_stencil(
    __read_only image2d_t /*TF::RGBA32F*/ points_in,
    __write_only image2d_t /*TF::RGBA32F*/ points_out,
    __read_only image2d_t /*TF::R16UI*/ stencil,
    OrthoSampler2D stencil_sampler) {

  const int x = get_global_id(0);
  const int y = get_global_id(1);

  const int2 points_dims = get_image_dim(points_in);
  const int2 stencil_dims = get_image_dim(stencil);

  if (x < points_dims.x && y < points_dims.y) {
    float4 position = read_imagef(points_in, (int2){x, y});
    int2 stencil_coords = get_img_coords(stencil_sampler, (float2){position.x, position.y});
    uint v = read_imageui(stencil, stencil_coords).x;
    write_imagef(points_out, (int2){x, y}, v ? position : (float4){0., 0., 0., 0.});
  }
};