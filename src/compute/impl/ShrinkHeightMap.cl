// #py_include <cglm.cl>

__kernel void shrink_height_map(
    __read_only image2d_t /*TF::R16UI*/ orig,
    __read_only image2d_t /*TF::R16UI*/ _in,
    __write_only image2d_t /*TF::R16UI*/ _out,
    int shrink) {

  const int x = get_global_id(0);
  const int y = get_global_id(1);

  const int2 dims = get_image_dim(_out);

  if (x < dims.x && y < dims.y) {

    uint in_val = read_imageui(_in, (int2){x, y}).x;
    const uint orig_in_val = in_val;

    uint out_val = orig_in_val;

    // clang-format off
      const int2 neighbor_dirs[8] = {
        {-1, -1}, { 0, -1}, {1, -1},
        {-1,  0},           {1,  0},
        {-1,  1}, { 0,  1}, {1,  1}
      };
    // clang-format on

    int num_neighbors = 0;
    for (int i = 0; i < 8; i++) {

      in_val = read_imageui(_in, (int2){x, y} + neighbor_dirs[i]).x;
      if (in_val) { num_neighbors++; }
    }

    if (shrink && orig_in_val && num_neighbors < 5) {
      out_val = 0;
    } else if (!shrink && !orig_in_val && num_neighbors > 2) {

      in_val = read_imageui(orig, (int2){x, y}).x;
      // only can set out_val to 1 if original image was populated there
      if (in_val) { out_val = 1; }
    }

    write_imageui(_out, (int2){x, y}, (uint4){out_val, 0, 0, 0});
  }
}
