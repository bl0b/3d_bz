// #py_include <cglm.cl>
// #py_include <OrthoSampler2D.cl>

typedef struct PlaneFit {
  float4 center;
  float4 normal;
  float percent_sampled;
  float sum_error;

  float padding[6];
} PlaneFit;

__kernel void fit_regions_to_planes(
    __local float4* local_positions,
    __global PlaneFit* planes,
    __read_only image2d_t /*TF::RGBA32F*/ positions) {

  const int x_start = get_local_id(0) + get_group_id(0) * get_local_size(0);
  const int y_start =
      ((get_local_id(1) * get_local_size(0)) / 2) + (get_group_id(1) * get_local_size(0));

  const int shared_idx = get_local_id(1) + (get_local_id(0) * 2);

  const int2 positions_dims = get_image_dim(positions);

  if (x_start < positions_dims.x && y_start < positions_dims.y) {
    float4 row_pos_sum = (float4){0., 0., 0., 0.};
    float4 in_pos;

    for (int y_temp = 0; y_temp < (get_local_size(0) / 2); y_temp++) {
      const int y = y_start + y_temp;
      if (y < positions_dims.y) {
        const int x = x_start;
        float4 p = read_imagef(positions, (int2){x, y});
        row_pos_sum = row_pos_sum + p;
      }
    }

    local_positions[shared_idx] = row_pos_sum;
  }

  barrier(CLK_LOCAL_MEM_FENCE);

  // 4 threads per group to combine the half-rows in each quadrant
  float4 temp_avg = (float4){0., 0., 0., 0.};
  if (shared_idx < 4 && x_start < positions_dims.x && y_start < positions_dims.y) {

    const int quad_x = shared_idx % 2 == 0 ? 0 : 1;
    const int quad_y = shared_idx < 2 ? 0 : 1;
    const int half_dim = get_local_size(0) / 2;

    for (int temp_x = 0; temp_x < half_dim; temp_x++) {
      const int shared_temp_x = temp_x + (quad_x * half_dim);
      const int shared_temp_y = quad_y;
      const int shared_temp_idx = shared_temp_y + (shared_temp_x * 2);

      temp_avg = temp_avg + local_positions[shared_temp_idx];
    }
  }

  barrier(CLK_LOCAL_MEM_FENCE);

  if (shared_idx < 4 && x_start < positions_dims.x && y_start < positions_dims.y) {
    local_positions[shared_idx] = temp_avg;
  }

  barrier(CLK_LOCAL_MEM_FENCE);

  const int thread_idx = get_group_id(0) + get_group_id(1) * get_num_groups(0);

  // combine the 4 quadrants
  if (shared_idx == 0 && x_start < positions_dims.x && y_start < positions_dims.y) {

    float4 pos = (float4){0., 0., 0., 0.};

    for (int i = 0; i < 4; i++) {
      float4 local_pos = local_positions[i];
      glm_vec4_add(&pos, &local_pos, &pos);
    }

    const bool has_els = pos.w > 0;

    float4 avg_pos = (float4){0, 0, 0, 0};
    if (has_els) {
      // average first 3 elements
      avg_pos.x = pos.x / pos.w;
      avg_pos.y = pos.y / pos.w;
      avg_pos.z = pos.z / pos.w;
      // w element 1 if any elements found else 0
      avg_pos.w = 1.;
    }

    planes[thread_idx].center = avg_pos;

    // quick way to generate normal:

    float4 l_pos_0 = local_positions[0];
    float4 l_pos_1 = local_positions[1];
    float4 l_pos_2 = local_positions[2];

    float4 l_pos_01 = l_pos_0 - l_pos_1;
    float4 l_pos_02 = l_pos_0 - l_pos_2;

    float4 crossed;
    // cross and normalize!
    glm_vec3_crossn((vec3)&l_pos_01, (vec3)&l_pos_02, (vec3)&crossed);

    if (crossed.z < 0) { crossed = crossed * -1; }
    crossed.w = 1.;

    planes[thread_idx].normal = crossed;

    planes[thread_idx].percent_sampled = pos.w / (get_local_size(0) * get_local_size(0));
  }

  barrier(CLK_LOCAL_MEM_FENCE);

  if (x_start < positions_dims.x && y_start < positions_dims.y) {

    PlaneFit fit = planes[thread_idx];

    const float A = fit.normal.x;
    const float B = fit.normal.y;
    const float C = fit.normal.z;
    const float D = -glm_vec3_dot((vec3)&fit.normal, (vec3)&fit.center);

    float dist_sum = 0.;

    for (int y_temp = 0; y_temp < (get_local_size(0) / 2); y_temp++) {
      const int y = y_start + y_temp;
      if (y < positions_dims.y) {

        const int x = x_start;
        float4 in_pos = read_imagef(positions, (int2){x, y});

        if (in_pos.w > 0.) {

          float4 z_axis = (float4){fit.normal.x, fit.normal.y, fit.normal.z, 0.};
          float4 x_axis_temp =
              (z_axis.z == 1.f) ? (float4){0., -1., 0., 0.} : (float4){0., 0., 1., 0.};
          float4 x_axis;
          glm_vec3_crossn((vec3)&z_axis, (vec3)&x_axis_temp, (vec3)&x_axis);
          float4 y_axis;
          glm_vec3_crossn((vec3)&z_axis, (vec3)&x_axis, (vec3)&y_axis);

          _mat4 tform_orientation = {{
              x_axis,
              y_axis,
              z_axis,
              (float4){0., 0., 0., 1},
          }};
          glm_mat4_transpose(&tform_orientation);

          float4 center_reverse = fit.center * -1.f;

          _mat4 tform_translate;
          glm_translate_make(&tform_translate, (vec3)&center_reverse);

          // combine translation and rotation matrixes for single matrix
          // to transform between camera & plane coordinates
          _mat4 to_plane_coords;
          glm_mat4_mul(&tform_orientation, &tform_translate, &to_plane_coords);

          float4 new_pos;
          glm_mat4_mulv(&to_plane_coords, &in_pos, &new_pos);

          dist_sum += fabs(new_pos.z);
        }
      }
    }

    // sum of distances!
    local_positions[shared_idx].x = dist_sum;
  }

  barrier(CLK_LOCAL_MEM_FENCE);

  // 4 threads per group to combine the half-rows in each quadrant
  float temp_dist_sum = 0.;
  if (shared_idx < 4 && x_start < positions_dims.x && y_start < positions_dims.y) {

    const int quad_x = shared_idx % 2 == 0 ? 0 : 1;
    const int quad_y = shared_idx < 2 ? 0 : 1;
    const int half_dim = get_local_size(0) / 2;

    for (int temp_x = 0; temp_x < half_dim; temp_x++) {
      const int shared_temp_x = temp_x + (quad_x * half_dim);
      const int shared_temp_y = quad_y;
      const int shared_temp_idx = shared_temp_y + (shared_temp_x * 2);

      temp_dist_sum += local_positions[shared_temp_idx].x;
    }
  }

  barrier(CLK_LOCAL_MEM_FENCE);

  if (shared_idx < 4 && x_start < positions_dims.x && y_start < positions_dims.y) {
    local_positions[shared_idx].x = temp_dist_sum;
  }

  barrier(CLK_LOCAL_MEM_FENCE);

  // combine the 4 quadrants to get sum of error between plane and points
  if (shared_idx == 0 && x_start < positions_dims.x && y_start < positions_dims.y) {

    float dist = 0.;
    for (int i = 0; i < 4; i++) {
      dist += local_positions[i].x;
    }

    planes[thread_idx].sum_error =
        dist / (planes[thread_idx].percent_sampled * get_local_size(0) * get_local_size(0));
  }
}

__kernel void to_vtx_colors(
    __global float4* colors,
    __global float4* positions,
    _mat4 to_surface,
    int2 vtx_grid_dims,
    float plane_thresh) {

  const int x = get_global_id(0);
  const int y = get_global_id(1);

  if (x < vtx_grid_dims.x && y < vtx_grid_dims.y) {

    const int idx = x + y * vtx_grid_dims.x;

    float4 cam_position = positions[idx];
    float4 plane_position;
    glm_mat4_mulv(&to_surface, &cam_position, &plane_position);

    const bool is_above = plane_position.z < -plane_thresh;

    colors[idx] = is_above ? (float4){0.2, fabs(plane_position.z) / 1000, 1., 1.}
                           : (float4){0.1, 0.2, 0.3, 1.};
  }
}

__kernel void to_vtx_colors_2(
    __global float4* colors,
    __global float4* positions,
    _mat4 to_surface,
    int2 vtx_grid_dims,
    float plane_thresh,
    __read_only image2d_t /*TF::RGBA32F*/ plane_map,
    __read_only image2d_t /*TF::R16UI*/ plane_int_map,
    OrthoSampler2D plane_sampler) {

  const int x = get_global_id(0);
  const int y = get_global_id(1);

  if (x < vtx_grid_dims.x && y < vtx_grid_dims.y) {

    const int idx = x + y * vtx_grid_dims.x;

    float4 cam_position = positions[idx];
    float4 plane_position;
    glm_mat4_mulv(&to_surface, &cam_position, &plane_position);

    uint plane_info_2 =
        read_imageui(
            plane_int_map,
            get_img_coords(plane_sampler, (float2){plane_position.x, plane_position.y}))
            .x;

    if (plane_info_2 > 0) {

      // part of the plane..
      float4 plane_info = read_imagef(
          plane_map, get_img_coords(plane_sampler, (float2){plane_position.x, plane_position.y}));

      // plane_map shows estimated height of plane for given x/y coordinate.
      // this will be more relevant the closer the point is to the plane ??
      float corrected_z_pos = plane_position.z - plane_info.z;

      if (corrected_z_pos < -plane_thresh || corrected_z_pos > plane_thresh) {
        colors[idx] = (float4){0.8, 0.2, 0.0, 1.0};
      } else {
        colors[idx] = (float4){0.4, 0.0, 0.0, 1.0};
      }

    } else {

      // out of bounds!
      colors[idx] = (float4){0.0, 0.4, 0.0, 1.0};
    }
  }
}