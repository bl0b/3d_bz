#pragma once

#include <engine/Engine.h>

#ifdef __NVCC__
#define STATIC_FN_DEC static __host__ __device__
#define FN_DEC __host__ __device__
#else
#define STATIC_FN_DEC static
#define FN_DEC
#endif

#include "Plane.hpp"
