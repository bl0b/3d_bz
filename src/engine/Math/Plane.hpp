#pragma once

#include <engine/Engine.h>

namespace Engine {
namespace Math {

class Plane {
public:
  // plane normal (same as z axis)
  glm::vec3 normal;
  float _padding0[1];

  // a point on the plane defining the center
  glm::vec3 center;
  float _padding1[1];

  // 3 axes defining plane as coordinate system
  glm::vec3 x_axis;
  float _padding2[1];

  glm::vec3 y_axis;
  float _padding3[1];

  glm::vec3 z_axis;
  float _padding4[1];

  // transformation matrices converting plane to/from coordinate system defined by plane
  glm::mat4 to_plane_coords, from_plane_coords;

  // variables in ax + by + cz + d = 0 equation defining plane
  glm::vec4 equation;

  FN_DEC Plane() {}

  FN_DEC Plane(const glm::vec3& n, const glm::vec3& p) { generate(n, p); }

  FN_DEC Plane(const glm::vec3 p1, const glm::vec3 p2, const glm::vec3 p3) {
    // generate just from 3 points!
    generate(p1, p2, p3);
  }

  FN_DEC void generate(const glm::vec3 p1, const glm::vec3 p2, const glm::vec3 p3) {
    glm::vec3 n = normalize(cross(p2 - p1, p3 - p1));
    generate(n, p1);
  }

  FN_DEC void generate(const glm::vec3 n, const glm::vec3 p) {
    normal = z_axis = glm::normalize(n);
    center = p;
    equation = plane_equation(normal, center);

    z_axis = normal;
    // pick relatively arbitrary axis - to cross with normal (either camera z, or if normal is
    // exactly camera z, then camera y)
    x_axis = glm::normalize(
        glm::cross(z_axis, z_axis.z == 1.f ? glm::vec3{0., -1., 0.} : glm::vec3{0., 0., 1.}));
    y_axis = glm::normalize(glm::cross(z_axis, x_axis));

    const glm::mat4 orientation_transform = glm::mat4{
        glm::vec4{x_axis, 0}, glm::vec4{y_axis, 0}, glm::vec4{z_axis, 0}, glm::vec4{0, 0, 0, 1}};

    to_plane_coords = glm::transpose(orientation_transform) * glm::translate(-center.xyz());
    from_plane_coords = glm::inverse(to_plane_coords);
    // from_plane_coords = glm::translate(center.xyz()) * orientation_transform;
  }

  /**

  Determines point of intersection between the plane and a line defined by a starting point and
  a direction.

  */
  FN_DEC glm::vec4 intersection_with_line(const glm::vec3& ray_dir, const glm::vec3& ray_origin) {

    // bug in GLM means we may have to call intersectRayPlane twice with opposite normals
    // TODO: file this against GLM!
    float t;
    if (!glm::intersectRayPlane(ray_origin, ray_dir, center, normal, t) &&
        !glm::intersectRayPlane(ray_origin, ray_dir, center, -normal, t)) {
      // w value is 0 if failed to find plane - ray is parallel to plane, either on plane so
      // infinite solutions, or off plane, so no solutions
      return {0, 0, 0, 0};
    }

    return {ray_origin + (ray_dir * t), 1.};
  }

  FN_DEC float distance_from_point(const glm::vec3& p) {

    const glm::vec4 eq = plane_equation(normal, center);

    const float dist = abs((p.x * eq.x) + (p.y * eq.y) + (p.z * eq.z) + eq.w) / length(eq.xyz());

    return dist;
  }

  /**

  Creates a plane equation from a (normalized) normal and a point.
  The elements of the returned vector correspond to the variables a, b, c, & d in the equation:

  ax + by + cz + d = 0

  */
  STATIC_FN_DEC glm::vec4 plane_equation(const glm::vec3& normal, const glm::vec3& point) {
    return glm::vec4{normal, -glm::dot(normal, point)};
  }
};

} // namespace Math
} // namespace Engine
