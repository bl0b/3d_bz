#pragma once

namespace Engine {
namespace Util {
// non-optimized syntax candy for Stl types
namespace Stl {

// Map by fn(T0)
template <typename T0, typename T1>
static vector<T1> v_map(vector<T0>& in, function<T1(T0&)> fn) {
  vector<T1> out;
  for (auto& v : in) out.emplace_back(fn(v));
  return std::move(out);
}

// Map by fn(T0, idx)
template <typename T0, typename T1>
static vector<T1> v_map(vector<T0>& in, function<T1(T0&, int)> fn) {
  vector<T1> out;
  for (int i = 0; i < in.size(); i++) out.emplace_back(fn(in[i], i));
  return std::move(out);
}

// Initialize by fn(idx)
template <typename T>
vector<T> v_init(int num, function<T(int)> fn) {
  vector<T> v;
  for (int i = 0; i < num; i++) v.emplace_back(fn(i));
  return std::move(v);
}

template <typename T>
void v_each(vector<T>& v, function<void(T)> fn) {
  for (auto& t : v) fn(t);
}

static void n_iter(int num, function<void(int)> fn) {
  for (int i = 0; i < num; i++) { fn(i); }
}

} // namespace Stl
} // namespace Util
} // namespace Engine