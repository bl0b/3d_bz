#pragma once

#include <engine/Engine.h>

#include <chrono>
#include <string>
#include <functional>

namespace Engine {
namespace Util {

/*

CPU timer for performance profiling

*/
class Timer {

public:
  void record(std::string s) {
    event_times.push_back(std::chrono::high_resolution_clock::now());
    event_names.push_back(s);
    current++;
  }

  void clear() {
    event_times.clear();
    event_names.clear();
    current = 0;
  }

  void render(std::function<void(std::string, float)> fn) {
    for (int i = 0; i < current - 1; i++) {
      const float ms = elapsed(i, i + 1);
      fn(event_names[i], ms);
    }
  }

  void render(std::string fmt) {
    for (int i = 0; i < current - 1; i++) {
      const float ms = elapsed(i, i + 1);
      printf(fmt.c_str(), event_names[i].c_str(), ms);
    }
  }

private:
  int current = 0;
  std::vector<std::chrono::high_resolution_clock::time_point> event_times;
  std::vector<std::string> event_names;

  float elapsed(const int e1, const int e2) {
    return std::chrono::duration_cast<std::chrono::microseconds>(event_times[e2] - event_times[e1])
               .count() /
           1000.f;
  }
};

} // namespace Util
} // namespace Engine