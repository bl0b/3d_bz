#pragma once

/*

#include <Shlobj.h>
#include <windows.h>

 */

#include <engine/Engine.h>

#include <fstream>
#include <iostream>
#include <sstream>
#include <string>

#include <sys/stat.h>

using namespace std;

class FileUtil {
public:
  static void init(int argc, char** argv) {

    // strip executable name from first argument (name of exe file), for executable directory
    if (argc) {
      string exe(argv[0]);
      exe_dir = exe.substr(0, exe.find_last_of("\\/")) + "/";
    }

#ifdef DIST_BUILD
    // if building the distributable build, store config in users appdata folder
    cfg_dir = get_app_data_dir();
#else
    // else just store config in binary directory
    cfg_dir = exe_dir;
#endif
  }

  /* Loads a text file */
  static string load_text(const string file_name) {
    ifstream is = get_input_stream(file_name);
    stringstream stream;
    stream << is.rdbuf();
    is.close();
    return stream.str();
  }

  /* Saves a text file */
  static void save_text(const string file_name, const string text) {
    ofstream os = get_output_stream(file_name);
    os.write(text.c_str(), text.size());
    os.close();
    return;
  }

  /* Loads a file that was serialized by cereal */
  template <class T>
  static T load(const string file_name) {
    ifstream is = get_input_stream(file_name);
    cereal::BinaryInputArchive in_archive(is);

    T data;
    in_archive(data);
    return move(data);
  }

  /* Uses cereal to save a serialized object to file */
  template <class T>
  static void save(const string file_name, T obj) {
    ofstream os = get_output_stream(file_name);
    cereal::BinaryOutputArchive archive(os);
    archive(obj);
  }

  static void save_png(const string file_name, vector<unsigned char>& tex, const glm::ivec2 dims) {
    const string full_file_name = full_path(file_name);
    unsigned err = lodepng::encode(full_file_name, tex, dims.x, dims.y);
    if (err) {
      // should throw here?
      printf("failed to write output file %s\n", full_file_name.c_str());
    }
  }

  static string exe_dir;
  static string cfg_dir;

  static string get_app_data_dir() {

    printf("Calling missing function!");

    /*


    PWSTR szPath = nullptr;
    std::string app_data;
    if (SUCCEEDED(SHGetKnownFolderPath(FOLDERID_RoamingAppData, NULL, NULL, &szPath))) {
      std::wstring app_data_w = std::wstring((wchar_t*)szPath);
      app_data.assign(app_data_w.begin(), app_data_w.end());
    }
    CoTaskMemFree(szPath);

    app_data += "\\3d-beats\\";

    if (CreateDirectory(app_data.c_str(), NULL) || ERROR_ALREADY_EXISTS == GetLastError()) {
    } else {
      printf("Failed to access AppData directory\n");
      return "";
    }

    return app_data;

    */
  }

  /* Returns full path used to look up a file, given a suffix */
  static string full_path(const string file_name) { return file_name; }

  struct FileUtilException {

    const std::string msg;
    FileUtilException(const std::string _msg) : msg(_msg) {}

    const string what() const throw() { return msg; }
  };

private:
  static ifstream get_input_stream(const string file_name) {
    const string full = full_path(file_name);
    std::ifstream is;
    is.exceptions(ifstream::failbit | ifstream::badbit);

    try {
      is.open(full, std::ios::binary);
      return move(is);
    } catch (std::ios_base::failure e) {
      printf("Could not open file %s for reading: %s\n", full.c_str(), e.what());
      throw FileUtilException("Could not open file " + full + " for reading");
    }
  }

  static ofstream get_output_stream(const string file_name) {
    const string full = full_path(file_name);
    std::ofstream os;
    os.exceptions(ifstream::failbit | ifstream::badbit);

    try {
      os.open(full, std::ios::binary);
      return os;
    } catch (std::ios_base::failure e) {
      throw FileUtilException("Could not open file " + full + " for writing");
    }
  }
};
