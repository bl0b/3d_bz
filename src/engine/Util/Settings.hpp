#pragma once

//#include <Shlobj.h>
//#include <windows.h>

#include <engine/Engine.h>

#include <fstream>
#include <iostream>
#include <sstream>
#include <string>

#include <sys/stat.h>

using namespace std;

class Settings {
public:
  float get_f(string key, float default_val) {
    if (!d.count(key)) { return default_val; }
    string s_val = d[key];
    return std::stof(s_val);
  }
  int get_i(string key, int default_val) {
    if (!d.count(key)) { return default_val; }
    string s_val = d[key];
    return std::stoi(s_val);
  }
  string get_s(string key, string default_val) {
    if (!d.count(key)) { return default_val; }
    string s_val = d[key];
    return s_val;
  }

  void set_f(string key, float f) { d[key] = std::to_string(f); }
  void set_i(string key, int i) { d[key] = std::to_string(i); }
  void set_s(string key, string s) { d[key] = s; }

  string to_json() {

    stringstream json_stream;
    {
      // wrap serialization in own scope, so destructor will close final json bracket
      cereal::JSONOutputArchive archive(json_stream);
      archive(cereal::make_nvp("settings", d));
    }

    return json_stream.str();
  }

  void load_json(string from_json) {
    std::istringstream from_json_stream(from_json);
    cereal::JSONInputArchive archive(from_json_stream);
    archive(d);
  }

private:
  unordered_map<string, string> d;
};
