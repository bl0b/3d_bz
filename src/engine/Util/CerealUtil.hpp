#pragma once

/* Conversion functions for cereal serialization */
namespace glm {
template <class Archive>
void serialize(Archive& a, vec4& m) {
  a(m.x, m.y, m.z, m.w);
}
} // namespace glm
