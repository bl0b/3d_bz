// Provides default values for externs/static variables declared in Engine package
// This needs to be included exactly once in any executable compilation unit.
#include <engine/Engine.h>

ComputeCtx Engine::compute_ctx = ComputeCtx();

std::string FileUtil::exe_dir = "";
std::string FileUtil::cfg_dir = "";
