#pragma once

#include <engine/Engine.h>

/*
Switch between multiple different applications using AppViewer. Handles OpenGL / CUDA context
creation and
*/
class AppViewer {
public:
  /*
  Register an app with the AppViewer by passing the name and a factory function to create a new
  app.
  */
  void register_app(string name, function<AbstractApp*(Engine::Gpu::GraphicsCtx&)> app_factory) {
    apps.push_back({name, app_factory});
  }

  /*
  Assign window configuration
  */
  void set_window_config(const Engine::Gpu::GraphicsCtx::Cfg* w) { cfg = w; }

  int current_app = 0;

  Engine::Gpu::GraphicsCtx* w;

  // bool show_performance_window = false;
  // bool show_debug_window = false;

  void run() {

    w = new Engine::Gpu::GraphicsCtx(*cfg);
    Engine::ImGuiCtx::init(w);

    ms_per_frame.resize(NUM_TIMER_FRAMES);

    // Initialize first app, must have at least one app registered!
    switch_to_app(0);

    last_frame_time = chrono::high_resolution_clock::now();

    while (!w->should_close()) {

      try {

        // Render loop!
        w->poll_events();
        w->set_as_render_target();

        // Default to black clear color
        GL::Renderer::setClearColor(0x00000000_rgbaf);
        GL::defaultFramebuffer.clear(GL::FramebufferClear::Color | GL::FramebufferClear::Depth);

        Engine::ImGuiCtx::new_frame();

        draw_imgui();

        // Perform per-frame work
        active_app->tick();

        // All ImGui actually gets rendered after everything else has been drawn by the app
        w->set_as_render_target();
        Engine::ImGuiCtx::draw();

        w->swap();

        record_frame_time();

      } catch (exception e) {

        printf(e.what());
        throw e;
      }
    }

    delete active_app;
  }

private:
  chrono::high_resolution_clock::time_point last_frame_time;

  int current_timer_frame = -1;
  int next_timer_frame = 0;
  const int NUM_FRAMES_TO_AVG = 10;
  const int NUM_TIMER_FRAMES = 60;
  vector<float> ms_per_frame;
  vector<float> framerate;

  void record_frame_time() {
    auto now = chrono::high_resolution_clock::now();
    const float ms_elapsed =
        chrono::duration_cast<chrono::microseconds>(now - last_frame_time).count() / 1000.f;
    last_frame_time = now;

    if (ms_per_frame.size() <= next_timer_frame) {
      ms_per_frame.push_back(ms_elapsed);
    } else {
      ms_per_frame[next_timer_frame] = ms_elapsed;
    }

    float sum = 0.;
    for (int i = 0; i < NUM_FRAMES_TO_AVG; i++) {
      const int ix = (next_timer_frame + NUM_TIMER_FRAMES - i) % NUM_TIMER_FRAMES;
      sum += ms_per_frame[ix];
      // sum += ms_per_frame[i];
    }
    const float avg_ms_per_frame = sum / NUM_FRAMES_TO_AVG;
    const float avg_framerate = 1000 / avg_ms_per_frame;

    if (framerate.size() <= next_timer_frame) {
      framerate.push_back(avg_framerate);
    } else {
      framerate[next_timer_frame] = avg_framerate;
    }

    current_timer_frame = next_timer_frame;
    next_timer_frame = (next_timer_frame + 1) % NUM_TIMER_FRAMES;
  }

  void draw_imgui() {

    if (active_app->show_debug_window) { active_app->draw_debug_gui(); }

    if (active_app->show_performance_window) {

      ImGui::SetNextWindowSize({185, 141});
      ImGui::SetNextWindowPos({(w->w - (185 + 16)) * 1.f, (w->h - (141 + 16)) * 1.f});

      ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, {2, 2});

      ImGui::Begin(
          "Framerate",
          NULL,
          ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoResize);

      if (current_timer_frame > -1) {

        if (framerate.size() == NUM_TIMER_FRAMES) {
          // generate non-ring-buffered arrays for plotting... todo: make real ring buffer or
          // somehow work around this
          vector<float> fps_norm;
          vector<float> ms_per_frame_norm;
          for (int i = 0; i < framerate.size(); i++) {
            fps_norm.push_back(framerate[(i + current_timer_frame) % framerate.size()]);
            ms_per_frame_norm.push_back(ms_per_frame[(i + current_timer_frame) % framerate.size()]);
          }
          ImGui::Text("FPS: %3.1f", framerate[current_timer_frame]);
          ImGui::PlotLines(
              "##avg_fps", fps_norm.data(), fps_norm.size(), 0, NULL, 0, 100, ImVec2{180, 40});
          ImGui::Text("ms/frame: %3.1f", ms_per_frame[current_timer_frame]);
          ImGui::PlotLines(
              "##ms_frame",
              ms_per_frame_norm.data(),
              ms_per_frame_norm.size(),
              0,
              NULL,
              0,
              40,
              ImVec2{180, 40});
        }
      }

      ImGui::End();

      ImGui::PopStyleVar();
    }

    if (apps.size() > 1) {

      if (ImGui::BeginMainMenuBar()) {
        if (ImGui::BeginMenu("File")) {

          ImGui::MenuItem("App Browser", NULL, false, false);
          ImGui::Separator();

          // const string current_app_name = active_app->name();
          const string current_app_name = apps[current_app].name;
          const string current_app_text = "Current: " + current_app_name;
          ImGui::MenuItem(current_app_text.c_str(), NULL, false, false);

          if (ImGui::BeginMenu("Switch...")) {
            for (int i = 0; i < apps.size(); i++) {
              const bool is_enabled = i != current_app;
              if (ImGui::MenuItem(apps[i].name.c_str(), NULL, false, is_enabled)) {
                switch_to_app(i);
              }
            }
            ImGui::EndMenu();
          }

          ImGui::Separator();

          if (ImGui::MenuItem("Exit")) { w->set_should_close(true); }
          ImGui::EndMenu();
        }

        if (ImGui::BeginMenu("View")) {
          ImGui::Checkbox("Performance info", &active_app->show_performance_window);
          ImGui::Checkbox("Debug info", &active_app->show_debug_window);

          ImGui::EndMenu();
        }
        ImGui::EndMainMenuBar();
      }
    }
  }

  /*
  Destroys existing app, if it exists, and loads a new app. Initialization process causes screen to
  freeze temporarily as GPU memory is allocated/deallocated, etc.
  */
  void switch_to_app(const int app_id) {

    w->clear_mouse_and_keyboard_callbacks();
    // call destructor on currently active app
    if (active_app != nullptr) { delete active_app; }

    // initialize new app
    current_app = app_id;
    active_app = apps[app_id].factory(*w);
  }

  struct AppInfo {
    string name;
    function<AbstractApp*(Engine::Gpu::GraphicsCtx&)> factory;
  };

  AbstractApp* active_app = nullptr;
  std::vector<AppInfo> apps;

  const Engine::Gpu::GraphicsCtx::Cfg* cfg = nullptr;
};
