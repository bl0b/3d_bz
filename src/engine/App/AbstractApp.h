#pragma once

#include <engine/Engine.h>

class AbstractApp {
public:
  // w for window
  Engine::Gpu::GraphicsCtx& w;

  explicit AbstractApp(Engine::Gpu::GraphicsCtx& _w) : w(_w) {}

  virtual void tick() = 0;

  virtual void draw_debug_gui() {}

  bool show_performance_window = true;
  bool show_debug_window = true;

  // Ensure child destructors get called too. Either default or custom destructors can be used
  virtual ~AbstractApp(){};

protected:
};
