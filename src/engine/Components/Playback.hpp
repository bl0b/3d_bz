#pragma once

namespace Engine {
namespace Components {

class Playback {
private:
  int frame = 0, num_frames;

  int playing = 0;
  int ticks_per_new_frame = 1;
  int current_tick = 0;

  bool zigzagging = false;
  int zigzag_slowdown;
  int zigzag_min, zigzag_max;

public:

  void zigzag_loop(int min, int max) {
    zigzagging = true;
    zigzag_slowdown = 2;
    zigzag_min = min;
    zigzag_max = max;
    current_tick = 0;
    frame = min;
  }

  void stop_zigzag() {
    zigzagging = false;
  }

  void set_num_frames(int n) {
    num_frames = n;
    if (frame >= num_frames) frame = num_frames - 1;
  }

  void set_current_frame(int n) { frame = n; }

  int get_current_frame() { return frame; }

  void draw_tick(bool own_window = false) {
    if (own_window) ImGui::Begin("Playback");

    ImGui::Text("Playback:");

    ImGui::SliderInt("current", &frame, 0, num_frames - 1);
    if (!playing) {
      if (ImGui::Button("<<")) {
        zigzagging = false;
        playing = -1;
        ticks_per_new_frame = 1;
        current_tick = 0;
      }
      ImGui::SameLine();
      if (ImGui::Button("<")) {
        zigzagging = false;
        playing = -1;
        ticks_per_new_frame = 5;
        current_tick = 0;
      }
      ImGui::SameLine();
      if (ImGui::Button(">")) {
        zigzagging = false;
        playing = 1;
        ticks_per_new_frame = 5;
        current_tick = 0;
      }
      ImGui::SameLine();
      if (ImGui::Button(">>")) {
        zigzagging = false;
        playing = 1;
        ticks_per_new_frame = 1;
        current_tick = 0;
      }
      if (ImGui::Button("prev")) {
        zigzagging = false;
        frame--;
        clamp_frame();
      }
      ImGui::SameLine();
      if (ImGui::Button("next")) {
        zigzagging = false;
        frame++;
        clamp_frame();
      }
    } else {
      if (ImGui::Button("[]")) {
        playing = 0;
        zigzagging = false;
      }
    }



    ImGui::Text("--");

    if (own_window) ImGui::End();


    if (zigzagging) {

      int range = (zigzag_max - zigzag_min) + 1;
      int modded = (current_tick / zigzag_slowdown) % (range * 2);
      if (modded < range) {
        frame = zigzag_min + modded;
      }
      else {
        modded = modded - range;
        frame = zigzag_max - (modded);
      }

      current_tick++;

    } else if (playing != 0) {
      current_tick++;
      if (current_tick % ticks_per_new_frame == 0) {
        frame += playing;
        if (clamp_frame()) {
          playing = false;
          current_tick = 0;
        };
      }
    }
  }

  bool clamp_frame() {
    if (frame >= num_frames) {
      frame = num_frames - 1;
      return true;
    } else if (frame < 0) {
      frame = 0;
      return true;
    }
    return false;
  }
};

} // namespace Components
} // namespace Engine
