#pragma once

// glm deps
#define GLM_FORCE_SWIZZLE
#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtx/intersect.hpp>
#include <glm/gtx/norm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/string_cast.hpp>
#include <glm/gtx/hash.hpp>

// Include all magnum dependencies here. These *should* be per-file for better
// compile times, but it's such a pain in the ass to stay out of dependency hell
// that way.
#include <Corrade/Containers/ArrayView.h>
#include <Corrade/Containers/Reference.h>

#include <Magnum/Magnum.h>
#include <Magnum/Math/Math.h>
#include <Magnum/Math/Vector.h>
#include <Magnum/Mesh.h>
#include <Magnum/ImageView.h>

#include <Magnum/GL/GL.h>
#include <Magnum/GL/Shader.h>
#include <Magnum/GL/Framebuffer.h>
#include <Magnum/GL/Renderbuffer.h>
#include <Magnum/GL/RenderbufferFormat.h>
#include <Magnum/GL/DefaultFramebuffer.h>
#include <Magnum/GL/Buffer.h>
#include <Magnum/GL/Texture.h>
#include <Magnum/GL/Renderer.h>
#include <Magnum/GL/TextureFormat.h>
#include <Magnum/GL/BufferImage.h>
#include <Magnum/GL/ImageFormat.h>
#include <Magnum/GL/Framebuffer.h>
#include <Magnum/GL/Version.h>
#include <Magnum/GL/AbstractShaderProgram.h>
#include <Magnum/Platform/GLContext.h>

#include <Magnum/Math/Color.h>
#include <Magnum/Math/Matrix.h>
#include <Magnum/Math/Matrix4.h>
#include <Magnum/GlmIntegration/Integration.h>

using namespace Magnum::Math::Literals;

// load GLFW and openGL functions. This must come after Magnum is included
#include <GLFW/glfw3.h>

// imgui!
#include <imgui.h>
#include <imgui_internal.h>
#include <imgui_impl_glfw.h>
#include <imgui_impl_opengl3.h>

// json!
#include <cereal/types/unordered_map.hpp>
#include <cereal/types/memory.hpp>
#include <cereal/types/string.hpp>
#include <cereal/types/vector.hpp>
#include <cereal/archives/binary.hpp>
#include <cereal/archives/json.hpp>

// PNG
#include <lodepng.h>

#include <assert.h>

// implementation code for engine..
// should this be in separate header/package?
#include "Util/Util.h"
#include "Gpu/Gpu.h"
#include "Gui/Gui.h"
#include "Math/Math.h"
#include "Scene/Scene.h"
#include "App/App.h"
#include "Components/Components.h"