#version 430 core

layout(location = 0) in vec4 position;

layout(location = 1) in vec4 uvCoords;
layout(location = 3) in vec4 color;

layout(binding = 2) uniform sampler2D tex;

out vec4 uv;
out vec4 c;
noperspective out vec4 world_pos_out;

uniform mat4 proj;
uniform mat4 cam;
uniform mat4 obj;

uniform bool mirror;

uniform bool drawing_color;
uniform vec4 default_color;

uniform int render_mode;

const int RENDER_MODE_COLORED_VTX = 1;
const int RENDER_MODE_TEXTURE = 2;

void main() {

  switch (render_mode) {
  case RENDER_MODE_COLORED_VTX:
    c = color;
    break;
  case RENDER_MODE_TEXTURE:
    c = texture(tex, vec2(20, 20));
    break;
  default:
    c = default_color;
  }

  world_pos_out = obj * position;

  vec4 clip_position = proj * cam * obj * position;
  if (mirror) { clip_position.x = -clip_position.x; }

  uv = uvCoords;

  gl_Position = clip_position;
}
