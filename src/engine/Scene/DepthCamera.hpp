#pragma once

#include <engine/Engine.h>

#include "MeshCamera.hpp"

#include <librealsense2/rs.hpp>

#define _USE_MATH_DEFINES
#include <math.h>

using namespace Magnum;
using namespace Magnum::Math::Literals;

template <class T>
class DepthCamera : public MeshCamera<T> {
public:
  typedef GL::Attribute<0, Vector4> Position;

  GL::Framebuffer fbo{Magnum::NoCreate};

  explicit DepthCamera(
      rs2_intrinsics& _intrinsics,
      const float _depth_scale,
      const int _render_minif,
      const Vector2i& out_dims)
      : intrinsics(_intrinsics), render_minif(_render_minif), depth_scale(_depth_scale) {

#include <generated/DepthCamera_vert.hpp>
#include <generated/DepthCamera_frag.hpp>

    init({DepthCamera_vert, DepthCamera_frag});

    depth_stencil.setStorage(RenderbufferFormat::DepthStencil, out_dims);
    fbo = move(GL::Framebuffer({{0, 0}, {0, 0}}));
    fbo.attachRenderbuffer(GL::Framebuffer::BufferAttachment::DepthStencil, depth_stencil);
    fbo.mapForDraw({{0, GL::Framebuffer::ColorAttachment(0)}});
  };

  void set_target_texture(Engine::Gpu::Tex<Engine::Gpu::TF::R16UI>& tex) {

    Vector2i dms = Vector2i{tex.get_dims()};
    fbo.setViewport({Vector2i{0, 0}, dms});
    depth_stencil.setStorage(RenderbufferFormat::DepthStencil, Vector2i{tex.get_dims()});
    fbo.attachTexture(GL::Framebuffer::ColorAttachment(0), tex.get(), 0);

    Engine::Gpu::GlUtil::verify_framebuffer_complete(fbo);
  }

  void clear_target_texture() { fbo.detach(GL::Framebuffer::ColorAttachment(0)); }

  void set_uniforms() {
    Vector2 dims{intrinsics.width * 1.0f, intrinsics.height * 1.0f};
    Vector2 i_pp{intrinsics.ppx, intrinsics.ppy};
    Vector2 i_f{intrinsics.fx, intrinsics.fy};

    setUniform(uniformLocation("dims"), dims);
    setUniform(uniformLocation("i_pp"), i_pp);
    setUniform(uniformLocation("i_f"), i_f);

    // setUniform(uniformLocation("depth_scale"), depth_scale);
  }

  rs2_intrinsics get_intrinsics() { return intrinsics; }

private:
  const rs2_intrinsics intrinsics;
  const int render_minif;
  const float depth_scale;

  GL::Renderbuffer depth_stencil;
};
