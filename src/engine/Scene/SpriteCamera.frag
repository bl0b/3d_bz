#version 430 core

in vec4 c;

out vec4 fragmentColor;

void main() {
  fragmentColor = c;
  // fragmentColor = vec4(1.0, 1.0, 0.0, 0.3);
  // fragmentColor = vec4(1.0, 0.0, 0.0, 1.0);
}
