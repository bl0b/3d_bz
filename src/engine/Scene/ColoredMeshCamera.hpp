#pragma once

#include <engine/Engine.h>

#include "MeshCamera.hpp"

using namespace Magnum;

class ColoredMeshCameraConfig {
public:
  bool mirror;
  float fov;
  float clip_near;
  float clip_far;

  ColoredMeshCameraConfig() {
    mirror = false;
    fov = 45.;
    clip_near = 10.;
    clip_far = 50000.;
  };

  void draw_imgui() {
    ImGui::Text("Perpsective cam projection");
    ImGui::SliderFloat("FOV", &fov, 1.0, 120.);
    ImGui::SliderFloat("near clip", &clip_near, 1.0, 10000.);
    ImGui::SliderFloat("far clip", &clip_far, 1.0, 25000.);
    ImGui::Checkbox("mirror", &mirror);
  }
};

template <class T>
class ColoredMeshCamera : public MeshCamera<T> {

public:
  // whether or not there is a colored texture to render
  bool drawing_color = true;
  Vector4 default_color{0.3, 0.2, .5, 1.0};

  ColoredMeshCamera() {

#include <generated/ColoredMeshCamera_vert.hpp>
#include <generated/ColoredMeshCamera_frag.hpp>

    init({ColoredMeshCamera_vert, ColoredMeshCamera_frag});
  }

  void set_uniforms() {
    set_uniform("proj", perspective_proj());
    setUniform(uniformLocation("mirror"), config.mirror);
    setUniform(uniformLocation("default_color"), default_color);
  }

  mat4 perspective_proj() {

    // TODO: probably is a better way to do this... GL::defaultFramebuffer::viewport() seems to be
    // not working
    GLint m_viewport[4];
    glGetIntegerv(GL_VIEWPORT, m_viewport);
    float aspect_ratio = 1. * m_viewport[2] / m_viewport[3];

    return glm::mat4{Matrix4::perspectiveProjection(
        Deg(config.fov), aspect_ratio, config.clip_near, config.clip_far)};
  }

  void draw_imgui() {
    if (ImGui::TreeNode("Projection")) {
      config.draw_imgui();
      ImGui::TreePop();
    }
    if (ImGui::TreeNode("Position")) {
      printf("calling disabled!!\n");
      /*
      position.draw_imgui();
      ImGui::TreePop();
       */
    }
  }

  ColoredMeshCameraConfig config;
};
