#pragma once

#include <engine/Engine.h>

#define _USE_MATH_DEFINES
#include <cmath>
#include <math.h>

namespace Engine {
namespace Scene {

class ArcBallCameraPositionConfig {
public:
  float distance = 4500;
  glm::vec2 distance_range = glm::vec2{1., 20000.};

  float min_distance = 150.;
  float angle_x = 0.45;

  float angle_y = -2.;

  glm::vec2 angle_y_range = glm::vec2{(float)M_PI * -1.f, (float)M_PI / -2.f};
  glm::vec2 angle_x_range = glm::vec2{(float)-M_PI, (float)M_PI};

  void draw_imgui() {
    ImGui::Text("Arcball config");
    ImGui::SliderFloat("Distance", &distance, distance_range.x, distance_range.y);
    ImGui::SliderFloat("angle x", &angle_x, angle_x_range.x, angle_x_range.y);
    ImGui::SliderFloat("angle y", &angle_y, angle_y_range.x, angle_y_range.y);
  }
};

class ArcBallCameraPosition : public Engine::Scene::AbstractPosition,
                              public Engine::Gui::GuiElement {
public:
  const glm::mat4 transform() override {
    return glm::rotate(config.angle_x, glm::vec3{0., 0., 1.}) *
           glm::rotate(config.angle_y, glm::vec3{1, 0, 0}) *
           glm::translate(glm::vec3{0, 0, config.distance});
  }

  const glm::mat4 transform_inverse() override { return glm::inverse(transform()); }

  void draw_imgui() override { config.draw_imgui(); }

  float min_y = -M_PI + 0.01;
  float max_y = (-M_PI / 2) - 0.01;

  void draw_imgui_pretty(bool check_scroll, const glm::ivec2 pos, bool* mirror = nullptr) {

    ImGui::SetNextWindowPos({pos.x * 1.f, pos.y * 1.f});

    const float dX = 0.07;
    const float dY = 0.07;

    const ImVec4 button_hover_color{0.8, 0.5, 0.3, 1.0};
    const ImVec4 button_normal_color{0.7, 0.4, 0.2, 1.0};
    const ImVec4 button_white_color_t{1., 1., 1., 0.6};
    const ImVec4 button_white_color{1., 1., 1., 0.9};

    auto draw_button =
        [&](const string name, const ImGuiDir dir, const int glfw_key, function<void(void)> fn) {
          const bool key_pressed =
              !ImGui::GetIO().WantCaptureKeyboard && ImGui::IsKeyDown(glfw_key);

          if (key_pressed) {
            ImGui::PushStyleColor(ImGuiCol_Button, ImGui::GetStyle().Colors[ImGuiCol_ButtonActive]);
            ImGui::PushStyleColor(
                ImGuiCol_ButtonHovered, ImGui::GetStyle().Colors[ImGuiCol_ButtonActive]);
          }

          ImGui::ArrowButton(name.c_str(), dir);
          if ((ImGui::IsItemHovered() && ImGui::IsMouseDown(0)) || key_pressed) { fn(); }

          if (key_pressed) {
            ImGui::PopStyleColor();
            ImGui::PopStyleColor();
          }
        };

    draw_invisible_window("arcball", [&]() {
      ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, {2.f, 2.f});

      ImGui::Indent(21.f);

      draw_button("up", ImGuiDir_Up, GLFW_KEY_UP, [&] {
        config.angle_y = config.angle_y - dY;
        if (config.angle_y < min_y) { config.angle_y = min_y; }
      });

      if (mirror != nullptr) {
        ImGui::SameLine();
        if (ImGui::Button(ICON_FA_UNDO)) { *mirror = !(*mirror); }
      }

      ImGui::Unindent(21.f);

      draw_button("left", ImGuiDir_Left, GLFW_KEY_LEFT, [&] {
        config.angle_x = config.angle_x - dX;
        while (config.angle_x < -M_PI) {
          config.angle_x += (2 * M_PI);
        }
      });

      ImGui::SameLine();

      draw_button("down", ImGuiDir_Down, GLFW_KEY_DOWN, [&] {
        config.angle_y = config.angle_y + dY;
        if (config.angle_y > max_y) { config.angle_y = max_y; }
      });

      ImGui::SameLine();

      draw_button("right", ImGuiDir_Right, GLFW_KEY_RIGHT, [&] {
        config.angle_x = config.angle_x + dX;
        while (config.angle_x > M_PI) {
          config.angle_x -= (2 * M_PI);
        }
      });

      ImGui::PushItemWidth(60.f);
      ImGui::SliderFloat("##dist", &config.distance, 150., 15000., "");
      ImGui::PopItemWidth();

      // change this so scale changes smoothly as it gets closer??
      if (check_scroll) {
        config.distance -= ImGui::GetIO().MouseWheel * 500.f;
        // 1.5 cm away :)
        if (config.distance < config.min_distance) { config.distance = config.min_distance; }
      }

      ImGui::PopStyleVar();
    });
  }

  ArcBallCameraPositionConfig config;

private:
  void draw_invisible_window(const string name, function<void(void)> draw_fn) {

    ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, {2, 2});
    ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 0);
    ImGui::PushStyleVar(ImGuiStyleVar_WindowMinSize, {0, 0});

    ImGui::Begin(
        name.c_str(),
        NULL,
        ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoBringToFrontOnFocus |
            ImGuiWindowFlags_NoBackground | ImGuiWindowFlags_NoTitleBar |
            ImGuiWindowFlags_NoScrollbar);

    draw_fn();

    ImGui::PopStyleVar();
    ImGui::PopStyleVar();
    ImGui::PopStyleVar();

    ImGui::End();
  }
};

} // namespace Scene
} // namespace Engine