#pragma once

#include <engine/Engine.h>

namespace Engine {
namespace Scene {

class AbstractPosition {
public:
  virtual const glm::mat4 transform() = 0;
  virtual const glm::mat4 transform_inverse() = 0;
};

} // namespace Scene
} // namespace Engine