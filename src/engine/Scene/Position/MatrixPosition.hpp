#pragma once

#include <engine/Engine.h>

namespace Engine {
namespace Scene {

class MatrixPosition : public AbstractPosition, public Engine::Gui::GuiElement {
public:
  const glm::mat4 transform() override { return _transform; }

  const glm::mat4 transform_inverse() override { return _transform_inverse; }

  void set(glm::mat4 t) {
    _transform = t;
    _transform_inverse = glm::inverse(t);
  }

  void draw_imgui() override {
    ImGui::Text("MatrixPosition");

#ifndef __NVCC__
    const std::string transform_string = glm::to_string(transform());
    const std::string transform_inverse_string = glm::to_string(transform_inverse());

    ImGui::Text("transform: \n%s", transform_string.c_str());
    ImGui::Text("inverse: \n%s", transform_inverse_string.c_str());
#endif
  }

private:
  glm::mat4 _transform{1.f};
  glm::mat4 _transform_inverse{1.f};
};

} // namespace Scene
} // namespace Engine