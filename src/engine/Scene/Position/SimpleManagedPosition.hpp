#pragma once

#include <engine/Engine.h>

using namespace Magnum;
using namespace std;

namespace Engine {
namespace Scene {

class SimpleManagedPosition : public AbstractPosition, public Engine::Gui::GuiElement {
public:
  // xyz translation
  glm::vec3 translation{0.};
  // pitch, yaw and roll.
  glm::vec3 rotation{0.};

  const glm::mat4 transform() override {
    return glm::translate(translation) * glm::rotate(rotation.x, glm::vec3{1, 0, 0}) *
           glm::rotate(rotation.y, glm::vec3{0, 1, 0}) *
           glm::rotate(rotation.z, glm::vec3{0, 0, 1});
  }

  const glm::mat4 transform_inverse() override { return glm::inverse(transform()); }

  void draw_imgui() override {

    ImGui::Text("Simple managed position");

    const float max_dim = 5000.;

    ImGui::SliderFloat("Translation X", &translation[0], -max_dim, max_dim);
    ImGui::SliderFloat("Translation Y", &translation[1], -max_dim, max_dim);
    ImGui::SliderFloat("Translation Z", &translation[2], -max_dim, max_dim);

    const float max_rot_dim = 4.;

    ImGui::SliderFloat("Rotation X", &rotation[0], -max_rot_dim, max_rot_dim);
    ImGui::SliderFloat("Rotation Y", &rotation[1], -max_rot_dim, max_rot_dim);
    ImGui::SliderFloat("Rotation Z", &rotation[2], -max_rot_dim, max_rot_dim);
  }
};

} // namespace Scene
} // namespace Engine