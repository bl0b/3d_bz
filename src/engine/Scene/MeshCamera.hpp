#pragma once

#include <engine/Engine.h>

#include "Obj.hpp"

using namespace Magnum;
using namespace std;

template <class T>
class MeshCamera : public Engine::Gpu::ShaderBase, public Engine::Scene::Obj<T> {

public:
  template <class T2>
  void draw(DepthCameraMesh<T2>& obj) {

    // Set camera uniforms
    /*
    set_uniforms();
    set_uniform("cam", position.transform_inverse());
     */
    printf("calling disabled function!\n");


    // Set mesh uniforms
    obj.set_uniforms(*this);
    set_uniform("obj", obj.position.transform());

    // Fetch mesh object from mesh
    GL::Mesh& m = obj.get_mesh();
    m.draw(*this);
  }

protected:
  MeshCamera() : Engine::Gpu::ShaderBase() {

    // for (auto& p : programs) {
    //  load(p);
    //}
    // finish();

    // init(programs);
    //{OrthoMapShader_vert, OrthoMapShaderGroupFilter_geom, OrthoMapShader_frag}
  }

  void init(vector<Engine::ProgramSource> programs) {
    for (auto& p : programs) {
      load(p);
    }
    finish();
  }

  virtual void set_uniforms() = 0;
};
