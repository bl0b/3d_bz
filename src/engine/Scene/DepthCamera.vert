#version 330 core

layout(location = 0) in vec4 aPos;

// uniform mat4 projection;
// uniform mat4 view;
uniform mat4 msh;
uniform mat4 obj;
uniform mat4 cam;

uniform vec2 dims;
uniform vec2 i_pp;
uniform vec2 i_f;

uniform float depth_scale;

// uniform int render_mode;

// const int RENDER_MODE_COLORED_VTX = 1;
// const int RENDER_MODE_TEXTURE = 2;

// Highest value for 16 bit unsigned int.
// Divide z pos by this max for NDC z coord
#define MAX_DEPTH 65536.0

out float zPos;

void main() {

  // not supported!
  // if (render_mode == RENDER_MODE_TEXTURE) {
  //  return;
  //}

  // vec4 pos = obj * msh * vec4(aPos.x, aPos.y, aPos.z, aPos.w);
  // pos.z = -pos.z;
  // pos.y = -pos.y;
  // vec4 world_pos = cam * pos;
  // world_pos.z = -world_pos.z;
  // world_pos.xyz = world_pos.xyz / depth_scale;
  // vec4 world_pos = obj * msh * pos;
  // vec4 world_pos = aPos;

  // vec4 world_pos = cam * aPos;
  vec4 world_pos = aPos;
  // world_pos.z = -world_pos.z;

  world_pos = cam * obj * world_pos;

  world_pos.z = -world_pos.z;

  float div = world_pos.z <= 0 ? -world_pos.z : world_pos.z;

  vec3 pixel_pos = vec3(
      ((world_pos.x / div) * i_f.x) + i_pp.x, ((world_pos.y / div) * i_f.y) + i_pp.y, world_pos.z);

  vec3 ndc_pos = vec3(
      2.0 * (pixel_pos.x + 0.5) / dims.x - 1.0,
      2.0 * (pixel_pos.y + 0.5) / dims.y - 1.0,
      2.0 * (pixel_pos.z / MAX_DEPTH) - 1.0);

  gl_Position = vec4(ndc_pos.xyz, 1.0);

  // TODO: Fix interpolation bug
  // There is a problem with the interpolation of this value, probably because the
  // ndc Z coordinate does folow the correct scale: currently being set as linear with
  // z value from -1 to 1, perspective projection (for example) uses non-linear scale
  //
  // Because of this, many smaller polygons produce more accurate depth images than a few huge
  // polygons
  zPos = world_pos.z;
}
