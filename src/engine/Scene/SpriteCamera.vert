#version 330 core

layout(location = 0) in vec4 position;
layout(location = 3) in vec4 color;

uniform mat4 proj;
uniform mat4 cam;
uniform mat4 transform;
uniform mat4 obj;

uniform vec4 full_color;

uniform int render_mode;

const int RENDER_MODE_COLORED_VTX = 1;
const int RENDER_MODE_TEXTURE = 2;

out vec4 c;

void main() {
  vec4 new_pos = proj * cam * obj * vec4(position.xy, 0.0, 1.0);
  // vec4 new_pos = proj * cam * obj * vec4(position.xy, 0.0, 1.0);
  // vec4 new_pos = proj * cam * transform * vec4(position.xy, 0.0, 1.0);
  // c = vec4(0.0, 0.5, 0.3, 0.4);

  if (render_mode == RENDER_MODE_COLORED_VTX) {
    c = color;
  } else {
    c = full_color;
  }
  gl_Position = vec4(new_pos.xy, 0.0, 1.0);
}
