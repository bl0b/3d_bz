#version 430 core

layout(binding = 2) uniform sampler2D tex;

in vec4 c;
in vec4 uv;
noperspective in vec4 world_pos_out;

layout(location = 0) out vec4 fragmentColor;
layout(location = 1) out vec4 fragment_position;

uniform int render_mode;

// uniform vec4 default_color;

uniform vec4 full_color;

const int RENDER_MODE_COLORED_VTX = 1;
const int RENDER_MODE_TEXTURE = 2;
const int RENDER_MODE_FULL_COLOR = 3;

void main() {

  switch (render_mode) {
  case RENDER_MODE_COLORED_VTX:
    fragmentColor = c;
    break;
  case RENDER_MODE_TEXTURE:
    fragmentColor = texture(tex, uv.xy);
    break;
  case RENDER_MODE_FULL_COLOR:
    fragmentColor = full_color;
    break;

    // default:
    //	fragmentColor = default_color;
  }

  // also write position in world space to 2nd output buffer
  fragment_position = world_pos_out;
}
