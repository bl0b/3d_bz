#pragma once;

#include <Engine/Engine.h>

#include "MeshCamera.hpp"

using namespace Magnum;
using namespace std;

class SpriteMesh : public SharedMesh {
public:
  GL::Buffer positions; // { NoCreate };
  GL::Buffer idxes;     // { NoCreate };
  unsigned int num_triangles = 0;

protected:
  void _init() {
    positions = move(GL::Buffer{});
    idxes = move(GL::Buffer{});
  }

  GL::Mesh& config_mesh(GL::Mesh& m) {
    return m.setPrimitive(MeshPrimitive::Triangles)
        .setCount(num_triangles * 3)
        .addVertexBuffer(positions, 0, Position{})
        .setIndexBuffer(idxes, 0, MeshIndexType::UnsignedInt);
  }

private:
  typedef GL::Attribute<0, Vector2> Position;
};