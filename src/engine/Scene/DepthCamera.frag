#version 330 core
layout(location = 0) out uint DepthOut;

in float zPos;

void main() {
  // DepthOut = uint(300);
  DepthOut = uint(zPos);
}
