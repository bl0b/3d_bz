#pragma once

#include <engine/Engine.h>

using namespace Magnum;

template <class T>
class SpriteCamera : public MeshCamera<T> {

public:
  GL::Framebuffer fbo{Magnum::NoCreate};

  SpriteCamera() {

#include <generated/SpriteCamera_vert.hpp>
#include <generated/SpriteCamera_frag.hpp>

    init({SpriteCamera_vert, SpriteCamera_frag});
    // no size of framebuffer, set_target_texture must be called!
    fbo = move(GL::Framebuffer({{0, 0}, {0, 0}}));
    fbo.mapForDraw({{0, GL::Framebuffer::ColorAttachment(0)}});
  }

  void set_target_texture(Engine::Gpu::Tex<Engine::Gpu::TF::RGBA8>& tex) {

    bound_tex = &tex;
    fbo.setViewport({Vector2i{0, 0}, Vector2i{tex.get_dims()}});
    fbo.attachTexture(GL::Framebuffer::ColorAttachment(0), tex.get(), 0);

    Engine::Gpu::GlUtil::verify_framebuffer_complete(fbo);
  }

  void clear_target_texture() {
    fbo.detach(GL::Framebuffer::ColorAttachment(0));
    bound_tex = nullptr;
  }

  void draw_imgui() {
    /*       ImGui::Begin("colored mesh camera");

           ImGui::End();*/
  }

protected:
  void set_uniforms() {

    if (bound_tex == nullptr) {
      printf("cant draw sprite without a bound texture!");
      throw new invalid_argument("bal;");
    }

    float x_dim = bound_tex->get_dims().x * 1.f;
    float y_dim = bound_tex->get_dims().y * 1.f;

    Matrix4 ndc_tform = Matrix4::scaling({2.0f / x_dim, 2.0f / y_dim, 1.0}) *
                        Matrix4::translation({-x_dim / 2.f, -y_dim / 2.f, 0.0});

    set_uniform("proj", glm::mat4{ndc_tform});
  }

private:
  Engine::Gpu::Tex<Engine::Gpu::TF::RGBA8>* bound_tex = nullptr;
};
