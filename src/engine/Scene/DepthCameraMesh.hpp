#pragma once

#include <engine/Engine.h>

using namespace Magnum;
using namespace std;
using namespace glm;

template <class T>
class DepthCameraMesh : public Engine::Scene::Obj<T> {
public:
  enum RenderMode {
    ColorVtx = 1,
    Texture = 2,
    FullColor = 3,
  };

  GL::Mesh m;

  // all have positions, idxes
  Engine::Gpu::Buff<vec4> positions;
  Engine::Gpu::Buff<unsigned int> idxes;
  unsigned int num_triangles = 0;

  // RenderMode::ColorVtx
  Engine::Gpu::Buff<vec4> colors;

  // RenderMode::Texture
  Engine::Gpu::Tex<Engine::Gpu::TF::RGBA8> tex;
  // Vec4 even though could only be 2
  Engine::Gpu::Buff<vec4> uv_coords;

  // RenderMode::FullColor.. default to pure white?
  vec4 full_color{1, 1, 1, 1.};

  void set_render_mode(const RenderMode new_mode) { render_mode = new_mode; }

  void set_uniforms(Engine::Gpu::ShaderBase& p) {
    if (render_mode == RenderMode::Texture) { tex.get().bind(2); }
    if (render_mode == RenderMode::FullColor) {
      string s = "full_color";
      p.set_uniform<4>(s, full_color);
    }
    p.set_uniform("render_mode", render_mode);
  }

public:
  GL::Mesh& get_mesh() {

    m.setPrimitive(MeshPrimitive::Triangles)
        .setCount(num_triangles * 3)
        .setIndexBuffer(idxes.get_gl(), 0, MeshIndexType::UnsignedInt)
        .addVertexBuffer(positions.get_gl(), 0, Position{});

    switch (render_mode) {
    case ColorVtx:
      m.addVertexBuffer(colors.get_gl(), 0, Color{});
      break;
    case Texture:
      m.addVertexBuffer(uv_coords.get_gl(), 0, UvCoords{});
      break;
    case FullColor:
      // no extra buffer here..
      break;
    default:
      throw new invalid_argument("mesh has no render mode set!");
    }

    return m;
  }

private:
  RenderMode render_mode = RenderMode::ColorVtx;

  typedef GL::Attribute<0, Vector4> Position;
  typedef GL::Attribute<3, Vector4> Color;
  typedef GL::Attribute<1, Vector4> UvCoords;
};