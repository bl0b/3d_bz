#pragma once

#include <engine/Engine.h>

#include <type_traits>

//#include <objects/position/ManagedPosition.hpp>

using namespace Magnum;
using namespace std;

namespace Engine {
namespace Scene {

template <class T>
class Obj {

  static_assert(
      std::is_base_of<Engine::Scene::AbstractPosition, T>::value,
      "Template class must inherit from AbstractPosition");

public:
  T position;

  string name;

  ~Obj() {}

protected:
};

} // namespace Scene
} // namespace Engine