#pragma once

#include <engine/Engine.h>

#include <functional>

namespace Engine {
namespace Gpu {

using TF = Magnum::GL::TextureFormat;

// dynamic because it supports resizing - format of pixel cannot be changed!
template <TF _texture_format>
class Tex {
public:
  // Compile-time constants derived from template argument. Seems way too complicated, but oh well
  const TF texture_format;
  const Magnum::GL::ImageFormat image_format;
  const size_t pixel_size;
  const Magnum::GL::PixelFormat pixel_format;
  const Magnum::GL::PixelType pixel_type;

  // If no dims provided, start as 1x1. Can be changed later
  Tex(const glm::ivec2 _dims = glm::ivec2{1, 1}, const void* data = nullptr)
      : texture_format(_texture_format), image_format(_get_image_format(_texture_format)),
        pixel_size(get_pixel_size(_texture_format)),
        pixel_format(get_pixel_format(_texture_format)),
        pixel_type(get_pixel_type(_texture_format)) {

    // nullptr: just allocate
    if (data == nullptr) {
      set_storage(_dims);
    } else {
      // real pointer: allocate and copy to gpu memory
      set_data(_dims, data);
    }
  }

  void set_data(const glm::ivec2 _dims, const void* data) {
    unmap_from_compute();
    set_storage(_dims);
    tex.setSubImage(
        0,
        {},
        Magnum::ImageView2D{pixel_format,
                            pixel_type,
                            Magnum::Vector2i{dims},
                            Magnum::Containers::arrayView(data, dims.x * dims.y * pixel_size)});
  }

  void set_storage(const glm::ivec2 _dims) {
    unmap_from_compute();
    if (!matches_current(_dims)) {
      initialized = true;
      dims = _dims;
      tex = std::move(Magnum::GL::Texture2D{});
      tex.setStorage(1, texture_format, Magnum::Vector2i{dims});

      unregister_compute();
    }
  }

  void save_png(const string file_name) {

    if (texture_format != Engine::Gpu::TF::RGBA8) {
      throw new invalid_argument("Can only save RGBA8 textures to PNG");
    }

    // this is somewhat slow - have to allocate the memory everytime, not ideal for run-time usage
    // yet
    const int num_chars = get_dims().x * get_dims().y * 4;
    vector<unsigned char> out_cpu(num_chars);
    read<glm::u8vec4>(
        [&](glm::u8vec4* d) { memcpy(out_cpu.data(), d, num_chars * sizeof(unsigned char)); });

    FileUtil::save_png(file_name, out_cpu, get_dims());
  }

  cl_mem get_cl_mem();
  cl_mem* get_cl_mem_addr();

  bool cl_aquired = false;
  cl_mem tex_cl = nullptr;

  template <class T>
  void cu_copy_single_value(const glm::ivec2 coord, T* val);

  //  template <TF _t_f>
  template <class T>
  void cu_write_single_value(const glm::ivec2 coord, T* val) {
    const size_t origin[3] = {coord.x, coord.y, 0};
    const size_t dims[3] = {1, 1, 1};
    CL(clEnqueueWriteImage(
        compute_ctx.command_queue, get_cl_mem(), true, origin, dims, 0, 0, val, NULL, NULL, NULL));
  }

  template <class T>
  void cu_copy_to(Engine::Gpu::Buff<T>& buff);

  void cu_copy_to(Engine::Gpu::Tex<_texture_format>& tex);

  template <class T>
  void cu_copy_to(vector<T>& vec);

  template <class T>
  void cu_copy_to(T* d);

  void cu_copy_from(const void* data, const glm::ivec2& d);

  template <class T>
  void cu_copy_from(Engine::Gpu::Buff<T>& buff, const glm::ivec2& dims);

  template <typename T>
  void cu_fill(T val);

  template <typename T>
  void read(std::function<void(T* data)> fn);

  void bind(const int unit, Magnum::GL::ImageAccess access);

  void copy(Tex& other);

  Magnum::GL::Texture2D& get();

  const glm::ivec2& get_dims();

  bool is_initialized();

private:
  Magnum::GL::Texture2D tex{Magnum::NoCreate};
  bool initialized = false;
  glm::ivec2 dims{0, 0};

  void verify_initialized();
  bool matches_current(const glm::ivec2 _dims);

  // helper functions for mapping between GL format enums
  static Magnum::GL::PixelFormat get_pixel_format(TF f);
  static Magnum::GL::PixelType get_pixel_type(TF f);
  static Magnum::GL::ImageFormat _get_image_format(TF f);
  static size_t get_pixel_size(TF f);

  void map_to_compute();
  void unmap_from_compute();
  void unregister_compute();
};

// Include implementation in header, so explicit initialization isn't required

//
template <TF _t_f> //, typename T>
template <typename T>
void Tex<_t_f>::read(std::function<void(T* data)> fn) {
  if (!initialized) {
    printf("can't read, not initialized!");
    throw std::invalid_argument("cant read texture, not initialized!");
  }

  if (sizeof(T) > 0 && sizeof(T) != pixel_size) {
    printf("desired data format size doesn't match texture!");
    throw std::invalid_argument("logs!");
  }

  unmap_from_compute();

  Magnum::GL::BufferImage2D img_buffer =
      tex.image(0, {pixel_format, pixel_type}, Magnum::GL::BufferUsage::StaticRead);

  T* ptr = (T*)img_buffer.buffer().map(Magnum::GL::Buffer::MapAccess::ReadOnly);

  fn(ptr);

  img_buffer.buffer().unmap();
}

template <TF _t_f>
void Tex<_t_f>::bind(const int unit, Magnum::GL::ImageAccess access) {
  // unmap_from_compute();
  get().bindImage(unit, 0, access, image_format);
}

template <TF _t_f>
void Tex<_t_f>::copy(Tex& other) {
  if (!initialized) {
    printf("can't copy, not initialized!");
    throw std::invalid_argument("cant copy..");
  }

  unmap_from_compute();

  // TODO: Maybe there's a faster way... this might map to CPU memory before going back to GPU
  Magnum::GL::BufferImage2D img_buffer{pixel_format, pixel_type};
  tex.image(0, img_buffer, Magnum::GL::BufferUsage::StaticRead);

  void* ptr = (void*)img_buffer.buffer().map(Magnum::GL::Buffer::MapAccess::ReadOnly);
  other.set_data(get_dims(), ptr);

  img_buffer.buffer().unmap();
}

template <TF _t_f>
Magnum::GL::Texture2D& Tex<_t_f>::get() {
  verify_initialized();
  unmap_from_compute();
  return tex;
}

template <TF _t_f>
const glm::ivec2& Tex<_t_f>::get_dims() {
  verify_initialized();
  return dims;
}

template <TF _t_f>
bool Tex<_t_f>::is_initialized() {
  return initialized;
}

template <TF tf>
void Tex<tf>::verify_initialized() {
  if (!initialized) {
    printf("texture not initialized!\n");
    throw std::invalid_argument("texture not initialized");
  }
}

template <TF tf>
bool Tex<tf>::matches_current(const glm::ivec2 _dims) {
  if (!initialized) { return false; }

  if (dims.x != _dims.x || dims.y != _dims.y) { return false; }

  return true;
}

template <TF tf>
Magnum::GL::PixelFormat Tex<tf>::get_pixel_format(Magnum::GL::TextureFormat f) {
  switch (f) {
  case Magnum::GL::TextureFormat::R16UI:
    return Magnum::GL::PixelFormat::RedInteger;
  case Magnum::GL::TextureFormat::R32UI:
    return Magnum::GL::PixelFormat::RedInteger;
  case Magnum::GL::TextureFormat::DepthStencil:
    return Magnum::GL::PixelFormat::DepthStencil;
  case Magnum::GL::TextureFormat::DepthComponent32:
    return Magnum::GL::PixelFormat::DepthComponent;
  case Magnum::GL::TextureFormat::RGBA8:
    return Magnum::GL::PixelFormat::RGBA;
  case Magnum::GL::TextureFormat::RGBA32F:
    return Magnum::GL::PixelFormat::RGBA;
  case Magnum::GL::TextureFormat::RG16UI:
    return Magnum::GL::PixelFormat::RGInteger;

  default:
    printf("texture format not supported!\n");
    throw std::invalid_argument("texture format not supported!");
  }
}

template <TF tf>
Magnum::GL::PixelType Tex<tf>::get_pixel_type(Magnum::GL::TextureFormat f) {
  switch (f) {
  case Magnum::GL::TextureFormat::R16UI:
    return Magnum::GL::PixelType::UnsignedShort;
  case Magnum::GL::TextureFormat::R32UI:
    return Magnum::GL::PixelType::UnsignedInt;
  case Magnum::GL::TextureFormat::DepthStencil:
    return Magnum::GL::PixelType::UnsignedInt248;
  case Magnum::GL::TextureFormat::DepthComponent32:
    return Magnum::GL::PixelType::UnsignedInt;
  case Magnum::GL::TextureFormat::RGBA8:
    return Magnum::GL::PixelType::UnsignedByte;
  case Magnum::GL::TextureFormat::RGBA32F:
    return Magnum::GL::PixelType::Float;
  case Magnum::GL::TextureFormat::RG16UI:
    return Magnum::GL::PixelType::UnsignedShort;
  default:
    printf("texture format not supported!\n");
    throw std::invalid_argument("texture format not supported!");
  }
}

template <TF tf>
Magnum::GL::ImageFormat Tex<tf>::_get_image_format(Magnum::GL::TextureFormat f) {
  switch (f) {
  case Magnum::GL::TextureFormat::R16UI:
    return Magnum::GL::ImageFormat::R16UI;
  case Magnum::GL::TextureFormat::R32UI:
    return Magnum::GL::ImageFormat::R32UI;
  case Magnum::GL::TextureFormat::DepthStencil:
    return Magnum::GL::ImageFormat::R32UI;
  case Magnum::GL::TextureFormat::DepthComponent32:
    return Magnum::GL::ImageFormat::R32UI;
  // case Magnum::GL::TextureFormat::DepthComponent24:
  // return Magnum::GL::ImageFormat::R32UI;
  case Magnum::GL::TextureFormat::RGBA8:
    return Magnum::GL::ImageFormat::RGBA8;
  case Magnum::GL::TextureFormat::RGBA32F:
    return Magnum::GL::ImageFormat::RGBA32F;
  case Magnum::GL::TextureFormat::RG16UI:
    return Magnum::GL::ImageFormat::RG16UI;
  default:
    printf("texture format not supported!\n");
    throw std::invalid_argument("texture format not supported!");
  }
}

template <TF tf>
size_t Tex<tf>::get_pixel_size(Magnum::GL::TextureFormat f) {
  switch (f) {
  case Magnum::GL::TextureFormat::R16UI:
    return sizeof(uint16_t);
  case Magnum::GL::TextureFormat::R32UI:
    return sizeof(uint32_t);
  case Magnum::GL::TextureFormat::DepthStencil:
    return sizeof(uint32_t);
  case Magnum::GL::TextureFormat::DepthComponent32:
    return sizeof(Magnum::UnsignedInt);
    // case Magnum::GL::TextureFormat::DepthComponent24:
    // 3 bytes?
    // return sizeof(unsigned char) * 3;
  case Magnum::GL::TextureFormat::RGBA8:
    return 4 * sizeof(unsigned char);
  case Magnum::GL::TextureFormat::RGBA32F:
    return 4 * sizeof(float);
  case Magnum::GL::TextureFormat::RG16UI:
    return 2 * sizeof(uint16_t);
  default:
    printf("texture format not supported!\n");
    throw std::invalid_argument("texture format not supported!");
  }
}

// explicit initialize classes
// template class Tex<TF::R16UI>;
// template void Tex<TF::R16UI>::read<uint16_t>(std::function<void(uint16_t*)>);

// template class Tex<TF::RGBA8>;
// template class Tex<TF::R32UI>;
// template class Tex<TF::RGBA32F>;

} // namespace Gpu
} // namespace Engine
