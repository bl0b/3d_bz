#pragma once

#include <engine/Engine.h>

#include <unordered_map>
#include <vector>
#include <functional>
#include <chrono>
#include <iostream>

namespace Engine {
namespace Gpu {

using namespace std;
using namespace chrono;

/*

Creates a GLFW window. Allows mouse/keyboard callbacks to be declared

*/
class GraphicsCtx {

public:
  struct Cfg {
    const int w;
    const int h;
    const string window_name;
    const int gl_maj;
    const int gl_min;
    const int cl_maj;
    const int cl_min;
  };

  int w = 0;
  int h = 0;

  GLFWwindow* handle;

  ImGuiIO* gui_io = nullptr;

  unordered_map<int, function<void(void)>> key_callbacks;
  unordered_map<int, bool> key_callbacks_must_reset;
  unordered_map<int, bool> key_callbacks_not_reset_yet;

  unordered_map<int, function<void(void)>> mouse_callbacks;
  unordered_map<int, high_resolution_clock::time_point> mouse_callback_last_called;
  unordered_map<int, int> mouse_callback_debounce;

  vector<function<void(const glm::ivec2, const bool, const bool)>> mouse_pos_callbacks;

  Magnum::Platform::GLContext ctx_main{Magnum::NoCreate};

  // ComputeCtx compute_ctx;

  GraphicsCtx(const Cfg& cfg) {

    w = cfg.w;
    h = cfg.h;

    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, cfg.gl_maj);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, cfg.gl_min);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);

    handle = glfwCreateWindow(w, h, cfg.window_name.c_str(), NULL, NULL);
    if (handle == NULL) {
      std::cout << "Failed to initialize window" << std::endl;
      return;
    }

    glfwMakeContextCurrent(handle);

    // disable framerate restriction!
    glfwSwapInterval(0);

    glfwSetWindowUserPointer(handle, this);

    glfwSetFramebufferSizeCallback(handle, framebuffer_size_callback);
    glfwSetInputMode(handle, GLFW_CURSOR, GLFW_CURSOR_NORMAL);

    glViewport(0, 0, w, h);
    glEnable(GL_DEPTH_TEST);

    ctx_main.create();

    Engine::compute_ctx.init();
  }

  ~GraphicsCtx() {}

  glm::ivec2 get_dims() { return glm::ivec2{w, h}; }

  void poll_events() {
    glfwPollEvents();

    bool handle_key = true;
    if (gui_io != nullptr) { handle_key = !ImGui::GetIO().WantCaptureKeyboard; }

    if (handle_key) {
      for (auto cb : key_callbacks) {
        const int key = cb.first;
        const bool key_pressed = press(key);
        auto fn = cb.second;

        if (!key_pressed) { key_callbacks_not_reset_yet[key] = false; }

        if (press(key)) {
          bool must_reset = key_callbacks_must_reset[key];
          if (!must_reset || !key_callbacks_not_reset_yet[key]) {
            fn();
            key_callbacks_not_reset_yet[key] = true;
          }
        }
      }
    }

    for (auto cb : mouse_callbacks) {
      const int key = cb.first;
      auto fn = cb.second;
      if (click(key)) {
        if (mouse_callback_debounce[key] > -1) {
          high_resolution_clock::time_point last_called = mouse_callback_last_called[key];
          high_resolution_clock::time_point now = high_resolution_clock::now();
          auto diff = duration_cast<milliseconds>(now - last_called).count();
          if (diff > mouse_callback_debounce[key]) {
            mouse_callback_last_called[key] = now;
            fn();
          }
        } else {
          fn();
        }
      }
    }

    glm::dvec2 d_pos;
    glfwGetCursorPos(handle, &d_pos.x, &d_pos.y);
    const glm::ivec2 pos = glm::ivec2{(int)round(d_pos.x), (int)round(d_pos.y)};

    bool on_screen = pos.x >= 0 && pos.y >= 0 && pos.x < w && pos.y < h;

    const bool imgui_want_mouse = ImGui::GetIO().WantCaptureMouse;
    for (auto cb : mouse_pos_callbacks) {
      cb(pos, on_screen, imgui_want_mouse);
    }
  }

  bool press(int key) { return glfwGetKey(handle, key) == GLFW_PRESS; }

  bool click(int button) { return glfwGetMouseButton(handle, button) == GLFW_PRESS; }

  void set_should_close(bool val) { glfwSetWindowShouldClose(handle, val); }

  void watch_mouse(function<void(const glm::ivec2, const bool, const bool)> callback) {
    mouse_pos_callbacks.push_back(callback);
  }

  void on_press(int key, function<void(void)> callback) { on_press(key, false, callback); }

  void on_press(int key, bool must_unpress_before_press_again, function<void(void)> callback) {
    key_callbacks[key] = callback;
    key_callbacks_must_reset[key] = must_unpress_before_press_again;
  }

  void on_click(int key, function<void(void)> callback) {
    mouse_callback_debounce[key] = -1;
    mouse_callbacks[key] = callback;
  }

  void on_click(int key, int debounce, function<void(void)> callback) {
    mouse_callback_debounce[key] = debounce;
    mouse_callback_last_called[key] = high_resolution_clock::now();
    mouse_callbacks[key] = callback;
  }

  void clear_mouse_and_keyboard_callbacks() {
    key_callbacks.clear();
    key_callbacks_not_reset_yet.clear();
    key_callbacks_must_reset.clear();
    mouse_callbacks.clear();
    mouse_callback_debounce.clear();
    mouse_pos_callbacks.clear();
  }

  bool should_close() { return glfwWindowShouldClose(handle); }

  void swap() { glfwSwapBuffers(handle); }

  void set_as_render_target() {
    glViewport(0, 0, w, h);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
  }

  static void framebuffer_size_callback(GLFWwindow* w, int _w, int _h) {
    GraphicsCtx* window = static_cast<GraphicsCtx*>(glfwGetWindowUserPointer(w));
    window->w = _w;
    window->h = _h;
    glViewport(0, 0, window->w, window->h);
  };
};

} // namespace Gpu
} // namespace Engine
