#pragma once

#include <engine/Engine.h>

namespace Engine {
namespace Gpu {

class GlUtil {
public:
  static void verify_framebuffer_complete(Magnum::GL::Framebuffer& fbo) {



    int st = static_cast<GLenum>(fbo.checkStatus(Magnum::FramebufferTarget::Draw));
    if (st != GL_FRAMEBUFFER_COMPLETE) {
      std::cout << "Frambuffer incomplete!" << std::endl;
      throw std::invalid_argument("fbo not complete~!");
    }

    st = static_cast<GLenum>(fbo.checkStatus(Magnum::FramebufferTarget::Read));
    if (st != GL_FRAMEBUFFER_COMPLETE) {
      std::cout << "Framebuffer incomplete!" << std::endl;
      throw std::invalid_argument("fbo not complete~!");
    }
  }
};

} // namespace Gpu
} // namespace Engine