#pragma once

#include <engine/Engine.h>
#include <unordered_map>
#include <string>

namespace Engine {
namespace Gpu {

class ClKernelInvocation {
public:
  ClKernelInvocation(const cl_kernel _k) : k(_k) {}

  ClKernelInvocation& dims(const glm::ivec2 grid, const glm::ivec2 _block) {
    if (dims_set) {
      printf("Invocation dims already set!");
      throw new invalid_argument("Invocation dims already set!");
    }

    dims_set = true;
    num_dims = 2;
    global[0] = grid.x * _block.x;
    global[1] = grid.y * _block.y;

    block[0] = _block.x;
    block[1] = _block.y;

    return *this;
  }

  ClKernelInvocation& dims(const glm::ivec3 grid, const glm::ivec3 _block) {
    if (dims_set) {
      printf("Invocation dims already set!");
      throw new invalid_argument("Invocation dims already set!");
    }

    dims_set = true;
    num_dims = 3;
    global[0] = grid.x * _block.x;
    global[1] = grid.y * _block.y;
    global[2] = grid.z * _block.z;

    block[0] = _block.x;
    block[1] = _block.y;
    block[2] = _block.z;

    return *this;
  }

  ClKernelInvocation& dims(const int grid, const int _block) {
    if (dims_set) {
      printf("Invocation dims already set!");
      throw new invalid_argument("Invocation dims already set!");
    }

    dims_set = true;
    num_dims = 1;
    global[0] = grid * _block;
    block[0] = _block;

    return *this;
  }

  template <Engine::Gpu::TF tf>
  ClKernelInvocation& arg(Engine::Gpu::Tex<tf>& tex) {
    CL(clSetKernelArg(k, current_arg_idx++, sizeof(cl_mem), tex.get_cl_mem_addr()));
    return *this;
  }

  template <class T>
  ClKernelInvocation& arg(Engine::Gpu::Buff<T>& buff) {
    CL(clSetKernelArg(k, current_arg_idx++, sizeof(cl_mem), buff.get_cl_mem_addr()));
    return *this;
  }

  template <class T>
  ClKernelInvocation& arg(T val) {
    CL(clSetKernelArg(k, current_arg_idx++, sizeof(T), &val));
    return *this;
  }

  // declares a work-group-local piece of memory of given size
  ClKernelInvocation& arg_local(size_t size) {
    CL(clSetKernelArg(k, current_arg_idx++, size, NULL));
    return *this;
  }

  void call() {

    if (!dims_set) {
      printf("dims must be set before invoking kernel!");
      throw new invalid_argument(":)");
    }
    CL(clEnqueueNDRangeKernel(
        Engine::compute_ctx.command_queue, k, num_dims, NULL, global, block, NULL, NULL, NULL));
  }

private:
  const cl_kernel k;

  bool dims_set = false;
  size_t global[3];
  size_t block[3];
  int num_dims;

  int current_arg_idx = 0;
};

class ClProgram {

public:
  void init(
      std::initializer_list<Engine::ProgramSource> programs,
      std::initializer_list<std::string> kernel_names) {

    for (Engine::ProgramSource p : programs) {
      load(p);
    }

    const char* c = full_string.c_str();
    size_t length = full_string.length();

    int cl_error;
    prog = clCreateProgramWithSource(Engine::compute_ctx.cl_gl_ctx, 1, &c, &length, &cl_error);
    if (cl_error) { throw new std::invalid_argument("Failed to create OpenCL Program"); }

    // Enforce CL 1.2!
    string flags = "-cl-fast-relaxed-math -cl-std=CL1.2";

    cl_error = clBuildProgram(prog, 0, NULL, flags.c_str(), NULL, NULL);
    if (cl_error) {
      char build_log[100240];
      clGetProgramBuildInfo(
          prog,
          Engine::compute_ctx.device_id,
          CL_PROGRAM_BUILD_LOG,
          sizeof(build_log),
          build_log,
          NULL);

      string b_l(build_log);

      printf("%s\n", build_log);
      throw new std::invalid_argument("Failed to build OpenCL Program!");
    }

    for (auto name : kernel_names) {
      kernels[name] = clCreateKernel(prog, name.c_str(), &cl_error);
      if (cl_error) {
        printf("couldn't find kernel named %s\n", name.c_str());
        throw new std::invalid_argument("Failed to create OpenCL Program");
      }
    }
  }

  ClKernelInvocation invoke(const string& name) {
    const auto k = get_kernel(name);
    ClKernelInvocation invocation(k);
    return invocation;
  }

private:
  cl_kernel get_kernel(const string& name) {
    if (!kernels.count(name)) {
      printf("Kernel %s not found!\n", name.c_str());
      throw new invalid_argument("invalid yo!");
    }
    return kernels[name];
  }

  ClProgram& load(Engine::ProgramSource& from) {
    full_string += from.get();
    return *this;
  }

  std::string full_string;
  cl_program prog;
  std::unordered_map<std::string, cl_kernel> kernels;
};

} // namespace Gpu
} // namespace Engine
