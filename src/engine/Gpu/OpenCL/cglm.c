// custom header file to load CGLM into OpenCL context
// replaces <cglm/cglm.h>, has to handle preprocessor definitions, etc.

// re-map calls to <math.h> functions to OpenCL built-ins
#define fabsf(x) fabs(x)
#define sqrtf(x) sqrt(x)
#define cosf(x) cos(x)
#define acosf(x) acos(x)
#define sinf(x) sin(x)

#define uint32_t uint

#define CGLM_ALL_UNALIGNED
#define cglm_common_h
#define CGLM_INLINE
#define GLM_SHUFFLE4(z, y, x, w) (((z) << 6) | ((y) << 4) | ((x) << 2) | (w))
#define GLM_SHUFFLE3(z, y, x) (((z) << 4) | ((y) << 2) | (x))

#include <cglm/types.h>
#include <cglm/vec3.h>
#include <cglm/vec4.h>
#include <cglm/mat4.h>
#include <cglm/mat3.h>
#include <cglm/affine.h>
#include <cglm/affine-mat.h>
