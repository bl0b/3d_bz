#pragma once

#include <engine/Engine.h>

#include <functional>

namespace Engine {
namespace Gpu {

template <TF _t_f>
void Tex<_t_f>::map_to_compute() {

  int err = 0;
  if (tex_cl == nullptr) {
    tex_cl = clCreateFromGLTexture(
        compute_ctx.cl_gl_ctx, CL_MEM_READ_WRITE, GL_TEXTURE_2D, 0, get().id(), &err);
    if (err) {
      printf("cl error: %d\n", err);
      throw new std::invalid_argument("Failed to create CL texture");
    }
  }

  if (!cl_aquired) {
    glFinish();
    CL(clEnqueueAcquireGLObjects(compute_ctx.command_queue, 1, &tex_cl, 0, 0, 0));
    cl_aquired = true;
  }
}

template <TF _t_f>
cl_mem Tex<_t_f>::get_cl_mem() {
  map_to_compute();
  return tex_cl;
}

template <TF _t_f>
cl_mem* Tex<_t_f>::get_cl_mem_addr() {
  map_to_compute();
  return &tex_cl;
}

template <TF _t_f>
void Tex<_t_f>::unmap_from_compute() {

  if (cl_aquired) {
    CL(clEnqueueReleaseGLObjects(compute_ctx.command_queue, 1, &tex_cl, 0, 0, 0));
    CL(clFinish(Engine::compute_ctx.command_queue));
    cl_aquired = false;
  }
}

template <TF _t_f>
void Tex<_t_f>::unregister_compute() {
  unmap_from_compute();
  // TODO: is this a memory leak in OpenCL?
  tex_cl = nullptr;
}

template <TF _t_f>
void Tex<_t_f>::cu_copy_from(const void* data, const glm::ivec2& d) {

  if (d.x != dims.x || d.y != dims.y) { set_storage(d); }

  map_to_compute();

  const size_t origin[3] = {0, 0, 0};
  const size_t dims[3] = {get_dims().x, get_dims().y, 1};

  CL(clEnqueueWriteImage(
      compute_ctx.command_queue, tex_cl, true, origin, dims, 0, 0, data, NULL, NULL, NULL));
}

template <TF _t_f>
template <class T>
void Tex<_t_f>::cu_copy_to(Buff<T>& buff) {
  buff.set_storage(get_dims().x * get_dims().y);
  map_to_compute();
  const size_t origin[3] = {0, 0, 0};
  const size_t dims[3] = {get_dims().x, get_dims().y, 1};
  CL(clEnqueueCopyImageToBuffer(
      compute_ctx.command_queue, tex_cl, buff.get_cl_mem(), origin, dims, 0, NULL, NULL, NULL));
}

template <TF _t_f>
void Tex<_t_f>::cu_copy_to(Tex<_t_f>& tex) {

  tex.set_storage(get_dims());
  map_to_compute();
  const size_t origin[3] = {0, 0, 0};
  const size_t dims[3] = {get_dims().x, get_dims().y, 1};

  CL(clEnqueueCopyImage(
      compute_ctx.command_queue, tex_cl, tex.get_cl_mem(), origin, origin, dims, NULL, NULL, NULL));
}

template <TF _t_f>
template <class T>
void Tex<_t_f>::cu_copy_to(vector<T>& vec) {

  map_to_compute();

  vec.resize(get_dims().x * get_dims().y);
  const size_t origin[3] = {0, 0, 0};
  const size_t dims[3] = {get_dims().x, get_dims().y, 1};
  CL(clEnqueueReadImage(
      compute_ctx.command_queue,
      tex_cl,
      true,
      origin,
      dims,
      0,
      // sizeof(T) * get_dims().x,
      0,
      vec.data(),
      0,
      0,
      0));
}

template <TF _t_f>
template <class T>
void Tex<_t_f>::cu_copy_single_value(const glm::ivec2 coord, T* val_ptr) {
  const size_t origin[3] = {coord.x, coord.y, 0};
  const size_t dims[3] = {1, 1, 1};
  CL(clEnqueueReadImage(
      compute_ctx.command_queue,
      get_cl_mem(),
      true,
      origin,
      dims,
      0,
      0,
      val_ptr,
      NULL,
      NULL,
      NULL));
}

template <TF _t_f>
template <class T>
void Tex<_t_f>::cu_copy_to(T* d) {
  if (!pixel_size == sizeof(T)) { throw invalid_argument("Data type sizes must match!"); }

  const size_t origin[3] = {0, 0, 0};
  const size_t dims[3] = {get_dims().x, get_dims().y, 1};

  CL(clEnqueueReadImage(
      compute_ctx.command_queue, get_cl_mem(), true, origin, dims, 0, 0, d, NULL, NULL, NULL));
}

template <TF _t_f>
template <class T>
void Tex<_t_f>::cu_copy_from(Engine::Gpu::Buff<T>& buff, const glm::ivec2& dims) {

  if (!pixel_size == sizeof(T)) { throw invalid_argument("data type sizse must match!"); }
  set_storage(dims);

  const size_t origin[3] = {0, 0, 0};
  const size_t sz_dims[3] = {dims.x, dims.y, 1};

  CL(clEnqueueCopyBufferToImage(
      compute_ctx.command_queue,
      buff.get_cl_mem(),
      get_cl_mem(),
      0,
      origin,
      sz_dims,
      NULL,
      NULL,
      NULL));
}

template <TF _t_f>
template <typename T>
void Tex<_t_f>::cu_fill(T val) {
  if (!pixel_size == sizeof(T)) { throw invalid_argument("data type sizse must match!"); }

  const size_t origin[3] = {0, 0, 0};
  const size_t sz_dims[3] = {dims.x, dims.y, 1};

  CL(clEnqueueFillImage(
      compute_ctx.command_queue, get_cl_mem(), &val, origin, sz_dims, NULL, NULL, NULL));
}

} // namespace Gpu
} // namespace Engine
