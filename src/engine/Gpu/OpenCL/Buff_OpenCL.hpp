#pragma once

#include <engine/Engine.h>

#include <functional>

namespace Engine {
namespace Gpu {

template <class T>
void Buff<T>::unregister_compute() {
  unmap_from_compute();
  // TODO: is this a memory leak in OpenCL?
  vbo_cl = nullptr;
}

template <class T>
void Buff<T>::unmap_from_compute() {
  if (cl_aquired) {
    CL(clEnqueueReleaseGLObjects(compute_ctx.command_queue, 1, &vbo_cl, 0, 0, 0));
    CL(clFinish(Engine::compute_ctx.command_queue));
    cl_aquired = false;
  }
}

template <class T>
void Buff<T>::cu_copy_from(T* data, int num_els) {

  if (num_els) {
    set_storage(num_els);
    CL(clEnqueueWriteBuffer(
        compute_ctx.command_queue,
        get_cl_mem(),
        true,
        0,
        sizeof(T) * num_els,
        data,
        NULL,
        NULL,
        NULL));
  }
}

template <class T>
void Buff<T>::cu_memset(unsigned char val) {
  CL(clEnqueueFillBuffer(
      compute_ctx.command_queue,
      get_cl_mem(),
      &val,
      sizeof(unsigned char),
      0,
      sizeof(T) * length,
      NULL,
      NULL,
      NULL));
}

template <class T>
cl_mem Buff<T>::get_cl_mem() {
  map_to_compute();
  return vbo_cl;
}

template <class T>
cl_mem* Buff<T>::get_cl_mem_addr() {
  map_to_compute();
  return &vbo_cl;
}

template <class T>
void Buff<T>::map_to_compute() {

  int err = 0;
  if (vbo_cl == nullptr) {
    vbo_cl = clCreateFromGLBuffer(compute_ctx.cl_gl_ctx, CL_MEM_READ_WRITE, get_gl().id(), &err);
    if (err) { throw new std::invalid_argument("Failed to create CL buffer"); }
  }

  if (!cl_aquired) {
    glFinish();
    CL(clEnqueueAcquireGLObjects(compute_ctx.command_queue, 1, &vbo_cl, 0, 0, 0));
    cl_aquired = true;
  }
}

template <class T>
void Buff<T>::cu_copy_to(T* host_ptr) {
  cu_copy_to(host_ptr, length);
}

template <class T>
void Buff<T>::cu_copy_to(Buff<T>& buff) {
  buff.set_storage(length);
  map_to_compute();
  CL(clEnqueueCopyBuffer(
      compute_ctx.command_queue,
      get_cl_mem(),
      buff.get_cl_mem(),
      0,
      0,
      sizeof(T) * length,
      NULL,
      NULL,
      NULL));
}

} // namespace Gpu
} // namespace Engine
