#pragma once

// WGL functions, used to line up contexts between GL and CL
// (this specific code is windows specific)
// todo: does magnum/glfw load this automatically?
#ifdef _MSC_VER
#include <windows.h>
#include <wingdi.h>
#endif
#ifdef __GNUC__
#include <GL/glx.h>
#endif

#include <CL/cl.h>
#include <CL/cl_gl.h>

#include <string>
#include <vector>
#include <unordered_map>

static void HANDLE_CL_ERROR(cl_uint err, const char* file, int line) {
  if (err != CL_SUCCESS) {
    std::printf("OpenCL error: %d\n", err);
    throw new std::invalid_argument("cuda error!");
  }
}

// Error handling wrapper for calls to cuda runtime API
#define CL(err) (HANDLE_CL_ERROR(err, __FILE__, __LINE__))

class ComputeCtx {
public:
  cl_device_id device_id;
  cl_context cl_gl_ctx;
  cl_command_queue command_queue;

  ComputeCtx() {}

  enum GpuVendor { NV = 1, AMD = 2, INTEL = 3 };
  GpuVendor cl_platform_vendor;

  bool initialized = false;

  void call() { printf("c"); }

  void init() {

    const auto gl_vendor = get_current_gl_vendor();

    std::string cl_platform_name;

    const std::vector<cl_platform_info> infos{
        // CL_PLATFORM_NAME,
        // CL_PLATFORM_PROFILE,
        CL_PLATFORM_VERSION,
        CL_PLATFORM_VENDOR,
        // CL_PLATFORM_EXTENSIONS
    };

#ifdef _MSC_VER
    cl_context_properties props[] = {CL_GL_CONTEXT_KHR,
                                     (cl_context_properties)wglGetCurrentContext(),
                                     CL_WGL_HDC_KHR,
                                     (cl_context_properties)wglGetCurrentDC(),
                                     CL_CONTEXT_PLATFORM,
                                     // Fill in below!
                                     0,
                                     0};
#endif
#ifdef __GNUC__
    cl_context_properties props[] = {
        CL_GL_CONTEXT_KHR,   (cl_context_properties)glXGetCurrentContext(),
        CL_GLX_DISPLAY_KHR,  (cl_context_properties)glXGetCurrentDisplay(),
        CL_CONTEXT_PLATFORM, (cl_context_properties)0, // Fill in below
        0
    };
#endif

    // Get the platform
    cl_uint num_platforms = 0;
    CL(clGetPlatformIDs(0, NULL, &num_platforms));

    std::vector<cl_platform_id> platforms(num_platforms);
    CL(clGetPlatformIDs(num_platforms, platforms.data(), NULL));

    int selected_platform = -1;
    for (int i = 0; i < num_platforms; i++) {

      std::string pform_name;

      cl_platform_id platform_id = platforms[i];

      printf("Available OpenCL platform:\n");
      for (auto info : infos) {
        char* v = new char[2048];
        CL(clGetPlatformInfo(platform_id, info, sizeof(char) * 2048, v, NULL));
        if (info == CL_PLATFORM_VERSION) { pform_name = std::string(v); }
        printf("  %s\n", v);
        delete[] v;
      }

      const auto cl_vendor = get_cl_vendor(platform_id);

      if (!platform_meets_minimum(platform_id, 1, 2)) {
        printf("Doesn't meet minimum version of %d.%d\n", 1, 2);
      }

      // todo: is this too brittle? find better way to figure out which OpenCL device can talk to
      // the current OpenGL context
      if (cl_vendor != gl_vendor) { continue; }

      if (selected_platform == -1) {

        props[5] = (cl_context_properties)platform_id;

        // up to 16 devices... loop through them until we can create an OpenCL that shares with
        // OpenGL. Would use clGetGLContextInfoKHR, but somehow this breaks things on intel
        // integrated GPU..
        uint32_t num_devices;
        cl_device_id device_ids[16];
        CL(clGetDeviceIDs(platform_id, CL_DEVICE_TYPE_GPU, 16, device_ids, &num_devices));

        bool context_created = false;
        for (int j = 0; j < num_devices && !context_created; j++) {
          // create GL-CL context
          int cl_error = 0;
          cl_gl_ctx = clCreateContext(props, 1, device_ids + j, NULL, NULL, &cl_error);

          if (cl_error) {
            cl_gl_ctx = nullptr;
            printf("failed to create context... cl error %d\n", cl_error);
          } else {
            device_id = device_ids[j];
            selected_platform = i;
            printf("Context created!: %s\n", pform_name.c_str());
            cl_platform_vendor = cl_vendor;
            context_created = true;
          }
        }
      }
    }

    if (selected_platform == -1) {
      printf("Suitable OpenCL platform not found..\n");
      throw new std::invalid_argument("Suitable OpenCL platform not found!");
    }

    int cl_error;
    command_queue = clCreateCommandQueue(cl_gl_ctx, device_id, 0, &cl_error);

    if (cl_error) {
      printf("failed to create command queue: %d\n", cl_error);
      throw new std::invalid_argument("command queue");
    }

    initialized = true;
  }

  ~ComputeCtx() {}

private:
  static GpuVendor get_current_gl_vendor() {
    const std::string vendor_string = std::string((const char*)glGetString(GL_VENDOR));

    if (!vendor_string.compare("NVIDIA Corporation")) {
      return GpuVendor::NV;
    } else if (!vendor_string.compare("ATI Technologies Inc.")) {
      return GpuVendor::AMD;
    } else if (!vendor_string.compare("Intel")) {
      return GpuVendor::INTEL;
    }

    printf(
        "could not determine vendor for OpenGL platform. vendor string: %s\n",
        vendor_string.c_str());
  }

  static GpuVendor get_cl_vendor(cl_platform_id id) {

    char* v = new char[2048];
    CL(clGetPlatformInfo(id, CL_PLATFORM_VENDOR, sizeof(char) * 2048, v, NULL));

    const std::string vendor_string = std::string(v);

    if (!vendor_string.compare("NVIDIA Corporation")) {
      return GpuVendor::NV;
    } else if (!vendor_string.compare("Advanced Micro Devices, Inc.")) {
      return GpuVendor::AMD;
    } else if (!vendor_string.compare("Intel(R) Corporation")) {
      return GpuVendor::INTEL;
    }

    printf(
        "could not determine vendor for OpenCL platform. vendor string: %s\n",
        vendor_string.c_str());

    delete[] v;
  }

  static bool platform_meets_minimum(cl_platform_id p_id, int maj, int min) {
    std::vector<char> version(512);
    CL(clGetPlatformInfo(
        p_id, CL_PLATFORM_VERSION, sizeof(char) * version.size(), version.data(), NULL));
    std::string version_str(version.data());

    size_t pos = 0;
    std::vector<std::string> els;
    while ((pos = version_str.find(" ")) != std::string::npos) {
      std::string token = version_str.substr(0, pos);
      version_str.erase(0, pos + 1);
      els.push_back(token);
    }
    els.push_back(version_str);

    int found_maj = std::stoi(els[1].substr(0, 1));
    int found_min = std::stoi(els[1].substr(2, 1));

    return found_maj > maj || (found_maj == maj && found_min >= min);
  }
};
