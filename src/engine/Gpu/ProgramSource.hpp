#pragma once

#include <engine/Engine.h>

#include <string.h>

namespace Engine {

class LoadableText {
public:
  virtual std::string get() = 0;
};

// shader/kernels can be found in <generated/filename_suffix.hpp>
// they declare objects of this type, depending on whether the kernel
// itself can be found in a text file, or the source code is included
// in the object.
class ProgramSource : LoadableText {
public:
  std::string file;
  bool pre_loaded;
  std::string suffix;

  std::string get() override {
    return pre_loaded ? file : FileUtil::load_text(FileUtil::exe_dir + file);
  }

  // final
  static ProgramSource from_many(std::initializer_list<ProgramSource> sources, std::string suffix) {
      ProgramSource new_p;
      new_p.pre_loaded = true;
      new_p.suffix = suffix;
      for (auto s : sources) {
        new_p.file += s.get();
      }
      return new_p;
  }
};


} // namespace Engine