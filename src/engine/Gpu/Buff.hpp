#pragma once

#include <engine/Engine.h>

namespace Engine {
namespace Gpu {

template <typename T>
class Buff : Magnum::GL::Buffer {
public:
  Buff(
      const int num_elements = 1,
      Magnum::GL::BufferUsage usage = Magnum::GL::BufferUsage::DynamicDraw) {
    set_storage(num_elements, usage);
  }

  Buff(std::vector<T>& data, Magnum::GL::BufferUsage usage = Magnum::GL::BufferUsage::DynamicDraw) {
    set_data(data, usage);
  }

  void set_data(
      std::vector<T>& data, Magnum::GL::BufferUsage usage = Magnum::GL::BufferUsage::DynamicDraw) {
    unmap_from_compute();
    if (data.size() > length) {
      length = data.size();
      unregister_compute();
    }

    setData(Corrade::Containers::ArrayView<T>(data.data(), data.size()), usage);
  }

  void bind(unsigned int target, Magnum::GL::Buffer::Target buffer_type) {
    get_gl().bind(buffer_type, target);
  }

  void set_storage(
      const int num_elements,
      Magnum::GL::BufferUsage usage = Magnum::GL::BufferUsage::DynamicDraw) {
    if (num_elements > length) {
      unregister_compute();
      length = num_elements;
      setData(Corrade::Containers::ArrayView<T>(nullptr, num_elements), usage);
    }
  }

  Magnum::GL::Buffer& get_gl() {
    unmap_from_compute();
    return (Magnum::GL::Buffer&)(*this);
  }

  int size() { return length; }

  /*
  T* get_cuda_dev_ptr() {
    map_to_compute();
    return cuda_dev_ptr;
  }
   */

  void acquire_cl();

  // ughh why does it have to ba uchar (OpenCL doesn't have this problem)
  void cu_memset(unsigned char val);

  void cu_copy_from(T* host_ptr, const int num_els);

  void cu_copy_to(T* host_ptr, const int num_els, const size_t offset = 0, const bool block = true) {
    if (num_els) {
      map_to_compute();
      CL(clEnqueueReadBuffer(
          compute_ctx.command_queue, vbo_cl, block, sizeof(T) * offset, sizeof(T) * num_els, host_ptr, 0, 0, 0));
    }
  }

  // make this just call other cu_copy_to
  void cu_copy_to(T* host_ptr);

  void cu_copy_to(Buff<T>& other_buff);

  void cu_copy_to(vector<T>& host_vector, const unsigned int num_els) {
    if (host_vector.size() < num_els) { host_vector.resize(num_els); }
    cu_copy_to(host_vector.data(), num_els);
  }

  void cu_copy_to(vector<T>& host_vector) { cu_copy_to(host_vector, length); }

  vector<T> cu_to_vector(const unsigned int num_els) {
    vector<T> h;
    cu_copy_to(h, num_els);
    return std::move(h);
  }

  vector<T> cu_to_vector() { return cu_to_vector(length); }


public:
  cl_mem get_cl_mem();
  cl_mem* get_cl_mem_addr();

private:
  cl_mem vbo_cl = nullptr;
  bool cl_aquired = false;

  // number of elements allocated in buffer
  int length = 0;

  void map_to_compute();
  void unmap_from_compute();
  void unregister_compute();
};

} // namespace Gpu
} // namespace Engine