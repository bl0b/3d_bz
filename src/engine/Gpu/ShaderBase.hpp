#pragma once

#include <engine/Engine.h>

#include <fstream>
#include <iostream>
#include <sstream>
#include <string>

namespace Engine {
namespace Gpu {

class ShaderBase : public Magnum::GL::AbstractShaderProgram {
public:
  ShaderBase& load(Engine::ProgramSource& from) {
    Magnum::GL::Shader shader{
        static_cast<Magnum::GL::Version>(0xFFFF), get_shader_type(from.suffix)};
    const string file_source = from.get();
    shader.addSource(file_source);
    CORRADE_INTERNAL_ASSERT_OUTPUT(Magnum::GL::Shader::compile({shader}));
    attachShader(shader);
    return *this;
  }

  void finish() { CORRADE_INTERNAL_ASSERT_OUTPUT(link()); }

  Magnum::GL::Shader::Type get_shader_type(const string suffix) {
    if (has_suffix(suffix, "frag")) {
      return Magnum::GL::Shader::Type::Fragment;
    } else if (has_suffix(suffix, "vert")) {
      return Magnum::GL::Shader::Type::Vertex;
    } else if (has_suffix(suffix, "geom")) {
      return Magnum::GL::Shader::Type::Geometry;
    } else {
      // all compute shaders are OpenCL, so not defined here
      throw invalid_argument("Not implemented yet!");
    }
  }

  explicit ShaderBase() {}

  void set_uniform(string name, Magnum::Int value) {
    setUniform(uniformLocation(name), value);
  }

  void set_uniform(string name, Magnum::Float value) {
    setUniform(uniformLocation(name), value);
  }

  void set_uniform(string name, Magnum::UnsignedInt value) {
    setUniform(uniformLocation(name), value);
  }

  void set_uniform(string name, Magnum::Double value) {
    setUniform(uniformLocation(name), value);
  }

  void set_uniform(string name, const glm::vec2& v) {
    Magnum::Math::Vector<2, float> m_v{v};
    setUniform(uniformLocation(name), {&m_v, 1});
  }

  void set_uniform(string name, const glm::vec4& v) {
    Magnum::Math::Vector<4, float> m_v{v};
    setUniform(uniformLocation(name), {&m_v, 1});
  }

  void set_uniform(string name, const glm::ivec2& v) {
    Magnum::Math::Vector<2, int> m_v{v};
    setUniform(uniformLocation(name), {&m_v, 1});
  }

  void set_uniform(string name, const glm::ivec4& v) {
    Magnum::Math::Vector<4, int> m_v{v};
    setUniform(uniformLocation(name), {&m_v, 1});
  }

  template <std::size_t size>
  void set_uniform(string name, const glm::vec<size, float>& v) {
    Magnum::Math::Vector<size, float> m_v = Magnum::Math::Vector<size, float>{v};
    setUniform(uniformLocation(name), {&m_v, 1});
  }

  void set_uniform(string name, const glm::mat4& m) {
    Magnum::Math::RectangularMatrix<4, 4, float> m_m{m};
    setUniform(uniformLocation(name), {&m_m, 1});
  }

  /*
  template <std::size_t rows, std::size_t cols, class T, glm::precision q>
  void set_uniform(string name, const glm::mat<rows, cols, T, q>& m) {
    Magnum::Math::RectangularMatrix<rows, cols, T> m_m =
        Magnum::Math::RectangularMatrix<rows, cols, T>{m};
    setUniform(uniformLocation(name), {&m_m, 1});
  }
   */

  void dispatch_compute(glm::uvec3 dims) { dispatchCompute(Magnum::Vector3ui{dims}); }

private:
  /* Just silly string util function */
  bool has_suffix(const std::string& str, const std::string& suffix) {
    return str.size() >= suffix.size() &&
           str.compare(str.size() - suffix.size(), suffix.size(), suffix) == 0;
  }
};

} // namespace Gpu
} // namespace Engine