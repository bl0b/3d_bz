#pragma once

#include "OpenCL/ComputeCtx_OpenCL.hpp"

#include "ProgramSource.hpp"

namespace Engine {
extern ComputeCtx compute_ctx;
} // namespace Engine

#include "Buff.hpp"
#include "Tex.hpp"

// OpenCL-specific impl files
#include "OpenCL/Buff_OpenCL.hpp"
#include "OpenCL/Tex_OpenCL.hpp"

#include "OpenCL/ClProgram.hpp"

#include "GraphicsCtx_OpenGL.hpp"
#include "GlUtil.hpp"
#include "ShaderBase.hpp"
