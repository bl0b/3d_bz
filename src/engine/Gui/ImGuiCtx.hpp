#pragma once

#include <engine/Engine.h>

#include <IconsFontAwesome4.h>

using namespace Magnum;
using namespace std;

namespace Engine {

/* Helper class hiding some ImGui boilerplate */
class ImGuiCtx {
public:
  static void init(Gpu::GraphicsCtx* window) {
    IMGUI_CHECKVERSION();

    ImGui::CreateContext();

    ImGuiIO& gui_io = ImGui::GetIO();

    /*
    // Keep this code! It can be used to configure imgui so save configuration settings in users
    // AppData directory

string ini_name = FileUtil::cfg_dir + "imgui.ini";
char* ini_name_c = new char[ini_name.size() + 1];
strcpy(ini_name_c, ini_name.c_str());
gui_io.IniFilename = ini_name_c;
    */

    gui_io.Fonts->AddFontDefault();

    static const ImWchar icons_ranges[] = {ICON_MIN_FA, ICON_MAX_FA, 0};
    ImFontConfig icons_config;
    icons_config.MergeMode = true;
    icons_config.OversampleH = true;
    icons_config.OversampleV = true;
    icons_config.GlyphMinAdvanceX = 13.0f;

    const string filename = FONT_ICON_FILE_NAME_FA;
    const string fullname = FileUtil::exe_dir + "assets/" + filename;

    gui_io.Fonts->AddFontFromFileTTF(fullname.c_str(), 13.0f, &icons_config, icons_ranges);

    ImGui_ImplGlfw_InitForOpenGL(window->handle, true);
    ImGui_ImplOpenGL3_Init("#version 330 core");
    ImGui::StyleColorsDark();
  }

  static void new_frame() {
    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplGlfw_NewFrame();
    ImGui::NewFrame();
  }

  static void draw() {
    ImGui::Render();
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
  }

  static void destroy() {
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();
  }

  static glm::ivec2
  get_projected_coordinate(glm::ivec2 orig_coord, int rot, bool mirror, glm::ivec2 img_dims) {

    glm::ivec2 projected_pos;

    if (mirror) {
      switch (rot) {
      case 0:
      case 2:
        orig_coord.x = img_dims.x - orig_coord.x - 1;
        break;

      case 1:
      case 3:
        orig_coord.y = img_dims.y - orig_coord.y - 1;
        break;
      }
    }

    switch (rot) {
    case 0:
      projected_pos = orig_coord;
      break;
    case 1:
      projected_pos = {
          orig_coord.y,
          img_dims.x - orig_coord.x - 1,
      };
      break;
    case 2:
      projected_pos = {
          img_dims.x - orig_coord.x - 1,
          img_dims.y - orig_coord.y - 1,
      };
      break;
    case 3:
      projected_pos = {
          img_dims.y - orig_coord.y - 1,
          orig_coord.x,
      };
      break;
    }

    return projected_pos;
  }

  static glm::ivec2 get_original_coordinate(
      glm::ivec2 sampled_coordinates, int rot, bool mirror, glm::ivec2 img_dims) {

    glm::ivec2 cursor_pos_on_orig_image;

    switch (rot) {
    case 0:
      cursor_pos_on_orig_image = sampled_coordinates;
      break;
    case 1:
      cursor_pos_on_orig_image = {img_dims.x - sampled_coordinates.y - 1, sampled_coordinates.x};
      break;
    case 2:
      cursor_pos_on_orig_image = {
          img_dims.x - sampled_coordinates.x - 1,
          img_dims.y - sampled_coordinates.y - 1,
      };
      break;
    case 3:
      cursor_pos_on_orig_image = {sampled_coordinates.y, img_dims.y - sampled_coordinates.x - 1};
      break;
    }

    if (mirror) {
      switch (rot) {
      case 0:
      case 2:
        cursor_pos_on_orig_image.x = img_dims.x - cursor_pos_on_orig_image.x - 1;
        break;

      case 1:
      case 3:
        cursor_pos_on_orig_image.y = img_dims.y - cursor_pos_on_orig_image.y - 1;
        break;
      }
    }

    return cursor_pos_on_orig_image;
  }

  static inline ImVec2 imVec(glm::vec2 v) { return ImVec2{1.f * v.x, 1.f * v.y}; }
  static inline glm::vec2 glmVec(ImVec2 v) { return glm::vec2{1.f * v.x, 1.f * v.y}; }

  // Draws image with hover & click handling!
  template <Engine::Gpu::TF T>
  static void Image2(
      Engine::Gpu::Tex<T>& img,
      bool mirror = false,
      int rot = 0, // 0, 1, 2, 3
      function<void(glm::ivec2)> on_click = [](glm::ivec2) {},
      function<void(glm::ivec2)> on_hover = [](glm::ivec2) {},
      function<void(void)> on_click_up = [] {},
      bool specify_pos_start = false,
      glm::vec2 pos_start = {0, 0}) {

    if (!specify_pos_start) { pos_start = glmVec(ImGui::GetCursorScreenPos()); }

    ImDrawList* draw_list = ImGui::GetWindowDrawList();

    const glm::ivec2 img_dims = img.get_dims();

    float max_x = rot == 0 || rot == 2 ? img_dims.x : img_dims.y;
    float max_y = rot == 0 || rot == 2 ? img_dims.y : img_dims.x;

    ImVec2 screen_top_l = imVec(pos_start + glm::vec2{0, 0});
    ImVec2 screen_top_r = imVec(pos_start + glm::vec2{max_x, 0});
    ImVec2 screen_bot_r = imVec(pos_start + glm::vec2{max_x, max_y});
    ImVec2 screen_bot_l = imVec(pos_start + glm::vec2{0, max_y});

    ImVec2 pos[4] = {mirror ? screen_top_r : screen_top_l,
                     mirror ? screen_top_l : screen_top_r,
                     mirror ? screen_bot_l : screen_bot_r,
                     mirror ? screen_bot_r : screen_bot_l};

    ImVec2 img_top_l{0.f, 0.};
    ImVec2 img_top_r{1.f, 0.};
    ImVec2 img_bot_r{1.f, 1.};
    ImVec2 img_bot_l{0.f, 1.};

    vector<ImVec2> uvs(4);
    switch (rot) {
    case 0:
      uvs = {img_top_l, img_top_r, img_bot_r, img_bot_l};
      break;
    case 1:
      uvs = {img_top_r, img_bot_r, img_bot_l, img_top_l};
      break;
    case 2:
      uvs = {img_bot_r, img_bot_l, img_top_l, img_top_r};
      break;
    case 3:
      uvs = {img_bot_l, img_top_l, img_top_r, img_bot_r};
      break;
    }

    draw_list->AddImageQuad(
        (void*)img.get().id(),
        pos[0],
        pos[1],
        pos[2],
        pos[3],
        uvs[0],
        uvs[1],
        uvs[2],
        uvs[3],
        IM_COL32_WHITE);

    if (ImGui::IsWindowHovered()) {

      glm::ivec2 hover_pos = {(int)round(ImGui::GetMousePos().x - pos_start.x),
                              (int)round(ImGui::GetMousePos().y - pos_start.y)};

      bool hovering =
          hover_pos.x >= 0 && hover_pos.y >= 0 && hover_pos.x < max_x && hover_pos.y < max_y;

      if (hovering) {
        glm::ivec2 orig_coords = get_original_coordinate(hover_pos, rot, mirror, img_dims);
        on_hover(orig_coords);
        // 0 is left mouse button :)
        // ImGui automatically debounces as well
        if (ImGui::IsMouseClicked(0)) { on_click(orig_coords); }

        if (ImGui::IsMouseReleased(0)) { on_click_up(); }

        // ImGui::IsM
        // if (ImGui::IsM)
      }
    }

    ImGui::SetCursorScreenPos({pos_start.x, pos_start.y + max_y});
  }
};

} // namespace Engine