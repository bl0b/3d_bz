#pragma once

namespace Engine {
namespace Gui {

class GuiElement {
public:
  virtual void draw_imgui() = 0;
};

} // namespace Gui
} // namespace Engine
