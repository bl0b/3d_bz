#pragma once

// Load windows-specific MIDI headers.
#ifdef _MSC_VER
#include <windows.h>
#include <mmsystem.h>
// TODO: this won't work with MinGW, and most likely not on linux either
#pragma comment(lib, "winmm.lib")
#endif
#include <RtMidi.h>

namespace Components {

class Midi {
private:
  RtMidiOut* midi_out;

  // todo: this really should be handled by dependency injection!
  const ImVec4 button_error_active_color{0.9, 0.2, 0.2, 1.};
  const ImVec4 button_error_hover_color{0.8, 0.15, 0.15, 1.};
  const ImVec4 button_error_normal_color{0.7, 0.15, 0.15, 1.};

public:
  struct Config {
    string active_port_name = "";
    int active_port_number = -1;
  };

  int active_port = -1;

  bool ports_available = false;

  vector<string> available_midi_ports;
  vector<string> banned_midi_prefixes = {"Microsoft GS Wavetable"};

  Midi() {
    try {
      midi_out = new RtMidiOut();
      const int num_ports = midi_out->getPortCount();
      int non_banned_available = -1;
      for (int i = 0; i < num_ports; i++) {
        const string port_name = midi_out->getPortName(i);
        available_midi_ports.push_back(port_name);
        // By default, select first non-banned MIDI port
        if (non_banned_available == -1 && !is_banned(port_name)) {
          non_banned_available = i;
          ports_available = true;
        }
      }

      if (non_banned_available > -1) { set_out_port(non_banned_available); }
    } catch (RtMidiError& error) {
      error.printMessage();
      active_port = -1;
    }
  }

  void set_config(Config new_config) {
    if (new_config.active_port_number == -1) { close_port_if_open(); }

    if (new_config.active_port_number == active_port) { return; }

    if (new_config.active_port_number > available_midi_ports.size()) { return; }

    if (is_banned(new_config.active_port_name)) { return; }

    if (available_midi_ports[new_config.active_port_number] != new_config.active_port_name) {
      return;
    }

    set_out_port(new_config.active_port_number);
  }

  Config get_config() {
    Config cfg;
    cfg.active_port_number = active_port;
    cfg.active_port_name = active_port == -1 ? "" : available_midi_ports[active_port];
    return cfg;
  }

  void draw_ui() {
    ImGui::Text("Midi out port");
    if (ImGui::RadioButton("None", active_port == -1)) { close_port_if_open(); }
    for (int i = 0; i < available_midi_ports.size(); i++) {
      const string name = available_midi_ports[i];
      if (!is_banned(name)) {
        if (ImGui::RadioButton(available_midi_ports[i].c_str(), active_port == i)) {
          set_out_port(i);
        }
      }
    }
  }

  void draw_imgui_pretty() {

    ImGui::Text("MIDI out: ");
    ImGui::SameLine();

    const auto button_pos = ImGui::GetCursorScreenPos();
    if (active_port == -1) {

      ImGui::PushStyleColor(ImGuiCol_Button, button_error_normal_color);
      ImGui::PushStyleColor(ImGuiCol_ButtonActive, button_error_active_color);
      ImGui::PushStyleColor(ImGuiCol_ButtonHovered, button_error_hover_color);

      if (ImGui::Button("None")) { ImGui::OpenPopup("midi editor"); }

      ImGui::PopStyleColor();
      ImGui::PopStyleColor();
      ImGui::PopStyleColor();

    } else {
      if (ImGui::Button(get_config().active_port_name.c_str())) { ImGui::OpenPopup("midi editor"); }
    }

    ImGui::SetNextWindowPos({button_pos.x - 32, button_pos.y - 13});
    ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, {12, 12});

    if (ImGui::BeginPopupModal(
            "midi editor", NULL, ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoTitleBar)) {

      if (ports_available) {

        if (radio_buttons()) { ImGui::CloseCurrentPopup(); }

      } else {

        // This should only ever apply to windows - linux provides
        // virtual MIDI ports out of the box.
        ImGui::Text("There are no available MIDI output ports");
        ImGui::Text("Consider the free");
        ImGui::SameLine();
        if (ImGui::SmallButton("LoopBe1")) {
          // use system call to open link in browser
          std::system("start https://www.nerds.de/en/loopbe1.html");
        }
      }

      if (ImGui::GetIO().MouseClicked[0] && !ImGui::IsMouseHoveringWindow()) {
        ImGui::CloseCurrentPopup();
      }

      ImGui::EndPopup();
    }

    ImGui::PopStyleVar();
  }

  bool radio_buttons() {
    bool clicked = false;

    ImGui::PushStyleVar(ImGuiStyleVar_FramePadding, {4, 4});

    if (ImGui::RadioButton("None", active_port == -1)) {
      close_port_if_open();
      clicked = true;
    }
    for (int i = 0; i < available_midi_ports.size(); i++) {
      const string name = available_midi_ports[i];
      if (!is_banned(name)) {
        if (ImGui::RadioButton(available_midi_ports[i].c_str(), active_port == i)) {
          set_out_port(i);
          clicked = true;
        }
      }
    }

    ImGui::PopStyleVar();

    return clicked;
  }

  ~Midi() {
    close_port_if_open();
    delete midi_out;
  }

  void send(unsigned char a, unsigned char b, unsigned char c) {

    if (active_port == -1) { printf("Note not sent, no open midi out port\n"); }

    std::vector<unsigned char> message;
    message.push_back(a);
    message.push_back(b);
    message.push_back(c);

    midi_out->sendMessage(&message);
  }

private:
  bool is_banned(string name) {
    bool banned = false;
    for (auto banned_prefix : banned_midi_prefixes) {
      if (name.rfind(banned_prefix, 0) == 0) { return true; };
    }
    return false;
  }

  void close_port_if_open() {
    if (midi_out->isPortOpen()) { midi_out->closePort(); }
    active_port = -1;
  }

  // Sets the output MIDI port by port number. Closes any
  // active port, then opens the specified port.
  void set_out_port(const int port_num) {
    close_port_if_open();
    midi_out->openPort(port_num);
    active_port = port_num;

    if (!midi_out->isPortOpen()) { printf("fail!\n"); }
  }
};

} // namespace Components
