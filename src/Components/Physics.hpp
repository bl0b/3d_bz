#pragma once

#include <engine/Engine.h>
#include <btBulletDynamicsCommon.h>
//#include "btBulletDynamicsCommon.h"

#include <BulletDynamics/Featherstone/btMultiBodyDynamicsWorld.h>
#include <BulletDynamics/Featherstone/btMultiBodyConstraintSolver.h>
#include <BulletDynamics/Featherstone/btMultiBodyPoint2Point.h>
#include <BulletDynamics/Featherstone/btMultiBodyLinkCollider.h>

namespace PhysicsConversion {
/*
btTransform toBt(glm::mat4& m) {
  btTransform m2;
  m2.setFromOpenGLMatrix(&m[0].x);
  return m2;
}
 */
// Gah! MSVC > clang for allowing inline reference types (function above..)
btTransform toBt(glm::mat4 m) {
  btTransform m2;
  m2.setFromOpenGLMatrix(&m[0].x);
  return m2;
}

btVector3 toBt(glm::vec3 v) { return btVector3{v.x, v.y, v.z}; }
/*
glm::mat4 toGlm(btTransform& m) {
  glm::mat4 m2;
  m.getOpenGLMatrix(&m2[0].x);
  return m2;
}
 */
glm::mat4 toGlm(const btTransform m) {
  glm::mat4 m2;
  m.getOpenGLMatrix(&m2[0].x);
  return m2;
}

} // namespace PhysicsConversion

class PhysicsCtx {

public:
  // blah so many pointers!
  btDefaultCollisionConfiguration* m_collision_configuration;
  btCollisionDispatcher* m_dispatcher;
  btSimpleBroadphase* m_broadphase;
  btMultiBodyConstraintSolver* m_solver;
  btMultiBodyDynamicsWorld* m_dynamics_world;

  PhysicsCtx() {
    m_collision_configuration = new btDefaultCollisionConfiguration();
    m_dispatcher = new btCollisionDispatcher(m_collision_configuration);
    m_broadphase = new btSimpleBroadphase();
    m_solver = new btMultiBodyConstraintSolver();

    m_dynamics_world = new btMultiBodyDynamicsWorld(
        m_dispatcher, m_broadphase, m_solver, m_collision_configuration);
  }
};


// physics object wrapper..
//template <typename T>
class PhysicsObjectGeneric {
public:
  btCollisionShape* shape;
  btDefaultMotionState* motion_state;
  btRigidBody* body;


  void make(btCollisionShape* collision_shape, float mass, mat4 start_tform) {
    shape = collision_shape;//new T((btVector3&)size);
    vec4 box_local_inertia{0., 0., 0., 0.};
    shape->calculateLocalInertia(mass, (btVector3&)box_local_inertia);
    btTransform b_tform;
    b_tform.setFromOpenGLMatrix(&start_tform[0].x);
    motion_state = new btDefaultMotionState(b_tform);
    btRigidBody::btRigidBodyConstructionInfo box_c_info(
        mass, motion_state, shape, (btVector3&)box_local_inertia);
    body = new btRigidBody(box_c_info);
    body->setUserIndex(-1);
    body->setActivationState(DISABLE_DEACTIVATION);
  }

  mat4 get_current_tform() {
    mat4 tform{1.f};
    btTransform b_tform;
    body->getMotionState()->getWorldTransform(b_tform);
    b_tform.getOpenGLMatrix(&tform[0].x);
    return tform;
  }
};

// physics object wrapper..
class PhysicsObject {
public:
  btBoxShape* shape;
  btDefaultMotionState* motion_state;
  btRigidBody* body;


  void make(vec3 size, float mass, mat4 start_tform) {
    shape = new btBoxShape((btVector3&)size);
    vec4 box_local_inertia{0., 0., 0., 0.};
    shape->calculateLocalInertia(mass, (btVector3&)box_local_inertia);
    btTransform b_tform;
    b_tform.setFromOpenGLMatrix(&start_tform[0].x);
    motion_state = new btDefaultMotionState(b_tform);
    btRigidBody::btRigidBodyConstructionInfo box_c_info(
        mass, motion_state, shape, (btVector3&)box_local_inertia);
    body = new btRigidBody(box_c_info);
    body->setUserIndex(-1);
  }

  mat4 get_current_tform() {
    mat4 tform{1.f};
    btTransform b_tform;
    body->getMotionState()->getWorldTransform(b_tform);
    b_tform.getOpenGLMatrix(&tform[0].x);
    return tform;
  }
};