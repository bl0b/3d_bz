#pragma once

#include <vector>

class Plottable {
public:
  explicit Plottable(int l = 150) {
    length = l;
    next = 0;
    count = 0;
    d.resize(l);
  }

  void put(float e) {
    d[next] = e;
    next = (next + 1) % length;
    if (count < length) { count++; }
  }

  std::vector<float> temp;

  void get_last(const int num, std::vector<float>& values) {
    values.clear();
    for (int i = 0; i < num; i++) {
      if (i < count) {
        const int idx = ((length + next - i - 1) % length);
        const float v = d[idx];
        values.push_back(v);
      }
    }
  }

  void draw() {

    if (temp.size() != length) { temp.resize(length); }

    std::fill(temp.begin(), temp.begin() + (length - count), 0);

    for (int i = 0; i < count; i++) {
      temp[i + (length - count)] = d[(next + i) % count];
    }

    ImGui::PlotLines(
        title.c_str(), temp.data(), length, 0, NULL, range.x, range.y, ImVec2{dims.x, dims.y});
  }

  void clear() {

    if (d.size() != length) { d.resize(length); }

    memset(d.data(), 0, sizeof(float) * length);

    count = 0;
    next = 0;
  }

  string title = "abc";
  vec2 range{0, 10};
  vec2 dims = {200, 500};

private:
  int next = 0;
  int count = 0;
  int length;
  std::vector<float> d;
};
