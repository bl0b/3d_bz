#pragma once

#include <engine/Engine.h>
#include <librealsense2/rs.hpp>

#include <rosbag/bag.h>
#include <rosbag/view.h>

#include <rosbag/msgs/sensor_msgs/Image.h>
#include <rosbag/msgs/sensor_msgs/CameraInfo.h>

using namespace std;

class RsDataBag {
public:
  vector<Engine::Gpu::Tex<Engine::Gpu::TF::R16UI>> depth_frames;
  vector<Engine::Gpu::Tex<Engine::Gpu::TF::RGBA8>> rgba_frames;

  rs2_intrinsics depth_intrinsics;
  rs2_intrinsics rgba_intrinsics;

  rs2_extrinsics depth_to_rgba_extrinsics;
  rs2_extrinsics rgba_to_depth_extrinsics;

  void from_file(string f, bool raw = false) {

    string path = raw ? f : FileUtil::full_path(FileUtil::exe_dir + "assets/test/data/" + f);
    rosbag::Bag bag;
    bag.open(path);

    rosbag::View info_view(bag, rosbag::TopicQuery("/device_0/sensor_0/Depth_0/info/camera_info"));
    for (auto& m : info_view) {
      auto z = m.instantiate<sensor_msgs::CameraInfo>();
      depth_intrinsics.height = z->height;
      depth_intrinsics.width = z->width;
      depth_intrinsics.fx = z->K[0];
      depth_intrinsics.fy = z->K[4];
      depth_intrinsics.ppx = z->K[2];
      depth_intrinsics.ppy = z->K[5];
      if (z->distortion_model == "Brown Conrady") {
        depth_intrinsics.model = RS2_DISTORTION_BROWN_CONRADY;
      }
    }
    const ivec2 depth_dims{depth_intrinsics.width, depth_intrinsics.height};

    rosbag::View info_viewz(bag, rosbag::TopicQuery("/device_0/sensor_1/Color_0/info/camera_info"));
    for (auto& m : info_viewz) {
      auto z = m.instantiate<sensor_msgs::CameraInfo>();
      rgba_intrinsics.height = z->height;
      rgba_intrinsics.width = z->width;
      rgba_intrinsics.fx = z->K[0];
      rgba_intrinsics.fy = z->K[4];
      rgba_intrinsics.ppx = z->K[2];
      rgba_intrinsics.ppy = z->K[5];
      if (z->distortion_model == "Brown Conrady") {
        rgba_intrinsics.model = RS2_DISTORTION_BROWN_CONRADY;
      }
    }
    const ivec2 rgba_dims{rgba_intrinsics.width, rgba_intrinsics.height};

    rosbag::View images_stream(bag, rosbag::TopicQuery("/device_0/sensor_0/Depth_0/image/data"));
    for (const auto& m : images_stream) {
      if (auto z = m.instantiate<sensor_msgs::Image>()) {
        if (z->encoding != "mono16") { throw new invalid_argument("wrong format!"); }
        Engine::Gpu::Tex<Engine::Gpu::TF::R16UI> tex(depth_dims, z->data.data());
        depth_frames.push_back(move(tex));
      }
    }

    rosbag::View images_streamz(bag, rosbag::TopicQuery("/device_0/sensor_1/Color_0/image/data"));

    for (const auto& m : images_streamz) {
      if (auto z = m.instantiate<sensor_msgs::Image>()) {
        if (z->encoding != "rgba8") { throw new invalid_argument("wrong format!"); }
        Engine::Gpu::Tex<Engine::Gpu::TF::RGBA8> tex(rgba_dims, z->data.data());
        rgba_frames.push_back(move(tex));
      }
    }
  }
};