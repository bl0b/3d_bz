#pragma once

#include <engine/Engine.h>

#include "compute/ComputeLib.hpp"

#include "ActivePlane.hpp"

using namespace glm;

using namespace ComputeLib;

class CalibrationState {
public:
  int num_frames_to_sample;
  float pct_flat_planes_thresh;

  const int PLANE_STENCIL_RESOLUTION = 512;
  const int MINIF_STENCIL_GENERATION = 4;
  const float ON_PLANE_THRESHOLD = 100.f;

  bool calibrated = false;
  float surface_area;

  // When calibrated, these are populated:
  Engine::Math::Plane plane;
  ActivePlane active_plane;

  GenTriangles triangle_generator;

  DeprojectDepth deproject_depth;

  FilterPoints filter_points;

  // Texture object holding positions for mesh used to make active_p_stencil
  Engine::Gpu::Tex<Engine::Gpu::TF::RGBA32F> vtxes;
  DepthCameraMesh<Engine::Scene::MatrixPosition> active_plane_mesh;

  explicit CalibrationState() {
    num_frames_to_sample = DEFAULT_NUM_FRAMES_TO_SAMPLE;
    pct_flat_planes_thresh = DEFAULT_PCT_FLAT_PLANES_THRESH;
  }

  void reset() {
    calibrated = false;
    most_recent_planes.clear();
  }

  // returns whether plane of best fit has been found
  bool post_plane(
      Engine::Gpu::Tex<Engine::Gpu::TF::R16UI>& in_depth_img,
      Engine::Math::Plane p,
      float pct_flat,
      const rs2_intrinsics depth_cam_intrin,
      const rs2_intrinsics rgba_cam_intrin,
      const rs2_extrinsics depth_to_rgba_extrin,
      const float depth_units) {
    if (calibrated) {
      // no need to post more planes while already calibrated
      return false;
    }
    if (pct_flat > pct_flat_planes_thresh) {
      most_recent_planes.push_back(p);
      if (most_recent_planes.size() >= num_frames_to_sample) {
        calibrated = true;

        vec3 avg_pos{0., 0., 0.};
        vec3 avg_norm{0., 0., 0.};
        for (const Engine::Math::Plane pl : most_recent_planes) {
          avg_pos += pl.center.xyz();
          avg_norm += pl.normal.xyz();
        }

        avg_pos = avg_pos * (1.f / most_recent_planes.size());
        avg_norm = normalize(avg_norm);

        plane = Engine::Math::Plane(avg_norm, avg_pos);

        generate_stencil(
            in_depth_img, depth_cam_intrin, rgba_cam_intrin, depth_to_rgba_extrin, depth_units);

        surface_area = determine_surface_area();

        return true;
      }
    } else {
      reset();
    }
    return false;
  }

  void draw_imgui() {

    ImGui::Text("calibration!");
    if (calibrated) {
      ImGui::Text("cam is calibrated");
      if (ImGui::Button("clear")) {
        reset();
        active_plane.generated = false;
      }
    } else {
      ImGui::Text("calibrating...");
      ImGui::Text("%d of %d..", most_recent_planes.size(), num_frames_to_sample);
    }
  }

  // computes surface area of plane stencil.
  // this is an expensive operation, would (probably) need to be optimized better for per-frame
  // operation
  float determine_surface_area() {
    if (!calibrated) { return 0.f; }

    vector<vec4> positions_cpu;
    active_plane.mesh.positions.cu_copy_to(positions_cpu);

    vector<unsigned int> idxes_cpu;
    active_plane.mesh.idxes.cu_copy_to(idxes_cpu);

    float total_area = 0.f;

    for (int t = 0; t < active_plane.mesh.num_triangles; t++) {

      // z values all 0!
      // division to convert to meters. This will break if we abstract out depth units at any point
      vec2 p[3] = {
          positions_cpu[idxes_cpu[t * 3]].xy() / 10000.f,
          positions_cpu[idxes_cpu[t * 3 + 1]].xy() / 10000.f,
          positions_cpu[idxes_cpu[t * 3 + 2]].xy() / 10000.f,
      };

      // https://math.stackexchange.com/questions/516219/finding-out-the-area-of-a-triangle-if-the-coordinates-of-the-three-vertices-are
      // :)
      const float area =
          abs(0.5f * ((p[0].x * (p[1].y - p[2].y)) + (p[1].x * (p[2].y - p[0].y)) +
                      (p[2].x * (p[0].y - p[1].y))));

      total_area += area;
    }

    return total_area;
  }

  Engine::Gpu::Tex<Engine::Gpu::TF::RGBA32F> vtxes_temp;

  /*

  Generates the active plane stencil by rendering the current incoming mesh that has been filtered
  to only include points that are currently considered on the surface of the plane.

  */
  void generate_stencil(
      Engine::Gpu::Tex<Engine::Gpu::TF::R16UI>& in_depth_img,
      const rs2_intrinsics depth_cam_intrin,
      const rs2_intrinsics rgba_cam_intrin,
      const rs2_extrinsics depth_to_rgba_extrin,
      const float depth_units) {

    // Generate minified positions from depth image, transformed to plane coordinate system
    deproject_depth.run(
        in_depth_img, vtxes, depth_cam_intrin, MINIF_STENCIL_GENERATION, plane.to_plane_coords);

    // Clear out points such that only remaining points are within given z value of the plane
    // (anywhere along x-y axis) in plane coordinate system
    filter_points.by_z_value(
        vtxes,
        vtxes_temp,
        FilterPoints::FilterBy::OUTSIDE,
        -ON_PLANE_THRESHOLD,
        ON_PLANE_THRESHOLD);

    // Generate triangles to render mesh
    triangle_generator.run(vtxes_temp, active_plane_mesh.idxes, &active_plane_mesh.num_triangles);

    vtxes_temp.cu_copy_to(active_plane_mesh.positions);

    // generate stencil texture held by active_p_stencil.
    active_plane.generate_stencil(
        PLANE_STENCIL_RESOLUTION,
        plane,
        depth_cam_intrin,
        rgba_cam_intrin,
        depth_to_rgba_extrin,
        depth_units,
        active_plane_mesh);
  }

private:
  // parallel vectors. most recent x planes & how many flat planes were in those most recent x
  // planes
  vector<Engine::Math::Plane> most_recent_planes;

  // defaults..
  const int DEFAULT_NUM_FRAMES_TO_SAMPLE = 50;
  // todo: should this depend on camera type?
  const float DEFAULT_PCT_FLAT_PLANES_THRESH = 0.2;
};
