#pragma once

#include <engine/Engine.h>

#include "Plottable.hpp"

class BoundingBoxHeightMaps {
public:
  ComputeLib::OrthoMapShader shader;

  ComputeLib::MapDepth map_depth;
  ivec2 resolution;

  ComputeLib::BoundingBoxHeightMapsLib mesh_group_gen_2;

  ComputeLib::GenTriangles gen_t;

  vector<Engine::Gpu::Tex<Engine::Gpu::TF::R16UI>> height_maps;
  vector<Engine::Gpu::Tex<Engine::Gpu::TF::RGBA8>> height_maps_rgba;
  vector<Engine::Gpu::Buff<uint32_t>> height_lists;
  vector<vector<uint32_t>> height_lists_cpu;

  // copy of groups buffer as a texture... for some reason geometry shader on AMD needs a texture
  // instead of buffer
  // todo: consolidate 'groups_tex' and 'groups' into single resource, to avoid extra copy between
  // buffer and texture to be used in ortho map geometry shader
  Engine::Gpu::Tex<Engine::Gpu::TF::R32UI> groups_tex;

  BoundingBoxHeightMaps() {}

  template <class T>
  void generate(
      const int max_num_groups,
      Engine::Gpu::Buff<uint32_t>& groups,
      ComputeLib::OrthoSampler2D groups_sampler,
      DepthCameraMesh<T>& mesh,
      std::vector<ComputeLib::MeshGroups::GroupInfo>& groups_info,
      std::unordered_map<int, vec3>& output_fingertip_positions) {

    resize_arrays(max_num_groups);

    const int num_to_sample = 16;
    vector<uint32_t> first(num_to_sample);
    vector<uint32_t> last(num_to_sample);

    const float min_hand_length = 1200.f; // 12 cm?

    for (int i = 0; i < groups_info.size(); i++) {

      const auto& info = groups_info[i];

      height_maps[i].set_storage(resolution);
      height_maps_rgba[i].set_storage(resolution);

      const bool x_dim_long = info.group_dims.x > info.group_dims.y;

      mat4 t_form =
          rotate(info.group_rot, vec3{0, 0, 1}) * translate(vec3{-info.group_center.xy(), 0});

      groups_tex.cu_copy_from(groups, groups_sampler.img_dims);
      shader.set_uniforms(
          info.group_dims / -2.f,
          info.group_dims / 2.f,
          t_form,
          groups_sampler,
          groups_tex,
          info.normalized_group_num);

      shader.draw_height = true;

      shader.set_target_texture(height_maps[i]);
      shader.fbo.bind();

      GL::Renderer::setClearColor(0x000000_rgbf);
      GL::Renderer::setPolygonMode(GL::Renderer::PolygonMode::Fill);
      shader.fbo.clear(GL::FramebufferClear::Color);

      mesh.get_mesh().draw(shader);

      shader.clear_target_texture();
    }

    for (int i = 0; i < groups_info.size(); i++) {

      const auto& info = groups_info[i];
      const bool x_dim_long = info.group_dims.x > info.group_dims.y;

      mat4 t_form =
          rotate(info.group_rot, vec3{0, 0, 1}) * translate(vec3{-info.group_center.xy(), 0});

#ifndef NDEBUG
      // and then map the stencil texture to RGBA for debugging
      map_depth.run(height_maps[i], height_maps_rgba[i]);
#endif

      mesh_group_gen_2.make_heights_list(height_lists[i], height_maps[i], x_dim_long);

      height_lists[i].cu_copy_to(height_lists_cpu[i]);

      const float longest_dim =
          info.group_dims.x > info.group_dims.y ? info.group_dims.x : info.group_dims.y;

      if (longest_dim > min_hand_length) {

        int current_sample = 0;
        int current_check = 0;
        while (current_sample < num_to_sample) {
          const float s = height_lists_cpu[i][current_check++];
          if (s > 0) { first[current_sample++] = s; }

          if (current_check == resolution.x) {
            throw new invalid_argument("failed to find front sample!");
          }
        }

        std::sort(first.begin(), first.end());

        current_sample = 0;
        current_check = 0;
        while (current_sample < num_to_sample) {
          const float s = height_lists_cpu[i][resolution.x - (current_check++ + 1)];
          if (s > 0) { last[current_sample++] = s; }

          if (current_check == resolution.x) {
            throw new invalid_argument("failed to find back sample!");
          }
        }
        std::sort(last.begin(), last.end());

        const uint32_t first_h = first[num_to_sample / 2];
        const uint32_t last_h = last[num_to_sample / 2];

        bool shortest_side_front = first_h < last_h;
        const uint32_t height = shortest_side_front ? first_h : last_h;

        vec2 sample_min_coords;
        vec2 sample_max_coords;
        if (x_dim_long) {
          // now this.. is ugly
          if (shortest_side_front) {
            sample_min_coords = {info.group_dims.x / -2.f, info.group_dims.y / -2.f};
            sample_max_coords = {(info.group_dims.x / -2.f) + (num_to_sample * 2),
                                 info.group_dims.y / 2.f};
          } else {
            sample_min_coords = {(info.group_dims.x / 2.f) - (num_to_sample * 2),
                                 info.group_dims.y / -2.f};
            sample_max_coords = {info.group_dims.x / 2.f, info.group_dims.y / 2.f};
          }
        } else {
          if (shortest_side_front) {
            sample_min_coords = {info.group_dims.x / -2.f, info.group_dims.y / -2.f};
            sample_max_coords = {info.group_dims.x / 2.f,
                                 (info.group_dims.y / -2.f) + (num_to_sample * 2)};
          } else {
            sample_min_coords = {info.group_dims.x / -2.f,
                                 (info.group_dims.y / 2.f) - (num_to_sample * 2)};
            sample_max_coords = {info.group_dims.x / 2.f, info.group_dims.y / 2.f};
          }
        }

        vec4 a_pos;
        mesh_group_gen_2.get_average_pos(
            &a_pos,
            height_maps[i],
            info.group_dims / -2.f,
            info.group_dims / 2.f,
            // range is full range of img: should be avg position of all!
            sample_min_coords,
            sample_max_coords);

        vec4 new_pos = inverse(t_form) * vec4{a_pos.xy() / a_pos.w, 0., 1.};

        output_fingertip_positions[info.normalized_group_num] = vec3{new_pos.xy(), height * 1.f};
      }
    }

    GL::defaultFramebuffer.bind();
  }

  void draw_imgui(std::vector<ComputeLib::MeshGroups::GroupInfo>& groups_info) {

    float* f = new float[resolution.x];

    for (int i = 0; i < groups_info.size(); i++) {
      const string label = "group " + to_string(groups_info[i].normalized_group_num);
      ImGui::Text(label.c_str());
      // Engine::ImGuiCtx::Image2(height_maps_rgba[i]);

      for (int j = 0; j < height_lists_cpu[i].size(); j++) {
        f[j] = height_lists_cpu[i][j] * 1.f;
      }

      /*
      const string label2 = "h" + to_string(groups_info[i].normalized_group_num);
      ImGui::PlotLines(label2.c_str(), f, resolution.x, 0, NULL, 0, 1000, ImVec2{ 400, 150 });
      */
    }

    ImGui::Text("---");

    delete[] f;
  }

private:
  void resize_arrays(const int max_num_groups) {
    if (height_maps.size() != max_num_groups) height_maps.resize(max_num_groups);
    if (height_maps_rgba.size() != max_num_groups) height_maps_rgba.resize(max_num_groups);
    if (height_lists.size() != max_num_groups) height_lists.resize(max_num_groups);
    if (height_lists_cpu.size() != max_num_groups) height_lists_cpu.resize(max_num_groups);
  }
};