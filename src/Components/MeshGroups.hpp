#pragma once

#include <engine/Engine.h>

using namespace Engine::Gpu;

class MeshGroups2 {
private:
public:

  static inline vector<unsigned int> get_neighbor_idxes(ivec2 dim, ivec2 coord) {
    vector<unsigned int> neighbors_out;
    for (int _j = 0; _j < 8; _j++) {
      const int j = _j < 4 ? _j : _j + 1;
      const ivec2 test_coord = coord + ivec2{(j % 3) - 1, (j / 3) - 1};
      if (test_coord.x < 0 || test_coord.y < 0 || test_coord.x >= dim.x ||
          test_coord.y >= dim.y)
        continue;

      neighbors_out.emplace_back(get_idx(dim, test_coord));
    }
    return std::move(neighbors_out);
  }

  static inline void iterate_neighbors(
      ivec2 img_dims, ivec2 img_coord, const function<void(ivec2, unsigned int)>& fn) {

    //    const ivec2 start_coord = get_coord(img_dims, idx);
    for (int _j = 0; _j < 8; _j++) {
      const int j = _j < 4 ? _j : _j + 1;
      const ivec2 test_coord = img_coord + ivec2{(j % 3) - 1, (j / 3) - 1};
      if (test_coord.x < 0 || test_coord.y < 0 || test_coord.x >= img_dims.x ||
          test_coord.y >= img_dims.y)
        continue;

      fn(test_coord, get_idx(img_dims, test_coord));
    }
  }

  static void make_distance_map_to_origin(
      ivec2 img_dims,
      unordered_set<unsigned int>& group,
      ivec2 origin_coords,
      unordered_map<unsigned int, float>& distances) {

    unsigned int origin_idx = get_idx(img_dims, origin_coords);

    queue<unsigned int> to_visit;
    to_visit.push(origin_idx);
    unordered_set<unsigned int> enqueued;

    distances.clear();
    //    unordered_map<unsigned int, float> best_distances;
    distances[origin_idx] = 0.f;

    while (!to_visit.empty()) {
      unsigned int idx = to_visit.front();
      to_visit.pop();

      const ivec2 start_coord = get_coord(img_dims, idx);
      iterate_neighbors(
          img_dims,
          start_coord,
          [img_dims, &group, idx, start_coord, &distances, &enqueued, &to_visit](
              ivec2 test_coord, unsigned int test_idx) {
            // only do anything if neighbor is part of the group
            if (group.find(test_idx) != group.end()) {
              // if neighbor has been assigned a distance, see if it can beat current distance
              if (distances.find(test_idx) != distances.end()) {
                const float neighbor_dist = glm::length(vec2{start_coord - test_coord});
                const bool dist_set = distances.find(idx) != distances.end();
                if (!dist_set || distances[test_idx] + neighbor_dist < distances[idx]) {
                  distances[idx] = distances[test_idx] + neighbor_dist;
                }
              } else {
                if (enqueued.find(test_idx) == enqueued.end()) {
                  to_visit.push(test_idx);
                  enqueued.insert(test_idx);
                }
              }
            }
          });
    }
  }

  static ivec2 get_mean_coord(ivec2 img_dims, unordered_set<unsigned int>& group) {
    const int img_dim_x = img_dims.x;
    vec2 img_avg{0., 0.};
    for (auto i : group) {
      img_avg += vec2{(i % img_dim_x) * 1.f, (i / img_dim_x) * 1.f};
    }
    img_avg /= group.size();
    return ivec2{img_avg.x, img_avg.y};
  }

  static unordered_set<unsigned int>&
  get_biggest_group(vector<unordered_set<unsigned int>>& groups) {
    const auto gp_compare_size = [](unordered_set<unsigned int>& g1,
                                    unordered_set<unsigned int>& g2) -> bool {
      return g1.size() < g2.size();
    };

    return *std::max_element(groups.begin(), groups.end(), gp_compare_size);
  }

  // this is slow as balls in debug mode! STL is the culprit..
  static void gen_groups(Tex<TF::R16UI>& _img, vector<unordered_set<unsigned int>>& groups) {

    const ivec2 img_dims = _img.get_dims();
    vector<uint16_t> img;
    _img.cu_copy_to(img);

    groups.clear();

    unordered_set<unsigned int> seen;

    for (int i = 0; i < img.size(); i++) {
      if (!img[i] || seen.find(i) != seen.end()) continue;

      unordered_set<unsigned int> group;
      queue<unsigned int> to_see;
      to_see.push(i);
      seen.insert(i);

      while (!to_see.empty()) {
        const auto idx = to_see.front();
        to_see.pop();

        group.insert(idx);

        const ivec2 start_coord = get_coord(img_dims, idx);
        iterate_neighbors(
            img_dims,
            start_coord,
            [img_dims, &img, &seen, &to_see](ivec2 test_coord, unsigned int test_idx) {
              const uint16_t test_val = img[test_idx];
              if (test_val && seen.find(test_idx) == seen.end()) {
                seen.insert(test_idx);
                to_see.push(test_idx);
              }
            });
      }

      groups.emplace_back(std::move(group));
    }
  }

  static void find_local_maxima(
      ivec2 img_dims,
      int max_depth,
      unordered_map<unsigned int, float>& dist_map,
      vector<unsigned int>& local_maxima) {

    vector<unsigned int> possible;

    const auto d = img_dims;

    // Only include pixels that don't have a neighbor that has a bigger distance score
    for (auto m : dist_map) {
      const ivec2 c = get_coord(img_dims, m.first);
      bool further_found = false;
      MeshGroups2::iterate_neighbors(
          d, c, [&dist_map, c, &m, &further_found](ivec2 test_coord, unsigned int test_idx) {
            if (dist_map.find(test_idx) != dist_map.end() && dist_map[test_idx] > m.second) {
              further_found = true;
            }
          });
      if (!further_found) { possible.emplace_back(m.first); }
    }

    local_maxima.clear();

    for (int i : possible) {

      unordered_set<unsigned int> enqueued;
      enqueued.insert(i);
      queue<pair<unsigned int, unsigned int>> to_visit;
      to_visit.push({i, 0});

      bool greater_found = false;
      while (!to_visit.empty() && !greater_found) {
        auto val = to_visit.front();
        to_visit.pop();
        const auto idx = val.first;
        const auto depth = val.second;

        const ivec2 c = get_coord(d, idx);

        if (dist_map.find(idx) != dist_map.end() && dist_map[idx] > dist_map[i]) {
          greater_found = true;
        } else {

          MeshGroups2::iterate_neighbors(
              d,
              c,
              [&dist_map, &enqueued, &to_visit, depth, max_depth](
                  ivec2 test_coord, unsigned int test_idx) {
                bool is_in_group = dist_map.find(test_idx) != dist_map.end();
                bool not_queued_yet = enqueued.find(test_idx) == enqueued.end();
                bool not_too_deep = depth < max_depth;

                if (is_in_group && not_queued_yet && not_too_deep) {
                  enqueued.insert(test_idx);
                  to_visit.push({test_idx, depth + 1});
                }
              });
        }
      }

      if (!greater_found) { local_maxima.emplace_back(i); }
    }
  }

  static void get_outlier_point(vector<vec2>& coords, vec2* out_coord, vector<vec2>& in_coords) {

    // pick biggest outlier!
    // and then pick 5 least-outlier-ish

    vector<float> avg_diffs;
    vector<int> idxes;
    for (int i = 0; i < coords.size(); i++) {
      vec2 c_avg{0, 0};
      vector<vec2> other_coords;
      for (int j = 0; j < coords.size(); j++) {
        if (j != i) {
          other_coords.emplace_back(coords[j]);
          c_avg += coords[j] / (coords.size() - 1.f);
        }
      }

      float avg_diff = 0.f;
      for (int j = 0; j < coords.size(); j++) {
        if (j != i) { avg_diff += glm::length(coords[j] - c_avg) / (coords.size() - 1.f); }
      }

      avg_diffs.emplace_back(avg_diff);
      idxes.emplace_back(i);
    }

    std::sort(idxes.begin(), idxes.end(), [&avg_diffs](int a, int b) -> bool {
      return avg_diffs[a] < avg_diffs[b];
    });

    int min_diff_idx = idxes[0];
    *out_coord = coords[min_diff_idx];
    in_coords.clear();
    for (int i = 0; i < 5; i++) {
      // pick the 5 least outlier-ish coords
      in_coords.emplace_back(coords[idxes[idxes.size() - 1 - i]]);
    }
  }

private:
  static unsigned int get_idx(const ivec2& dims, const ivec2& coord) {
    return dims.x * coord.y + coord.x;
  }

  static ivec2 get_coord(const ivec2& dims, const unsigned int idx) {
    return ivec2{idx % dims.x, idx / dims.x};
  }
};