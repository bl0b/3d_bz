#pragma once

#include <engine/Engine.h>

#include <compute/ComputeLib.hpp>

#include "Plottable.hpp"

class ActivePlaneHeightMap {
public:
  int num_rounds = 1;
  ComputeLib::OrthoMapShader shader;
  ComputeLib::ShrinkHeightMap shrink_height_map;

  OrthoSampler2D height_map_sampler;
  Engine::Gpu::Tex<Engine::Gpu::TF::R16UI> height_map, height_map_back, height_map_orig;
  Engine::Gpu::Tex<Engine::Gpu::TF::RGBA8> height_map_rgba;

  ComputeLib::MapDepth map_depth;

  // should just be a vector, except std containers are very slow in debug builds, so just generic
  // memory chunk will have to do
  uint16_t* height_map_cpu = nullptr;
  int copy_cpu_length = 0;

  ~ActivePlaneHeightMap() {
    if (height_map_cpu != nullptr) delete[] height_map_cpu;
  }

  template <class T>
  void
  generate(const int resolution, DepthCameraMesh<T>& mesh, OrthoSampler2D active_plane_sampler) {

    const int num_els = resolution * resolution;
    if (copy_cpu_length != num_els) {
      if (height_map_cpu != nullptr) delete[] height_map_cpu;
      height_map_cpu = new uint16_t[num_els];
      copy_cpu_length = num_els;
    }

    height_map.set_storage({resolution, resolution});
    height_map_rgba.set_storage({resolution, resolution});

    height_map_sampler.img_dims = {resolution, resolution};
    height_map_sampler.min_coords = active_plane_sampler.min_coords;
    height_map_sampler.max_coords = active_plane_sampler.max_coords;

    shader.draw_height = true;
    shader.set_uniforms(height_map_sampler.min_coords, height_map_sampler.max_coords);

    shader.set_target_texture(height_map);
    shader.fbo.bind();

    GL::Renderer::setClearColor(0x000000_rgbf);
    GL::Renderer::setPolygonMode(GL::Renderer::PolygonMode::Fill);
    shader.fbo.clear(GL::FramebufferClear::Color);

    mesh.get_mesh().draw(shader);

    // GL::defaultFramebuffer.bind();
    shader.clear_target_texture();

    height_map.cu_copy_to(height_map_orig);

    // figure out how to optimize this!
    for (int i = 0; i < num_rounds; i++) {
      shrink_height_map.run(height_map_orig, height_map, height_map_back);
    }

// and then map the stencil texture to RGBA for debugging..
#ifndef NDEBUG
    map_depth.run(height_map, height_map_rgba);
#endif

    // will need to process image on CPU
    height_map.cu_copy_to(height_map_cpu);
  }
};
