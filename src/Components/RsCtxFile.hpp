#pragma once

#include <Engine/Engine.h>

#include <librealsense2/rs.hpp>
#include <librealsense2/rs_advanced_mode.hpp>
#include <librealsense2/rsutil.h>

#include <chrono>
#include <iostream>
#include <string>
#include <thread>
#include <unordered_map>
#include <vector>
#include <atomic>
#include <mutex>

using namespace std;
using namespace Magnum;

class ActiveStream {
public:
  bool open;
  rs2::frame_queue queue;
  float depth_units;
  rs2::stream_profile stream_profile;
  int sensor_idx;

  rs2::video_stream_profile video_stream_profile() {
    return stream_profile.as<rs2::video_stream_profile>();
  }
};

class RsCtxFile {
public:
  rs2::context ctx;
  rs2::device dev;
  bool dev_set = false;
  string dev_from_filename;

  // Overview of connected device: sensors connected, streams available by sensor, and all
  // available video stream profiles
  vector<rs2::sensor> sensors_list;
  vector<unordered_set<rs2_stream>> streams_by_sensor_list;
  unordered_map<rs2_stream, vector<rs2::video_stream_profile>> video_stream_profiles;

  // Current state of each stream.
  unordered_map<rs2_stream, ActiveStream> streams;

  RsCtxFile() {}

  ~RsCtxFile() {
    for (auto s : streams) {
      if (s.second.open) { stop(s.first); }
    }
  }

  void start(rs2::video_stream_profile stream_profile) {

    if (!dev_set) { return; }

    const rs2_stream stream_type = stream_profile.stream_type();
    ActiveStream& stream_info = streams[stream_type];

    rs2::sensor sensor = sensors_list[streams[stream_type].sensor_idx];
    sensor.open(stream_profile);
    sensor.start(stream_info.queue);

    stream_info.open = true;
    stream_info.stream_profile = stream_profile;
    if (stream_type == RS2_STREAM_DEPTH) {
      stream_info.depth_units = sensor.get_option(RS2_OPTION_DEPTH_UNITS);
    }
  }

  void stop(rs2_stream stream_type) {
    if (streams[stream_type].open) {

      rs2::sensor& s = sensors_list[streams[stream_type].sensor_idx];
      s.stop();
      s.close();

      streams[stream_type].open = false;
    }
  }

  rs2_intrinsics intrinsics(rs2_stream stream_type) {
    return streams[stream_type].video_stream_profile().get_intrinsics();
  }

  rs2_extrinsics get_extrinsics(rs2_stream from, rs2_stream to) {

    rs2::stream_profile from_profile = streams[from].stream_profile;
    rs2::stream_profile to_profile = streams[to].stream_profile;

    const rs2_stream_profile* f_profile = from_profile.get();
    const rs2_stream_profile* t_profile = to_profile.get();

    rs2_extrinsics ex;
    rs2_error* e = nullptr;
    rs2_get_extrinsics(f_profile, t_profile, &ex, &e);

    rs2::error::handle(e);

    return ex;
  }

  rs2::frame try_wait(rs2_stream stream_type, int format, const int timeout) {
    rs2::frame f = streams[stream_type].queue.wait_for_frame(timeout);
    assert(f.get_profile().format() == format);
    return f;
  }

  void load_saved(string path) {

    if (dev_set) { ctx.unload_device(dev_from_filename); }
    dev_set = false;

    streams.clear();
    sensors_list.clear();
    video_stream_profiles.clear();
    streams_by_sensor_list.clear();

    dev_from_filename = path;
    dev = ctx.load_device(path);

    vector<rs2::sensor> device_sensors = dev.query_sensors();
    int i = 0;
    for (rs2::sensor s : device_sensors) {
      vector<rs2::stream_profile> device_stream_profiles = s.get_stream_profiles();

      if (device_stream_profiles.size()) {

        unordered_set<rs2_stream> streams_of_sensor;
        sensors_list.push_back(s);
        for (auto s_p : device_stream_profiles) {
          rs2_stream stream_type = s_p.stream_type();
          streams[stream_type].open = false;
          streams[stream_type].sensor_idx = i;
          streams[stream_type].stream_profile = s_p;
          if (!streams_of_sensor.count(stream_type)) { streams_of_sensor.insert(stream_type); }
          video_stream_profiles[stream_type].push_back(s_p.as<rs2::video_stream_profile>());
        }

        streams_by_sensor_list.push_back(move(streams_of_sensor));
      }

      i++;
    }

    dev_set = true;
  }
};
