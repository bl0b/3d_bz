#pragma once

#include <engine/Engine.h>

#include <compute/ComputeLib.hpp>

#include "Plottable.hpp"

class ActivePlane {

public:
  bool generated = false;

  ComputeLib::OrthoMapShader shader;

  ComputeLib::MakeSimplifiedStencilMesh make_simplified_stencil_mesh;

  ComputeLib::MapDepth map_depth;

  // various representations of the active plane - all in plane coordinate system (origin of plane
  // is {0, 0, 0})
  ComputeLib::OrthoSampler2D stencil_sampler;
  Engine::Gpu::Tex<Engine::Gpu::TF::R16UI> stencil;
  Engine::Gpu::Tex<Engine::Gpu::TF::RGBA8> stencil_intermediate_rgba;
  Engine::Gpu::Tex<Engine::Gpu::TF::RGBA8> stencil_rgba;
  DepthCameraMesh<Engine::Scene::MatrixPosition> mesh;
  Engine::Gpu::Buff<vec4> border;

  // essentially take orthographic rendering pointing down z axis, to map to x-y axis of plane
  template <class T>
  void generate_stencil(
      const int resolution,
      Engine::Math::Plane& plane,
      const rs2_intrinsics depth_cam_intrin,
      const rs2_intrinsics rgba_cam_intrin,
      const rs2_extrinsics depth_to_rgba_extrin,
      const float depth_units,
      DepthCameraMesh<T>& in_mesh) {

    stencil.set_storage({resolution, resolution});

    // Given the intrinsics and the calculated plane, determine on-plane coords of 4 corners of
    // depth camera viewport, to determine maximum possible plane size
    vector<vec2> corner_coords = {{0, 0},
                                  {0, depth_cam_intrin.height},
                                  {depth_cam_intrin.width, 0},
                                  {depth_cam_intrin.width, depth_cam_intrin.height}};
    vector<vec4> corner_positions;
    for (auto coord : corner_coords) {
      // simple deprojection of point at z = 1
      vec3 ray_dir = normalize(vec3(
          (coord.x - depth_cam_intrin.ppx) / depth_cam_intrin.fx,
          (coord.y - depth_cam_intrin.ppy) / depth_cam_intrin.fy,
          1));

      vec4 pos = plane.to_plane_coords * plane.intersection_with_line(ray_dir, {0, 0, 0});
      corner_positions.push_back(pos);
    }

    stencil_sampler.img_dims = {resolution, resolution};
    stencil_sampler.set_min_and_max(corner_positions);

    draw_stencil(in_mesh);

    // output the intermediate rgba for debug purposeses
    map_depth.run(stencil, stencil_intermediate_rgba);

    // PART 2: Simplify raw stencil to smooth edges.

    const int num_rays = 48;
    const int num_sections_per_ray = 8;

    const float auto_shrinkage = 10.f;

    make_simplified_stencil_mesh.run(
        stencil,
        stencil_sampler,
        border,
        mesh,
        num_rays,
        num_sections_per_ray,
        auto_shrinkage,
        depth_cam_intrin,
        rgba_cam_intrin,
        depth_to_rgba_extrin,
        depth_units,
        plane);

    // find min/max x/y coordinates of designated border area, for re-rendered stencil bounds
    vector<vec4> border_cpu;
    border.cu_copy_to(border_cpu);
    stencil_sampler.set_min_and_max(border_cpu);

    draw_stencil(mesh);

    // and then map the stencil texture to RGBA for debugging
    map_depth.run(stencil, stencil_rgba);

    generated = true;
  }

  template <class T>
  void render(MeshCamera<T>& cam) {
    GL::Renderer::setPolygonMode(GL::Renderer::PolygonMode::Fill);
    cam.draw(mesh);
  }

  template <class T>
  void draw_stencil(DepthCameraMesh<T>& in_mesh) {
    shader.set_uniforms(stencil_sampler.min_coords, stencil_sampler.max_coords);

    shader.set_target_texture(stencil);
    shader.fbo.bind();

    GL::Renderer::setClearColor(0x000000_rgbf);
    GL::Renderer::setPolygonMode(GL::Renderer::PolygonMode::Fill);
    shader.fbo.clear(GL::FramebufferClear::Color);
    in_mesh.get_mesh().draw(shader);

    shader.clear_target_texture();
  }
};
