#pragma once

#include <engine/Engine.h>

#include <librealsense2/rs.hpp>
#include <librealsense2/rs_advanced_mode.hpp>
#include <librealsense2/rsutil.h>

#include <chrono>
#include <iostream>
#include <string>
#include <thread>
#include <unordered_map>
#include <vector>
#include <atomic>
#include <mutex>

class RsCtxLive {
public:
  struct StreamProfile {
    rs2_stream type;
    rs2_format format;
    ivec2 dims;
    int fps;
  };

  struct ActiveStream {
    StreamProfile profile;
    // rs2::video_stream_profile rs_profile;
    rs2::frame_queue queue;
    bool active;
  };

  vector<StreamProfile> desired_streams;

  mutex active_streams_mutex;
  unordered_map<rs2_stream, ActiveStream> active_streams;

  rs2::context ctx;
  rs2::device dev;
  atomic<bool> dev_set;

  atomic<bool> kill_stream_changes_queue;
  SafeQueue<function<void(void)>> stream_changes_queue;
  thread stream_changes_thread;

  // callback called when a device is connected for configuration
  function<void(RsCtxLive&)> config_callback = [](RsCtxLive& rs) {};

  bool gui_control = false;

  // context for using a live realsense camera
  RsCtxLive() {

    dev_set = false;
    kill_stream_changes_queue = false;

    stream_changes_thread = thread([&]() {
      while (!kill_stream_changes_queue || !stream_changes_queue.empty()) {
        if (stream_changes_queue.empty()) {
          this_thread::sleep_for(chrono::milliseconds(25));
        } else {
          auto fn = stream_changes_queue.pop();
          fn();
        }
      }
    });
  }

  void init() {
    stream_changes_queue.push([&]() { load_initial(); });
  }

  void set_gui_control(bool _gui_control) { gui_control = _gui_control; }

  ~RsCtxLive() {

    // stop any streams that are currently alive
    for (auto s : active_streams) {
      if (s.second.active) {
        const rs2_stream stream_type = s.first;
        stream_changes_queue.push([&, stream_type]() { stop_stream(stream_type); });
      }
    }

    kill_stream_changes_queue = true;
    stream_changes_thread.join();

    // take control of the mutex before destroying
    lock_guard<mutex> lock(active_streams_mutex);
  }

  bool fetch_frame_if_available(rs2_stream stream_type, int format, rs2::frame* f) {
    bool frame_fetched = active_streams[stream_type].queue.poll_for_frame(f);
    if (frame_fetched) { assert(f->get_profile().format() == format); }
    return frame_fetched;
  }

  rs2::frame try_wait(rs2_stream stream_type, int format, const int timeout) {
    rs2::frame f = active_streams[stream_type].queue.wait_for_frame(timeout);
    assert(f.get_profile().format() == format);
    return f;
  }

  rs2_intrinsics intrinsics(rs2_stream stream_type) {
    return get_rs_stream_profile(active_streams[stream_type].profile).get_intrinsics();
  }

  rs2_extrinsics get_extrinsics(rs2_stream from, rs2_stream to) {

    const rs2_stream_profile* f_profile = get_rs_stream_profile(active_streams[from].profile).get();
    const rs2_stream_profile* t_profile = get_rs_stream_profile(active_streams[to].profile).get();

    rs2_extrinsics ex;
    rs2_error* e = nullptr;
    rs2_get_extrinsics(f_profile, t_profile, &ex, &e);

    rs2::error::handle(e);

    return ex;
  }

  bool is_active(rs2_stream stream_type) { return active_streams[stream_type].active; }

  string get_camera_name() { return dev.get_info(RS2_CAMERA_INFO_NAME); }

  void draw_imgui() {

    if (ImGui::CollapsingHeader("Camera info")) {

      ImGui::Indent(20.f);

      if (!dev_set) {
        ImGui::Text("No Camera connected");

      } else {

        ImGui::Text("Camera connected: %s", dev.get_info(RS2_CAMERA_INFO_NAME));

        for (int i = 0; i < device_sensors.size(); i++) {

          const rs2::sensor& sensor = device_sensors[i];

          const string name = sensor.get_info(RS2_CAMERA_INFO_NAME);
          ImGui::Text(name.c_str());

          for (auto stream_type_sensor : device_sensor_idxes_by_stream_type) {
            if (i == stream_type_sensor.second) {

              const rs2_stream stream_type = stream_type_sensor.first;
              if (ImGui::TreeNode(rs2_stream_to_string(stream_type))) {

                if (active_streams[stream_type].active) {

                  auto rs_profile = get_rs_stream_profile(active_streams[stream_type].profile);
                  ImGui::Text(
                      "Stream active: %d x %d @ %d fps, format: %s",
                      rs_profile.width(),
                      rs_profile.height(),
                      rs_profile.fps(),
                      rs2_format_to_string(rs_profile.format()));

                  if (gui_control) {
                    ImGui::SameLine();
                    if (ImGui::Button("stop")) { remove_desired_stream(stream_type); }
                  }

                } else {
                  ImGui::Text("Inactive");

                  if (ImGui::TreeNode("Available streams")) {
                    int j = 0;
                    for (auto p : video_stream_profiles_by_stream_type[stream_type]) {
                      ImGui::PushID((char)j++);

                      ImGui::Text(
                          "- %4d x %4d @ %2d fps, format: %5s",
                          p.width(),
                          p.height(),
                          p.fps(),
                          rs2_format_to_string(p.format()));

                      if (gui_control) {
                        ImGui::SameLine();
                        if (ImGui::Button("start")) {
                          add_desired_stream(
                              {stream_type, p.format(), {p.width(), p.height()}, p.fps()});
                        }
                      }

                      ImGui::PopID();
                    }

                    ImGui::TreePop();
                  }
                }

                ImGui::TreePop();
              }
            }
          }

          /*

                if (ImGui::TreeNode("Available streams")) {

                  int i = 0;
                  for (rs2::video_stream_profile& stream_profile :
                    video_stream_profiles[stream_type]) {
                    ImGui::PushID((char)i);



                    if (gui_control) {

                      bool stream_on = streams[stream_type].open;
                      if (stream_on) {

                        rs2::video_stream_profile& active_profile =
                          streams[stream_type].stream_profile.as<rs2::video_stream_profile>();

                        stream_on = stream_profile.width() == active_profile.width() &&
                          stream_profile.height() == active_profile.height() &&
                          stream_profile.fps() == active_profile.fps() &&
                          stream_profile.format() == active_profile.format();
                      }

                      if (!stream_on && !pending_device_start && !streams[stream_type].open) {
                        ImGui::SameLine();
                        if (ImGui::Button("start")) { start(stream_type, i); }
                      }
                    }

                    i++;
                    ImGui::PopID();
                  }

                  ImGui::TreePop();
                }

                ImGui::TreePop();
              }
            }
          }

          */
        }
      }

      ImGui::Unindent(20.f);
    }

    /*

    ImGui::Text("rs!");
    ImGui::Text("num sensors: %d", device_sensors.size());

    */
  }

  void set_desired_streams(vector<StreamProfile> streams) {
    lock_guard<mutex> lock(active_streams_mutex);

    desired_streams.clear();
    desired_streams.assign(streams.begin(), streams.end());

    start_desired_streams();
  }

  void remove_desired_stream(rs2_stream stream_type) {
    lock_guard<mutex> lock(active_streams_mutex);

    vector<StreamProfile> desired_streams_backup;
    desired_streams_backup.assign(desired_streams.begin(), desired_streams.end());
    desired_streams.clear();

    for (auto d : desired_streams_backup) {
      if (d.type != stream_type) { desired_streams.push_back(d); }
    }

    start_desired_streams();
  }

  void add_desired_stream(StreamProfile stream) {
    lock_guard<mutex> lock(active_streams_mutex);

    desired_streams.push_back(stream);

    start_desired_streams();
  }

  void set_depth_units(const float units) {
    lock_guard<mutex> lock(active_streams_mutex);

    if (!dev_set) { throw invalid_argument("cannot set depth units if device is not connected!"); }

    rs2::sensor s =
        device_sensors[device_sensor_idxes_by_stream_type[rs2_stream::RS2_STREAM_DEPTH]];
    s.set_option(RS2_OPTION_DEPTH_UNITS, units);
  }

  void set_disparity_shift(const int disparity_shift) {
    lock_guard<mutex> lock(active_streams_mutex);

    if (!dev_set) {
      throw invalid_argument("cannot set disparity shift if device is not connected!");
    }

    auto dev_adv = dev.as<rs400::advanced_mode>();
    if (!dev_adv.is_enabled()) { dev_adv.toggle_advanced_mode(true); }
    STDepthTableControl depth_t = dev_adv.get_depth_table();
    depth_t.disparityShift = disparity_shift;
    dev_adv.set_depth_table(depth_t);
  }

  void set_config_callback(function<void(RsCtxLive&)> fn) { config_callback = fn; }

private:
  rs2::device_list devices;
  vector<rs2::sensor> device_sensors;
  unordered_map<rs2_stream, int> device_sensor_idxes_by_stream_type;
  unordered_map<rs2_stream, vector<rs2::video_stream_profile>> video_stream_profiles_by_stream_type;

  void load_initial() {
    devices = ctx.query_devices();
    if (devices.size()) {
      dev = devices[0];
      load_device_info(devices[0]);
    } else {
      dev_set = false;
    }

    ctx.set_devices_changed_callback([&](rs2::event_information info) {
      if (dev_set && info.was_removed(dev)) {
        dev_set = false;
        clear_device_info();
      } else if (!dev_set) {
        devices = info.get_new_devices();
        load_device_info(devices[0]);
      }
    });
  }

  void clear_device_info() {
    device_sensors.clear();
    video_stream_profiles_by_stream_type.clear();
    device_sensor_idxes_by_stream_type.clear();
    active_streams.clear();
  }

  void load_device_info(rs2::device d) {

    dev = d;
    dev_set = true;

    clear_device_info();

    // make sure advanced mode is enabled
    auto dev_adv = dev.as<rs400::advanced_mode>();
    if (!dev_adv.is_enabled()) { dev_adv.toggle_advanced_mode(true); }

    vector<rs2::sensor> dev_sensors = dev.query_sensors();
    for (int i = 0; i < dev_sensors.size(); i++) {
      auto s = dev_sensors[i];
      device_sensors.push_back(s);

      auto sensor_stream_profiles = s.get_stream_profiles();
      for (auto p : sensor_stream_profiles) {
        rs2_stream stream_type = p.stream_type();

        device_sensor_idxes_by_stream_type[stream_type] = i;

        rs2::video_stream_profile video_profile = p.as<rs2::video_stream_profile>();
        video_stream_profiles_by_stream_type[stream_type].push_back(video_profile);
      }
    }

    stream_changes_queue.push([&]() { config_callback(*this); });

    start_desired_streams();
  }

  void stop_stream(rs2_stream stream_type) {
    lock_guard<mutex> lock(active_streams_mutex);

    if (active_streams[stream_type].active) {
      rs2::sensor s = device_sensors[device_sensor_idxes_by_stream_type[stream_type]];
      s.stop();
      s.close();
      active_streams[stream_type].active = false;
    }
  }

  void start_stream(StreamProfile stream_profile) {
    lock_guard<mutex> lock(active_streams_mutex);
    auto& active_stream = active_streams[stream_profile.type];

    if (!active_stream.active) {
      const auto rs_p = get_rs_stream_profile(stream_profile);

      rs2::sensor s = device_sensors[device_sensor_idxes_by_stream_type[stream_profile.type]];
      s.open(rs_p);
      active_streams[stream_profile.type].profile = stream_profile;
      s.start(active_streams[stream_profile.type].queue);
      active_streams[stream_profile.type].active = true;
    }
  }

  rs2::video_stream_profile get_rs_stream_profile(StreamProfile p) {
    vector<rs2::video_stream_profile> rs_profiles = video_stream_profiles_by_stream_type[p.type];
    for (auto rs_p : rs_profiles) {
      if (rs_p.fps() == p.fps && rs_p.format() == p.format && rs_p.width() == p.dims.x &&
          rs_p.height() == p.dims.y) {
        return rs_p;
      }
    }

    throw invalid_argument("Could not find requested stream profile");
  }

  void start_desired_streams() {

    if (dev_set) {

      unordered_set<rs2_stream> new_active_streams;
      for (auto s : desired_streams) {
        new_active_streams.insert(s.type);
      }

      unordered_set<rs2_stream> current_active_streams;
      for (auto s : active_streams) {
        const rs2_stream stream_type = s.first;
        if (s.second.active && !new_active_streams.count(stream_type)) {
          stream_changes_queue.push([&, stream_type]() { stop_stream(stream_type); });
        }
      }

      for (auto stream : desired_streams) {
        bool matching_stream_already_exists = false;
        bool stream_type_already_active = false;
        if (active_streams.count(stream.type) && active_streams[stream.type].active) {
          StreamProfile& active_profile = active_streams[stream.type].profile;
          stream_type_already_active = true;
          if (active_profile.dims.x == stream.dims.x && active_profile.dims.y == stream.dims.y &&
              active_profile.format == stream.format && active_profile.fps == stream.fps) {
            matching_stream_already_exists = true;
          }
        }

        if (!matching_stream_already_exists) {
          if (stream_type_already_active) {
            stream_changes_queue.push([&, stream]() { stop_stream(stream.type); });
          }
          stream_changes_queue.push([&, stream]() { start_stream(stream); });
        }
      }
    }
  }
};