#pragma once

#include <engine/Engine.h>

#include <Components/Midi.hpp>

#include <chrono>

using namespace glm;

class NotesManager {
public:
  DepthCameraMesh<Engine::Scene::MatrixPosition> note_mesh;

  DepthCameraMesh<Engine::Scene::MatrixPosition> note_outline_mesh;

  struct NoteScale {
    string name;
    vector<int> semi_step;
  };

  struct FnHolder {
    static bool ItemGetter(void* data, int idx, const char** out_str) {
      auto* d = (vector<string>*)data;
      *out_str = d->at(idx).c_str();
      return true;
    }

    static bool ScaleItemGetter(void* data, int idx, const char** out_str) {
      auto* d = (vector<NoteScale>*)data;
      *out_str = d->at(idx).name.c_str();
      return true;
    }
  };

  const int START_NOTE = 36;
  const float DEFAULT_NOTE_RADIUS = 420.;

  int root_note = START_NOTE;
  int next_note_id = 1;

  vector<NoteScale> scale_options{
      {"Chromatic (all)", {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}},
      {"Major", {2, 2, 1, 2, 2, 2, 1}},
      {"Pent. Major", {2, 2, 3, 2, 3}},
      {"Pent. Major +b3", {2, 1, 1, 3, 2, 3}},
      {"Natural Minor", {2, 1, 2, 2, 1, 2, 2}},
      {"Pent. Minor", {3, 2, 2, 3, 2}},
      {"Pent. Minor +b5", {3, 2, 1, 1, 3, 2}},
      {"Harmonic Minor", {2, 1, 2, 2, 1, 3, 1}},
      {"Melodic Minor", {2, 1, 2, 2, 2, 2, 1}},
  };

  vector<string> note_names = {"C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"};

  int current_scale = 0;
  int current_scale_note = 0;

  Components::Midi midi;

  vector<string> all_note_names;

  NotesManager() {

    for (int i = 0; i < 128; i++) {

      string note_name = get_note_name(i);
      all_note_names.push_back(note_name);
    }

    // make a circle.. must be a better way!
    note_mesh.set_render_mode(
        DepthCameraMesh<Engine::Scene::MatrixPosition>::RenderMode::FullColor);

    note_outline_mesh.set_render_mode(
        DepthCameraMesh<Engine::Scene::MatrixPosition>::RenderMode::FullColor);

    vector<vec4> note_mesh_positions_cpu;
    vector<uint32_t> note_mesh_idxes_cpu;
    note_mesh_positions_cpu.push_back({0, 0, 0, 1});

    vector<vec4> note_mesh_outline_positions_cpu;
    vector<uint32_t> note_mesh_outline_idxes_cpu;

    const int num_segments = 30;
    for (int i = 0; i < num_segments; i++) {
      const float theta = (M_PI * 2.f * i / num_segments);

      vec4 p = {cos(theta), sin(theta), 0, 1};
      note_mesh_positions_cpu.push_back(p);

      note_mesh_outline_positions_cpu.push_back(p * vec4{0.94, 0.94, 1, 1});
      note_mesh_outline_positions_cpu.push_back(p * vec4{1.001, 1.001, 1, 1});

      note_mesh_outline_idxes_cpu.push_back(i * 2);
      note_mesh_outline_idxes_cpu.push_back(i * 2 + 1);
      note_mesh_outline_idxes_cpu.push_back((i * 2 + 2) % (num_segments * 2));

      note_mesh_outline_idxes_cpu.push_back((i * 2 + 1) % (num_segments * 2));
      note_mesh_outline_idxes_cpu.push_back((i * 2 + 2) % (num_segments * 2));
      note_mesh_outline_idxes_cpu.push_back((i * 2 + 3) % (num_segments * 2));

      note_mesh_idxes_cpu.push_back(0);
      note_mesh_idxes_cpu.push_back(i + 1);
      note_mesh_idxes_cpu.push_back(((i + 1) % num_segments) + 1);
    }

    note_mesh.idxes.set_data(note_mesh_idxes_cpu);
    note_mesh.num_triangles = num_segments;
    note_mesh.positions.set_data(note_mesh_positions_cpu);

    note_outline_mesh.idxes.set_data(note_mesh_outline_idxes_cpu);
    note_outline_mesh.positions.set_data(note_mesh_outline_positions_cpu);
    note_outline_mesh.num_triangles = num_segments * 2;
  }

  int get_next_note() {

    int note = root_note;
    auto& semi_step = scale_options[current_scale].semi_step;
    for (int i = 0; i < current_scale_note; i++) {
      note += semi_step[i % semi_step.size()];
    }
    return note;
  }

  function<void(void)> click_surface(const vec2 note_pos) {
    if (mouse_hover_over_note == -1) {
      Note n;

      n.midi_note = get_next_note();
      current_scale_note++;

      n.note_id = next_note_id++;
      n.note_on = false;
      n.radius = DEFAULT_NOTE_RADIUS;
      n.pos = note_pos;

      n.z_rand_offset = static_cast<float>(rand()) / static_cast<float>(RAND_MAX);
      notes.push_back(n);

      return [] {};
    } else {
      // play the note, returning callback to stop playing note
      int a = hit(note_pos, 127);
      return [&, a]() { on_note_off(a); };
    }
    set_pending(false);
    return [] {};
  }

  void set_pending(bool _is_pending, const vec2 _pending_pos = vec2{0, 0}) {
    is_pending = _is_pending;
    pending_pos = _pending_pos;

    if (is_pending) { mouse_hover_over_note = get_note_idx(pending_pos); }

    mouse_hover_over_note = is_pending ? mouse_hover_over_note = get_note_idx(_pending_pos) : -1;
  }

  // returns -1 for no note, or idx is note found
  int get_note_idx(vec2 note_pos) {

    for (int i = 0; i < notes.size(); i++) {
      float dist = length(notes[i].pos - note_pos);
      if (dist < notes[i].radius) { return i; }
    }

    return -1;
  }

  int hit(vec2 note_pos, const int velocity) {

    int note_idx = get_note_idx(note_pos);
    if (note_idx == -1) { return 0; }

    auto& n = notes[note_idx];

    n.note_on = true;
//    chrono::high_resolution_clock::now();

    n.note_on_time = chrono::steady_clock::now();
    midi.send(144, n.midi_note, (unsigned char)velocity);
    printf("on  %d\n", n.midi_note);
    return n.note_id;
  }

  void on_note_off(int note_id) {

    for (auto& n : notes) {
      if (n.note_id == note_id) {
        n.note_on = false;
        midi.send(128, n.midi_note, (unsigned char)127);
        printf("off %d\n", n.midi_note);
      }
    }
  }

  string get_note_name(const int note_num, bool format_space = true) {

    const int c1 = 36;
    int diff = note_num - c1;

    int oct = (diff / 12) + (diff >= 0 ? 1 : 0);
    int n = diff % 12;
    while (n < 0) {
      n += 12;
    }

    string name = note_names[n] + to_string(oct);

    while (format_space && name.size() < 4) {
      name += " ";
    }

    name += " (" + to_string(note_num) + ")";
    return name;
  }

  /* Draws both local-on-note-ImGui and 3d renderings */
  template <class T>
  void render(ColoredMeshCamera<T>& cam, const ivec2 viewport_dims) {

    const long now_ms = chrono::duration_cast<chrono::milliseconds>(
                            chrono::high_resolution_clock::now().time_since_epoch())
                            .count();

    if (is_pending && mouse_hover_over_note == -1) {

      note_mesh.full_color = vec4{0.3,
                                  0.3,
                                  0.2,
                                  // give pending note a slight pulse
                                  0.3 + (0.15 * cos(now_ms * 0.002))};
      note_mesh.position.set(
          translate(vec3{pending_pos, -1.2}) *
          scale(vec3{DEFAULT_NOTE_RADIUS, DEFAULT_NOTE_RADIUS, 1.}));
      cam.draw(note_mesh);
    }

    ImGui::SetNextWindowPos({16, 132});

    ImGui::Begin("Notes", NULL, ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoMove);

    ImGui::Text("Scale:");

    ImGui::PushItemWidth(130.f);
    if (ImGui::Combo(
            "##scale",
            &current_scale,
            &FnHolder::ScaleItemGetter,
            &scale_options,
            scale_options.size())) {
      current_scale_note = 0;
    }
    ImGui::PopItemWidth();
    ImGui::SameLine();

    ImGui::PushItemWidth(90.f);

    if (ImGui::Combo(
            "##scale-root",
            &root_note,
            &FnHolder::ItemGetter,
            &all_note_names,
            all_note_names.size())) {

      current_scale_note = 0;
    }
    ImGui::PopItemWidth();

    {

      NoteScale ss = scale_options[current_scale];

      int semitone = 0;
      ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, {0., 3.});
      ImGui::PushStyleVar(ImGuiStyleVar_FramePadding, {3., 3.});
      ImGui::PushItemFlag(ImGuiItemFlags_Disabled, true);

      for (auto s : ss.semi_step) {
        for (int i = 0; i < s; i++) {
          ImGui::PushID(semitone);

          string note_name = note_names[(root_note + semitone) % 12];
          while (note_name.size() < 2) {
            note_name += " ";
          }

          if (i == 0) {
            ImGui::Button(note_name.c_str());
          } else {
            // after start, skipped note
            ImGui::PushStyleVar(ImGuiStyleVar_Alpha, ImGui::GetStyle().Alpha * 0.5f);
            ImGui::Button(note_name.c_str());

            ImGui::PopStyleVar();
          }

          ImGui::SameLine();
          ImGui::PopID();
          semitone++;
        }
      }

      ImGui::PopItemFlag();
      ImGui::PopStyleVar();
      ImGui::PopStyleVar();
    }

    ImGui::Text("");
    ImGui::Text("Next note: %-18s", get_note_name(get_next_note(), false).c_str());
    ImGui::SameLine();
    if (ImGui::Button(ICON_FA_UNDO)) { clear(); }
    ImGui::Text("");

    ImGui::BeginChild("Notes", {220, 200});

    bool note_hovered = false;
    int delete_note = -1;
    ImVec2 pos = ImGui::GetCursorScreenPos();
    for (int i = 0; i < notes.size(); i++) {
      auto& n = notes[i];
      ImGui::PushID((char)i);

      ImGui::SetCursorScreenPos(pos);
      bool showing_details_now = n.showing_details;
      ImVec2 box_dims{200, showing_details_now ? 82.f : 19.f};

      ImU32 bg_col = (mouse_hover_over_note == i || mouse_hover_over_note_imgui == i)
                         ? IM_COL32(220, 220, 200, 90)
                         : IM_COL32(220, 220, 200, 40);

      ImGui::GetWindowDrawList()->AddRectFilled(
          pos, {pos.x + box_dims.x, pos.y + box_dims.y}, bg_col, ImGui::GetStyle().FrameRounding);

      ImGui::BeginGroup();

      if (ImGui::Button(n.showing_details ? ICON_FA_ANGLE_DOWN : ICON_FA_ANGLE_RIGHT)) {
        n.showing_details = !n.showing_details;
      }
      ImGui::SameLine();

      ImGui::PushItemWidth(90.f);

      ImGui::Combo(
          "##midi-note",
          &n.midi_note,
          &FnHolder::ItemGetter,
          &all_note_names,
          all_note_names.size());

      ImGui::SameLine();

      bool colors_set = n.note_on;
      if (colors_set) {
        ImGui::PushStyleColor(ImGuiCol_Button, ImGui::GetStyle().Colors[ImGuiCol_ButtonActive]);
        ImGui::PushStyleColor(
            ImGuiCol_ButtonHovered, ImGui::GetStyle().Colors[ImGuiCol_ButtonActive]);
      }

      ImGui::Button(ICON_FA_PLAY_CIRCLE);

      if (ImGui::IsItemHovered() && ImGui::IsMouseDown(0) && !n.note_on) {
        hit(n.pos, 127);
        n.note_on_from_click = true;
      } else if (n.note_on_from_click && !ImGui::IsMouseDown(0)) {
        on_note_off(n.note_id);
        n.note_on_from_click = false;
      }

      if (colors_set) {
        ImGui::PopStyleColor();
        ImGui::PopStyleColor();
      }

      ImGui::SameLine();
      if (ImGui::Button(ICON_FA_TRASH)) { delete_note = i; }

      if (showing_details_now) {
        ImGui::SliderFloat("radius", &n.radius, 100., 5000., "%3.0f", 2.);
      }

      ImGui::EndGroup();

      if (ImGui::IsMouseHoveringRect(pos, {pos.x + box_dims.x, pos.y + box_dims.y}) ||
          ImGui::IsItemHovered()) {
        mouse_hover_over_note_imgui = i;
        note_hovered = true;
      }

      ImGui::PopID();

      pos.y = pos.y + box_dims.y + (2 * ImGui::GetStyle().FramePadding.y);
    }

    bool child_hovered = ImGui::IsWindowHovered();

    ImGui::Text(" ");

    ImGui::EndChild();

    if (!note_hovered) {
      if (ImGui::IsWindowHovered() || child_hovered) { mouse_hover_over_note_imgui = -1; }
    }

    ImGui::End();

    for (int i = 0; i < notes.size(); i++) {

      auto& n = notes[i];

      const float FADE_MS = 500.f;
      const long note_on_time_ms =
          chrono::duration_cast<chrono::milliseconds>(n.note_on_time.time_since_epoch()).count();
      const float ms_since_last_note = note_on_time_ms ? (now_ms - note_on_time_ms) * 1.f : FADE_MS;
      auto equation = [&](const float t) -> float {
        return t < FADE_MS ? (t - FADE_MS) * (t - FADE_MS) / (FADE_MS * FADE_MS) : 0;
      };

      const bool note_hover = (mouse_hover_over_note == i || mouse_hover_over_note_imgui == i);

      note_mesh.full_color =
          vec4{equation(ms_since_last_note), 0.5, 0.2, n.note_on ? 0.9 : note_hover ? 0.7 : 0.5};

      // -1 z value for z-fighting with plane.
      note_mesh.position.set(
          translate(vec3{n.pos, -3 + n.z_rand_offset + (note_hover ? -6 : 0)}) *
          scale(vec3{n.radius, n.radius, 1.}));
      cam.draw(note_mesh);

      note_outline_mesh.full_color =
          note_hover ? vec4{0.8, 0.8, 0.8, 0.8} : vec4{0.8, 0.8, 0.8, 0.5};

      note_outline_mesh.position.set(
          translate(vec3{n.pos, -6 + n.z_rand_offset + (note_hover ? -6 : 0)}) *
          scale(vec3{n.radius, n.radius, 1.}));
      cam.draw(note_outline_mesh);
    }

    // if user wanted to delete a note, do it here
    if (delete_note > -1) {
      notes.erase(notes.begin() + delete_note, notes.begin() + delete_note + 1);
      showing_note_detail = -1;
    }
  }

  // resets the whole notes manager setup
  void clear() {
    root_note = START_NOTE;
    next_note_id = 1;

    current_scale_note = 0;
    current_scale = 0;

    showing_note_detail = -1;
    notes.clear();
  }

private:
  class Note {
  public:
    vec2 pos;
    int midi_note;
    int note_id;
    bool note_on;
    bool note_on_from_click = false;
    bool showing_details = false;
    chrono::steady_clock::time_point note_on_time;

    float radius;

    float z_rand_offset;
  };

  // which of the current notes for which the UI detail is being shown, or -1 for none.
  int showing_note_detail = -1;

  // which of the current notes for which mouse is hovering over on 3d rendering, or -1 for none.
  int mouse_hover_over_note = -1;
  // which of the current notes for which mouse is hovering over on ImGui list of notes
  int mouse_hover_over_note_imgui = -1;

  vector<Note> notes;

  // xy coords of where mouse is hovering, and whether the mouse is actually hovering there
  bool is_pending;
  vec2 pending_pos;
};