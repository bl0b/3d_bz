#pragma once

#include "Plottable.hpp"
#include <engine/Engine.h>

class HandPositionsManager {
public:
  bool note_velocities_on = true;
  const float DEFAULT_MIN_VELOCITY = 0.2f;
  float min_velocity = DEFAULT_MIN_VELOCITY; // lightest hit is still 0.3
private:
  const int MAX_VELOCITY = 127;
  float min_slope = 20.f;
  float max_slope = 70.f;

  class HandPositionState {
  public:
    float note_on_threshold;
    float note_off_threshold;

    // fingertip musst be posted continuously for x frames before becoming 'live'
    const int frames_to_live = 8;

    const int frames_count_back = 8;

    const int MAX_VELOCITY = 127;
    bool note_velocities_on;
    float min_slope;
    float max_slope;
    float min_velocity;

    uint64_t tick_count = 0;

    bool live() { return tick_count > frames_to_live; }

    // list of last n z values!
    Plottable last_positions;

    bool note_on = false;
    int note_on_id = 0;
    vec2 note_xy_coords;

    vec3 last_coords;

    void push(vec3 pos, function<int(vec2, int)> on_note, function<void(int)> on_note_off) {
      if (live()) {
        last_positions.put(pos.z);
        last_coords = pos;

        if (pos.z < note_on_threshold && !note_on) {

          vector<float> last_z_values;
          last_positions.get_last(frames_count_back, last_z_values);

          float max_val = 0.;
          int max_val_time = 0;
          for (int i = 0; i < last_z_values.size(); i++) {
            const float v = last_z_values[i] - last_z_values[0];
            if (v > max_val) {
              max_val = v;
              max_val_time = i;
            }
          }

          int velocity = MAX_VELOCITY;

          if (note_velocities_on) {

            float slope = max_val / max_val_time;

            slope = slope < min_slope ? min_slope : slope;
            slope = slope > max_slope ? max_slope : slope;
            slope = ((slope - min_slope) / (max_slope - min_slope));
            slope = (1.0f - min_velocity) * slope;
            slope = min_velocity + slope;

            velocity = (int)round(slope * 127.);
          }

          note_on = true;
          note_on_id = on_note(pos.xy(), velocity);
          note_xy_coords = last_coords.xy();

        } else if (note_on && pos.z > (note_on_threshold + note_off_threshold)) {
          note_on = false;
          on_note_off(note_on_id);
          note_on_id = 0;
        }
      }
      tick_count++;
    }

    void push_empty(function<void(int)> on_note_off) {
      if (note_on) {
        note_on = false;
        on_note_off(note_on_id);
        note_on_id = 0;
      }
      tick_count = 0;
      last_positions.clear();
    }
  };

public:
  int max_num_groups;

  vector<HandPositionState> hand_positions;

  const float DEFAULT_NOTE_ON_THRESHOLD = 170.f;
  const float DEFAULT_NOTE_OFF_THRESHOLD = 10.f;

  float note_on_threshold = DEFAULT_NOTE_ON_THRESHOLD;
  float note_off_threshold = DEFAULT_NOTE_OFF_THRESHOLD;

  DepthCameraMesh<Engine::Scene::MatrixPosition> position_mesh;

  HandPositionsManager() {

    // todo: better way to make a cube! (and all primitives, for that matter)
    position_mesh.num_triangles = 12;
    {
      auto idxes = vector<uint32_t>{0, 1, 2, 0, 2, 3, 0, 1, 5, 0, 5, 4,
                                    1, 5, 6, 1, 6, 2, 2, 6, 7, 2, 7, 3,
                                    0, 3, 7, 0, 7, 4, 4, 5, 6, 5, 6, 7};
      position_mesh.idxes.set_data(idxes);
      auto vtxes = vector<vec4>{
          vec4{-0.5, -0.5, -0.5, 1},
          vec4{0.5, -0.5, -0.5, 1},
          vec4{0.5, 0.5, -0.5, 1},
          vec4{-0.5, 0.5, -0.5, 1},
          vec4{-0.5, -0.5, 0.5, 1},
          vec4{0.5, -0.5, 0.5, 1},
          vec4{0.5, 0.5, 0.5, 1},
          vec4{-0.5, 0.5, 0.5, 1}};
      position_mesh.positions.set_data(vtxes);
    }

    position_mesh.set_render_mode(
        DepthCameraMesh<Engine::Scene::MatrixPosition>::RenderMode::FullColor);
  }

  template <class T>
  void render_positions(MeshCamera<T>& cam) {

    // scale for all fingertip position markers from a 1x1x1 cube (0.1 mm)
    const float s = 50.;

    for (auto& h_pos : hand_positions) {
      if (h_pos.live()) {

        const mat4 t_form =
            translate(vec3{h_pos.last_coords.xy(), -h_pos.last_coords.z}) * scale(vec3{s, s, s});

        position_mesh.full_color = h_pos.note_on ? vec4{.02, 1., .02, 1.} : vec4{.2, .7, .2, 1.};

        position_mesh.position.set(t_form);
        cam.draw(position_mesh);
      }
    }
  }

  void
  put(unordered_map<int, vec3>& new_positions,
      function<int(vec2, int)> on_note,
      function<void(int)> on_note_off) {

    if (hand_positions.size() != max_num_groups) { hand_positions.resize(max_num_groups); }

    for (int i = 0; i < max_num_groups; i++) {
      if (new_positions.count(i + 1)) {

        hand_positions[i].note_on_threshold = note_on_threshold;
        hand_positions[i].note_off_threshold = note_off_threshold;

        hand_positions[i].min_velocity = min_velocity;
        hand_positions[i].note_velocities_on = note_velocities_on;
        hand_positions[i].min_slope = min_slope;
        hand_positions[i].max_slope = max_slope;

        hand_positions[i].push(new_positions[i + 1], on_note, on_note_off);
      } else {
        hand_positions[i].push_empty(on_note_off);
      }
    }
  }

  void draw_imgui() {

    ImGui::SliderFloat("on", &note_on_threshold, 100, 250);
    ImGui::SliderFloat("off", &note_off_threshold, 0, 50);

    ImGui::Text("note velocity checking");
    ImGui::Checkbox("?", &note_velocities_on);

    if (note_velocities_on) { ImGui::SliderFloat("min vel", &min_velocity, 0., 1.); }

    for (int i = 0; i < max_num_groups; i++) {

      auto& h = hand_positions[i];

      if (h.live()) {

        ImGui::Text("Active: %d", i);

        if (h.note_on) {
          ImGui::SameLine();
          ImGui::Text(" (note on!)");
        }

        h.last_positions.dims = {400, 150};
        h.last_positions.title = ":)";
        h.last_positions.range = {0, 800};
        h.last_positions.draw();
      }
    }

    ImGui::Text("---");
  };
};