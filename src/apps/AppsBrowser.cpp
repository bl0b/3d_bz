#include <engine/Engine.h>

/*
#include "AutoCalibrate/AutoCalibrateDemo.hpp"
#include "AutoCalibrate/AutoCalibrateOnly.hpp"
#include "AutoCalibrate/ClipLoader.hpp"

#include "Apps/RsViewer.hpp";
#include "Apps/BtTest.hpp";
#include "AutoCalibrate/CubeEstimate.hpp"
#include "AutoCalibrate/HandSizeFit.hpp"
*/
#include "Apps/SimpleMidiDrumpad.hpp";

#include "AutoCalibrate/NurbsDemo.hpp"
#include "apps/HandPoseEstimation/NodesDemo.hpp"
#include "Apps/HandPoseEstimation/ObjectsDemo.hpp"

constexpr int WINDOW_W = 1400; // 1080;
constexpr int WINDOW_H = 800; // 720;
// todo: verify that everything actually works on a 4.3 card - ideally 4.1 would work (mac!)
constexpr int _GL_MAJ = 4;
constexpr int _GL_MIN = 3;

const Engine::Gpu::GraphicsCtx::Cfg window_cfg{
    WINDOW_W, WINDOW_H, "~-/ (.)(.) \\-~", _GL_MAJ, _GL_MIN};

int main(int argc, char** argv) {

  FileUtil::init(argc, argv);

  AppViewer viewer;
  viewer.set_window_config(&window_cfg);

  viewer.register_app("Nodes demo", [](Engine::Gpu::GraphicsCtx& c) { return new NodesDemo(c); });

  viewer.register_app("Objetcs demo", [](Engine::Gpu::GraphicsCtx& c) { return new ObjectsDemo(c); });


  // viewer.register_app(
  //"Nurbs hand fit", [](Engine::Gpu::GraphicsCtx& c) { return new NurbsHandFit(c); });

  viewer.register_app(
      "Demo de NURBS", [](Engine::Gpu::GraphicsCtx& c) { return new NurbsDemo(c); });

  /*
   *
   Commenting out for now.. will it compile faster?

  viewer.register_app(
      "Cube Estimator", [](Engine::Gpu::GraphicsCtx& c) { return new CubeEstimate(c); });

  viewer.register_app(
      "HandSizeFit", [](Engine::Gpu::GraphicsCtx& c) { return new HandSizeFit(c); });
//
//  viewer.register_app(
//      "Cube Estimator", [](Engine::Gpu::GraphicsCtx& c) { return new CubeEstimate(c); });

  viewer.register_app("Clip Loader", [](Engine::Gpu::GraphicsCtx& c) { return new ClipLoader(c); });

  viewer.register_app("BtTest", [](Engine::Gpu::GraphicsCtx& c) { return new BtTest(c); });

  viewer.register_app(
      "Full system", [](Engine::Gpu::GraphicsCtx& c) { return new AutoCalibrateDemo(c); });

  viewer.register_app(
      "Just calibration", [](Engine::Gpu::GraphicsCtx& c) { return new AutoCalibrateOnly(c); });

  // Test app for realsense functionality
  viewer.register_app(
      "Realsense viewer", [](Engine::Gpu::GraphicsCtx& c) { return new RsViewer(c); });

  // Test app for MIDI functionality
  viewer.register_app(
      "Midi Drumpad", [](Engine::Gpu::GraphicsCtx& c) { return new SimpleMidiDrumpad(c); });
  */
  viewer.run();

  return 0;
}
