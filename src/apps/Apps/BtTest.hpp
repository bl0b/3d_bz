#include <stdio.h>

#include <engine/Engine.h>

#include <thread>
#include <chrono>

#include <Components/Physics.hpp>

using namespace PhysicsConversion;

class BtTest : public AbstractApp {
public:
  ColoredMeshCamera<Engine::Scene::ArcBallCameraPosition> colored_mesh_cam;

  DepthCameraMesh<Engine::Scene::MatrixPosition> m_origin;

  PhysicsCtx physics_ctx;
  PhysicsObject box1;
  PhysicsObject box2;
  PhysicsObject box3;

  BtTest(Engine::Gpu::GraphicsCtx& _w) : AbstractApp(_w) {

    // Defaults for arcball cam
    colored_mesh_cam.position.config.distance = 100.f;
    colored_mesh_cam.config.clip_near = 1.f;
    colored_mesh_cam.config.clip_far = 500.f;
    colored_mesh_cam.config.mirror = false;

    {
      auto idxes = vector<unsigned int>{0, 1, 2, 0, 2, 3};
      m_origin.num_triangles = 2;
      m_origin.idxes.set_data(idxes);
      auto vtxes =
          vector<vec4>{{-1., -1., 0., 1.}, {1., -1., 0., 1.}, {1., 1., 0., 1.}, {-1., 1., 0., 1.}};
      m_origin.positions.set_data(vtxes);
      auto colors =
          vector<vec4>{{1., 1., 0., 1.}, {1., 1., 0., 1.}, {0., 1., 0., 1.}, {1., 0., 0.5, 1.}};
      m_origin.colors.set_data(colors);
    }

    // Enable ESC to close
    w.on_press(GLFW_KEY_ESCAPE, [&] { w.set_should_close(true); });

    physics_ctx.m_dynamics_world->setGravity({0., 0., 0.});

    box1.make({5, 5, .5}, 10., glm::translate(vec3{0., 3., -15.}));
    box2.make({5, 5, .5}, 10., glm::translate(vec3{0., 13., -15.}));

    box3.make(vec3{8, 8, 10}, 0., glm::translate(vec3{0., 0., 10}));

    physics_ctx.m_dynamics_world->addRigidBody(box1.body);
    physics_ctx.m_dynamics_world->addRigidBody(box2.body);
    physics_ctx.m_dynamics_world->addRigidBody(box3.body);

    mat4 tf0 = glm::translate(vec3{0., 5., 0.});
    mat4 tf1 = glm::translate(vec3{0., -5., 0.});
    btGeneric6DofSpring2Constraint* c =
        new btGeneric6DofSpring2Constraint(*box1.body, *box2.body, toBt(tf0), toBt(tf1));

    c->setAngularLowerLimit({-0.5, 0, -0.5});
    c->setAngularUpperLimit({0.5, 0, 0.5});

    physics_ctx.m_dynamics_world->addConstraint(c, true);

    box1.body->setActivationState(DISABLE_DEACTIVATION);
    box2.body->setActivationState(DISABLE_DEACTIVATION);

    box1.body->applyForce({0., 0., 50.}, {0, 2, 0.});
    box2.body->applyForce({-20., 0., 0.}, {0., 0., 0.});
  }

  void tick() override {

    GL::defaultFramebuffer.bind();
    GL::Renderer::setPolygonMode(GL::Renderer::PolygonMode::Line);
    GL::Renderer::disable(GL::Renderer::Feature::FaceCulling);

    const float timestep = 0.1f;

    physics_ctx.m_dynamics_world->stepSimulation(timestep, 10);

    m_origin.position.set(box1.get_current_tform() * glm::scale(vec3{5., 5., 1.}));
    colored_mesh_cam.draw(m_origin);

    m_origin.position.set(box2.get_current_tform() * glm::scale(vec3{5., 5., 1.}));
    colored_mesh_cam.draw(m_origin);

    m_origin.position.set(glm::translate(vec3{0., 0., 0.}) * glm::scale(vec3{8., 8., 1.}));
    colored_mesh_cam.draw(m_origin);

    // camera GUI on top-right of screen
    const float cam_gui_width = 64;
    const float top_bar_height = 16;
    const float gui_right_padding = 16;
    const float gui_top_padding = 16;
    colored_mesh_cam.position.config.min_distance = 0.5;
    colored_mesh_cam.position.draw_imgui_pretty(
        true,
        {w.w - (cam_gui_width + gui_right_padding), gui_top_padding},
        &colored_mesh_cam.config.mirror);

    // std::this_thread::sleep_for(std::chrono::milliseconds(5));
  }
};
