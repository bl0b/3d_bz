#pragma once

#include <engine/Engine.h>

#include <Components/RsCtxLive.hpp>

class RsViewer : public AbstractApp {
public:
  RsCtxLive rs;

  rs2::frame depth_frame;
  rs2::frame color_frame;

  // Incoming depth image, and image for RGBA interpretation
  Engine::Gpu::Tex<Engine::Gpu::TF::R16UI> depth_in;
  Engine::Gpu::Tex<Engine::Gpu::TF::RGBA8> depth_in_rgba;

  // Incoming color image
  Engine::Gpu::Tex<Engine::Gpu::TF::RGBA8> rgba_in;

  ComputeLib::MapDepth map_depth;

  const float DEPTH_UNITS = 0.0001;

  RsViewer(Engine::Gpu::GraphicsCtx& _w) : AbstractApp(_w) {

    // Enable ESC to close
    w.on_press(GLFW_KEY_ESCAPE, [&] { w.set_should_close(true); });

    rs.set_gui_control(true);
    rs.init();
  }

  void tick() override {

    ImGui::Begin("Realsense");
    rs.draw_imgui();
    ImGui::End();

    if (rs.is_active(rs2_stream::RS2_STREAM_DEPTH)) {
      ImGui::Begin("Depth");

      bool new_frame = rs.fetch_frame_if_available(RS2_STREAM_DEPTH, RS2_FORMAT_Z16, &depth_frame);

      if (new_frame) {
        const ivec2 dims = rs.active_streams[rs2_stream::RS2_STREAM_DEPTH].profile.dims;
        depth_in.set_data(dims, depth_frame.get_data());
        depth_in_rgba.set_storage(dims);

        map_depth.run(depth_in, depth_in_rgba);
      }

      Engine::ImGuiCtx::Image2(depth_in_rgba);

      ImGui::End();
    }

    if (rs.is_active(rs2_stream::RS2_STREAM_COLOR)) {
      ImGui::Begin("Color");

      bool new_frame =
          rs.fetch_frame_if_available(RS2_STREAM_COLOR, RS2_FORMAT_RGBA8, &color_frame);

      if (new_frame) {
        const ivec2 dims = rs.active_streams[rs2_stream::RS2_STREAM_COLOR].profile.dims;
        rgba_in.set_data(dims, color_frame.get_data());
      }

      Engine::ImGuiCtx::Image2(rgba_in);

      ImGui::End();
    }

    this_thread::sleep_for(chrono::milliseconds(10));
  }
};
