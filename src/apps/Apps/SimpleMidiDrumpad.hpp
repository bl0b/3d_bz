#pragma once

#include <engine/Engine.h>

#include <Components/Midi.hpp>

/** Simple app meant to prove that the MIDI layer is working. */
class SimpleMidiDrumpad : public AbstractApp {
public:
  Components::Midi midi;

  SimpleMidiDrumpad(Engine::Gpu::GraphicsCtx& _w) : AbstractApp(_w) {

    // Enable ESC to close
    w.on_press(GLFW_KEY_ESCAPE, [&] { w.set_should_close(true); });

    vector<int> keys = {GLFW_KEY_V,
                        GLFW_KEY_B,
                        GLFW_KEY_N,
                        GLFW_KEY_M,
                        GLFW_KEY_F,
                        GLFW_KEY_G,
                        GLFW_KEY_H,
                        GLFW_KEY_J,
                        GLFW_KEY_R,
                        GLFW_KEY_T,
                        GLFW_KEY_Y,
                        GLFW_KEY_U};

    w.on_press(keys[0], true, [&] { send_msg(36 + 0); });
    w.on_press(keys[1], true, [&] { send_msg(36 + 1); });
    w.on_press(keys[2], true, [&] { send_msg(36 + 2); });
    w.on_press(keys[3], true, [&] { send_msg(36 + 3); });
    w.on_press(keys[4], true, [&] { send_msg(36 + 4); });
    w.on_press(keys[5], true, [&] { send_msg(36 + 5); });
    w.on_press(keys[6], true, [&] { send_msg(36 + 6); });
    w.on_press(keys[7], true, [&] { send_msg(36 + 7); });
    w.on_press(keys[8], true, [&] { send_msg(36 + 8); });
    w.on_press(keys[9], true, [&] { send_msg(36 + 9); });
    w.on_press(keys[10], true, [&] { send_msg(36 + 10); });
    w.on_press(keys[11], true, [&] { send_msg(36 + 11); });
  }

  void tick() override {
    GL::Renderer::setClearColor(0x220088_rgbf);
    GL::defaultFramebuffer.clear(GL::FramebufferClear::Color | GL::FramebufferClear::Depth);

    ImGui::Begin("Midi!");
    midi.draw_ui();

    ImGui::Text("---");

    for (int i = 0; i < 4; i++) {
      for (int j = 0; j < 4; j++) {
        const int button = 36 + j + (i * 4);
        const string a = "-" + to_string(button) + "-";

        if (ImGui::Button(a.c_str(), {100, 100})) { send_msg(button); }

        if (j < 3) { ImGui::SameLine(); }
      }
    }

    ImGui::End();
  }

  void send_msg(const int button) {
    midi.send(144, button, 90);
    midi.send(128, button, 90);
  }
};
