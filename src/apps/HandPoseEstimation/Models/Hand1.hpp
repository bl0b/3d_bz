#pragma once

// approx. rectangular patch that stretches from 2 knuckles back to wrist
class HandPatch1 {
public:
  static string PRIMITIVE_ID() { return "nurbs-hand-patch-1"; }

  static void init(NurbsObj& o, ivec2 eval_dims) {
    o.s.degree_u = 3;
    o.s.degree_v = 2;
    o.s.knots_u = {0, 0, 0, 0, 0.5, 1, 1, 1, 1};
    o.s.knots_v = {0, 0, 0, 1, 1, 1};
    const ivec2 cp_dims = {5, 3};
    o.gen_dims(eval_dims, cp_dims);
  }

  static void gen_cp_set(
      vector<vec4> b_pts,
      vec4 k_p0,
      vec4 k_p1,
      vector<vec4>& cp_sets,
      mat4 bake_tform = mat4{1.f}) {

    // first row - wrist
    for (int i = 0; i < 5; i++) { cp_sets.emplace_back(bake_tform * b_pts[i]); }

    // second row - flat section to flatten dips where knuckles are
    for (int i = 0; i < 5; i++) {
      vec3 m_p0 = ((0.75f * k_p0) + (0.25f * b_pts[0])).xyz();
      vec3 m_p1 = ((0.75f * k_p1) + (0.25f * b_pts[4])).xyz();
      vec3 m_p = vec4{(m_p0 * (4.f - i) / 4.f) + (m_p1 * (i / 4.f)), 1.f}.xyz;
      vec4 pos = bake_tform * vec4{m_p, 1.f};
      cp_sets.emplace_back(vec4{pos.xyz, 4.f});
    }

    // knuckle row
    for (int i = 0; i < 5; i++) {
      vec3 i_pos = vec4{(k_p0.xyz * (4.f - i) / 4.f) + (k_p1.xyz * (i / 4.f)), 1.f}.xyz;
      if (i == 2) i_pos.z += 100.f;
      const float w = (i == 0 || i == 4) ? 1.f : 2.f;
      vec4 pos = bake_tform * vec4{i_pos, 1.f};
      cp_sets.emplace_back(vec4{pos.xyz, w});
    }
  }
};

// approx. rectangular patch that covers far pinkie side of hand
class HandPatch2 {
public:
  static string PRIMITIVE_ID() { return "nurbs-hand-patch-2"; }

  static void init(NurbsObj& o, ivec2 eval_dims) {
    // o.s.degree_u = 3;
    o.s.degree_u = 2;
    o.s.degree_v = 2;
    o.s.knots_u = {0, 0, 0, 0.5, 0.5, 1, 1, 1};
    // o.s.knots_u = {0, 0, 0, 0, 0.5, 1, 1, 1, 1};
    o.s.knots_v = {0, 0, 0, 1, 1, 1};
    const ivec2 cp_dims = {5, 3};
    o.gen_dims(eval_dims, cp_dims);
  }

  static void gen_cp_set(
      vector<vec4> b_pts,
      vector<vec4> k_pts,
      float bulge_pos,
      float bulge_scale,
      vector<vec4>& cp_sets,
      mat4 bake_tform = mat4{1.f}) {

    // first row - wrist
    for (int i = 0; i < 5; i++) { cp_sets.emplace_back(bake_tform * b_pts[i]); }
    for (int i = 0; i < 5; i++) {
      auto pt = (bulge_pos * b_pts[i]) + ((1.f - bulge_pos) * k_pts[i]);
      if (i == 2) {
        pt.y *= bulge_scale;
      } else if (i == 1 || i == 3) {
        pt.y *= (bulge_scale + 1.f) / 2.f;
      }
      cp_sets.emplace_back(bake_tform * pt);
    }
    for (int i = 0; i < 5; i++) { cp_sets.emplace_back(bake_tform * k_pts[i]); }
  }
};

class Hand1 {
public:
  static string WORLD_CONFIG_TYPE_ID() { return "Hand1"; }

  vec2 wrist_scale{180, 270};
  vec2 elbow_scale{250, 250};
  float arm_length = 2500.f;

  mat4 wrist_origin = mat4{1.f};

  float k_base_length = 800.f;
  // index-ring-pinkie
  // skip middle finger, which is k_base_length
  float k_back[3] = {30., 180., 270.};

  // scale of fingers vs knuckles!
  float f_scale_mult = 0.75;
  float k_scale[4] = {105., 105., 105., 95.};
  // clang-format off
  float f_length[4][3] = {
    {260, 220, 120}, 
    {260, 220, 120},
    {260, 220, 120},
    {200, 170, 100}};
  // clang-format on

  bool rh = false;
  float t_theta = 0.9;
  vec2 t_xy = {150., 250.};
  float t_scale = 110.;
  float t_length0 = 300.;
  float t_length1 = 300.;
  float t_length2 = 200.;

  float p_bulge_pos = 0.85;
  float p_bulge_scale = 1.2f;
  float i_bulge_pos = 0.64;
  float i_bulge_scale = 2.3f;

  static void init(MultiplePhysicsWorlds& worlds) {

    worlds.add_config_world([](btMultiBodyDynamicsWorld* w) { w->setGravity({0., 0., 0.}); });

    const auto noop_body_config = [](Hand1& c, btRigidBody* b) {};

    const auto mass_fn = [](float mass) { return [mass](Hand1& c) { return mass; }; };

    PhysicsWorldPrimitives::Cylinder::Params c_p;
    c_p.c_eval_f = {4, 12};
    c_p.c_eval_d = {4, 12};
    c_p.h_eval_f = {6, 6};
    c_p.tip0 = false;
    c_p.tip1 = true;
    c_p.physics_scale = PhysicsWorldPrimitives::Cylinder::PhysicsRadiusScale::MIN;

    PhysicsWorldPrimitives::Cylinder::add_to_worlds<Hand1>(
        worlds,
        [](auto& c) {
          auto& _s = c.wrist_scale;
          const float s = _s.x > _s.y ? _s.y : _s.x;
          return NurbsPrimitives::Cylinder::Config{c.elbow_scale, vec2{s, s}, c.arm_length};
        },
        [](auto& c) {
          return c.wrist_origin * glm::translate(vec3{(c.arm_length) / -2.f, 0., 0.});
        },
        mass_fn(2.f),
        noop_body_config,
        c_p);

    c_p.tip0 = true;
    c_p.tip1 = true;

    const auto hand_body_id = PhysicsWorldPrimitives::Hull_16::add_to_worlds<Hand1>(
        worlds,
        [](auto& vtxes, auto& c) {
          const auto to_h_local = glm::inverse(c.hand_origin_from_wrist());

          // 0 to 10
          auto wp = [&c, to_h_local](int i) {
            const float t = i * float(M_PI) / 5.f;
            vec2 xy = c.wrist_scale.yx * vec2{cos(t), -sin(t)};
            return to_h_local * vec4{0.f, xy, 1.f};
          };

          // 0 to 4
          auto kp = [&c, to_h_local](bool top, int i) {
            vec3 p = c.knuckle_pos(i);
            p.z = (top ? -1.f : 1.f) * c.k_scale[i];
            return to_h_local * vec4{p, 1.f};
          };

          for (int i = 0; i < 10; i++) {
            if (i == 0 || i == 5) continue;
            vtxes.emplace_back(wp(i));
          }
          if (c.rh) {
            for (int i = 0; i < 4; i++) { vtxes.emplace_back(kp(true, i)); }
            for (int i = 3; i >= 0; i--) { vtxes.emplace_back(kp(false, i)); }
          } else {
            for (int i = 3; i >= 0; i--) { vtxes.emplace_back(kp(true, i)); }
            for (int i = 0; i < 4; i++) { vtxes.emplace_back(kp(false, i)); }
          }
        },
        [](auto& c) { return c.wrist_origin * c.hand_origin_from_wrist(); },
        [](auto& c) { return 3.f; },
        [](auto& c, auto* b) {},
        false);

    // arm-hand (wrist) joint
    worlds.add_joint<Hand1>(
        [](auto& c, auto& j) {
          j.body_idxes = {0, 1};
          j.origin_body0 = glm::translate(vec3{c.arm_length / 2.f, 0., 0.});
        },
        [](auto& c, btGeneric6DofSpring2Constraint* j) {
          j->setAngularLowerLimit({0., -.8, 0});
          j->setAngularUpperLimit({0., 1.3, 0});
        });

    auto wps = [](Hand1& c, int i0, int i1) {
      vector<vec4> p;
      for (int i = 0; i < 5; i++) {
        const float t0 = i0 * float(M_PI) / 5.f;
        const float t1 = i1 * float(M_PI) / 5.f;
        const float t = t0 + ((t1 - t0) * i / 4.f);
        vec2 xy = c.wrist_scale.yx * vec2{cos(t), -sin(t)};
        p.emplace_back(vec4{0.f, xy, 1.f});
      }
      return std::move(p);
    };

    auto fp = [](Hand1& c, int i) {
      vec3 p = c.knuckle_pos(i);
      p.z = -1.f * c.k_scale[i]; // *c.f_scale_mult;
      return vec4{p, 1.f};
    };

    for (int i = 0; i < 3; i++) {
      worlds.add_full_nurbs_obj<HandPatch1, Hand1>(
          ivec2{10, 14}, hand_body_id, [i, wps, fp](vector<vec4>& cps, Hand1& c) {
            HandPatch1::gen_cp_set(
                c.rh ? wps(c, i + 2, i + 1) : wps(c, 3 - i, 4 - i),
                fp(c, i + 1),
                fp(c, i),
                cps,
                glm::inverse(c.hand_origin_from_wrist()));
          });
    }

    // pinky side!
    worlds.add_full_nurbs_obj<HandPatch2, Hand1>(
        ivec2{6, 12}, hand_body_id, [wps, fp](vector<vec4>& cps, Hand1& c) {
          auto _wps = c.rh ? wps(c, 6, 4) : wps(c, -1, 1);
          vector<vec4> _wps2;

          const auto k_pos = c.knuckle_pos(3);
          for (int j = 0; j < 5; j++) {
            const float t = (float)M_PI * (j / 4.f);
            vec2 l = vec2{sin(t) * (c.rh ? -1.f : 1.f), cos(t)} * c.k_scale[3];
            _wps2.emplace_back(vec4{k_pos + vec3{0, l.x, l.y}, 1.f});
          }

          HandPatch2::gen_cp_set(
              _wps,
              _wps2,
              c.p_bulge_pos,
              c.p_bulge_scale,
              cps,
              glm::inverse(c.hand_origin_from_wrist()));
        });

    // thumb side!
    worlds.add_full_nurbs_obj<HandPatch2, Hand1>(
        ivec2{6, 12}, hand_body_id, [wps, fp](vector<vec4>& cps, Hand1& c) {
          auto _wps = c.rh ? wps(c, -1, 1) : wps(c, 6, 4);
          vector<vec4> _wps2;

          const auto k_pos = c.knuckle_pos(0);
          for (int j = 0; j < 5; j++) {
            const float t = (float)M_PI * (j / 4.f);
            vec2 l = vec2{sin(t) * (c.rh ? 1.f : -1.f), cos(t)} * c.k_scale[0];
            _wps2.emplace_back(vec4{k_pos + vec3{0, l.x, l.y}, 1.f});
          }

          HandPatch2::gen_cp_set(
              _wps,
              _wps2,
              c.i_bulge_pos,
              c.i_bulge_scale,
              cps,
              glm::inverse(c.hand_origin_from_wrist()));
        });

    // 4 fingers
    for (int i = 0; i < 4; i++) {
      // 3 sections per finger
      for (int j = 0; j < 3; j++) {

        // after 1st..
        c_p.tip0 = j == 0;

        PhysicsWorldPrimitives::Cylinder::add_to_worlds<Hand1>(
            worlds,
            [i, j](Hand1& c) {
              const float s = c.k_scale[i] * c.f_scale_mult;
              const float s0 = j == 0 ? c.k_scale[i] : s;
              return NurbsPrimitives::Cylinder::Config{{s0, s0}, {s, s}, c.f_length[i][j]};
            },
            [i, j](Hand1& c) {
              const auto k_pos = c.knuckle_pos(i);
              auto* l = c.f_length[i];
              float x_diff = 0.f;
              for (int k = 0; k <= j; k++) { x_diff += (k == j) ? l[k] / 2.f : l[k]; }
              return c.wrist_origin * glm::translate(vec3{k_pos.x + x_diff, k_pos.y, 0.});
            },
            // finger_origin_fn(i, j),
            mass_fn(1.5f),
            noop_body_config,
            c_p);

        worlds.add_joint<Hand1>(
            [i, j](auto& c, auto& joint) {
              const unsigned int f_idx = 2 + (3 * i) + j;
              joint.body_idxes = {j == 0 ? 1 : f_idx - 1, f_idx};
              joint.origin_body0 =
                  (j == 0 ? c.knuckle_hand_local_tform(i)
                          : glm::translate(vec3{c.f_length[i][j - 1] / 2.f, 0., 0.})) *
                  // rotate axis before any other tforms, so that y axis is not involved!
                  glm::rotate((float)M_PI_2, vec3{0., 0., 1.});
            },
            [j](Hand1& c, btGeneric6DofSpring2Constraint* joint) {
              // regular
              joint->setAngularLowerLimit({-0.35f, 0.f, j == 0 ? -0.7f : 0.f});
              joint->setAngularUpperLimit({1.7f, 0.f, j == 0 ? 0.7f : 0.f});
            });
      }
    }

    // thumb0
    PhysicsWorldPrimitives::Cylinder::add_to_worlds<Hand1>(
        worlds,
        [](auto& c) {
          const float s = c.t_scale;
          return NurbsPrimitives::Cylinder::Config{{s, s}, {s, s}, c.t_length0};
        },
        [](auto& c) {
          return c.wrist_origin * c.hand_origin_from_wrist() * c.thumb_joint_tform_hand_local() *
                 glm::translate(vec3{c.t_length0 / 2.f, 0., 0.});
        },
        [](auto& c) { return 1.5f; },
        [](auto& c, auto* b) {},
        c_p);

    // thumb0 joint
    worlds.add_joint<Hand1>(
        [](auto& c, auto& j) {
          j.body_idxes = {1, 14};
          j.origin_body0 = c.thumb_joint_tform_hand_local();
        },
        [](auto& c, btGeneric6DofSpring2Constraint* j) {
          const float max_z = 1.3;
          const float max_y = 1.3;
          j->setAngularLowerLimit({0., 0, c.rh ? 0.f : -max_z});
          j->setAngularUpperLimit({0., max_y, c.rh ? max_z : 0.f});
        });

    // thumb1
    PhysicsWorldPrimitives::Cylinder::add_to_worlds<Hand1>(
        worlds,
        [](auto& c) {
          const float s = c.t_scale;
          return NurbsPrimitives::Cylinder::Config{{s, s}, {s, s}, c.t_length1};
        },
        [](auto& c) {
          return c.wrist_origin * c.hand_origin_from_wrist() * c.thumb_joint_tform_hand_local() *
                 glm::translate(vec3{c.t_length0 + (c.t_length1 / 2.f), 0., 0.});
        },
        [](auto& c) { return 1.5f; },
        [](auto& c, auto* b) {},
        c_p);

    // thumb1 joint
    worlds.add_joint<Hand1>(
        [](auto& c, auto& j) {
          j.body_idxes = {14, 15};
          j.origin_body0 = glm::translate(vec3{c.t_length0 / 2.f, 0., 0.});
        },
        [](auto& c, btGeneric6DofSpring2Constraint* j) {
          const float max_z = 0.5;
          j->setAngularLowerLimit({0., 0, c.rh ? 0.f : -max_z});
          j->setAngularUpperLimit({0., 0.f, c.rh ? max_z : 0.f});
        });

    // thumb2
    PhysicsWorldPrimitives::Cylinder::add_to_worlds<Hand1>(
        worlds,
        [](auto& c) {
          const float s = c.t_scale;
          return NurbsPrimitives::Cylinder::Config{{s, s}, {s, s}, c.t_length2};
        },
        [](auto& c) {
          return c.wrist_origin * c.hand_origin_from_wrist() * c.thumb_joint_tform_hand_local() *
                 glm::translate(vec3{c.t_length0 + c.t_length1 + (c.t_length2 / 2.f), 0., 0.});
        },
        [](auto& c) { return 1.5f; },
        [](auto& c, auto* b) {},
        c_p);

    // thumb2 joint
    worlds.add_joint<Hand1>(
        [](auto& c, auto& j) {
          j.body_idxes = {15, 16};
          j.origin_body0 = glm::translate(vec3{c.t_length1 / 2.f, 0., 0.});
        },
        [](auto& c, btGeneric6DofSpring2Constraint* j) {
          const float max_z = 1.;
          j->setAngularLowerLimit({0., 0, c.rh ? 0.f : -max_z});
          j->setAngularUpperLimit({0., 0.f, c.rh ? max_z : 0.f});
        });
  }

  static bool draw_config_gui(Hand1& c) {
    bool change = false;
    //
    change |= ImGui::Checkbox("rh", &c.rh);
    change |= ImGui::SliderFloat("arm length", &c.arm_length, 0., 2000.);
    change |= ImGui::SliderFloat2("wrist scale", &c.wrist_scale.x, 0., 500.);
    change |= ImGui::SliderFloat("k base dist", &c.k_base_length, 100., 1000.);
    change |= ImGui::SliderFloat3("k back", c.k_back, 0., 100.);
    change |= ImGui::SliderFloat4("ks", c.k_scale, 0., 200.);
    change |= ImGui::SliderFloat3("f0 l", c.f_length[0], 0., 1000.);
    change |= ImGui::SliderFloat3("f1 l", c.f_length[1], 0., 1000.);
    change |= ImGui::SliderFloat3("f2 l", c.f_length[2], 0., 1000.);
    change |= ImGui::SliderFloat3("f3 l", c.f_length[3], 0., 1000.);

    change |= ImGui::SliderFloat2("t xy", &c.t_xy.x, 0., 500.);
    change |= ImGui::SliderFloat("t scale", &c.t_scale, 0., 200.);
    change |= ImGui::SliderFloat("t length0", &c.t_length0, 0., 500.);
    change |= ImGui::SliderFloat("t length1", &c.t_length1, 0., 500.);
    change |= ImGui::SliderFloat("t length2", &c.t_length2, 0., 500.);

    ImGui::Text("pinky/index bulges");
    change |= ImGui::SliderFloat("p bulge pos", &c.p_bulge_pos, 0., 1.);
    change |= ImGui::SliderFloat("p bulge scale", &c.p_bulge_scale, 1., 3.);
    change |= ImGui::SliderFloat("i bulge pos", &c.i_bulge_pos, 0., 1.);
    change |= ImGui::SliderFloat("i bulge scale", &c.i_bulge_scale, 1., 3.);

    return change;
  }

  float wrist_to_middle_tip() {
    return k_base_length + f_length[1][0] + f_length[1][1] + f_length[1][2];
  }

private:
  mat4 hand_origin_from_wrist() { return glm::translate(vec3{k_base_length / 2.f, 0., 0.}); }

  mat4 hand_origin_world() {
    return wrist_origin * glm::translate(vec3{k_base_length / 2.f, 0., 0.});
  }

  vec3 knuckle_pos(int k) {

    float x = k_base_length;
    float y = 0.f;
    if (k == 0) {
      y = -k_scale[0] - k_scale[1];
      x -= k_back[0];
    } else if (k == 2) {
      y = k_scale[1] + k_scale[2];
      x -= k_back[1];
    } else if (k == 3) {
      y = k_scale[1] + (k_scale[2] * 2) + k_scale[3];
      x -= k_back[2];
    }

    if (rh) y = -y;

    return vec3{x, y, 0.f};
  }

  mat4 knuckle_hand_local_tform(int k) {
    return glm::inverse(hand_origin_from_wrist()) * glm::translate(knuckle_pos(k));
  }

  mat4 thumb_joint_tform_hand_local() {
    return glm::inverse(hand_origin_from_wrist()) *
           glm::translate(vec3{t_xy * vec2{1.f, rh ? 1.f : -1.f}, 0.f}) *
           glm::rotate(t_theta, vec3{0., 0., rh ? 1.f : -1.f});
  }
};
