#pragma once

class TestConfig3 {

public:
  static string WORLD_CONFIG_TYPE_ID() { return "TestConfig3"; }

  // fingertip scale
  vec2 scale_00{200., 400.};
  // hand width
  vec2 scale_01 = {100, 400.};
  float length_0 = 500.f;
  mat4 origin_0 = mat4{1.f};

  // arm
  float scale_10 = 200.f;
  float scale_11 = 200.f;
  float length_1 = 1000.f;

  static void init(MultiplePhysicsWorlds& worlds) {

    PhysicsWorldPrimitives::Cylinder::Params c_p;
    c_p.c_eval_f = {5, 16};
    c_p.c_eval_d = {5, 16};
    c_p.h_eval_f = {8, 8};

    // Hand cylinder!
    PhysicsWorldPrimitives::Cylinder::add_to_worlds<TestConfig3>(
        worlds,
        [](auto& c) {
          return NurbsPrimitives::Cylinder::Config{c.scale_00, c.scale_01, c.length_0};
        },
        [](auto& c) { return c.origin_0; },
        [](auto& c) { return 15.f; },
        [](auto& c, btRigidBody* b) {},
        c_p);

    c_p.tip0 = false;
    c_p.tip1 = false;

    // Arm cylinder!
    PhysicsWorldPrimitives::Cylinder::add_to_worlds<TestConfig3>(
        worlds,
        [](auto& c) {
          const float s0 = c.scale_10;
          const float s1 = c.scale_11;
          return NurbsPrimitives::Cylinder::Config{{s1, s1}, {s0, s0}, c.length_1};
        },
        [](auto& c) {
          return c.origin_0 * glm::translate(vec3{(c.length_0 + c.length_1) / -2.f, 0.f, 0.f});
        },
        [](auto& c) { return 5.f; },
        [](auto& c, btRigidBody* b) {},
        c_p);

    worlds.add_joint<TestConfig3>(
        [](TestConfig3& c, auto& j) {
          j.body_idxes = {1, 0};
          j.origin_body0 = glm::translate(vec3{c.length_1 / 2.f, 0., 0.});
        },
        [](TestConfig3& c, btGeneric6DofSpring2Constraint* j) {
          j->setAngularLowerLimit({1., -1.3, -1.3});
          j->setAngularUpperLimit({-1., 1.3, 1.3});
        });
  }

  static bool draw_config_gui(TestConfig3& c) {
    bool change = false;
    change |= ImGui::SliderFloat("l0", &c.length_0, 0., 2000.f);
    change |= ImGui::SliderFloat2("s00", &c.scale_00.x, 0., 500.f);
    change |= ImGui::SliderFloat2("s01", &c.scale_01.x, 0., 500.f);
    change |= ImGui::SliderFloat("l1", &c.length_1, 0., 2000.f);
    change |= ImGui::SliderFloat("s10", &c.scale_10, 0., 500.f);
    change |= ImGui::SliderFloat("s11", &c.scale_11, 0., 500.f);
    return change;
  }

  struct RandomizeVar {
    float* val;
    float std_dev;
  };

  static const vector<function<RandomizeVar(TestConfig3& c)>> config3_randomize_variables;

  static void
  RANDOMIZE(TestConfig3& c, int i, std::default_random_engine& _g, float std_dev_m = 1.f) {
    auto r_var = config3_randomize_variables[i % config3_randomize_variables.size()](c);
    *r_var.val = std::normal_distribution<float>(*r_var.val, std_dev_m * r_var.std_dev)(_g);

    if (c.scale_10 > 200.f) { c.scale_10 = 200.f; }
  }
};

// static const auto
const vector<function<TestConfig3::RandomizeVar(TestConfig3& c)>>
    TestConfig3::config3_randomize_variables = {
        [](auto& c) {
          return RandomizeVar{&c.length_0, 50.f};
        },
        [](auto& c) {
          return RandomizeVar{&c.scale_00.x, 20.f};
        },
        [](auto& c) {
          return RandomizeVar{&c.scale_00.y, 20.f};
        },
        [](auto& c) {
          return RandomizeVar{&c.scale_01.x, 20.f};
        },
        [](auto& c) {
          return RandomizeVar{&c.scale_01.y, 20.f};
        },
        [](auto& c) {
          return RandomizeVar{&c.scale_10, 20.f};
        },
        [](auto& c) {
          return RandomizeVar{&c.scale_11, 20.f};
        }};
