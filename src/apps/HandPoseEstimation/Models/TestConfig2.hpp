#pragma once

// Simplest possible configuration - just a long cylinder!
class TestConfig2 {

public:
  static string WORLD_CONFIG_TYPE_ID() { return "TestConfig2"; }

  float scale = 200.f;
  mat4 origin = mat4{1.f};
  float length = 1000.f;

  static void init(MultiplePhysicsWorlds& worlds) {

    PhysicsWorldPrimitives::Cylinder::Params c_p;
    c_p.c_eval_f = {5, 16};
    c_p.c_eval_d = {5, 16};
    c_p.h_eval_f = {8, 8};

    // Finger cylinder!
    PhysicsWorldPrimitives::Cylinder::add_to_worlds<TestConfig2>(
        worlds,
        [](auto& c) {
          const float s = c.scale;
          return NurbsPrimitives::Cylinder::Config{{s, s}, {s, s}, c.length};
        },
        [](auto& c) { return c.origin; },
        [](auto& c) { return 10.f; },
        [](auto& c, btRigidBody* b) {},
        c_p);
  }

  static bool draw_config_gui(TestConfig2& c) {
    return false;
  }
};
