#pragma once

class TestConfig4 {

public:
  static string WORLD_CONFIG_TYPE_ID() { return "TestConfig4"; }

  float wrist_scale = 200.f;
  mat4 origin_0 = mat4{1.f};

  // arm
  float arm_length = 1000.f;

  // fingers
  float k_theta[5] = {-0.7, -0.35, 0, 0.35, 0.7};
  float k_scale[5] = {100., 100., 100., 100., 100.};
  float k_length[5] = {800., 800., 800., 800., 800.};
  float f_length[5] = {800, 800, 800, 800, 800};
  float f_scale[5] = {80, 80, 80, 80, 80};

  // local to hand!
  static mat4 knuckle_joint_local(TestConfig4& c, int knuckle_num) {
    return glm::translate(vec3{c.k_length[2] / -2.f, 0., 0.}) *
           glm::rotate(c.k_theta[knuckle_num], vec3{0, 0, 1}) *
           glm::translate(vec3{c.k_length[knuckle_num], 0., 0.});
  }

  static mat4 knuckle_joint_world(TestConfig4& c, int knuckle_num, mat4 hand_origin) {
    return hand_origin * knuckle_joint_local(c, knuckle_num);
  }

  static TestConfig4 from3(
      const TestConfig3& c3, float hand_scale, vector<mat4>& in_tforms, vector<mat4>& out_tforms) {

    TestConfig4 c;
    c.wrist_scale = c3.scale_01.x;
    mat4 origin_0 = mat4{1.f};

    for (int i = 0; i < 5; i++) {
      c.k_length[i] = c3.length_0 * hand_scale;
      c.f_length[i] = c3.length_0 * hand_scale;
    }

    c.arm_length = c3.length_1;

    out_tforms.clear();
    v_each<mat4>(in_tforms, [&out_tforms](auto& m) { out_tforms.emplace_back(m); });
    out_tforms[0] =
        out_tforms[0] * glm::translate(vec3{(-c3.length_0 / 2.f) + (c.k_length[2] / 2.f), 0., 0.});

    n_iter(5, [&out_tforms, &c](int i) {
      out_tforms.emplace_back(
          knuckle_joint_world(c, i, out_tforms[0]) *
          glm::translate(vec3{c.f_length[i] / 2.f, 0., 0.}));
    });

    return c;
  }

  static void init(MultiplePhysicsWorlds& worlds) {

    PhysicsWorldPrimitives::Cylinder::Params c_p;
    c_p.c_eval_f = {5, 16};
    c_p.c_eval_d = {5, 16};
    c_p.h_eval_f = {8, 8};

    PhysicsWorldPrimitives::Hull_20::add_to_worlds<TestConfig4>(
        worlds,
        [](auto& vtxes, auto& c) {
          auto wc = [s = c.wrist_scale](int num) {
            const float theta = (float)M_PI * 2.f * (num / 12.f);
            return vec2{cos(theta), sin(theta)} * s;
          };

          const float mid_l = c.k_length[2];

          auto fc = [mid_l, &c](int num, bool pos_h) {
            const float t = c.k_theta[num];
            return vec4{
                (-mid_l / 2.f) + (cos(t) * c.k_length[num]),
                sin(t) * c.k_length[num],
                c.k_scale[num] * (pos_h ? 1.f : -1.f),
                1.f};
          };

          const float l = mid_l / 2.f;
          auto v = vector<vec4>{
              // wrist side!
              {-l, wc(1), 1.},
              {-l, wc(2), 1.},
              {-l, wc(3), 1.},
              {-l, wc(4), 1.},
              {-l, wc(5), 1.},
              {-l, wc(7), 1.},
              {-l, wc(8), 1.},
              {-l, wc(9), 1.},
              {-l, wc(10), 1.},
              {-l, wc(11), 1.},
              // hand side!
              fc(4, true),
              fc(3, true),
              fc(2, true),
              fc(1, true),
              fc(0, true),
              fc(0, false),
              fc(1, false),
              fc(2, false),
              fc(3, false),
              fc(4, false),
          };
          for (auto& _v : v) { vtxes.emplace_back(_v); }
        },
        [](auto& c) { return c.origin_0; },
        [](auto& c) { return 10.f; },
        [](auto& c, btRigidBody* b) {});

    // Arm cylinder!
    PhysicsWorldPrimitives::Cylinder::add_to_worlds<TestConfig4>(
        worlds,
        [](auto& c) {
          const auto s0 = c.wrist_scale;
          const auto s1 = c.wrist_scale;
          return NurbsPrimitives::Cylinder::Config{{s1, s1}, {s0, s0}, c.arm_length};
        },
        [](auto& c) {
          return c.origin_0 * glm::translate(vec3{(c.k_length[2] + c.arm_length) / -2.f, 0.f, 0.f});
        },
        [](auto& c) { return 8.f; },
        [](auto& c, btRigidBody* b) {},
        c_p);

    worlds.add_joint<TestConfig4>(
        [](TestConfig4& c, auto& j) {
          j.body_idxes = {1, 0};
          j.origin_body0 = glm::translate(vec3{c.arm_length / 2.f, 0., 0.});
        },
        [](TestConfig4& c, btGeneric6DofSpring2Constraint* j) {
          j->setAngularLowerLimit({1., -1.3, -1.3});
          j->setAngularUpperLimit({-1., 1.3, 1.3});
          // disable all angular constraints?
          // j->setAngularLowerLimit({1., 1., 1.});
          // j->setAngularUpperLimit({-1., -1., -1.});
        });

    c_p.physics_scale = PhysicsWorldPrimitives::Cylinder::PhysicsRadiusScale::MIN;
    c_p.physics_scale_mult = 1.0;

    // fingers!!
    n_iter(5, [&worlds, c_p](int i) {
      PhysicsWorldPrimitives::Cylinder::add_to_worlds<TestConfig4>(
          worlds,
          [i](auto& c) {
            const auto s0 = c.k_scale[i];
            const auto s1 = c.f_scale[i];
            return NurbsPrimitives::Cylinder::Config{{s0, s0}, {s1, s1}, c.f_length[i]};
          },
          [i](auto& c) {
            return knuckle_joint_world(c, i, c.origin_0) *
                   glm::translate(vec3{c.f_length[i] / 2.f, 0., 0.});
          },
          [](auto& c) { return 3.f; },
          [](auto& c, btRigidBody* b) {},
          c_p);

      worlds.add_joint<TestConfig4>(
          [i](TestConfig4& c, auto& j) {
            j.body_idxes = {0, 2 + i};
            j.origin_body0 = knuckle_joint_world(c, i, c.origin_0);
          },
          [](TestConfig4& c, btGeneric6DofSpring2Constraint* j) {
            j->setAngularLowerLimit({0., -.4, -1.3});
            j->setAngularUpperLimit({0., .4, 1.3});
            // disable all angular constraints?
            // j->setAngularLowerLimit({1., 1., 1.});
            // j->setAngularUpperLimit({-1., -1., -1.});
          });
    });

    PhysicsWorldPrimitives::Cube::add_to_worlds<TestConfig4>(
        worlds,
        [](auto& c) { return PLANE_SCALE; },
        [](auto& c) { return PLANE_TFORM(); },
        [](auto& c) { return 0.f; },
        [](auto& c, btRigidBody* b) { b->setFriction(0.f); },
        false);
  }

  static bool draw_config_gui(TestConfig4& c) {

    bool change = false;

    change |= ImGui::SliderFloat("a length", &c.arm_length, 0., 5000.);
    change |= ImGui::SliderFloat("w scale", &c.wrist_scale, 0., 500.f);
    for (int i = 0; i < 5; i++) {
      ImGui::Text("finger %i", i);
      ImGui::PushID(i);
      change |= ImGui::SliderFloat("th", c.k_theta + i, -1., 1.);
      change |= ImGui::SliderFloat("kl", c.k_length + i, 0.f, 1500.f);
      change |= ImGui::SliderFloat("ks", c.k_scale + i, 0.f, 500.f);
      change |= ImGui::SliderFloat("fl", c.f_length + i, 0.f, 1500.f);
      change |= ImGui::SliderFloat("fs", c.f_scale + i, 0.f, 500.f);
      ImGui::PopID();
    }

    return change;
    // ImGUI::SliderFloat("")
  }

  struct RandomizeVar {
    float* val;
    float std_dev;
  };

  static vector<function<RandomizeVar(TestConfig4&)>> GET_RANDOMIZE_VARS() {
    vector<function<RandomizeVar(TestConfig4&)>> v{[](auto& c) {
      return RandomizeVar{&c.wrist_scale, 20.f};
    }};
    n_iter(5, [&v](int i) {
      v.emplace_back([i](auto& c) { return RandomizeVar{c.f_length + i, 25.f}; });
      v.emplace_back([i](auto& c) { return RandomizeVar{c.k_length + i, 25.f}; });
      v.emplace_back([i](auto& c) { return RandomizeVar{c.f_scale + i, 5.f}; });
      v.emplace_back([i](auto& c) { return RandomizeVar{c.k_scale + i, 5.f}; });
      v.emplace_back([i](auto& c) { return RandomizeVar{c.k_theta + i, 0.04f}; });
    });
    return std::move(v);
  }

  const static vec3 PLANE_SCALE;
  static mat4 PLANE_TFORM() { return glm::translate(vec3{0., 0., PLANE_SCALE.z / 2.f}); }

  static void RANDOMIZE(
      TestConfig4& c,
      vector<function<RandomizeVar(TestConfig4&)>>& vars,
      std::default_random_engine& _g) {
    const int i = std::uniform_int_distribution<int>(0, vars.size() - 1)(_g);
    auto r_var = vars[i](c);
    *r_var.val = std::normal_distribution<float>(*r_var.val, r_var.std_dev)(_g);
  }

  void print_debug() {

    printf("TestConfig4:\n  wrist scale: %f\n", wrist_scale);

    n_iter(5, [this](int i) {
      printf(
          "  finger %i: kt: %f - kl: %.2f - ks: %.2f - fl: %.2f - fs: %.2f\n",
          i,
          k_theta[i],
          k_length[i],
          k_scale[i],
          f_length[i],
          f_scale[i]);
    });
  }
};

const vec3 TestConfig4::PLANE_SCALE = vec3{10000., 10000., 200.};