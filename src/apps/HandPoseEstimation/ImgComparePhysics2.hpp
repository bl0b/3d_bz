#pragma once

namespace ComputeLib {

using namespace Engine::Gpu;

struct PhysicsForceGenerator_Force {
  // Force applied
  vec4 f;
  // Position, in world space, but relative to position of body
  vec4 r_pos;
  // Index of body to which to apply force
  unsigned int body_idx;

  unsigned int world_id;

  // padding for GPU alignment
  unsigned int _p0, p1;
};

struct PhysicsForceGenerator_Both {
  ivec2 coord;
  unsigned int v0;
  unsigned int v1;
  unsigned int v1_id;
  unsigned int _p0, _p1, _p2;
};

class ImgComparePhysics2 : protected ComputeBase {

#include <generated/ImgComparePhysics2_cl_signature.hpp>

  // there are 3 types of overlap we are interested in:
  Buff<uint> _n_0_1;    // both images have a point
  Buff<uint> _n_0_not1; // original image has a point, rendered image does not
  Buff<uint> _n_not0_1; // original image has point on the 'not' image, rendered image has a point
  void get_overlap_coords_for_force_generation(
      Tex<TF::RG16UI>& img0_split,
      Tex<TF::RG16UI>& img1, // r: depth, g: body IDs
      unsigned int num_vps,
      Buff<vec4>& vps,
      Buff<uvec4>& c_not0_1,
      vector<uint>& n_not0_1,
      Buff<uvec4>& c_0_not1,
      vector<uint>& n_0_not1,
      Buff<PhysicsForceGenerator_Both>& c_0_1,
      vector<uint>& n_0_1) {

    init_counter(_n_0_1, num_vps);
    init_counter(_n_0_not1, num_vps);
    init_counter(_n_not0_1, num_vps);

    auto d = img0_split.get_dims();
    const int max_els = num_vps * d.x * d.y;
    c_not0_1.set_storage(max_els);
    c_0_not1.set_storage(max_els);
    c_0_1.set_storage(max_els);

    const int block = 32;

    kernel_ImgComparePhysics2_get_overlap_coords_for_force_generation(
        ivec3{num_vps, (d.x / block) + 1, d.y},
        ivec3{1, block, 1},
        img0_split,
        img1,
        num_vps,
        vps,
        _n_not0_1,
        c_not0_1,
        _n_0_not1,
        c_0_not1,
        _n_0_1,
        c_0_1);

    _n_not0_1.cu_copy_to(n_not0_1, num_vps);
    _n_0_not1.cu_copy_to(n_0_not1, num_vps);
    _n_0_1.cu_copy_to(n_0_1, num_vps);
  }

  Buff<uint> _all_count;

  void get_coords_with_nearby_simulated_coords_all(
      Tex<TF::RG16UI>& img_mesh_ids,
      const vector<uint>& num_coords_in,
      Buff<uvec4>& coords_in,
      Buff<vec4>& vps,
      vector<uint>& num_coords_out,
      Buff<uvec4>& coords_out,
      Buff<vec4>& coords_out_neighbors_avg) {

    vector<uvec4> _num_coords_in_mapped;
    for (int i = 0; i < num_coords_in.size(); i++) {
      for (int j = 0; j < num_coords_in[i]; j++) {
        // VP id, coord found id
        _num_coords_in_mapped.emplace_back(uvec4{i, j, 0, 0});
      }
    }

    // odd bug when using set_data - why doesn't that work????
    num_coords_in_mapped.cu_copy_from(_num_coords_in_mapped.data(), _num_coords_in_mapped.size());

    init_counter(_all_count, num_coords_in.size());
    coords_out.set_storage(coords_in.size());
    coords_out_neighbors_avg.set_storage(coords_in.size());

    int block = 32;

    kernel_ImgComparePhysics2_get_coords_with_nearby_simulated_coords_all(
        (_num_coords_in_mapped.size() / block) + 1,
        block,
        _num_coords_in_mapped.size(),
        num_coords_in_mapped,
        coords_in,
        vps,
        img_mesh_ids,
        _all_count,
        coords_out,
        coords_out_neighbors_avg);

    _all_count.cu_copy_to(num_coords_out, num_coords_in.size());
  }

  Buff<uvec4> num_coords_in_mapped;
  void get_coords_with_nearby_empty_coords_all(
      Tex<TF::RG16UI>& img_split,
      const vector<uint>& num_coords_in,
      Buff<uvec4>& coords_in,
      vector<uint>& num_coords_out,
      Buff<uvec4>& coords_out,
      Buff<vec4>& coords_out_empty_neighbors_avg) {

    Engine::Util::Timer t;
    t.record("make mapped arr");

    vector<uvec4> _num_coords_in_mapped;
    for (int i = 0; i < num_coords_in.size(); i++) {
      for (int j = 0; j < num_coords_in[i]; j++) {
        // VP id, coord found id
        _num_coords_in_mapped.emplace_back(ivec4{i, j, 0, 0});
      }
    }

    t.record("copy mapped arr");

    // odd bug when using set_data - why doesn't that work????
    num_coords_in_mapped.cu_copy_from(_num_coords_in_mapped.data(), _num_coords_in_mapped.size());

    t.record("init counter");

    init_counter(_all_count, num_coords_in.size());

    t.record("allocate");

    coords_out.set_storage(coords_in.size());
    coords_out_empty_neighbors_avg.set_storage(coords_in.size());

    const unsigned int block = 32;

    t.record("kernel");

    kernel_ImgComparePhysics2_get_coords_with_nearby_empty_coords_all(
        (_num_coords_in_mapped.size() / block) + 1,
        block,
        _num_coords_in_mapped.size(),
        num_coords_in_mapped,
        img_split,
        coords_in,
        _all_count,
        coords_out,
        coords_out_empty_neighbors_avg);

    t.record("copy counts");

    _all_count.cu_copy_to(num_coords_out, num_coords_in.size());

    t.record("end");

    // printf("-- -- -- -- get coords with nearby empty!\n");
    // t.render([](auto s, auto f) { printf("-- -- -- -- %s: %f\n", s.c_str(), f); });
  }

  void get_all_coords_with_both_and_values(
      Tex<TF::R16UI>& img0,
      Tex<TF::R16UI>& img1,
      Tex<TF::R16UI>& img1_meshids,
      unsigned int num_vps,
      Buff<vec4>& vps,
      vector<uint>& num_coords_out,
      // ivec4{coord_idx, mesh_id, img0_val, img1_val}
      Buff<ivec4>& coords_out) {

    init_counter(_all_count, num_vps);
    ivec2 d = img0.get_dims();
    int block = 8;
    // mostly empty space!
    coords_out.set_storage(num_vps * d.x * d.y);

    kernel_ImgComparePhysics2_get_all_coords_with_both_and_values(
        {num_vps, (d / block) + 1},
        {1, block, block},
        img0,
        img1,
        img1_meshids,
        num_vps,
        vps,
        _all_count,
        coords_out);

    _all_count.cu_copy_to(num_coords_out, num_vps);
  }

  Buff<uint> _num_coords_out, _coord_offsets_out;
  void compress_buffer_vec4(
      const uint in_group_size,
      vector<uint>& num_coords_out,
      Buff<vec4>& buff_in,
      Buff<vec4>& buff_out) {

    uint max_group_size, num_groups;

    init_compress(
        num_coords_out,
        _num_coords_out,
        _coord_offsets_out,
        buff_out,
        &max_group_size,
        &num_groups);

    const int block = 32;

    kernel_ImgComparePhysics2_compress_buffer_vec4(
        ivec2{num_groups, (max_group_size / block) + 1},
        ivec2{1, block},
        num_groups,
        in_group_size,
        _num_coords_out,
        _coord_offsets_out,
        buff_in,
        buff_out);
  }

  // Buff<uint> _num_coords_out, _coord_offsets_out;
  void compress_buffer_uvec4(
      const uint in_group_size,
      vector<uint>& num_coords_out,
      Buff<uvec4>& buff_in,
      Buff<uvec4>& buff_out) {

    uint max_group_size, num_groups;

    init_compress(
        num_coords_out,
        _num_coords_out,
        _coord_offsets_out,
        buff_out,
        &max_group_size,
        &num_groups);

    const int block = 32;

    kernel_ImgComparePhysics2_compress_buffer_uvec4(
        ivec2{num_groups, (max_group_size / block) + 1},
        ivec2{1, block},
        num_groups,
        in_group_size,
        _num_coords_out,
        _coord_offsets_out,
        buff_in,
        buff_out);
  }

  void compress_buffer_ivec4(
      const uint in_group_size,
      vector<uint>& num_coords_out,
      Buff<ivec4>& buff_in,
      Buff<ivec4>& buff_out) {

    uint max_group_size, num_groups;

    init_compress(
        num_coords_out,
        _num_coords_out,
        _coord_offsets_out,
        buff_out,
        &max_group_size,
        &num_groups);

    const int block = 32;

    kernel_ImgComparePhysics2_compress_buffer_ivec4(
        ivec2{num_groups, (max_group_size / block) + 1},
        ivec2{1, block},
        num_groups,
        in_group_size,
        _num_coords_out,
        _coord_offsets_out,
        buff_in,
        buff_out);
  }

  Buff<ivec2> _num_coords;
  void gen_invocation_list(
      vector<uint>& num_coords, Buff<ivec2>& invocations_out, unsigned int* num_invocations) {

    unsigned int total = 0;
    unsigned int max = 0;
    vector<ivec2> offsets;
    for (auto n : num_coords) {
      offsets.emplace_back(ivec2{(int)n, (int)total});
      total += n;
      if (n > max) max = n;
    }
    *num_invocations = total;

    // invocation
    _num_coords.set_storage(num_coords.size());
    _num_coords.cu_copy_from(offsets.data(), offsets.size());

    invocations_out.set_storage(total);

    const int block = 32;
    const ivec2 d = {num_coords.size(), (max / block) + 1};

    kernel_ImgComparePhysics2_gen_invocations(
        d, ivec2{1, block}, num_coords.size(), _num_coords, invocations_out);
  }

  void gen_forces_out(
      vector<uint>& num_coords,
      // Buff<ivec2>& invocations,
      // unsigned int num_invocations,
      rs2_intrinsics intrinsics,
      mat4 cam_to_world,
      Buff<PhysicsForceGenerator_Both>& all_pixels,
      unsigned int all_pixels_group_stride,
      Buff<vec4>& body_origins,
      unsigned int num_bodies_per_group,
      Tex<TF::RGBA32F>& neighbors_info,
      Buff<PhysicsForceGenerator_Force>& forces_out,
      unsigned int* num_forces_out) {

    unsigned int total = 0;
    unsigned int max = 0;
    vector<ivec2> offsets;
    for (auto n : num_coords) {
      offsets.emplace_back(ivec2{(int)n, (int)total});
      total += n;
      if (n > max) max = n;
    }
    *num_forces_out = total;

    // printf("num forces out: %u")

    unsigned int num_groups = num_coords.size();

    // invocation
    _num_coords.set_storage(num_coords.size());
    _num_coords.cu_copy_from(offsets.data(), offsets.size());

    forces_out.set_storage(*num_forces_out);

    const int block = 32;

    // unsigned int max_world_id = num_groups

    kernel_ImgComparePhysics2_gen_forces_out(
        ivec2{num_groups, (max / block) + 1},
        ivec2{1, block},
        _num_coords,
        num_groups,
        intrinsics,
        cam_to_world,
        all_pixels,
        all_pixels_group_stride,
        body_origins,
        neighbors_info,
        num_bodies_per_group,
        forces_out);
  }

private:
  template <typename T>
  static void init_compress(
      vector<uint>& num_coords_out,
      Buff<uint>& _num_out,
      Buff<uint>& _offsets,
      Buff<T>& buff_out,
      uint* max_group_size,
      uint* num_groups) {

    *num_groups = num_coords_out.size();
    *max_group_size = 0;
    uint offset = 0;
    vector<uint> coord_offsets_out;
    uint total_num_els = 0;
    for (auto& n : num_coords_out) {
      if (n > *max_group_size) { *max_group_size = n; }
      coord_offsets_out.emplace_back(offset);
      offset += n;
      total_num_els += n;
    }

    buff_out.set_storage(total_num_els);

    _num_out.set_storage(num_coords_out.size());
    _num_out.cu_copy_from(num_coords_out.data(), num_coords_out.size());

    _offsets.set_storage(num_coords_out.size());
    _offsets.cu_copy_from(coord_offsets_out.data(), num_coords_out.size());
  }

  static void init_counter(Buff<uint>& counter, unsigned int num_items = 1) {
    counter.set_storage(num_items);
    counter.cu_memset(0);
  }

  static void read_counter(Buff<uint>& counter, uint* out) { counter.cu_copy_to(out, 1); }
};

} // namespace ComputeLib