#pragma once

class CalibratedPlane2 {

public:
  RansacPlane_Pipeline ransac_pipeline;
  DeprojectDepth deproject_depth;
  FilterPoints filter_points;
  MapDepth map_depth;

  int minif_factor = 2;
  float calibrate_z_thresh = 100.f;
  float calibrate_split_z_thresh = 40.f;
  int num_iterations = 200;
  ivec2 ortho_map_dims{128, 128};

  rs2_intrinsics intrinsics{};

  void calibrate(Tex<TF::R16UI>& d, rs2_intrinsics i) {
    intrinsics = i;
    ransac_pipeline.find_calibration(
        d, i, minif_factor, calibrate_z_thresh, num_iterations, ortho_map_dims);
  }

public:
  void split_incoming_image(
      Tex<TF::R16UI>& d,
      int minif,
      Tex<TF::RG16UI>& d_out,
      float z_thresh,
      rs2_intrinsics* intrinsics_out) {

    filter_points.divide_original_2_single(
        d,
        ransac_pipeline.p.to_plane_coords,
        intrinsics,
        minif,
        d_out,
        intrinsics_out,
        ransac_pipeline.ortho_map_points,
        ransac_pipeline.ortho_sampler,
        z_thresh);
  }
};
