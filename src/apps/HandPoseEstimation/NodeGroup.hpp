#pragma once

class NodeGroup {

public:
  // Initial configuration! Call this before finalizing to setup object data schema
  template <class P, class T>
  void push_nurbs_obj(
      const ivec2 eval_dims, unsigned int body_id, function<void(vector<vec4>&, T&)> get_cps_fn) {
    assert(!finalized);
    auto& o = n_objs[get_sig<P>(eval_dims)];
    if (!o.obj_initialized()) { P::init(o.obj, eval_dims); }
    o.body_ids.emplace_back(body_id);
    o.gen_data_fns.emplace_back([get_cps_fn](vector<vec4>& v, void* p) { get_cps_fn(v, *(T*)p); });
  }

  template <class P, class T>
  void push_mesh_obj(unsigned int body_id, function<void(vector<vec4>&, T&)> get_vtxs_fn) {
    assert(!finalized);
    auto& o = m_objs[P::PRIMITIVE_ID()];
    if (!o.obj_initialized()) { P::init(o.obj); }
    o.body_ids.emplace_back(body_id);
    o.gen_data_fns.emplace_back(
        [get_vtxs_fn](vector<vec4>& v, void* p) { get_vtxs_fn(v, *(T*)p); });
  }

  // Call this after adding all nurbs objects to the schema.
  void finalize() {
    assert(!finalized);
    if (finalized) { printf("Can't finalize twice!\n"); }
    for (auto& n : n_objs) { n.second.obj.get_data().body_ids.set_data(n.second.body_ids); }
    for (auto& m : m_objs) { m.second.obj.get_data().body_ids.set_data(m.second.body_ids); }
    finalized = true;
  }

  void reset() {
    assert(finalized);
    finalized = false;
    n_objs.clear();
    m_objs.clear();
    written_num_instances = 0;
  }

  void draw(
      NurbsShader& n_cam,
      StdCamera& m_cam,
      unsigned int num_instances = 1,
      unsigned int start_num = 0,
      StdFramebufferSubViewports* viewports = nullptr,
      unsigned int start_vp = 0) {
    assert(finalized);
    _draw(n_cam, n_objs, num_instances, start_num, viewports, start_vp);
    _draw(m_cam, m_objs, num_instances, start_num, viewports, start_vp);
  }

  void draw2(
      NurbsShader& n_cam,
      StdCamera& m_cam,
      Buff<mat4>& t,
      unsigned int bodies_per_world,
      unsigned int num_worlds,
      unsigned int start_world,
      StdFramebufferSubViewports* viewports = nullptr,
      unsigned int start_vp = 0) {

    assert(finalized);
    _draw2(n_cam, n_objs, t, bodies_per_world, num_worlds, start_world, viewports, start_vp);
    _draw2(m_cam, m_objs, t, bodies_per_world, num_worlds, start_world, viewports, start_vp);
  }

  template <typename T>
  void write_all_data(vector<T>& data_configs) {
    clear_staged_data();
    for (auto& n : n_objs) n.second.gen_data(data_configs);
    for (auto& m : m_objs) m.second.gen_data(data_configs);
    write_staged_data();
  }

  // Defines how to add a single staged control point set
  typedef std::function<void(vector<vec4>&)> AddDataSetFn;

  template <class P>
  void add_staged_cp_set(const ivec2 eval_dims, AddDataSetFn add_fn) {
    assert(finalized);
    n_objs[get_sig<P>(eval_dims)].add_staged_data(add_fn);
  }

  template <class P>
  void add_staged_vtx_set(AddDataSetFn add_fn) {
    assert(finalized);
    m_objs[P::PRIMITIVE_ID()].add_staged_data(add_fn);
  }

  void clear_staged_data() {
    assert(finalized);
    for (auto& n : n_objs) n.second.clear_staged_data();
    for (auto& m : m_objs) m.second.clear_staged_data();
  }

  unsigned int write_staged_data() {
    assert(finalized);
    written_num_instances = get_num_staged_instances();
    for (auto& n : n_objs) n.second.write_staged_data();
    for (auto& m : m_objs) m.second.write_staged_data();
    return written_num_instances;
  }

private:
  typedef pair<string, ivec2> ObjSig;

  struct ObjSigHash {
    std::size_t operator()(const ObjSig& p) const {
      return std::hash<string>()(p.first) ^ std::hash<ivec2>()(p.second);
    }
  };

  template <typename P>
  ObjSig get_sig(const ivec2 eval_dims) {
    return ObjSig(P::PRIMITIVE_ID(), eval_dims);
  }

  template <typename O>
  class MappedObj {
  public:
    O obj;
    vector<unsigned int> body_ids;
    vector<function<void(vector<vec4>&, void*)>> gen_data_fns;

    void add_staged_data(std::function<void(vector<vec4>&)>& add_fn) {
      const auto start_size = staged_data.size();
      add_fn(staged_data);
      const auto diff_size = staged_data.size() - start_size;
      assert(diff_size == obj.get_data().vtx_set_size);
    }

    virtual void write_staged_data() = 0;

    void clear_staged_data() { staged_data.clear(); }

    inline unsigned int num_staged_instances() {
      assert(num_staged_data_sets() % data_sets_per_instance() == 0);
      return num_staged_data_sets() / data_sets_per_instance();
    }

    inline bool obj_initialized() const { return body_ids.size() > 0; }

    template <typename T>
    void gen_data(vector<T>& ts) {
      for (auto& d : ts) {
        for (auto& fn : gen_data_fns) {
          const auto start_size = staged_data.size();
          fn(staged_data, &d);
          assert(staged_data.size() - start_size == obj.get_data().vtx_set_size);
        }
      }
    }

  protected:
    // Staged CP sets, not written to GPU yet
    vector<vec4> staged_data;

    inline unsigned int data_sets_per_instance() const { return body_ids.size(); }

    inline unsigned int num_staged_data_sets() {
      assert(staged_data.size() % obj.get_data().vtx_set_size == 0);
      return staged_data.size() / obj.get_data().vtx_set_size;
    }
  };

  class MappedObj_Nurbs : public MappedObj<NurbsObj> {
  public:
    void write_staged_data() override {
      obj.set_cp_sets(staged_data);
      clear_staged_data();
    }
  };

  class MappedObj_Mesh : public MappedObj<MeshObj> {
  public:
    void write_staged_data() override {
      obj.get_data().vtxes.set_data(staged_data);
      obj.get_data().vtx_set_count = num_staged_data_sets();
    }
  };

  unsigned int get_num_staged_instances() {
    // Num instances should be same for every
    unordered_set<unsigned int> num_instances_map;
    for (auto& n : n_objs) num_instances_map.insert(n.second.num_staged_instances());
    for (auto& m : m_objs) num_instances_map.insert(m.second.num_staged_instances());

    assert(num_instances_map.size() == 1);
    return *num_instances_map.begin();
  }

  // nurbs objects!
  unordered_map<ObjSig, MappedObj_Nurbs, ObjSigHash> n_objs;

  // mesh objects! Keyed only by primitive-id string!
  unordered_map<string, MappedObj_Mesh> m_objs;

  unsigned int written_num_instances = 0;

  bool finalized = false;

  template <typename C, typename O>
  static void _draw(
      C& cam,
      O& objs,
      unsigned int num_instances,
      unsigned int start_num,
      StdFramebufferSubViewports* viewports,
      unsigned int start_vp) {

    for (auto& o : objs) {
      const unsigned int num_per_instance = o.second.body_ids.size();
      auto cfg = DrawConfig()
                     .num(num_instances * num_per_instance)
                     .both({start_num * num_per_instance, 1});
      if (viewports != nullptr) { cfg.viewports(*viewports, {start_vp, num_per_instance}); }
      cam.draw(o.second.obj, cfg);
    }
  }

  template <typename C, typename O>
  static void _draw2(
      C& cam,
      O& objs,
      Buff<mat4>& t,
      unsigned int bodies_per_world,
      unsigned int num_worlds,
      unsigned int start_world,
      StdFramebufferSubViewports* viewports,
      unsigned int start_vp) {

    for (auto& o : objs) {

      DrawSharedCfg c;
      c.bodies_per_world = bodies_per_world;
      c.obj_bodies_per_world = o.second.body_ids.size();
      c.start_world = start_world;
      c.num_worlds = num_worlds;
      c.vps = viewports;
      c.start_vp = start_vp;
      cam.draw_shared_tforms(o.second.obj, t, c);
    }
  }
};
