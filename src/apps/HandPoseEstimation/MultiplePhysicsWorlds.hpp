#pragma once

#include "NodeGroup.hpp"

using namespace Engine::Util::Stl;

class MultipleWorldCtx {
public:
  // State of the physics world
  PhysicsCtx physics_ctx;
  vector<PhysicsObjectGeneric> physics_bodies;
  vector<btGeneric6DofSpring2Constraint*> constraint_ptrs;

  void reset() {
    for (auto* c : constraint_ptrs) {
      physics_ctx.m_dynamics_world->removeConstraint(c);
      delete c;
    }
    constraint_ptrs.clear();
    for (auto& p : physics_bodies) {
      physics_ctx.m_dynamics_world->removeRigidBody(p.body);
      delete p.body->getMotionState();
      delete p.body;
    }
    physics_bodies.clear();

    physics_ctx.m_dynamics_world->setGravity({0., 0., 0.f});
  }

  ~MultipleWorldCtx() { reset(); }

  btGeneric6DofSpring2Constraint*
  make_joint(const unsigned int b0_idx, const unsigned int b1_idx, const mat4 joint_world) {

    mat4 b0_tform = physics_bodies[b0_idx].get_current_tform();
    mat4 b1_tform = physics_bodies[b1_idx].get_current_tform();

    auto* c = new btGeneric6DofSpring2Constraint(
        *physics_bodies[b0_idx].body,
        *physics_bodies[b1_idx].body,
        toBt(glm::inverse(b0_tform) * joint_world),
        toBt(glm::inverse(b1_tform) * joint_world));

    physics_ctx.m_dynamics_world->addConstraint(c, true);
    constraint_ptrs.emplace_back(c);

    return c;
  }

  void apply_impulse(const unsigned int body_idx, vec4 force, vec4 rel_pos) {
    physics_bodies[body_idx].body->applyImpulse(toBt(force.xyz()), toBt(rel_pos.xyz()));
  }

  void scale_momentum(const float x) {
    for (auto& b : physics_bodies) {
      auto av = b.body->getAngularVelocity();
      auto lv = b.body->getLinearVelocity();
      b.body->setAngularVelocity(av * x);
      b.body->setLinearVelocity(lv * x);
    }
  }
};

class MultiplePhysicsWorlds {
private:
  string configured_type = "";

public:
  template <typename T>
  void verify_configured_type() {
    assert(configured_type == T::WORLD_CONFIG_TYPE_ID());
  }

  class JointConstructor {
  public:
    struct Params {
      pair<unsigned int, unsigned int> body_idxes;
      mat4 origin_body0; // origin in local space of first body!
    };

    template <typename T>
    static JointConstructor
    from_fns(function<void(T&, Params&)> m, function<void(T&, btGeneric6DofSpring2Constraint*)> c) {
      JointConstructor b;
      b.make_joint = [m](void* t, Params& c) { return m(*(T*)t, c); };
      b.config_joint = [c](void* t, btGeneric6DofSpring2Constraint* b) { c(*(T*)t, b); };
      return b;
    }

    void make(MultipleWorldCtx& w, void* t) {
      Params j;
      make_joint(t, j);
      const auto j_world =
          w.physics_bodies[j.body_idxes.first].get_current_tform() * j.origin_body0;
      auto* j_ptr = w.make_joint(j.body_idxes.first, j.body_idxes.second, j_world);
      config_joint(t, j_ptr);
    }

  private:
    JointConstructor(){};
    function<void(void*, Params&)> make_joint;
    function<void(void*, btGeneric6DofSpring2Constraint*)> config_joint;
  };

  class BodyConstructor {

  public:
    struct Params {
      float mass;
      btCollisionShape* shape;
      mat4 origin;
    };

    template <typename T>
    static BodyConstructor
    from_fns(function<void(T&, Params&)> m, function<void(T&, btRigidBody*)> c) {
      BodyConstructor b;
      b.make_body = [m](void* t, Params& c) { m(*(T*)t, c); };
      b.config_body = [c](void* t, btRigidBody* b) { c(*(T*)t, b); };
      return b;
    }

    void make(MultipleWorldCtx& w, void* t) {
      PhysicsObjectGeneric b;
      {
        Params p;
        make_body(t, p);
        b.make(p.shape, p.mass, p.origin);
      }
      config_body(t, b.body);
      w.physics_ctx.m_dynamics_world->addRigidBody(b.body);
      w.physics_bodies.emplace_back(b);
    }

  private:
    BodyConstructor() {}
    function<void(void*, Params&)> make_body;
    function<void(void*, btRigidBody*)> config_body;
  };

  // 2 worlds to render!
  NodeGroup full, debug;

  template <typename T>
  void init() {
    if (configured_type.size() > 0) {
      full.reset();
      debug.reset();
      for (auto* w : worlds) delete w;
      worlds.clear();

      bodies.clear();
      joints.clear();
      world_configs.clear();
    }

    configured_type = T::WORLD_CONFIG_TYPE_ID();
    T::init(*this);
    full.finalize();
    debug.finalize();
  }

  unsigned int size() { return worlds.size(); }

  unsigned int bodies_per_world() { return bodies.size(); }

  void each_world(function<void(MultipleWorldCtx& w, unsigned int i)> fn) {
    for (int i = 0; i < worlds.size(); i++) { fn(*worlds[i], i); }
  }

  template <typename T>
  void add_joint(
      function<void(T&, JointConstructor::Params&)> get_joint,
      function<void(T&, btGeneric6DofSpring2Constraint*)> config_joint) {
    verify_configured_type<T>();
    joints.emplace_back(JointConstructor::from_fns<T>(get_joint, config_joint));
  }

  // Returns the body id
  unsigned int add_body(BodyConstructor c) {
    bodies.emplace_back(c);
    return bodies.size();
  }

  template <typename P, typename T>
  void add_full_mesh_obj(unsigned int body_id, function<void(vector<vec4>&, T&)> gen_vtxes) {
    full.push_mesh_obj<P, T>(body_id, gen_vtxes);
  }

  template <typename P, typename T>
  void add_full_nurbs_obj(
      const ivec2 eval, unsigned int body_id, function<void(vector<vec4>&, T&)> gen_cps) {
    full.push_nurbs_obj<P, T>(eval, body_id, gen_cps);
  }

  template <typename P, typename T>
  void add_debug_mesh_obj(unsigned int body_id, function<void(vector<vec4>&, T&)> gen_vtxes) {
    debug.push_mesh_obj<P, T>(body_id, gen_vtxes);
  }

  template <typename P, typename T>
  void add_debug_nurbs_obj(
      const ivec2 eval, unsigned int body_id, function<void(vector<vec4>&, T&)> gen_cps) {
    debug.push_nurbs_obj<P, T>(eval, body_id, gen_cps);
  }

  void add_config_world(function<void(btMultiBodyDynamicsWorld*)> fn) {
    world_configs.emplace_back(fn);
  }

  // writes config and initializes physics world to initial configuration
  template <typename T>
  void write_all_data(
      vector<T>& configs,
      ThreadPool& tp,
      bool keep_old_tforms = false,
      vector<vector<mat4>> custom_tforms = {}) {
    verify_configured_type<T>();

    const bool write_custom_tforms = !custom_tforms.empty();
    assert(!(keep_old_tforms && write_custom_tforms));
    auto old_tforms = (keep_old_tforms && !write_custom_tforms) ? get_all_tforms_nested()
                                                                : vector<vector<mat4>>{};
    if (keep_old_tforms) { assert(worlds.size() == configs.size()); }
    if (write_custom_tforms) { assert(custom_tforms.size() == configs.size()); }

    // runtime per-config mesh scaling
    full.write_all_data(configs);
    debug.write_all_data(configs);

    allocate_and_reset_worlds(configs.size());

    vector<future<void>> results;

    for (int i = 0; i < configs.size(); i++) {
      void* c = configs.data() + i;
      auto** _w = worlds.data() + i;

      auto fn = [this, c, _w]() -> void {
        if (*_w == nullptr) { *_w = new MultipleWorldCtx(); }
        auto& w = **_w;

        for (auto& b : bodies) b.make(w, c);
        for (auto& j : joints) j.make(w, c);

        for (auto& w_cfg : world_configs) w_cfg(w.physics_ctx.m_dynamics_world);
      };

      results.emplace_back(tp.enqueue(fn));
    }

    for (auto&& r : results) r.get();

    // TODO: this could be added to the thread pool as well
    if (keep_old_tforms || write_custom_tforms) {

      for (int i = 0; i < worlds.size(); i++) {
        auto& w = *worlds[i];
        for (int j = 0; j < w.physics_bodies.size(); j++) {
          auto& b = w.physics_bodies[j];
          mat4 t = keep_old_tforms ? old_tforms[i][j] : custom_tforms[i][j];
          btTransform b_tform;
          b_tform.setFromOpenGLMatrix(&t[0].x);
          auto* new_ms = new btDefaultMotionState(b_tform);
          delete b.body->getMotionState();
          b.body->setMotionState(new_ms);
        }
      }
    }
  }

  Buff<mat4> all_tf;
  void update_tforms_from_physics_worlds() {
    auto tforms = get_all_tforms();
    all_tf.set_data(tforms);
  }

  void step_all_worlds(ThreadPool& tp) {
    const float timestep = .1f;
    const int max_substeps = 1;
    vector<future<void>> rs;
    for (int i = 0; i < worlds.size(); i++) {
      auto* w = worlds[i]->physics_ctx.m_dynamics_world;
      rs.emplace_back(
          tp.enqueue([w, timestep, max_substeps]() { w->stepSimulation(timestep, max_substeps); }));
    }
    for (auto&& r : rs) r.get();
  }

  void step_all_worlds() {
    const float timestep = .1f;
    const int max_substeps = 1;
    for (auto& w : worlds) w->physics_ctx.m_dynamics_world->stepSimulation(timestep, max_substeps);
  }

  void
  apply_impulse(const unsigned int world_idx, const unsigned int idx, vec4 force, vec4 rel_pos) {
    worlds[world_idx]->physics_bodies[idx].body->applyImpulse(
        toBt(force.xyz()), toBt(rel_pos.xyz()));
  }

  void scale_momentum(const unsigned int world_idx, const float x) {
    worlds[world_idx]->scale_momentum(x);
  }

  vector<mat4> get_all_tforms() {
    vector<mat4> tforms;
    for (auto& w : worlds)
      for (auto& b : w->physics_bodies) tforms.emplace_back(b.get_current_tform());
    return std::move(tforms);
  }

  vector<vector<mat4>> get_all_tforms_nested() {
    return v_init<vector<mat4>>(worlds.size(), [this](auto i) { return get_tforms_for_world(i); });
  }

  vector<mat4> get_tforms_for_world(const unsigned int world_idx) {
    return v_map<PhysicsObjectGeneric, mat4>(
        worlds[world_idx]->physics_bodies, [](auto& b) { return b.get_current_tform(); });
  }

private:
  // functions defining how to re-initialize a world given a generic configuration object.
  vector<BodyConstructor> bodies;
  vector<JointConstructor> joints;
  vector<function<void(btMultiBodyDynamicsWorld*)>> world_configs;

  vector<MultipleWorldCtx*> worlds;

  void allocate_and_reset_worlds(int n /* num_worlds */) {

    // Clear existing extra worlds, if they exist
    if (worlds.size() > n) {
      for (int i = n; i < worlds.size(); i++) {
        auto* w = worlds[i];
        delete w;
        worlds[i] = nullptr;
      }
      worlds.resize(n);
    }

    // Reset existing worlds that are not trimmed away
    for (auto& w : worlds) w->reset();

    // And allocate nullptrs for any remaining
    worlds.resize(n, nullptr);
  }
};

// full objects to be added to physics worlds and renderable worlds (full/debug)
namespace PhysicsWorldPrimitives {

class Cube {
public:
  template <typename T>
  static void add_to_worlds(
      MultiplePhysicsWorlds& worlds,
      function<vec3(T&)> cube_scale,
      function<mat4(T&)> get_initial_tform,
      function<float(T&)> mass,
      function<void(T&, btRigidBody* b)> body_config,
      bool render_full = true) {

    worlds.verify_configured_type<T>();

    auto b = MultiplePhysicsWorlds::BodyConstructor::from_fns<T>(
        [cube_scale, mass, get_initial_tform](T& t, auto& b) {
          b.mass = mass(t);
          b.origin = get_initial_tform(t);
          const auto s = cube_scale(t);
          b.shape = new btBoxShape({s.x / 2.f, s.y / 2.f, s.z / 2.f});
        },
        body_config);

    const unsigned int body_id = worlds.add_body(b);

    const auto gen_cube = [cube_scale](vector<vec4>& vtxes, T& t) {
      const auto scale = cube_scale(t);
      MeshPrimitives::Cube::gen_vtx_set(vtxes, scale);
    };

    if (render_full) worlds.add_full_mesh_obj<MeshPrimitives::Cube, T>(body_id, gen_cube);

    worlds.add_debug_mesh_obj<MeshPrimitives::Cube, T>(body_id, gen_cube);
  }
};

class Cylinder {
public:
  enum PhysicsRadiusScale : unsigned int {
    AVG = 0,
    MIN = 1,
    MAX = 2,
  };

  class Params {
  public:
    ivec2 c_eval_f{8, 16}, c_eval_d{8, 16}, h_eval_f{8, 8};
    bool tip0 = true, tip1 = true;
    PhysicsRadiusScale physics_scale = PhysicsRadiusScale::AVG;
    float physics_scale_mult = 1.f;
  };

  template <typename T>
  static unsigned int add_to_worlds(
      MultiplePhysicsWorlds& worlds,
      function<NurbsPrimitives::Cylinder::Config(T&)> cylinder_cfg,
      function<mat4(T&)> get_initial_tform,
      function<float(T&)> get_mass,
      function<void(T&, btRigidBody* b)> config_body,
      Params p) {

    worlds.verify_configured_type<T>();

    auto b = MultiplePhysicsWorlds::BodyConstructor::from_fns<T>(
        [cylinder_cfg, get_initial_tform, get_mass, p](T& t, auto& b) {
          b.origin = get_initial_tform(t);
          b.mass = get_mass(t);

          auto cfg = cylinder_cfg(t);
          const auto s = get_physics_cylinder_radius(p, cfg);
          b.shape = new btCylinderShapeX({cfg.length / 2.f, s.x, s.y});
        },
        config_body);

    const unsigned int body_id = worlds.add_body(b);

    // cylinder with 4 radius values: both x/y for each end
    const auto gen_full_cyl = [cylinder_cfg](auto& cps, T& t) {
      auto cfg = cylinder_cfg(t);
      NurbsPrimitives::Cylinder::gen_cp_set(cps, cfg);
    };

    // cylinder with 1 radius value: average the original 4
    const auto gen_avg_cyl = [p, cylinder_cfg](auto& cps, T& t) {
      auto cfg = cylinder_cfg(t);
      const auto s = get_physics_cylinder_radius(p, cfg);
      NurbsPrimitives::Cylinder::gen_cp_set(cps, s, s, cfg.length);
    };

    const auto gen_cyl_tip = [cylinder_cfg](int side) -> auto {
      // side 0: scale0, -x
      // side 1: scale1, +x
      return [cylinder_cfg, side](vector<vec4>& cps, T& t) {
        auto cfg = cylinder_cfg(t);
        const auto s = side == 0 ? cfg.scale0 : cfg.scale1;
        const vec3 hs_s{s.x, s.y, (s.x + s.y) / 2.f};
        const auto x_s = side == 0 ? -1.f : 1.f;
        const auto x = (x_s * cfg.length / 2.f);
        NurbsPrimitives::HalfSphere::gen_cp_set(
            cps, hs_s, glm::translate(vec3{x, 0., 0.}) * glm::scale(vec3{x_s, 1., 1.}));
      };
    };

    worlds.add_full_nurbs_obj<NurbsPrimitives::Cylinder, T>(p.c_eval_f, body_id, gen_full_cyl);

    if (p.tip0)
      worlds.add_full_nurbs_obj<NurbsPrimitives::HalfSphere, T>(
          p.h_eval_f, body_id, gen_cyl_tip(0));

    if (p.tip1)
      worlds.add_full_nurbs_obj<NurbsPrimitives::HalfSphere, T>(
          p.h_eval_f, body_id, gen_cyl_tip(1));

    worlds.add_debug_nurbs_obj<NurbsPrimitives::Cylinder, T>(p.c_eval_d, body_id, gen_avg_cyl);

    return body_id;
  }

private:
  ivec2 cylinder_eval_full{10, 32}, hsphere_eval_full{16, 16}, cylinder_eval_debug{5, 16};

  static inline vec2
  get_physics_cylinder_radius(const Params& p, NurbsPrimitives::Cylinder::Config& c) {
    float s;
    switch (p.physics_scale) {
    case PhysicsRadiusScale::AVG:
      s = (c.scale0.x + c.scale0.y + c.scale1.x + c.scale1.y) / 4.f;
      break;
    case PhysicsRadiusScale::MIN:
      vector<float> ss{c.scale0.x, c.scale0.y, c.scale1.x, c.scale1.y};
      s = *std::min_element(ss.begin(), ss.end());
      break;
    }
    return p.physics_scale_mult * vec2{s, s};
  }
};

class Hull_8 {
public:
  template <typename T>
  static void add_to_worlds(
      MultiplePhysicsWorlds& worlds,
      function<void(vector<vec4>&, T&)> get_vtxes,
      function<mat4(T&)> get_initial_tform,
      function<float(T&)> get_mass,
      function<void(T&, btRigidBody*)> config_body) {

    worlds.verify_configured_type<T>();

    auto b = MultiplePhysicsWorlds::BodyConstructor::from_fns<T>(
        [get_vtxes, get_mass, get_initial_tform](T& t, auto& b) {
          b.mass = get_mass(t);
          b.origin = get_initial_tform(t);
          vector<vec4> vtxes;
          get_vtxes(vtxes, t);
          auto* shape = new btConvexHullShape();
          for (int i = 0; i < vtxes.size(); i++)
            shape->addPoint((btVector3&)vtxes[i], i == vtxes.size() - 1);
          b.shape = shape;
        },
        config_body);

    const unsigned int body_id = worlds.add_body(b);

    worlds.add_debug_mesh_obj<MeshPrimitives::Hull_8>(body_id, get_vtxes);
    worlds.add_full_mesh_obj<MeshPrimitives::Hull_8>(body_id, get_vtxes);
  }
};

class Hull_16 {
public:
  template <typename T>
  static unsigned int add_to_worlds(
      MultiplePhysicsWorlds& worlds,
      function<void(vector<vec4>&, T&)> get_vtxes,
      function<mat4(T&)> get_initial_tform,
      function<float(T&)> get_mass,
      function<void(T&, btRigidBody* b)> config_body,
      bool make_full_mesh) {

    worlds.verify_configured_type<T>();

    auto b = MultiplePhysicsWorlds::BodyConstructor::from_fns<T>(
        [get_vtxes, get_mass, get_initial_tform](T& t, auto& b) {
          b.mass = get_mass(t);
          b.origin = get_initial_tform(t);
          vector<vec4> hull_vtxes;
          get_vtxes(hull_vtxes, t);
          auto* shape = new btConvexHullShape();
          for (int i = 0; i < hull_vtxes.size(); i++)
            shape->addPoint((btVector3&)hull_vtxes[i], i == hull_vtxes.size() - 1);
          b.shape = shape;
        },
        config_body);

    const unsigned int body_id = worlds.add_body(b);

    worlds.add_debug_mesh_obj<MeshPrimitives::Hull_16>(body_id, get_vtxes);
    if (make_full_mesh) worlds.add_full_mesh_obj<MeshPrimitives::Hull_16>(body_id, get_vtxes);

    return body_id;
  }
};

class Hull_20 {
public:
  template <typename T>
  static void add_to_worlds(
      MultiplePhysicsWorlds& worlds,
      function<void(vector<vec4>&, T&)> get_vtxes,
      function<mat4(T&)> get_initial_tform,
      function<float(T&)> get_mass,
      function<void(T&, btRigidBody* b)> config_body) {

    worlds.verify_configured_type<T>();

    auto b = MultiplePhysicsWorlds::BodyConstructor::from_fns<T>(
        [get_vtxes, get_mass, get_initial_tform](T& t, auto& b) {
          b.mass = get_mass(t);
          b.origin = get_initial_tform(t);
          vector<vec4> hull_vtxes;
          get_vtxes(hull_vtxes, t);
          auto* shape = new btConvexHullShape();
          for (int i = 0; i < hull_vtxes.size(); i++)
            shape->addPoint((btVector3&)hull_vtxes[i], i == hull_vtxes.size() - 1);
          b.shape = shape;
        },
        config_body);

    const unsigned int body_id = worlds.add_body(b);

    worlds.add_debug_mesh_obj<MeshPrimitives::Hull_20>(body_id, get_vtxes);
    worlds.add_full_mesh_obj<MeshPrimitives::Hull_20>(body_id, get_vtxes);
  }
};

}; // namespace PhysicsWorldPrimitives
