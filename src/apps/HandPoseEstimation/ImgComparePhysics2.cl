// #py_include <cglm.cl>
// #py_include <RsTypes.cl>

typedef struct _Both {
  int2 coord;
  uint v0;
  uint v1;
  uint v1_id;
  uint _p0, _p1, _p2; // padding :)
} PhysicsForceGenerator_Both;

__kernel void get_overlap_coords_for_force_generation(
    __read_only image2d_t /*TF::RG16UI*/ img0_split,
    __read_only image2d_t /*TF::RG16UI*/ img1,
    uint num_vps,
    __global float4* vps,
    __global uint* n_not0_1,
    __global uint4* c_not0_1,
    __global uint* n_0_not1,
    __global uint4* c_0_not1,
    __global uint* n_0_1,
    __global PhysicsForceGenerator_Both* c_0_1) {

  // initialize local counts
  __local uint l_count[3];
  const bool t1 = get_local_id(0) == 0 && get_local_id(1) == 0 && get_local_id(2) == 0;
  if (t1) {
    l_count[0] = 0;
    l_count[1] = 0;
    l_count[2] = 0;
  };
  barrier(CLK_LOCAL_MEM_FENCE);

  int2 dim = get_image_dim(img0_split);
  uint vp_id = get_global_id(0);
  const int2 c = (int2){get_global_id(1), get_global_id(2)};

  const bool in_bounds = vp_id < num_vps && c.x < dim.x && c.y < dim.y;
  bool write_out[3] = {false, false, false};
  uint l_offset[3] = {0, 0, 0};

  uint2 v0;
  uint2 v1;
  // uint v1, v1_id;
  if (in_bounds) {

    float4 vp_f = vps[vp_id];
    int2 vf = (int2){(int)round(vp_f.x), (int)round(vp_f.y)};

    v0 = read_imageui(img0_split, c).xy;
    v1 = read_imageui(img1, vf + c).xy; // r: rendered depth value, g: rendered body ID

    if (v0.y && v1.x) {
      l_offset[0] = atomic_add(l_count + 0, 1);
      write_out[0] = true;
    }

    if (v0.x && !v1.x) {
      l_offset[1] = atomic_add(l_count + 1, 1);
      write_out[1] = true;
    }

    if (v0.x && v1.x) {
      l_offset[2] = atomic_add(l_count + 2, 1);
      write_out[2] = true;
    }
  }

  barrier(CLK_LOCAL_MEM_FENCE);
  if (t1) {
    l_count[0] = atomic_add(n_not0_1 + vp_id, l_count[0]);
    l_count[1] = atomic_add(n_0_not1 + vp_id, l_count[1]);
    l_count[2] = atomic_add(n_0_1 + vp_id, l_count[2]);
  }
  barrier(CLK_LOCAL_MEM_FENCE);

  if (in_bounds) {
    const uint g_offset = vp_id * dim.x * dim.y;

    // get neighbors!
    uint neighbors = 0;
    if (write_out[0] || write_out[1]) {
      int2 _c = {-1, -1};
      for (; _c.x < 2; _c.x++) {
        for (; _c.y < 2; _c.y++) {
          if (_c.x == 0 && _c.y == 0) continue;
          // read_imageui()
          neighbors |= 1 << _c.x;
          // const int2 test_c = (int2) {}
        }
      }
    }

    if (write_out[0]) {
      c_not0_1[g_offset + l_count[0] + l_offset[0]] = (uint4){c.x, c.y, (int)v1.y, neighbors};
    }

    if (write_out[1]) {
      c_0_not1[g_offset + l_count[1] + l_offset[1]] = (uint4){c.x, c.y, (int)v1.y, neighbors};
    }

    if (write_out[2]) {
      int c_idx = (c.y * dim.x) + c.x;

      c_0_1[g_offset + l_count[2] + l_offset[2]] =
          (PhysicsForceGenerator_Both){c, v0.x, v1.x, v1.y, 0, 0, 0};

      //(int4){c_idx, (int)v1.y, (int)v0.x, (int)v1.x};
    }
  }
}

__kernel void get_coords_with_nearby_simulated_coords_all(
    uint total_num_coords,
    __global uint4* coords_in_mapped,
    __global uint4* coords_in,
    __global float4* vps,
    __read_only image2d_t /*TF::RG16UI*/ img_simulated,
    __global uint* coord_out_num,
    __global uint4* coords_out,
    __global float4* coords_out_avg_neighbor) {

  // First viewport is {0, 0, max_x, max_y}!
  int2 img_dim = (int2){(int)round(vps[0].z), (int)round(vps[0].w)};
  uint i = get_global_id(0);

  if (i < total_num_coords) {

    const uint4 _c = coords_in_mapped[i];
    const int vp_id = _c.x;
    const int coord_id = _c.y;

    const int coords_in_offset = img_dim.x * img_dim.y * vp_id;

    float4 img_vp_f = vps[vp_id];
    int2 img_vp = (int2){(int)round(img_vp_f.x), (int)round(img_vp_f.y)};

    uint4 c = coords_in[coords_in_offset + coord_id];

    bool sim_neighbor_mesh_id_set = false;
    uint sim_neighbor_mesh_id = 0;
    int sim_neighbor_count = 0;
    int2 sim_neighbor_coord_sum = (int2){0, 0};

    bool sim_mesh_id_conflict = false;

    for (int _j = 0; _j < 8 && !sim_mesh_id_conflict; _j++) {
      int j = j < 4 ? _j : _j + 1;
      int2 c_test = (int2){c.x - 1 + (j % 3), c.y - 1 + (j / 3)};

      if (c_test.x >= 0 && c_test.y >= 0 && c_test.x < img_dim.x && c_test.y < img_dim.y) {
        uint c_test_mesh_id =
            read_imageui(img_simulated, img_vp + c_test).y; // mesh ID in y channel
        if (c_test_mesh_id) {
          // make sure mesh ids are all the same!
          if (!sim_neighbor_mesh_id_set) {
            sim_neighbor_mesh_id_set = true;
            sim_neighbor_mesh_id = c_test_mesh_id;
          } else {
            if (sim_neighbor_mesh_id != c_test_mesh_id) { sim_mesh_id_conflict = true; }
          }

          sim_neighbor_count++;
          sim_neighbor_coord_sum.x += c_test.x;
          sim_neighbor_coord_sum.y += c_test.y;
        }
      }
    }

    if (sim_neighbor_count && !sim_mesh_id_conflict) {
      uint out_num = atomic_add(coord_out_num + vp_id, 1);
      coords_out[coords_in_offset + out_num] = (uint4){c.x, c.y, (int)sim_neighbor_mesh_id, 0};

      float2 neighbor_avg = (float2){(1.f * sim_neighbor_coord_sum.x) / sim_neighbor_count,
                                     (1.f * sim_neighbor_coord_sum.y) / sim_neighbor_count};
      coords_out_avg_neighbor[coords_in_offset + out_num] =
          (float4){neighbor_avg.x, neighbor_avg.y, 0, 0};
    }
  }
}

__kernel void get_all_coords_with_both_and_values(
    __read_only image2d_t /*TF::R16UI*/ img0,
    __read_only image2d_t /*TF::R16UI*/ img1,
    __read_only image2d_t /*TF::R16UI*/ img1_meshids,
    uint num_vps,
    __global float4* vps,
    __global uint* coord_out_num,
    __global int4* coords_out) {

  int2 img_dim = get_image_dim(img0);
  uint vp_id = get_global_id(0);
  int2 c = (int2){get_global_id(1), get_global_id(2)};

  if (vp_id < num_vps && c.x < img_dim.x && c.y < img_dim.y) {

    float4 img_vp_f = vps[vp_id];
    int2 img_vp = (int2){(int)round(img_vp_f.x), (int)round(img_vp_f.y)};

    uint v0 = read_imageui(img0, c).x;
    uint v1 = read_imageui(img1, img_vp + c).x;

    if (v0 && v1) {
      uint m_id = read_imageui(img1_meshids, img_vp + c).x;

      uint start_idx = img_dim.x * img_dim.y * vp_id;
      uint n = atomic_add(coord_out_num + vp_id, 1);

      int idx = c.y * img_dim.x + c.x;

      coords_out[start_idx + n] = (int4){idx, (int)m_id, (int)v0, (int)v1};
    }
  }
}

__kernel void get_coords_with_nearby_empty_coords_all(
    uint full_num_coords,
    __global uint4* full_num_coords_map,
    __read_only image2d_t /*TF::RG16UI*/ img_split,
    __global uint4* coords_in,
    __global uint* coord_out_num,
    __global uint4* coords_out,
    __global float4* coords_out_avg_empty_neighbor) {

  int2 img_dim = get_image_dim(img_split);

  uint i = get_global_id(0);
  if (i < full_num_coords) {
    const uint4 _c = full_num_coords_map[i];
    const int vp_id = _c.x;
    const int coord_id = _c.y;

    const uint coord_set_offset = img_dim.x * img_dim.y * vp_id;
    const uint4 c = coords_in[coord_set_offset + coord_id];

    int empty_count = 0;
    int2 empty_sum = (int2){0, 0};

    for (int _j = 0; _j < 8; _j++) {
      int j = j < 4 ? _j : _j + 1;
      int2 c_test = (int2){c.x - 1 + (j % 3), c.y - 1 + (j / 3)};

      if (c_test.x >= 0 && c_test.y >= 0 && c_test.x < img_dim.x && c_test.y < img_dim.y) {
        uint c_test_val = read_imageui(img_split, c_test).y; // looking for 'not' channel
        if (!c_test_val) {
          empty_count++;
          empty_sum.x += c_test.x;
          empty_sum.y += c_test.y;
        }
      }
    }

    if (empty_count) {
      float2 empty_avg =
          (float2){(1.f * empty_sum.x) / empty_count, (1.f * empty_sum.y) / empty_count};

      uint out_num = atomic_add(coord_out_num + vp_id, 1);
      coords_out[coord_set_offset + out_num] = c;
      coords_out_avg_empty_neighbor[coord_set_offset + out_num] =
          (float4){empty_avg.x, empty_avg.y, 0, 0};
    }
  }
}

__kernel void compress_buffer_ivec4(
    uint num_groups,
    uint in_group_size,
    __global uint* coord_counts,
    __global uint* coord_offsets_out,
    __global int4* buff_in,
    __global int4* buff_out) {

  const uint group_id = get_global_id(0);
  const uint coord_id = get_global_id(1);

  if (group_id < num_groups) {
    if (coord_id < coord_counts[group_id]) {
      const uint in_offset = in_group_size * group_id;
      const uint in_idx = in_offset + coord_id;

      const uint out_offset = coord_offsets_out[group_id];
      const uint out_idx = out_offset + coord_id;

      buff_out[out_idx] = buff_in[in_idx];
    }
  }
}

__kernel void compress_buffer_uvec4(
    uint num_groups,
    uint in_group_size,
    __global uint* coord_counts,
    __global uint* coord_offsets_out,
    __global uint4* buff_in,
    __global uint4* buff_out) {

  const uint group_id = get_global_id(0);
  const uint coord_id = get_global_id(1);

  if (group_id < num_groups) {
    if (coord_id < coord_counts[group_id]) {
      const uint in_offset = in_group_size * group_id;
      const uint in_idx = in_offset + coord_id;

      const uint out_offset = coord_offsets_out[group_id];
      const uint out_idx = out_offset + coord_id;

      buff_out[out_idx] = buff_in[in_idx];
    }
  }
}

__kernel void compress_buffer_vec4(
    uint num_groups,
    uint in_group_size,
    __global uint* coord_counts,
    __global uint* coord_offsets_out,
    __global float4* buff_in,
    __global float4* buff_out) {

  const uint group_id = get_global_id(0);
  const uint coord_id = get_global_id(1);

  if (group_id < num_groups) {
    if (coord_id < coord_counts[group_id]) {
      const uint in_offset = in_group_size * group_id;
      const uint in_idx = in_offset + coord_id;

      const uint out_offset = coord_offsets_out[group_id];
      const uint out_idx = out_offset + coord_id;

      buff_out[out_idx] = buff_in[in_idx];
    }
  }
}

__kernel void gen_invocations(uint n_groups, __global int2* g_infos, __global int2* out) {
  const uint g_id = get_global_id(0);
  const uint c_id = get_global_id(1);
  if (g_id < n_groups) {
    const int2 g_info = g_infos[g_id]; // x: num coords in group. y: invocation out offset
    if (c_id < g_info.x) { out[g_info.y + c_id] = (int2){g_id, c_id}; }
  }
}

typedef struct _Force {
  float4 f;
  float4 r_pos;
  uint body_idx;
  uint world_id;
  uint _p0, _p1; // padding
} PhysicsForceGenerator_Force;

__kernel void gen_forces_out(
    __global int2* groups_info,
    uint num_groups,
    rs2_intrinsics intrin,
    _mat4 cam_to_world,
    __global PhysicsForceGenerator_Both* all_pixels,
    uint all_pixels_group_stride,
    __global float4* body_origins,
    __read_only image2d_t /*TF::RGBA32F*/ neighbors_info,
    uint num_bodies_per_group,
    __global PhysicsForceGenerator_Force* f_out) {

  uint g_id = get_global_id(0);
  if (g_id < num_groups) {
    uint c_id = get_global_id(1);
    const int2 group_info = groups_info[g_id];
    // x: num groups, y: start offset
    if (c_id < group_info.x) {

      const uint out_idx = group_info.y + c_id;

      const PhysicsForceGenerator_Both pixel_info =
          all_pixels[(all_pixels_group_stride * g_id) + c_id];

      const int2 pixel_coords = pixel_info.coord;
      const uint body_idx = pixel_info.v1_id - 1;
      const uint v0 = pixel_info.v0;
      const uint v1 = pixel_info.v1;

      float4 n_info = read_imagef(neighbors_info, pixel_coords);
      // if (n_info.w > 0.f) {
      // printf("force %f!\n", n_info.x);
      //}

      const float4 b = body_origins[(num_bodies_per_group * g_id) + body_idx];

      float4 v0_pt, v1_pt;
      rs2_pixel_to_point(&intrin, &pixel_coords, v0, &v0_pt);
      rs2_pixel_to_point(&intrin, &pixel_coords, v1, &v1_pt);

      float4 v0_pt_world, v1_pt_world;
      glm_mat4_mulv(&cam_to_world, &v0_pt, &v0_pt_world);
      glm_mat4_mulv(&cam_to_world, &v1_pt, &v1_pt_world);

      float4 f = v0_pt_world - v1_pt_world;
      if (n_info.x < 9.f) {
        f *= 0.f;
      } else {
        f *= 2.f;
      }
      f.w = 0.f;

      float4 r_pos = v1_pt_world - b;
      r_pos.w = 1.f;

      f_out[out_idx] = (PhysicsForceGenerator_Force){f, r_pos, body_idx, g_id, 0, 0};
    }
  }
}