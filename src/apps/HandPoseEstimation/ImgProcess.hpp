#pragma once

class ImgProcess : protected ComputeBase {
  //using namespace Engine::Gpu;

#include <generated/ImgProcess_cl_signature.hpp>

  void make_neighbors_info(Tex<TF::RGBA32F>& pts, Tex<TF::RGBA32F>& neighbors_info) {

    const ivec2 d = pts.get_dims();
    neighbors_info.set_storage(d);

    const int block = 8;
    kernel_ImgProcess_make_neighbors_info((d / block) + 1, {block, block}, pts, neighbors_info);
  }

  void make_neighbors_info_c(Tex<TF::RGBA32F>& neighbors_info, Tex<TF::RGBA8>& rgba) {

    const ivec2 d = neighbors_info.get_dims();
    rgba.set_storage(d);

    const int block = 8;
    kernel_ImgProcess_make_neighbors_info_c((d / block) + 1, {block, block}, neighbors_info, rgba);
  }
};
