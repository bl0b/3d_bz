#pragma once

#include <engine/Engine.h>

#include <compute/ComputeLib.hpp>
#include <render/render.hpp>

#include <Components/Physics.hpp>
#include <Components/MeshGroups.hpp>

#include <random>

#include <thread>
#include <chrono>

#include <queue>

using namespace ComputeLib;
using namespace Engine::Gpu;
using namespace PhysicsConversion;
using namespace std;

#include <components/RsDataBag.hpp>
#include "PhysicsForceGenerator.hpp"

#include "CalibratedPlane2.hpp"
#include "NodeGroup.hpp"
#include "MultiplePhysicsWorlds.hpp"
#include "HandFitter.hpp"

class NodesDemo : public AbstractApp {
public:
  Tex<TF::RGBA8> dbg;
  const ivec2 dbg_dims{600, 400};
  StdFramebuffer dbg_fbo;

  NurbsShader n_cam;
  StdCamera m_cam;

  MapDepth map_depth;
  DeprojectDepth deproject_depth;
  GenTriangles gen_triangles;

  HandFitter hand_fitter;

  MeshObj forces_debug_obj;

  bool draw_full = true, draw_debug = false, draw_img_debug = true, draw_forces = false;

  const string dfile = "C:\\Users\\Carson\\Documents\\hand_demo_23.bag";
  RsDataBag data_bag;

  Tex<TF::RG16UI> img_split_minif, img_split_full;
  Tex<TF::RGBA8> img_split_minif_c, img_split_full_c;

  Tex<TF::RG16UI> sim_depth_img;
  StdFramebuffer sim_depth_img_fbo;
  Tex<TF::RGBA8> depth_img_c, sim_depth_img_c;

  Tex<TF::RGBA32F> obj_vtxes;
  MeshObj debug_obj;

  CalibratedPlane2 calibrated_plane;

  const int fit_frame_1 = 65;  // hand flat, fingers & thumb in
  const int fit_frame_2 = 200; // hand flat, fingers & thumb out
  int minif_factor_1 = 4;
  float live_z_threshold = 40.f;

  Engine::Scene::ArcBallCameraPosition cam_pos;

  ThreadPool tp;

  vector<float> best_costs;

  Engine::Components::Playback playback_ctrl;

  explicit NodesDemo(Engine::Gpu::GraphicsCtx& _w) : AbstractApp(_w), tp(16) {

    w.on_press(GLFW_KEY_ESCAPE, [&] { w.set_should_close(true); });

    data_bag.from_file(dfile, true);

    sim_depth_img.set_storage({data_bag.depth_intrinsics.width, data_bag.depth_intrinsics.height});

    playback_ctrl.set_num_frames(data_bag.depth_frames.size());
    playback_ctrl.set_current_frame(fit_frame_1);

    calibrated_plane.calibrate(data_bag.depth_frames[0], data_bag.depth_intrinsics);

    dbg.set_storage(dbg_dims);

    {
      const mat4 depth_cam_tform =
          glm::scale(vec3{1.f, 1.f, -1.f}) * calibrated_plane.ransac_pipeline.p.from_plane_coords;
      ivec2 vp_dim =
          ivec2{data_bag.depth_intrinsics.width, data_bag.depth_intrinsics.height} / minif_factor_1;

      hand_fitter.init(vp_dim, depth_cam_tform, data_bag.depth_intrinsics, &tp);
    }

    {
      cam_pos.config.angle_y_range = {(float)M_PI_2, 3.1};
      // cam_pos.config.angle_y = 1.57;
      cam_pos.config.angle_y = 2.2;

      cam_pos.config.angle_x_range = {0.f, 2.f * (float)M_PI};
      // cam_pos.config.angle_x = 4.66;
      cam_pos.config.angle_x = 3.14;
    }

    {
      const auto& i = data_bag.depth_intrinsics;
      n_cam.cam_proj = m_cam.cam_proj =
          glm::perspectiveFromIntrinsics(
              vec2{i.fx, i.fy}, vec2{i.ppx, i.ppy}, vec2{i.width, i.height}, vec2{1., 50000.}) *
          glm::scale(vec3{1., 1., -1});
    };

    MeshPrimitives::DebugForce::init(forces_debug_obj);
  }

  bool set_stage1 = false;
  bool set_stage2 = false;
  bool set_stage3 = false;
  void tick() override {



    if (hand_fitter.stage == 2 && !set_stage2) {
      set_stage2 = true;
      //playback_ctrl.zigzag_loop(fit_frame_1, fit_frame_2);
    }

    if (hand_fitter.stage == 3 && !set_stage3) {
      set_stage3 = true;
      //playback_ctrl.stop_zigzag();
    }

    auto& depth_img = data_bag.depth_frames[playback_ctrl.get_current_frame()];

    Engine::Util::Timer t;

    t.record("split incoming");

    rs2_intrinsics minif_intrinsics;
    calibrated_plane.split_incoming_image(
        depth_img, minif_factor_1, img_split_minif, live_z_threshold, &minif_intrinsics);

    t.record("run hand fit");

    hand_fitter.run_fit(
        img_split_minif,
        minif_intrinsics,
        calibrated_plane.ransac_pipeline.p.to_plane_coords,
        tp,
        calibrated_plane.ransac_pipeline.ortho_sampler,
        calibrated_plane.ransac_pipeline.ortho_map_binary,
        &forces_debug_obj);

    t.record("gen costs");

    auto frame_costs = hand_fitter.gen_costs(
        img_split_minif,
        minif_intrinsics,
        calibrated_plane.ransac_pipeline.p.to_plane_coords,
        calibrated_plane.ransac_pipeline.ortho_sampler,
        calibrated_plane.ransac_pipeline.ortho_map_binary);

    t.record("get best cost");

    int best_cost_idx;
    float best_cost;
    for (int i = 0; i < frame_costs.size(); i++) {
      const float c = frame_costs[i];
      if (!i || c < best_cost) {
        best_cost = c;
        best_cost_idx = i;
      }
    }

    
    //if (hand_fitter.stage == 1 && !set_stage1) {
      //set_stage1 = true;
      //hand_fitter.current_world = 0;
    //}
    

    //if (hand_fitter.stage != 1) { hand_fitter.current_world = best_cost_idx; }
    hand_fitter.current_world = best_cost_idx;
    best_costs.emplace_back(frame_costs[hand_fitter.current_world]);

    t.record("map incoming split (dbg)");

    map_depth.run_split(img_split_minif, img_split_minif_c);

    t.record("gen debug object");

    gen_debug_mesh(minif_intrinsics);

    t.record("draw debug");

    m_cam.color_mode = StdCamera::ColorMode::Full;
    dbg_fbo.set_target(StdFramebuffer::StdTargetConfig().add(&dbg), true);
    GL::Renderer::setPolygonMode(GL::Renderer::PolygonMode::Line);

    n_cam.cam_pos = m_cam.cam_pos = cam_pos.transform_inverse();

    if (draw_full) {
      m_cam.color_out = n_cam.color_out = vec4{0.2, 0.8, 0.9, 1.};
      hand_fitter.draw_current(n_cam, m_cam);
    }

    if (draw_debug) {
      m_cam.color_out = n_cam.color_out = vec4{0.2, 0.4, 0.3, 1.};
      hand_fitter.draw_current(n_cam, m_cam, false);
    }

    if (draw_img_debug) {
      m_cam.color_out = vec4{0., 0.8, 0.3, 1.};
      m_cam.draw(debug_obj);
    }

    if (draw_forces) {
      m_cam.color_mode = StdCamera::ColorMode::Vtx;
      m_cam.color_out = vec4{1., 1., 1., 1.};
      const int n = forces_debug_obj.get_data().tforms_count / 2;
      m_cam.draw(forces_debug_obj, DrawConfig().num(n).cps({0, 0}).tforms({0, 1}));
    }

    dbg_fbo.clear_target();

    hand_fitter.draw_current_world_depth(sim_depth_img_fbo, sim_depth_img);

    GL::defaultFramebuffer.bind();

    t.record("map depth depth img");

    map_depth.run_depth(sim_depth_img, sim_depth_img_c, 0);
    //map_depth.run_meshid(sim_depth_img, sim_depth_img_c, 1);

    t.record("split full incoming image");

    {
      rs2_intrinsics i;
      calibrated_plane.split_incoming_image(depth_img, 1, img_split_full, live_z_threshold, &i);
      t.record("split full incoming image MAP");
      map_depth.run_split(img_split_full, img_split_full_c);
    }

    t.record("imgui");

    ImGui::Begin("full");
    Engine::ImGuiCtx::Image2(dbg);
    ImGui::End();

    ImGui::Begin("best costs");
    const int num_to_plot = 60;
    while (best_costs.size() < num_to_plot) {
      best_costs.emplace_back(0.);
    }
    float* da = best_costs.data() +
                (best_costs.size() >= num_to_plot ? best_costs.size() - num_to_plot : 0);
    const int num_els = best_costs.size() > num_to_plot ? num_to_plot : best_costs.size();
    ImGui::PlotLines("best costs", da, num_to_plot, 0, nullptr, 0., 50., {500., 200.});
    // ImGui::PlotLines("best costs", da, num_els, 0, nullptr, 0., 5000.);
    ImGui::End();

    ImGui::Begin("hi");
    // Engine::ImGuiCtx::Image2(obj_c);
    // Engine::ImGuiCtx::Image2(table_minus_obj_c);
    Engine::ImGuiCtx::Image2(hand_fitter.force_generator.obj_neighbors_info_c);
    Engine::ImGuiCtx::Image2(depth_img_c);
    Engine::ImGuiCtx::Image2(img_split_full_c);
    Engine::ImGuiCtx::Image2(sim_depth_img_c);
    Engine::ImGuiCtx::Image2(calibrated_plane.ransac_pipeline.ortho_map_binary_rgba);

    Engine::ImGuiCtx::Image2(img_split_minif_c);
    Engine::ImGuiCtx::Image2(hand_fitter.physics_sim_depth_c);
    Engine::ImGuiCtx::Image2(hand_fitter.physics_sim_body_ids_c);

    ImGui::End();

    ImGui::Begin("controls");
    ImGui::Checkbox("draw full", &draw_full);
    ImGui::Checkbox("draw debug", &draw_debug);
    ImGui::Checkbox("draw img", &draw_img_debug);
    ImGui::Checkbox("draw forces", &draw_forces);

    playback_ctrl.draw_tick();

    // ImGui::SliderInt("current frame", &current_frame, 0, data_bag.depth_frames.size() - 1);

    hand_fitter.gui();

    ImGui::End();

    cam_pos.draw_imgui();

    t.record("end");

     //printf("tick!\n");
     //t.render([](auto s, auto f) { printf("-- %s: %f\n", s.c_str(), f); });
  }

  void gen_debug_mesh(rs2_intrinsics& minif_intrinsics) {

    deproject_depth.run_split(
        img_split_minif,
        obj_vtxes,
        minif_intrinsics,
        1,
        calibrated_plane.ransac_pipeline.p.to_plane_coords);

    obj_vtxes.cu_copy_to(debug_obj.get_data().vtxes);
    debug_obj.get_data().set_tform(mat4{1.f});

    gen_triangles.run(obj_vtxes, debug_obj.get_view().idxes, &debug_obj.get_view().idxes_count);
    debug_obj.get_view().idxes_count *= 3;
  }
};
