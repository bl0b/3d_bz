__kernel void make_neighbors_info(
    __read_only image2d_t /*TF::RGBA32F*/ pts_in, __write_only image2d_t /*TF::RGBA32F*/ out) {

  const int2 c = (int2){get_global_id(0), get_global_id(1)};
  const int2 d = get_image_dim(pts_in);

  if (c.x < d.x && c.y < d.y) {

    int num_neighbors = 0;

    for (int i = 0; i < 3; i++) {
      for (int j = 0; j < 3; j++) {
        int2 t_c = c + (int2) { i - 1, j - 1 };
        if (t_c.x >= 0 && t_c.y >= 0 && t_c.x < d.x && t_c.y < d.y) {
          float4 p = read_imagef(pts_in, t_c);
          if (p.w > 0.f) { num_neighbors++; }
        }
      }
    }

    float4 o;
    if (num_neighbors == 0) {
      o = (float4){0., 0., 0., 0.};
    } else {
      o = (float4){num_neighbors * 1.f, 0., 0., 1.};
    }

    write_imagef(out, c, o);
  }
}

__kernel void make_neighbors_info_c(
    __read_only image2d_t /*TF::RGBA32F*/ neighbors_info,
    __write_only image2d_t /*TF::RGBA8*/ color_out) {

  int2 c = (int2){get_global_id(0), get_global_id(1)};
  int2 dims = get_image_dim(neighbors_info);

  if (c.x < dims.x && c.y < dims.y) {

    float4 i = read_imagef(neighbors_info, c);

    int neighbor_count = (int)round(i.x);

    float4 o = (float4){i.x / 9., 0., i.x > 0.f ? 0.5 : 0., 1.};
    write_imagef(color_out, c, o);
  }
}
