#pragma once

#include "ImgComparePhysics2.hpp"
#include "ImgProcess.hpp"
#include "ThreadPool.h"

class PhysicsForceGenerator {
public:
  const unsigned int THREAD_POOL_SIZE = 32;
  ThreadPool* thread_pool = nullptr;

  ImgComparePhysics2 img_compare;
  DeprojectDepth deproject_depth;
  ImgProcess img_process;

  Buff<uvec4> c_not0_1, c_0_not1;
  Buff<PhysicsForceGenerator_Both> c_0_1;

  Tex<TF::RGBA32F> obj_pts, obj_neighbors_info;
  Tex<TF::RGBA8> obj_neighbors_info_c;

  void run(
      // Original depth image, split into 2 channels
      // r: object, g: definitively not object
      Tex<TF::RG16UI>& obj_split,
      // Rendered depth image.
      // r: object depth img, g: object body IDs (1 idxed)
      Tex<TF::RG16UI>& obj_simulated,
      // obj_simulated includes many images rendered on multiple viewports. which one to read?
      unsigned int num_worlds,
      Buff<vec4>& obj_simulated_viewports,
      rs2_intrinsics intrinsics,
      glm::mat4 cam_to_world,
      vector<vector<vec4>>& all_body_origins,
      vector<vector<PhysicsForceGenerator_Force>>& all_forces_generated,
      ThreadPool* tp = nullptr) {

    Engine::Util::Timer t;
    t.record("make points");

    deproject_depth.run_split(obj_split, obj_pts, intrinsics, 1, mat4{1.f});

    t.record("make points neighbor info");

    // deproject_depth
    img_process.make_neighbors_info(obj_pts, obj_neighbors_info);
    img_process.make_neighbors_info_c(obj_neighbors_info, obj_neighbors_info_c);

    t.record("make planes");

    if (tp == nullptr && thread_pool == nullptr) { thread_pool = new ThreadPool(THREAD_POOL_SIZE); }
    auto* _tp = tp == nullptr ? thread_pool : tp;

    vector<vector<Engine::Math::Plane>> all_physics_body_planes;
    const vec4 cam_pos = cam_to_world * vec4{0., 0., 0., 1.};
    for (const auto& body_origins : all_body_origins) {
      vector<Engine::Math::Plane> physics_body_planes;
      for (auto b : body_origins) {
        vec3 cam_to_b = glm::normalize(vec3{b - cam_pos});
        // Forces applied to this body must live entirely in this plane, so the only rotational
        // torque applied causes the object to turn along axis defined by camera-to-obj-origin
        // direction
        physics_body_planes.emplace_back(
            Engine::Math::Plane(cam_to_b /* normal */, b.xyz() /* origin */));
      }
      all_physics_body_planes.emplace_back(std::move(physics_body_planes));
    }

    const ivec2 d = obj_split.get_dims();
    all_forces_generated.resize(num_worlds);

    t.record("initial overlaps");

    vector<uint> n_not0_1, n_0_not1, n_0_1;
    img_compare.get_overlap_coords_for_force_generation(
        obj_split,
        obj_simulated,
        num_worlds,
        obj_simulated_viewports,
        c_not0_1,
        n_not0_1,
        c_0_not1,
        n_0_not1,
        c_0_1,
        n_0_1);

    t.record("push out");

    TpResults tp_results;

    // Part 1:
    // Find coordinates where rendered object overlaps onto known non-object pixels. For every
    // one of these cases, push the rendered body away from this 'forbidden' area.
    push_out_generator.run_all(
        tp_results,
        _tp,
        obj_split,
        obj_simulated,
        num_worlds,
        obj_simulated_viewports,
        intrinsics,
        cam_to_world,
        c_not0_1,
        n_not0_1,
        all_physics_body_planes,
        all_forces_generated);

    t.record("pull in");

    // Part 2:
    // Find coordinates where rendered object is missing from known object pixels. For every one
    // of these cases, pull the nearest object towards this 'missing' area.
    pull_in_generator.run_all(
        tp_results,
        _tp,
        obj_split,
        obj_simulated,
        num_worlds,
        obj_simulated_viewports,
        intrinsics,
        cam_to_world,
        c_0_not1,
        n_0_not1,
        all_physics_body_planes,
        all_forces_generated);

    t.record("up/down");

    // Part 3:
    // Find coordinates where rendered object and real object both have depth values, and
    // push/pull object along camera ray going through both points
    depth_correction_generator.run_all(
        tp_results,
        _tp,
        intrinsics,
        cam_to_world,
        c_0_1,
        n_0_1,
        obj_neighbors_info,
        all_body_origins,
        all_forces_generated);

    tp_results.get();

    t.record("end");
    // printf("force generation: \n");
    // t.render("-- -- %s: %f\n");
  }

  // to setup the renderable..
  static void make_debug_forces(
      MeshObj& o,
      int world_idx,
      vector<vector<vec4>>& all_body_positions,
      vector<vector<PhysicsForceGenerator_Force>>& all_gen_forces) {

    const auto& body_positions = all_body_positions[world_idx];
    const auto& gen_forces = all_gen_forces[world_idx];

    auto tforms =
        Engine::Util::Stl::v_init<mat4>(gen_forces.size(), [&body_positions, &gen_forces](int i) {
          const auto& f = gen_forces[i];
          const vec4 b_pos = body_positions[f.body_idx];
          const vec4 f_pos = b_pos + f.r_pos;
          return glm::translate(vec3{f_pos}) * glm::scale(vec3{f.f});
        });

    auto& d = o.get_data();
    d.set_tforms(tforms);
  }

  class TpResults {
  public:
    vector<future<void>> threads;

    void get() {
      for (auto&& t : threads) t.get();
      threads.clear();
    }
  };

private:
  class PushOutForceGenerator {
  private:
    Buff<uvec4> /*all_pixels, */ border_pixels;
    Buff<uvec4> all_pixels_compressed, border_pixels_compressed;
    Buff<vec4> border_pixel_neighbors, border_pixel_neighbors_compressed;

    ImgComparePhysics2 img_compare;

    // per-pixel state for graph traversal
    struct PixelState {
      uint16_t body_id;
      uint8_t seen;
      uint8_t parent_found;
      vec2 parent;
    };

    vector<PixelState> pixels_state;

    vector<uvec4> all_pixels_cpu;
    vector<uvec4> all_border_pixels;
    vector<vec4> all_border_pixels_neighbors;

  public:
    // return function to be called
    void run_all(
        TpResults& results,
        ThreadPool* tp,
        Tex<TF::RG16UI>& obj_split,     // y channel!
        Tex<TF::RG16UI>& obj_simulated, // look for IDs in y channel
        unsigned int num_worlds,
        Buff<vec4>& obj_simulated_viewports,
        rs2_intrinsics intrinsics,
        glm::mat4 to_world,
        Buff<uvec4>& all_pixels,
        vector<uint>& all_pixels_n,
        vector<vector<Engine::Math::Plane>>& all_body_planes,
        vector<vector<PhysicsForceGenerator_Force>>& all_forces_out) {

      const ivec2 d = obj_split.get_dims();

      Engine::Util::Timer t;
      t.record("border coords");

      vector<uint> border_pixels_n;
      img_compare.get_coords_with_nearby_empty_coords_all(
          obj_split,
          all_pixels_n,
          all_pixels,
          border_pixels_n,
          border_pixels,
          border_pixel_neighbors);

      t.record("compress");

      const bool multiple_worlds = all_pixels_n.size() > 1;
      if (multiple_worlds) {
        img_compare.compress_buffer_uvec4(
            d.x * d.y, all_pixels_n, all_pixels, all_pixels_compressed);

        img_compare.compress_buffer_uvec4(
            d.x * d.y, border_pixels_n, border_pixels, border_pixels_compressed);

        img_compare.compress_buffer_vec4(
            d.x * d.y, border_pixels_n, border_pixel_neighbors, border_pixel_neighbors_compressed);
      }

      t.record("copy to cpu");

      int all_pixels_cpu_size = 0;
      for (auto count : all_pixels_n) all_pixels_cpu_size += count;

      (multiple_worlds ? all_pixels_compressed : all_pixels)
          .cu_copy_to(all_pixels_cpu, all_pixels_cpu_size);

      int all_border_pixels_cpu_size = 0;
      for (auto count : border_pixels_n) all_border_pixels_cpu_size += count;

      (multiple_worlds ? border_pixels_compressed : border_pixels)
          .cu_copy_to(all_border_pixels, all_border_pixels_cpu_size);

      (multiple_worlds ? border_pixel_neighbors_compressed : border_pixel_neighbors)
          .cu_copy_to(all_border_pixels_neighbors, all_border_pixels_cpu_size);

      t.record("allocate/clear shared memory");

      {
        pixels_state.resize(num_worlds * d.x * d.y);
        memset(pixels_state.data(), 0, sizeof(PixelState) * d.x * d.y * num_worlds);
      }

      t.record("gen forces");

      int all_pixels_offset = 0, border_pixels_offset = 0;

      // clear pending results
      results.get();

      for (int i = 0; i < num_worlds; i++) {

        auto& planes = all_body_planes[i];
        auto& forces_out = all_forces_out[i];

        auto* all_pixels_cpu_ptr = all_pixels_cpu.data() + all_pixels_offset;
        all_pixels_offset += all_pixels_n[i];

        auto* b_pixels = all_border_pixels.data() + border_pixels_offset;
        auto* b_pixel_neighbors = all_border_pixels_neighbors.data() + border_pixels_offset;
        border_pixels_offset += border_pixels_n[i];

        const auto o = i * d.x * d.y;
        auto* pixels_state_local = pixels_state.data() + (o);

        const auto work_fn = [pixels_state_local,
                              d,
                              to_world,
                              intrinsics,
                              &planes,
                              all_pixels_cpu_ptr,
                              all_pixels_cpu_n = all_pixels_n[i],
                              b_pixels,
                              b_pixel_neighbors,
                              b_pixels_cpu_n = border_pixels_n[i],
                              &forces_out]() {
          gen_forces_out(
              pixels_state_local,
              d,
              to_world,
              intrinsics,
              planes,
              all_pixels_cpu_ptr,
              all_pixels_cpu_n,
              b_pixels,
              b_pixel_neighbors,
              b_pixels_cpu_n,
              forces_out);
        };

        results.threads.emplace_back(tp->enqueue(work_fn));
      }

      t.record("end");

      /*
      printf(
          "push out generator! all pixels: %i - border pixels: %i\n",
          all_pixels_cpu_size,
          all_border_pixels_cpu_size);
      t.render([](auto s, auto f) { printf("-- -- -- -%s: %f\n", s.c_str(), f); });

      printf("-- -- -- --inner timer\n");
      for (auto& _v : profile) {
        auto v = _v.load();
        printf(" --- ---- %f\n", v / (1000000.f * num_worlds));
      }
      */
    }

  private:
    static void gen_forces_out(
        PixelState* pixels_state,
        const ivec2 d,
        const mat4 to_world,
        rs2_intrinsics intrinsics,
        vector<Engine::Math::Plane>& body_planes,
        // every eligible pixel idx in this map {xy, body_id, 0}
        uvec4* all_pixels,
        unsigned int all_pixels_n,
        uvec4* _border_pixels,
        vec4* _border_pixel_neighbors,
        unsigned int border_pixels_n,
        vector<PhysicsForceGenerator_Force>& forces_out) {

      for (int i = 0; i < all_pixels_n; i++) {
        auto& p = all_pixels[i];
        pixels_state[get_idx(d, p.xy())].body_id = (uint16_t)p.z;
      }

      queue<unsigned int> coords_queue;

      for (int i = 0; i < border_pixels_n; i++) {
        ivec2 coord = _border_pixels[i].xy();
        vec2 neighbor_coord = _border_pixel_neighbors[i].xy();

        const unsigned int c_idx = get_idx(d, coord);
        pixels_state[c_idx].seen = 1;
        pixels_state[c_idx].parent_found = 1;
        pixels_state[c_idx].parent = neighbor_coord;

        for (auto c_test_idx : MeshGroups2::get_neighbor_idxes(d, coord)) {
          auto& s = pixels_state[c_test_idx];
          if (s.body_id && !s.seen) {
            s.seen = 1;
            coords_queue.push(c_test_idx);
          }
        }
      }

      while (!coords_queue.empty()) {
        const unsigned int c_idx = coords_queue.front();
        coords_queue.pop();

        bool parent_set = false;
        vec2 parent;
        float min_parent_dist;
        ivec2 start_coord = get_coord(d, c_idx);

        for (auto c_test_idx : MeshGroups2::get_neighbor_idxes(d, start_coord)) {
          auto& s = pixels_state[c_test_idx];
          if (s.body_id) {
            if (s.parent_found) {
              // this neighbor already has found its closest parent
              vec2 n = s.parent;
              float parent_dist = glm::length(n - vec2{start_coord});
              if (!parent_set || parent_dist < min_parent_dist) {
                min_parent_dist = parent_dist;
                parent = n;
                parent_set = true;
              }
            } else if (!s.seen) {
              s.seen = 1;
              coords_queue.push(c_test_idx);
            }
          }
        }

        pixels_state[c_idx].parent_found = 1;
        pixels_state[c_idx].parent = parent;
      }

      const vec4 camera_origin = to_world * vec4{0., 0., 0., 1.};

      for (int _i = 0; _i < all_pixels_n; _i++) {

        const auto _i_idx = get_idx(d, all_pixels[_i].xy());
        const auto& s = pixels_state[_i_idx];
        if (!s.parent_found) continue;

        // Start and end points in image
        const vec2 img_start = vec2{get_coord(d, _i_idx)}, img_end = s.parent;

        // body array 0-idxed, body IDs 1-idxed
        const int body_id = s.body_id - 1;

        vec4 force, rel_pos;
        if (make_horizontal_force(
                intrinsics,
                to_world,
                camera_origin,
                body_planes[body_id],
                img_start,
                img_end,
                force,
                rel_pos)) {

          forces_out.emplace_back(
              PhysicsForceGenerator_Force{force, rel_pos, (unsigned int)body_id});
        }
      }
    }
  };

  class PullInForceGenerator {
  private:
    ImgComparePhysics2 img_compare;

    Buff<uvec4> all_pixels_compressed, border_pixels, border_pixels_compressed;
    Buff<vec4> border_pixel_neighbors, border_pixel_neighbors_compressed;

    vector<uvec4> all_pixels_cpu;
    vector<uvec4> all_border_pixels;
    vector<vec4> all_border_pixels_neighbors;

    // per-pixel state for graph traversal
    struct PixelState {
      uint8_t is_pixel;
      uint8_t seen;
      uint8_t parent_found;
      vec2 parent;
      uint16_t parent_body_id;
    };

    vector<PixelState> pixels_state;

  public:
    void run_all(
        TpResults& results,
        ThreadPool* tp,
        Tex<TF::RG16UI>& obj_split,     // x channel
        Tex<TF::RG16UI>& obj_simulated, // y channel for IDs
        unsigned int num_worlds,
        Buff<vec4>& obj_simulated_viewports,
        rs2_intrinsics intrinsics,
        mat4 cam_to_world,
        Buff<uvec4>& all_pixels,
        vector<uint>& all_pixels_n,
        vector<vector<Engine::Math::Plane>>& all_physics_body_planes,
        vector<vector<PhysicsForceGenerator_Force>>& all_forces_generated) {

      Engine::Util::Timer t;
      t.record("overlap coords");

      const ivec2 d = obj_split.get_dims();
      const vec4 cam_pos = cam_to_world * vec4{0., 0., 0., 1.};

      t.record("nearby coords");

      vector<uint> all_border_pixels_n;
      img_compare.get_coords_with_nearby_simulated_coords_all(
          obj_simulated,
          all_pixels_n,
          all_pixels,
          obj_simulated_viewports,
          all_border_pixels_n,
          border_pixels,
          border_pixel_neighbors);

      t.record("compress buffers");

      const bool multiple_worlds = all_pixels_n.size() > 1;
      if (multiple_worlds) {
        img_compare.compress_buffer_uvec4(
            d.x * d.y, all_pixels_n, all_pixels, all_pixels_compressed);

        img_compare.compress_buffer_uvec4(
            d.x * d.y, all_border_pixels_n, border_pixels, border_pixels_compressed);

        img_compare.compress_buffer_vec4(
            d.x * d.y,
            all_border_pixels_n,
            border_pixel_neighbors,
            border_pixel_neighbors_compressed);
      }

      t.record("copy to CPU");

      int all_pixels_cpu_size = 0;
      for (auto count : all_pixels_n) all_pixels_cpu_size += count;
      (multiple_worlds ? all_pixels_compressed : all_pixels)
          .cu_copy_to(all_pixels_cpu, all_pixels_cpu_size);

      int all_border_pixels_cpu_size = 0;
      for (auto count : all_border_pixels_n) all_border_pixels_cpu_size += count;
      (multiple_worlds ? border_pixels_compressed : border_pixels)
          .cu_copy_to(all_border_pixels, all_border_pixels_cpu_size);
      (multiple_worlds ? border_pixel_neighbors_compressed : border_pixel_neighbors)
          .cu_copy_to(all_border_pixels_neighbors, all_border_pixels_cpu_size);

      pixels_state.resize(num_worlds * d.x * d.y);
      memset(pixels_state.data(), 0, sizeof(PixelState) * num_worlds * d.x * d.y);

      t.record("clear pending");

      results.get();

      t.record("gen forces");

      int all_pixels_offset = 0, border_pixels_offset = 0;
      for (int i = 0; i < num_worlds; i++) {

        auto& planes = all_physics_body_planes[i];
        auto& force_out = all_forces_generated[i];

        auto* all_pxls = all_pixels_cpu.data() + all_pixels_offset;
        all_pixels_offset += all_pixels_n[i];

        auto* b_pxls = all_border_pixels.data() + border_pixels_offset;
        auto* b_pxls_n = all_border_pixels_neighbors.data() + border_pixels_offset;
        border_pixels_offset += all_border_pixels_n[i];

        auto* pixels_state_local = pixels_state.data() + (i * d.x * d.y);

        auto work_fn = [pixels_state_local,
                        d,
                        intrinsics,
                        cam_to_world,
                        cam_pos,
                        &planes,
                        b_pxls,
                        b_pxls_n,
                        b_pxls_count = all_border_pixels_n[i],
                        all_pxls,
                        all_pxls_count = all_pixels_n[i],
                        &force_out]() {
          gen_forces_out(
              pixels_state_local,
              d,
              intrinsics,
              cam_to_world,
              cam_pos,
              planes,
              b_pxls,
              b_pxls_n,
              b_pxls_count,
              all_pxls,
              all_pxls_count,
              force_out);
        };

        results.threads.emplace_back(tp->enqueue(work_fn));
      }

      // for (auto&& r : results) r.get();

      t.record("end");

      // printf(
      //"pull in force generator. count all: %i, count border: %i\n",
      // all_pixels_cpu_size,
      // all_border_pixels_cpu_size);
      // t.render([](auto s, auto f) { printf("-- -- --%s: %f\n", s.c_str(), f); });
    }

  private:
    static void gen_forces_out(
        PixelState* pixel_state,
        const ivec2 d,
        rs2_intrinsics intrinsics,
        mat4 cam_to_world,
        vec4 cam_pos,
        vector<Engine::Math::Plane>& physics_body_planes,
        uvec4* _border_pixels,
        vec4* _border_pixel_neighbors,
        unsigned int _border_pixels_n,
        uvec4* _all_pixels_ptr,
        unsigned int _all_pixels_n,
        vector<PhysicsForceGenerator_Force>& forces_generated) {

      for (int i = 0; i < _all_pixels_n; i++) {
        pixel_state[get_idx(d, _all_pixels_ptr[i].xy)].is_pixel = 1;
      }

      // unordered_set<unsigned int> seen;
      queue<unsigned int> coords_queue;

      // store {body_id, parent_coord}
      // unordered_map<unsigned int, pair<unsigned int, vec2>> nearest_parents;

      for (int i = 0; i < _border_pixels_n; i++) {
        ivec2 c = _border_pixels[i].xy();
        auto body_id = (unsigned int)_border_pixels[i].z;
        vec2 parent = _border_pixel_neighbors[i];

        const unsigned int c_idx = get_idx(d, c);
        auto& s = pixel_state[c_idx];
        s.seen = 1;
        s.parent = parent;
        s.parent_found = 1;
        s.parent_body_id = body_id;

        for (auto c_test_idx : MeshGroups2::get_neighbor_idxes(d, c)) {
          auto& s = pixel_state[c_test_idx];
          if (!s.is_pixel || s.seen) continue;
          s.seen = 1;
          coords_queue.push(c_test_idx);
        }
      }

      while (!coords_queue.empty()) {
        const unsigned int c_idx = coords_queue.front();
        coords_queue.pop();

        const ivec2 c = get_coord(d, c_idx);

        vec2 nearest_parent;
        uint16_t nearest_parent_body_id;
        bool nearest_parent_set = false;

        for (auto c_test_idx : MeshGroups2::get_neighbor_idxes(d, c)) {
          auto& s = pixel_state[c_test_idx];
          if (!s.is_pixel) continue;
          if (s.parent_found) {
            // if (nearest_parents.count(c_test_idx)) {
            const auto parent = s.parent;
            if (!nearest_parent_set ||
                glm::length(vec2{c} - parent) < glm::length(vec2{c} - nearest_parent)) {
              nearest_parent_set = true;
              nearest_parent = parent;
              nearest_parent_body_id = s.parent_body_id;
            }

          } else if (!s.seen) {
            s.seen = true;
            coords_queue.push(c_test_idx);
          }
        }

        pixel_state[c_idx].parent_found = 1;
        pixel_state[c_idx].parent = nearest_parent;
        pixel_state[c_idx].parent_body_id = nearest_parent_body_id;
      }

      // generate pull forces!!!
      for (int i = 0; i < _all_pixels_n; i++) {

        const auto c_idx = get_idx(d, _all_pixels_ptr[i].xy);

        const auto& s = pixel_state[c_idx];

        if (!s.parent_found) continue;

        // body array 0-idxed
        const unsigned int body_id = s.parent_body_id - 1;

        // Force start and end points in image
        vec2 img_end = vec2{get_coord(d, c_idx)}, img_start = s.parent;

        vec4 force;
        vec4 rel_pos;
        if (make_horizontal_force(
                intrinsics,
                cam_to_world,
                cam_pos,
                physics_body_planes[body_id],
                img_start,
                img_end,
                force,
                rel_pos)) {

          forces_generated.emplace_back(
              PhysicsForceGenerator_Force{force, rel_pos, (unsigned int)body_id});
        }
      }
    }
  };

  class DepthCorrectionForceGenerator {
  private:
    ImgComparePhysics2 img_compare;
    Buff<ivec4> all_pixels, all_pixels_compressed;

    Buff<ivec2> force_gen_invocation;
    Buff<PhysicsForceGenerator_Force> forces_buff;

    Buff<vec4> pbos;

    vector<PhysicsForceGenerator_Force> f_cpu;

  public:
    void run_all(
        TpResults& results,
        ThreadPool* tp,
        rs2_intrinsics intrinsics,
        mat4 cam_to_world,
        Buff<PhysicsForceGenerator_Both>& all_pixels,
        vector<uint>& all_pixels_n,
        Tex<TF::RGBA32F>& obj_neighbors_info,
        vector<vector<vec4>>& physics_body_origins,
        vector<vector<PhysicsForceGenerator_Force>>& forces_generated) {

      vector<vec4> pbos_cpu;
      for (auto& vs : physics_body_origins)
        for (auto v : vs) pbos_cpu.emplace_back(v);

      pbos.set_storage(pbos_cpu.size());
      pbos.cu_copy_from(pbos_cpu.data(), pbos_cpu.size());

      const unsigned int num_bodies_per_group = physics_body_origins[0].size();

      unsigned int num_forces_out;
      img_compare.gen_forces_out(
          all_pixels_n,
          intrinsics,
          cam_to_world,
          all_pixels,
          intrinsics.width * intrinsics.height,
          pbos,
          num_bodies_per_group,
          obj_neighbors_info,
          forces_buff,
          &num_forces_out);

      forces_buff.cu_copy_to(f_cpu, num_forces_out);

      results.get();

      for (int i = 0; i < num_forces_out; i++) {
        forces_generated[f_cpu[i].world_id].emplace_back(f_cpu[i]);
      }
    }
  };

private:
  PushOutForceGenerator push_out_generator;
  PullInForceGenerator pull_in_generator;
  DepthCorrectionForceGenerator depth_correction_generator;

  // Used by PushOut / PullIn force generators.
  static inline bool make_horizontal_force(
      rs2_intrinsics intrinsics,
      glm::mat4 cam_to_world,
      vec4 cam_pos,
      Engine::Math::Plane& force_plane,
      vec2 pixel_start,
      vec2 pixel_end,
      vec4& force,
      vec4& rel_pos) {

    // World coordinates of where force should start & end.
    vec4 force_start = force_plane.intersection_with_line(
             ray_dir(intrinsics, cam_to_world, pixel_start), cam_pos.xyz()),
         force_end = force_plane.intersection_with_line(
             ray_dir(intrinsics, cam_to_world, pixel_end), cam_pos.xyz());

    force = force_end - force_start;
    if (glm::length(vec3{force.xyz()}) < 0.1f) return false;

    // application point in world space, but relative to obj center
    rel_pos = vec4{force_start.xyz() - force_plane.center, 1.};
  }

  // Utils that have been written 100 times by now
  static inline vec4 deproject(rs2_intrinsics& i, vec2 c, float d) {
    return vec4{d * ((c.x - i.ppx) / i.fx), d * ((c.y - i.ppy) / i.fy), d, 1.};
  }

  static inline vec3 ray_dir(rs2_intrinsics& i, mat4& to_world, vec2 cam_pixel) {
    auto c = cam_pixel;
    vec4 p1 = to_world * vec4{(c.x - i.ppx) / i.fx, (c.y - i.ppy) / i.fy, 1.f, 1.};
    vec4 p0 = to_world * vec4{0., 0., 0., 1.};
    return glm::normalize(vec3{(p1 - p0).xyz()});
  }

  static inline unsigned int get_idx(uvec2 d, uvec2 c) { return c.y * d.x + c.x; }
  static inline uvec2 get_coord(uvec2 d, unsigned int i) { return uvec2{i % d.x, i / d.x}; }

  template <typename DataType, typename ContainerType>
  using FillFn = std::function<void(DataType&, ContainerType&)>;

  template <typename DataType, typename ContainerType>
  static void fill_coords_list(
      const unsigned int max_per,
      Buff<DataType>& b,
      vector<unsigned int>& num,
      vector<ContainerType>& out,
      FillFn<DataType, ContainerType>& fn) {

    auto* _all = new DataType[max_per * num.size()];
    b.cu_copy_to(_all, max_per * num.size());

    out.resize(num.size());

    for (int i = 0; i < num.size(); i++) {
      for (int j = 0; j < num[i]; j++) { fn(_all[(i * max_per) + j], out[i]); }
    }
    delete[] _all;
  }

  template <typename T>
  static vector<vector<T>> process_coords_list_2(Buff<T>& buff, vector<unsigned int>& num) {

    int total_els = 0;
    for (auto n : num) { total_els += n; }

    vector<vector<T>> out(num.size());

    auto* d = new T[total_els];

    buff.cu_copy_to(d, total_els);

    int offset = 0;
    for (int i = 0; i < num.size(); i++) {
      auto& v = out[i];
      const auto n = num[i];
      v.resize(n);
      memcpy(v.data(), d + offset, sizeof(T) * n);
      offset += n;
    }

    delete[] d;

    return std::move(out);
  }

  template <typename Data, typename Container>
  static void process_coords_list(
      Data* d, vector<unsigned int>& num, vector<Container>& out, FillFn<Data, Container>& fn) {

    out.resize(num.size());

    int offset = 0;
    for (int i = 0; i < num.size(); i++) {
      for (int j = 0; j < num[i]; j++) { fn(d[offset + j], out[i]); }
      offset += num[i];
    }
  }
};
