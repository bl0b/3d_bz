#pragma once

#include "Models/TestConfig2.hpp"
#include "Models/TestConfig3.hpp"
#include "Models/TestConfig4.hpp"
#include "Models/Hand1.hpp"

class ObjectsDemo : public AbstractApp {
public:
  explicit ObjectsDemo(Engine::Gpu::GraphicsCtx& _w) : AbstractApp(_w), tp(16) {
    w.on_press(GLFW_KEY_ESCAPE, [&] { w.set_should_close(true); });

    cam_pos.config.angle_y_range = {(float)M_PI_2, 3.1};
    cam_pos.config.angle_y = 2.2;
    cam_pos.config.angle_x_range = {0.f, 2.f * (float)M_PI};
    cam_pos.config.angle_x = 3.14;

    n_cam.cam_proj = m_cam.cam_proj = glm::perspectiveFromIntrinsics(
                                          // f
                                          vec2{638.624, 638.624},
                                          // pp
                                          vec2{411.355, 239.944},
                                          // dims
                                          d,
                                          vec2{1., 50000.}) *
                                      glm::scale(vec3{1.f, 1.f, -1.f});

    dbg_img.set_storage(d);

    register_type<TestConfig2>();
    register_type<TestConfig3>();
    register_type<TestConfig4>();
    register_type<Hand1>(true);
  }

  void tick() {

    // update_current_config();

    if (step_worlds)
    ws.step_all_worlds();

    ws.update_tforms_from_physics_worlds();
    m_cam.color_mode = StdCamera::ColorMode::Full;
    GL::Renderer::setPolygonMode(GL::Renderer::PolygonMode::Line);
    n_cam.cam_pos = m_cam.cam_pos = cam_pos.transform_inverse();

    dbg_img_fbo.set_target(StdFramebuffer::StdTargetConfig{}.add(&dbg_img), true);

    if (draw_full) {
      m_cam.color_out = n_cam.color_out = vec4{0.2, 0.7, 0.3, 1.};
      ws.full.draw2(n_cam, m_cam, ws.all_tf, ws.bodies_per_world(), 1, 0);
    }
    if (draw_debug) {
      m_cam.color_out = n_cam.color_out = vec4{0.3, 0.4, 0.5, 1.};
      ws.debug.draw2(n_cam, m_cam, ws.all_tf, ws.bodies_per_world(), 1, 0);
    }

    dbg_img_fbo.clear_target();

    ImGui::Begin("render");
    ImGui::Checkbox("step physics", &step_worlds);
    ImGui::Checkbox("full", &draw_full);
    ImGui::Checkbox("debug", &draw_debug);
    ImGui::End();

    ImGui::Begin("img");
    Engine::ImGuiCtx::Image2(dbg_img);
    ImGui::End();

    ImGui::Begin("cam");
    cam_pos.draw_imgui();
    ImGui::End();

    draw_buttons();
  }

private:
  bool step_worlds = false;
  bool draw_full = true, draw_debug = false;

  struct TypeFns {
    function<void()> delete_existing_config, make_current, update_config, reinit_config;
    function<bool()> draw_config_gui;
  };

  void delete_existing_config() {
    if (current_type.size()) { registered_types[current_type].delete_existing_config(); }
  }

  void update_current_config() {
    if (current_type.size()) { registered_types[current_type].update_config(); }
  }

  template <typename T>
  void register_type(bool make_current = false) {
    TypeFns fns;

    fns.delete_existing_config = [this]() {
      if (current_type_config != nullptr) {
        auto* c = (T*)current_type_config;
        delete c;
        current_type_config = nullptr;
      }
    };

    fns.make_current = [this]() {
      delete_existing_config();
      ws.init<T>();
      auto* c = new T();
      current_type_config = (void*)c;
      vector<T> cfgs{*(T*)current_type_config};
      ws.write_all_data(cfgs, tp);
      current_type = T::WORLD_CONFIG_TYPE_ID();
    };

    fns.draw_config_gui = [this]() {
      auto* c = (T*)current_type_config;
      return T::draw_config_gui(*c);
    };

    fns.reinit_config = [this]() {
      vector<T> cfgs{*(T*)current_type_config};
      ws.write_all_data(cfgs, tp, false);
      ws.update_tforms_from_physics_worlds();
    };

    fns.update_config = [this]() {
      vector<T> cfgs{*(T*)current_type_config};
      ws.write_all_data(cfgs, tp, true);
    };

    if (make_current) { fns.make_current(); }
    registered_types[T::WORLD_CONFIG_TYPE_ID()] = std::move(fns);
  }

  string current_type;
  void* current_type_config = nullptr;
  unordered_map<string, TypeFns> registered_types;

  void draw_buttons() {
    ImGui::Begin("model");
    for (auto& t : registered_types) {
      if (!t.first.compare(current_type)) {
        ImGui::Text(t.first.c_str());
      } else {
        if (ImGui::Button(t.first.c_str())) { t.second.make_current(); }
      }
    }
    ImGui::End();

    ImGui::Begin("config");

    auto& t = registered_types[current_type];
    if (ImGui::Button("reinit physics")) { t.reinit_config(); }
    if (t.draw_config_gui()) {
      t.reinit_config();
      // update_current_config();
    }

    ImGui::End();
  }

  Tex<TF::RGBA8> dbg_img;
  StdFramebuffer dbg_img_fbo;

  MultiplePhysicsWorlds ws;

  Engine::Scene::ArcBallCameraPosition cam_pos;

  ivec2 d{848, 480};
  NurbsShader n_cam;
  StdCamera m_cam;

  ThreadPool tp;
};
