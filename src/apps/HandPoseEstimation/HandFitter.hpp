#pragma once

using namespace Engine::Util::Stl;

#include "Models/TestConfig2.hpp"
#include "Models/TestConfig3.hpp"
#include "Models/TestConfig4.hpp"
#include "Models/Hand1.hpp"

// Runs through various stages of hand fitting. Divides work up into smaller frames, so it's
// progress can be displayed interactively
class HandFitter {
private:
  MultiplePhysicsWorlds config2_worlds, config3_worlds; // , config5_worlds;

  std::default_random_engine _g;

public:
  int fit_count = 0;

  int current_world = 0;

  vector<TestConfig2> test_configs;
  vector<Hand1> test_configs2;
  vector<float> test_configs2_costs;

  Tex<TF::RGBA8> physics_sim_depth_c, physics_sim_body_ids_c;

  ThreadPool* tp;

  void init(ivec2 vp_dim, mat4 cam_pos, rs2_intrinsics cam_intrinsics, ThreadPool* _tp) {

    tp = _tp;

    setup_vps(vp_dim, ivec2{8, 12});
    setup_cams(cam_pos, cam_intrinsics);

    // Setup start configuration data!
    test_configs.clear();
    const int num_thetas = 4;
    const int side_dim = 4;
    const float side_dist = 1500.f;
    for (int k = 0; k < num_thetas; k++)
      for (int i = 0; i < side_dim; i++)
        for (int j = 0; j < side_dim; j++) {
          TestConfig2 c;
          // rotate halfway around..
          const float theta = k * (float)M_PI / num_thetas;
          const float x = (i - (side_dim / 2.f)) * side_dist;
          const float y = (j - (side_dim / 2.f)) * side_dist;
          c.origin = glm::translate(vec3{x, y, 0.f}) * glm::rotate(theta, vec3{0., 0., 1.});
          c.length = 5000.f;
          c.scale = 250.f;
          test_configs.emplace_back(c);
        }

    config2_worlds.init<TestConfig2>();
    config2_worlds.write_all_data(test_configs, *tp);
    config2_worlds.update_tforms_from_physics_worlds();

    config3_worlds.init<Hand1>();
    vector<Hand1> d2(100);
    config3_worlds.write_all_data(d2, *tp);
    config3_worlds.update_tforms_from_physics_worlds();
  }

  void next_stage() {

    const auto tf = config2_worlds.get_tforms_for_world(current_world)[0];

    const auto l1 = test_configs[current_world].length;

    // const int num_tests = 2;
    test_configs2.clear();

    // j: rotate 180 on z axis
    for (int j = 0; j < 2; j++) {

      // rotate transform along local x axis, to determine proper y and z directions.
      // at stage0, the cylinder can just spin freely, so any x rotation is possible
      const auto fingertip_origin =
          tf * glm::rotate((j % 2 == 0) ? (float)M_PI : 0.f, vec3{0., 0., 1.}) *
          glm::translate(vec3{l1 / 2.f, 0., 0.});

      const vec4 origin_pos = fingertip_origin * vec4{0., 0., 0., 1.};
      mat4 best_fingertip_origin = fingertip_origin;
      float best_y_axis_z;
      const int num_rot_tests = 32;
      for (int i = 0; i < num_rot_tests; i++) {
        mat4 test_tform = fingertip_origin *
                          glm::rotate(i * (float)M_PI / (num_rot_tests / 2.f), vec3{1., 0., 0.});
        vec4 y_axis_pos = (test_tform * vec4{0., 1., 0., 1.}) - origin_pos;
        vec4 z_axis_pos = (test_tform * vec4{0., 0., 1., 1.}) - origin_pos;
        float y_axis_z = y_axis_pos.z;
        float z_axis_z = z_axis_pos.z;
        if (i == 0 || (z_axis_z > 0.f && abs(y_axis_z) < abs(best_y_axis_z))) {
          best_y_axis_z = y_axis_z;
          best_fingertip_origin = test_tform;
        }
      }

      const vector<float> scales{0.7, 1., 1.3};

      for (auto s : scales)
        // i: left/right hand
        for (int i = 0; i < 2; i++) {

          Hand1 h;

          h.wrist_origin =
              best_fingertip_origin * glm::translate(vec3{-h.wrist_to_middle_tip(), 0., 0.});

          h.rh = (i % 2) == 0;

          for (int _i = 0; _i < 4; _i++) {
            h.k_scale[_i] *= s;
            for (int _j = 0; _j < 3; _j++) { h.f_length[_i][_j] *= s; }
          }

          h.t_length0 *= s;
          h.t_length1 *= s;
          h.t_length2 *= s;
          h.t_scale *= s;

          h.arm_length *= s;
          h.elbow_scale *= s;

          h.elbow_scale *= s;
          h.k_base_length *= s;

          test_configs2.emplace_back(h);
        }
    }

    config3_worlds.write_all_data(test_configs2, *tp);
    config3_worlds.update_tforms_from_physics_worlds();

    stage = 1;
  }

  // void prev_stage() {
  // config2_worlds.write_all_data(test_configs, *tp);
  // config2_worlds.update_tforms_from_physics_worlds();

  // stage = 0;
  //}

  void gui() {
    auto& w = get_current_world();
    ImGui::SliderInt("current world debug draw", &current_world, 0, w.size() - 1);
  }

  void run_fit(
      Tex<TF::RG16UI>& img,
      rs2_intrinsics minif_intrinsics,
      mat4 to_world,
      ThreadPool& tp,
      OrthoSampler2D world_ortho_stencil_sampler,
      Tex<TF::R16UI>& world_ortho_stencil,
      MeshObj* forces_debug_out = nullptr) {

    Engine::Util::Timer t;
    t.record("draw all worlds");

    // draw_all_worlds();

    auto& w = get_current_world();

    t.record("gen positions");

    vector<vector<mat4>> all_body_tforms = w.get_all_tforms_nested();
    vector<vector<vec4>> all_body_positions =
        v_map<vector<mat4>, vector<vec4>>(all_body_tforms, [](auto& tforms) {
          return v_map<mat4, vec4>(tforms, [](auto& t) { return t * vec4{0., 0., 0., 1.}; });
        });

    vector<vector<PhysicsForceGenerator_Force>> all_forces_out(w.size());

    t.record("gen forces");

    force_generator.run(
        img,
        physics_sim,
        w.size(),
        physics_sim_vps.vp_buff,
        minif_intrinsics,
        to_world,
        all_body_positions,
        all_forces_out,
        &tp);

    t.record("scale & apply");

    // if (stage != 1) {

    w.each_world([&all_forces_out](auto& w, auto i) {
      // Dampen any existing momentum
      w.scale_momentum(0.5f);
      // apply generated impulses
      for (auto& f : all_forces_out[i]) w.apply_impulse(f.body_idx, f.f * 0.25f, f.r_pos);
    });

    //}

    t.record("make debug");

    if (forces_debug_out != nullptr) {
      PhysicsForceGenerator::make_debug_forces(
          *forces_debug_out, current_world, all_body_positions, all_forces_out);
    }

    t.record("step worlds");

    w.step_all_worlds(tp);

    t.record("update tforms from physics worlds");

    w.update_tforms_from_physics_worlds();

    t.record("draw em..");

    draw_all_worlds();

    t.record("per-stage hoo hoo");

    // handle_stage();
    {

      if (stage == 0) {

        if (stage_count == 100) {
          next_stage();
          stage_count = 0;
        }

      } else if (stage == 1) {

        auto costs = gen_costs(
            img, minif_intrinsics, to_world, world_ortho_stencil_sampler, world_ortho_stencil);

        if (test_configs2_costs.size() != costs.size())
          test_configs2_costs.resize(costs.size(), 0.f);
        for (int i = 0; i < costs.size(); i++) { test_configs2_costs[i] += costs[i]; }

        if (stage_count && stage_count == 100) {
          // prune_and_randomize_hand1();

          auto ci = v_map<float, pair<float, int>>(test_configs2_costs, [](float c, int i) {
            return std::pair<float, int>{c, i};
          });

          // lowest cost configs to front of vector!
          std::sort(ci.begin(), ci.end(), [](auto& ci0, auto& ci1) -> bool {
            return ci0.first < ci1.first;
          });

          // pick top one!
          const int idx = ci[0].second;
          Hand1 h = test_configs2[idx];

          test_configs2.clear();
          test_configs2.emplace_back(h);

          vector<vector<mat4>> new_config_tforms;
          auto world_tforms = w.get_tforms_for_world(idx);
          new_config_tforms.emplace_back(std::move(world_tforms));

          w.write_all_data(test_configs2, tp, false, new_config_tforms);
          // test_configs

          // for (auto& _ci : ci) { printf("c: %f, id: %i\n", _ci.first, _ci.second); }

          // for (int i = 0; i < costs.size(); i++) { printf("c: %f\n", costs[i]); }

          // printf("\n\n");

          test_configs2_costs.clear();
        }
      }

      stage_count++;
    }

    if (stage == 1) {

      // printf("stage 1: %i\n", stage1_count);

      /*

      if (stage1_count && stage1_count % 5 == 0 && stage1_count <= 100) {
        auto costs = gen_costs(
            img, minif_intrinsics, to_world, world_ortho_stencil_sampler, world_ortho_stencil);

        auto ci = v_map<float, pair<float, int>>(costs, [](float c, int i) {
          return std::pair<float, int>{c, i};
        });

        // lowest cost configs to front of vector!
        std::sort(ci.begin(), ci.end(), [](auto& ci0, auto& ci1) -> bool {
          return ci0.first < ci1.first;
        });

        if (stage1_count == 100) {

          const int best_world_idx = ci[0].second;
          const auto b = test_configs_3[best_world_idx];

          //test_configs_4.clear();
          test_configs_4_costs.clear();
          vector<vector<mat4>> start_tforms;

          vector<float> w_scale_ms{0.75, 1.0};
          vector<float> t_thetas{0.9, 1.1, 1.3};
          vector<float> hand_scales = {0.5, 0.7};


          test_configs_4_costs.resize(test_configs_4.size(), 0.f);
          //config4_worlds.write_all_data(test_configs_4, tp, false, start_tforms);
          config4_worlds.update_tforms_from_physics_worlds();

          current_world = 0;

          stage = 2;
        } else {

          const int num_to_keep = stage1_count > 50 ? 4 : 8;
          const int new_guesses_per = stage1_count > 50 ? 4 : 10;
          const float std_dev_multiplier = stage1_count > 50 ? 0.2 : 1.0;

          vector<TestConfig3> new_configs;

          vector<vector<mat4>> new_config_tforms;

          for (int i = 0; i < num_to_keep; i++) {
            for (int j = 0; j < new_guesses_per; j++) {

              const int orig_idx = ci[i].second;
              auto c = test_configs_3[orig_idx];

              if (j) { TestConfig3::RANDOMIZE(c, j - 1, _g, std_dev_multiplier); }

              new_configs.emplace_back(c);

              auto world_tforms = w.get_tforms_for_world(orig_idx);
              new_config_tforms.emplace_back(std::move(world_tforms));
            }
          }

          test_configs_3.clear();
          for (auto c : new_configs) test_configs_3.emplace_back(c);

          w.write_all_data(test_configs_3, tp, false, new_config_tforms);
        }
      }

      */

      // stage1_count++;

    } else if (stage == 0) {
      // if (stage0_count == 100) { next_stage(); }
      // stage0_count++;
    } else if (stage == 2) {

      /*

      // always read costs for stage 2
      auto costs = gen_costs(
          img, minif_intrinsics, to_world, world_ortho_stencil_sampler, world_ortho_stencil);

      const int slowdown = 2;
      const int frames_per_new_guesses = 272;
      const int rounds_per_new_guesses = frames_per_new_guesses * slowdown;
      const int num_guesses = 50;
      const int total_num_rounds = rounds_per_new_guesses * num_guesses;

      assert(costs.size() == test_configs_4.size());
      assert(costs.size() == test_configs_4_costs.size());
      auto& sum_costs = test_configs_4_costs;
      n_iter(costs.size(), [&sum_costs, &costs](int i) { sum_costs[i] += costs[i]; });

      if (false) {

      //if (stage2_count && stage2_count % rounds_per_new_guesses == 0 &&
          //stage2_count <= total_num_rounds) {
        auto ci = v_map<float, pair<float, int>>(sum_costs, [](float c, int i) {
          return std::pair<float, int>{c, i};
        });

        // lowest cost configs to front of vector!
        std::sort(ci.begin(), ci.end(), [](auto& ci0, auto& ci1) -> bool {
          return ci0.first < ci1.first;
        });

        if (stage2_count == total_num_rounds) {

          test_configs_5.clear();
          test_configs_5_tforms.clear();

          test_configs_5.emplace_back(test_configs_4[ci[0].second]);
          test_configs_5_tforms.emplace_back(w.get_tforms_for_world(ci[0].second));

          w.write_all_data(test_configs_5, tp, false, test_configs_5_tforms);

          current_world = 0;

          stage = 3;

          printf("done!\n");

        } else {

          printf(
              "Round : %i -- Best cost: %f\n",
              (stage2_count / rounds_per_new_guesses),
              ci[0].first);

          // test_configs_4[ci[0].second].print_debug();

          const int num_to_keep = 8;
          const int new_guesses_per = 10;

          vector<TestConfig4> new_configs;

          vector<vector<mat4>> new_config_tforms;

          auto vars = TestConfig4::GET_RANDOMIZE_VARS();

          for (int i = 0; i < num_to_keep; i++) {
            for (int j = 0; j < new_guesses_per; j++) {

              const int orig_idx = ci[i].second;
              auto c = test_configs_4[orig_idx];

              if (j) { TestConfig4::RANDOMIZE(c, vars, _g); }

              new_configs.emplace_back(c);

              auto world_tforms = w.get_tforms_for_world(orig_idx);
              new_config_tforms.emplace_back(std::move(world_tforms));
            }
          }

          test_configs_4.clear();
          for (auto c : new_configs) test_configs_4.emplace_back(c);

          test_configs_4_costs.clear();
          test_configs_4_costs.resize(test_configs_4.size(), 0.f);

          w.write_all_data(test_configs_4, tp, false, new_config_tforms);
        }
      }

      */

      // stage2_count++;
    } else if (stage == 3) {
      // stage3_count++;
    }

    t.record("end");
    // printf("1 frame for hand fitting!\n");
    // t.render("-- -- -- %s: %f\n");
  }

public:
  int stage = 0;
  int stage_count = 0;

  vector<float> gen_costs(
      Tex<TF::RG16UI>& img_split,
      rs2_intrinsics intrinsics,
      mat4 to_world,
      OrthoSampler2D world_ortho_stencil_sampler,
      Tex<TF::R16UI>& world_ortho_stencil) {

    auto& w = get_current_world();

    // get costs for each physics world against the current frame
    vector<float> frame_costs(w.size());
    unsigned int mismatch_scale = 500;
    img_compare.gen_costs2(
        img_split,
        physics_sim,
        physics_sim_vps.vp_buff,
        w.size(),
        frame_costs,
        mismatch_scale,
        intrinsics,
        to_world,
        world_ortho_stencil_sampler,
        world_ortho_stencil);

    return std::move(frame_costs);
  }

  void draw_all_worlds() {
    auto& w = get_current_world();
    GL::Renderer::setPolygonMode(GL::Renderer::PolygonMode::Fill);
    physics_sim_fbo.set_target(StdFramebuffer::StdTargetConfig().add(&physics_sim), true);

    w.full.draw2(n_cam, m_cam, w.all_tf, w.bodies_per_world(), w.size(), 0, &physics_sim_vps, 0);

    // w.full.draw(n_cam, m_cam, w.size(), 0, &physics_sim_vps, 0);
    physics_sim_fbo.clear_target();
    GL::defaultFramebuffer.bind();

    // DEBUG!
    // map_depth.run(physics_sim_depth, physics_sim_depth_c);
    // map_depth.run_meshid(physics_sim_body_ids, physics_sim_body_ids_c);
  }

  void draw_current_world_depth(StdFramebuffer& fbo, Tex<TF::RG16UI>& d) {
    auto& w = get_current_world();
    GL::Renderer::setPolygonMode(GL::Renderer::PolygonMode::Fill);
    fbo.set_target(StdFramebuffer::StdTargetConfig().add(&d), true);
    w.full.draw2(n_cam, m_cam, w.all_tf, w.bodies_per_world(), 1, current_world);
    fbo.clear_target();
    GL::defaultFramebuffer.bind();
  }

  void draw_current(NurbsShader& n_dcam, StdCamera& m_dcam, bool full = true) {
    auto& w = get_current_world();
    (full ? w.full : w.debug)
        .draw2(n_dcam, m_dcam, w.all_tf, w.bodies_per_world(), 1, current_world);
  }

  // Buff<mat4> all_tfs;

  MultiplePhysicsWorlds& get_current_world() {
    if (stage == 0) {
      return config2_worlds;
    } else if (stage == 1) {
      return config3_worlds;
    } else if (stage == 2) {
      return config3_worlds;
    }
  }

  PhysicsForceGenerator force_generator;

private:
  ImgCompare img_compare;
  MapDepth map_depth;

  NurbsShader n_cam;
  StdCamera m_cam;

  Tex<TF::RG16UI> physics_sim;
  StdFramebufferSubViewports physics_sim_vps;
  StdFramebuffer physics_sim_fbo;

  void setup_vps(ivec2 vp_dim, ivec2 render_grid) {
    physics_sim.set_storage(render_grid * vp_dim);
    physics_sim_vps.init(vp_dim, physics_sim.get_dims());
  }

  void setup_cams(mat4 p, rs2_intrinsics i) {
    n_cam.cam_proj = m_cam.cam_proj =
        glm::perspectiveFromIntrinsics(
            vec2{i.fx, i.fy}, vec2{i.ppx, i.ppy}, vec2{i.width, i.height}, vec2{1., 50000.}) *
        glm::scale(vec3{1., 1., -1});
    n_cam.cam_pos = m_cam.cam_pos = p;
  }
};
