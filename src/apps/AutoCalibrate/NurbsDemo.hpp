#pragma once

#include <engine/Engine.h>

#include <compute/ComputeLib.hpp>
#include <render/render.hpp>

#include <Components/Physics.hpp>

#include <tinynurbs/tinynurbs.h>

#include <thread>
#include <chrono>

#include <random>
#include <queue>

using namespace ComputeLib;
using namespace Engine::Gpu;
using namespace PhysicsConversion;
using namespace std;

class NurbsDemo : public AbstractApp {
public:
  Engine::Scene::ArcBallCameraPosition cam_pos;

  Tex<TF::RGBA8> dbg_out, depth_out_c;
  Tex<TF::RG16UI> depth_out;
//  const ivec2 dbg_out_dims{900, 500};
  const ivec2 dbg_out_dims{848, 480};

  NurbsObj sphere, cylinder, waves, wav;

  MeshObj mesh_obj;

  NurbsShader cam;
  StdCamera cp_cam;

  StdFramebuffer std_fbo;
  StdFramebufferSubViewports sub_viewports;

  MapDepth map_depth;

  struct CylinderWithSpheresCfg {
    vec3 p0{0., 0., 0.}, p1{0., 1., 0.};
    vec2 r0{1., 1.}, r1{1., 1.};
  };

  vector<CylinderWithSpheresCfg> cylinder_with_spheres;

  explicit NurbsDemo(Engine::Gpu::GraphicsCtx& _w) : AbstractApp(_w) {

    {
      // basic mesh object!
      auto& m = mesh_obj.get_data();
      m.set_tform(glm::translate(vec3{10., 6., -1.}));
      m.vtx_set_size = 4;
      m.vtx_set_count = 1;
      vector<vec4> vtxs = {
          {-1., -1., 0., 1.},
          {1., -1., 0., 1.},
          {-1., 1., 0., 1.},
          {1., 1., 0., 1.},
      };
      m.vtxes.set_data(vtxs);

      auto& mv = mesh_obj.get_view();
      vector<unsigned int> idxs = {0, 1, 2, 1, 2, 3};
      mv.idxes_count = idxs.size();
      mv.idxes.set_data(idxs);
    }

    cylinder_with_spheres = {{vec3{0., -2., 0.}, vec3{0., 2., 0.}, vec2{1.5, 1.5}, vec2{1.5, 1.3}},
                             {vec3{3., 3., 0.}, vec3{3., 6., 0.}, vec2{0.5, .5}, vec2{.5, .5}},
                             {vec3{-1.5, 1., 0.}, vec3{-1., 6., 0.}, vec2{0.5, .5}, vec2{.5, .5}}};

    NurbsPrimitives::Cylinder::init(cylinder, {5, 16});
    NurbsPrimitives::HalfSphere::init(sphere, {23, 19});

    {
      wav.s.degree_u = 3;
      wav.s.degree_v = 1;
      wav.s.knots_u = {0, 0, 0, 0, 0.5, 1, 1, 1, 1};
      wav.s.knots_v = {0, 0, 1, 1};

      const ivec2 cp_dims = {5, 2};
      const ivec2 d{16, 16};
      wav.gen_dims(d, cp_dims);

      wav.data.set_tform(glm::translate(vec3{5., 0., 0.}));
      // clang-format off
      auto cps = vector<vec4>{
          vec4{-2, -2, 0, 1}, vec4{-1, -2, 0, 2}, vec4{0, -2, 2, 2}, vec4{1, -2, 0, 2}, vec4{2, -2, 0, 1},
          vec4{-2,  2, 0, 1}, vec4{-1,  2, 0, 1}, vec4{0,  2, 0, 1}, vec4{1,  2, 0, 1}, vec4{2,  2, 0, 1}};
      // clang-format on
      wav.set_cp_sets(cps);
    }
    {
      waves.s.degree_u = 4;
      waves.s.degree_v = 4;
      waves.s.knots_u = {0, 0, 0, 0, 0, 0.5, 1, 1, 1, 1, 1};
      waves.s.knots_v = {0, 0, 0, 0, 0, 0.5, 1, 1, 1, 1, 1};

      const ivec2 cp_dims = {6, 6};
      const ivec2 d{32, 32};
      waves.gen_dims(d, cp_dims);

      waves.data.set_tform(glm::translate(vec3{-5., -2., 0.}));
      // clang-format off
      auto cps = vector<vec4>{
          vec4{-2.5, -2.5, 0, 1}, vec4{-1.5, -2.5, 0, 1}, vec4{-.5, -2.5, 0, 1}, vec4{.5, -2.5, 0, 1}, vec4{1.5, -2.5, 0, 1}, vec4{2.5, -2.5, 0, 1},
          vec4{-2.5, -1.5, 0, 1}, vec4{-1.5, -1.5, 2, 1}, vec4{-.5, -1.5, 2, 1}, vec4{.5, -1.5, 2, 1}, vec4{1.5, -1.5, 2, 1}, vec4{2.5, -1.5, 0, 1},
          vec4{-2.5, -.5, 0, 1},  vec4{-1.5, -.5, 2, 1},  vec4{-.5, -.5, -4, 1},  vec4{.5, -.5, -4, 1},  vec4{1.5, -.5, 2, 1},  vec4{2.5, -.5, 0, 1},
          vec4{-2.5, .5, 0, 1},   vec4{-1.5, .5, 2, 1},   vec4{-.5, .5, -4, 1},   vec4{.5, .5, -4, 1},   vec4{1.5, .5, 2, 1},   vec4{2.5, .5, 0, 1},
          vec4{-2.5, 1.5, 0, 1},  vec4{-1.5, 1.5, 2, 1},  vec4{-.5, 1.5, 2, 1},  vec4{.5, 1.5, 2, 1},  vec4{1.5, 1.5, 2, 1},  vec4{2.5, 1.5, 0, 1},
          vec4{-2.5, 2.5, 0, 1},  vec4{-1.5, 2.5, 0, 1},  vec4{-.5, 2.5, 0, 1},  vec4{.5, 2.5, 0, 1},  vec4{1.5, 2.5, 0, 1},  vec4{2.5, 2.5, 0, 1},
          };
      // clang-format on
      waves.set_cp_sets(cps);
    }

    cam_pos.config.distance = 12.f;
    cam_pos.config.distance_range = vec2{1., 50};
    cam_pos.config.angle_y = -cam_pos.config.angle_y;
    cam_pos.config.angle_y_range = -cam_pos.config.angle_y_range;


    // flip handed-ness of camera projection. but why?

    cam.cam_proj =
        glm::perspectiveFromIntrinsics(
            vec2{638.6247, 638.6247}, vec2{411.355, 239.944}, vec2{848, 480}, vec2{1., 50000.}) *
        glm::scale(vec3{1., 1., -1});


    //cam.cam_proj = glm::ortho({-200, -200, 1}, {200, 200, 50000});


    //cam.cam_proj = glm::perspective(45.f, (1.f * dbg_out_dims.x) / dbg_out_dims.y, .01f, 500.f);

    cam.color_out = vec4{0.3, 0.8, 0.5, 1.};
    cp_cam.cam_proj = cam.cam_proj;
    //    cp_cam.color_out = vec4{0.4, 0.4, 0.5, 1.};

    sub_viewports.init(dbg_out_dims / 2, dbg_out_dims);

    w.on_press(GLFW_KEY_ESCAPE, [&] { w.set_should_close(true); });
  }

  void tick() override {

    GL::Renderer::enable(GL::Renderer::Feature::DepthTest);

    // make_sphere_control_points();
    make_cylinder_sphere_control_points(cylinder_with_spheres);

    dbg_out.set_storage(dbg_out_dims);
    depth_out.set_storage(dbg_out_dims);

    cam.cam_pos = cam_pos.transform_inverse();
    cp_cam.cam_pos = cam.cam_pos;

    std_fbo.set_target(StdFramebuffer::StdTargetConfig().add(&dbg_out));

    GL::Renderer::setClearColor(0x000000_rgbf);
    GL::Renderer::setPolygonMode(GL::Renderer::PolygonMode::Line);
    std_fbo.fbo.clear(GL::FramebufferClear::Color | GL::FramebufferClear::Depth);

    // vec4 pt_cam_pos = glm::translate(vec3{0., 0., -400}) * vec4{0., 0., 0., 1.};
    // vec4 pt_cam_pos = cam.cam_pos * vec4{0., 0., 0., 1.};
    // vec4 origin_ndc = cam.cam_proj * pt_cam_pos;
    // vec4 origin_ndc_flip = cam.cam_proj * glm::scale(vec3{1., 1., -1.}) * pt_cam_pos;

    cp_cam.color_out = {.2, 0.2, 0.24, 1.};
    cp_cam.draw(wav.cp_mesh);
    cp_cam.draw(waves.cp_mesh);
    auto cylinder_cfg = DrawConfig().num(3).both({0, 1}),
         sphere_cfg = DrawConfig().num(6).both({0, 1});

    cp_cam.draw(cylinder.cp_mesh, cylinder_cfg);
    cp_cam.draw(sphere.cp_mesh, sphere_cfg);

    cam.draw(cylinder, cylinder_cfg);
    cam.draw(sphere, sphere_cfg);
    cam.draw(waves);
    cam.draw(wav);

    cp_cam.color_out = {1., 0.4, 0., 1.};
    cp_cam.draw(mesh_obj);

    std_fbo.clear_target();

    std_fbo.set_target(StdFramebuffer::StdTargetConfig().add(&depth_out));
    GL::Renderer::setPolygonMode(GL::Renderer::PolygonMode::Fill);
    std_fbo.fbo.clear(GL::FramebufferClear::Color | GL::FramebufferClear::Depth);

    cam.draw(cylinder, DrawConfig().num(12).both({0, 1}).viewports(sub_viewports, {0, 3}));
    cam.draw(sphere, DrawConfig().num(24).both({0, 1}).viewports(sub_viewports, {0, 6}));
    cp_cam.draw(mesh_obj, DrawConfig().num(4).both({0, 1}).viewports(sub_viewports, {0, 1}));

    std_fbo.clear_target();

    GL::defaultFramebuffer.bind();

    map_depth.run_split(depth_out, depth_out_c);

    ImGui::Begin("img");
    Engine::ImGuiCtx::Image2(dbg_out);
    Engine::ImGuiCtx::Image2(depth_out_c);
    ImGui::End();

    ImGui::Begin("controls");
    // raImGui::Slider
    auto& c0 = cylinder_with_spheres[0];
    ImGui::SliderFloat2("rad0", &c0.r0.x, 0., 3.5);
    ImGui::SliderFloat2("rad1", &c0.r1.x, 0., 3.5);
    ImGui::SliderFloat3("pos0", &c0.p0.x, -5., 5.);
    ImGui::SliderFloat3("pos1", &c0.p1.x, -5., 5.);
    ImGui::End();

    cam_pos.draw_imgui();
  }

  static vec3 get_length_and_thetas(vec3 p0, vec3 p1) {
    vec3 diff = p1 - p0;
    const float l = glm::length(diff);
    const float z_theta = atan2(diff.y, diff.x);
    const float y_theta = atan2(-diff.z, glm::length(vec2{diff.xy()}));
    return vec3{l, y_theta, z_theta};
  }

  void make_cylinder_sphere_control_points(vector<CylinderWithSpheresCfg>& cfgs) {

    vector<vec4> cylinder_cp_sets, sphere_cp_sets;
    vector<mat4> cylinder_tforms, sphere_tforms;

    for (auto c : cfgs) {

      NurbsPrimitives::Cylinder::gen_data(
          cylinder_cp_sets, cylinder_tforms, c.p0, c.p1, c.r0, c.r1);

      vec3 _lyz = get_length_and_thetas(c.p0, c.p1);
      float y_theta = _lyz.y, z_theta = _lyz.z;

      mat4 rot = glm::rotate(z_theta, vec3{0., 0., 1.}) * glm::rotate(y_theta, vec3{0., 1., 0.});
      mat4 tf0 = glm::translate(vec3{c.p0}) * rot * glm::scale(vec3{-1., 1., 1});
      mat4 tf1 = glm::translate(vec3{c.p1}) * rot;

      sphere_tforms.emplace_back(tf0);
      sphere_tforms.emplace_back(tf1);

      NurbsPrimitives::HalfSphere::gen_cp_set(
          sphere_cp_sets, vec3{c.r0.xy(), (c.r0.x + c.r0.y) / 2.f});
      NurbsPrimitives::HalfSphere::gen_cp_set(
          sphere_cp_sets, vec3{c.r1.xy(), (c.r1.x + c.r1.y) / 2.f});
    }

    cylinder.data.set_tforms(cylinder_tforms);
    cylinder.set_cp_sets(cylinder_cp_sets);

    sphere.data.set_tforms(sphere_tforms);
    sphere.set_cp_sets(sphere_cp_sets);
  }
};
