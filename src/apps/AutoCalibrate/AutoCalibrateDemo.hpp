#pragma once

#include <engine/Engine.h>

#include <compute/ComputeLib.hpp>

#include <Components/Midi.hpp>
#include <Components/RsCtxLive.hpp>

#include <Components/BoundingBoxHeightMaps.hpp>
#include <Components/CalibrationState.hpp>
#include <Components/HandPositionsManager.hpp>
#include <Components/NotesManager.hpp>

#include <Components/ActivePlaneHeightMap.hpp>

#include <Components/Plottable.hpp>

#include <atomic>
#include <queue>

#include "Constants.hpp"

using namespace ComputeLib;

class AutoCalibrateDemo : public AbstractApp {
public:
  CalibrationState cam_calibration;

  // this is essentially the order of the processing pipeline..
  MapDepth map_depth;
  FilterPoints filter_points;
  ActivePlaneHeightMap active_p_height_map;
  ComputeLib::MeshGroups mesh_groups;
  BoundingBoxHeightMaps active_bb_height_maps;
  HandPositionsManager hand_positions_manager;
  NotesManager notes_manager;
  MakeSimplifiedStencilMesh make_simplified_stencil_mesh;

  ComputeLib::GenTriangles triangle_generator;
  ComputeLib::FitRegionsToPlanes fit_regions_to_planes;

  // classes for processing
  DeprojectDepth deproject_depth;

  // GPU texture blocks
  Engine::Gpu::Tex<Engine::Gpu::TF::R16UI> depth_img;
  Engine::Gpu::Tex<Engine::Gpu::TF::RGBA8> depth_img_rgba;
  Engine::Gpu::Tex<Engine::Gpu::TF::RGBA32F> vtxes;
  Engine::Gpu::Tex<Engine::Gpu::TF::RGBA32F> vtxes_filtered;
  Engine::Gpu::Tex<Engine::Gpu::TF::RGBA32F> vtxes_filtered_temp;

  // current generated out plane, GPU buffer storing grid of generated planes of best fit
  Engine::Math::Plane current_plane;
  Engine::Gpu::Buff<FitRegionsToPlanes::PlaneFit> planes;

  // 2nd framebuffer with 2 outputs: color & fragment world positions
  Magnum::GL::Framebuffer full_fbo{GL::defaultFramebuffer.viewport()};
  Engine::Gpu::Tex<Engine::Gpu::TF::RGBA8> full_fbo_rgba;
  Engine::Gpu::Tex<Engine::Gpu::TF::RGBA32F> full_fbo_positions;
  GL::Renderbuffer full_fbo_depth_stencil;

  // Meshes generated from incoming depth images
  DepthCameraMesh<Engine::Scene::MatrixPosition> generated_depth_camera_mesh;
  DepthCameraMesh<Engine::Scene::MatrixPosition> generated_filtered_mesh;

  // Camera for rendering meshes to screen
  ColoredMeshCamera<Engine::Scene::ArcBallCameraPosition> colored_mesh_cam;

  // Realsense context
  RsCtxLive rs;
  bool rgba_cam_intrin_set = false;
  rs2_intrinsics depth_cam_intrin, rgba_cam_intrin;
  rs2_extrinsics depth_to_rgba_extrin;
  rs2::frame depth_frame, rgba_frame;

  // timer! TODO: put this class in Engine::Util, just use CPU clock instead of CUDA timers so
  // it can work everywhere
  Engine::Util::Timer t;

  int minif = 1;
  int high_minif = 4;
  float pct_measured_thresh = 0.2;
  float error_thresh = 35.;
  const float DEFAULT_PLANE_THRESH = -75.;
  float plane_thresh;
  const float top_thresh = -1500.;

  const int height_map_resolution = 192;
  const int bounding_box_height_map_resolution = 512;

  float min_group_size = 0.01; // a group must take up 1% of area to be considered a valid group.
  const int max_num_groups = 8;

  ivec2 current_mouse_pos;
  bool current_mouse_live = false, last_mouse_live = false;
  bool current_mouse_click = false, last_mouse_click = false;

  const int MIN_COLOR_FRAMES_BEFORE_COPY = 15;
  int current_color_frame_number = 0;
  bool copy_next_color_frame = false;

  vector<u8vec4> black_image{RGBA_DIMS.x * RGBA_DIMS.y, u8vec4{0, 0, 0, 255}};

  glm::ivec2 render_dims;

  function<void(void)> mouseup_callback = [] {};

  int frame_number = 0;
  bool showing_camera_troubleshoot = false;

  bool about_register_modal_open = false;

  Settings app_settings;

  ~AutoCalibrateDemo() { save_settings(); }

  AutoCalibrateDemo(Engine::Gpu::GraphicsCtx& _w) : AbstractApp(_w) {

    // register 'sticky' settings:
    {
      // hand detection parameters
      register_setting_f(
          "note_on_threshold",
          &hand_positions_manager.note_on_threshold,
          hand_positions_manager.DEFAULT_NOTE_ON_THRESHOLD);
      register_setting_f(
          "note_off_threshold",
          &hand_positions_manager.note_off_threshold,
          hand_positions_manager.DEFAULT_NOTE_OFF_THRESHOLD);
      register_setting_i(
          "detect_note_strength", (int*)&hand_positions_manager.note_velocities_on, 1);
      register_setting_f(
          "min_note_velocity",
          &hand_positions_manager.min_velocity,
          hand_positions_manager.DEFAULT_MIN_VELOCITY);
      register_setting_f("plane_threshold", &plane_thresh, DEFAULT_PLANE_THRESH);

      // default camera position
      register_setting_f("arcball.pos.angle_y", &colored_mesh_cam.position.config.angle_y, -2.0);
      register_setting_f("arcball.pos.angle_x", &colored_mesh_cam.position.config.angle_x, 0);
      register_setting_f("arcball.pos.distance", &colored_mesh_cam.position.config.distance, 10000);
      register_setting_f("arcball.prj.near", &colored_mesh_cam.config.clip_near, 50);
      register_setting_f("arcball.prj.far", &colored_mesh_cam.config.clip_far, 50000.);
      register_setting_i("arcball.prj.mirror", (int*)&colored_mesh_cam.config.mirror, 1);
    }

    // and load existing settings from file, if it exists, defaulting to provided default values
    load_settings();

    render_dims = ivec2{1080, 720};

    // configure renderer features..
    GL::Renderer::setDepthFunction(GL::Renderer::DepthFunction::Less);
    Magnum::GL::Renderer::enable(Magnum::GL::Renderer::Feature::Blending);
    Magnum::GL::Renderer::setBlendFunction(
        Magnum::GL::Renderer::BlendFunction::SourceAlpha,
        Magnum::GL::Renderer::BlendFunction::OneMinusSourceAlpha);

    active_bb_height_maps.resolution = {
        bounding_box_height_map_resolution, bounding_box_height_map_resolution};

    setup_fbo();

    {
      auto colors = vector<vec4>{(const uint64_t)(DEPTH_DIMS.x * DEPTH_DIMS.y), {1, 1, 0, 1}};
      generated_filtered_mesh.colors.set_data(colors);
    }

    // Declare desired streams
    // clang-format off
    rs.set_desired_streams({
        {rs2_stream::RS2_STREAM_DEPTH, rs2_format::RS2_FORMAT_Z16, DEPTH_DIMS, DEPTH_FPS},
        {rs2_stream::RS2_STREAM_COLOR, rs2_format::RS2_FORMAT_RGBA8, RGBA_DIMS, RGBA_FPS} });
    // clang-format on

    // Declare desired configuration values
    rs.set_config_callback([&](RsCtxLive& rs) {
      rs.set_depth_units(RS_DEPTH_UNITS);
      string prod_id = string(rs.dev.get_info(RS2_CAMERA_INFO_NAME));
      bool is_d415 = prod_id.find("415") != string::npos;
      rs.set_disparity_shift(is_d415 ? DISPARITY_SHIFT_D415 : DISPARITY_SHIFT_D435);
    });

    rs.init();

    hand_positions_manager.max_num_groups = max_num_groups;

    w.on_press(GLFW_KEY_ESCAPE, [&] { w.set_should_close(true); });

    ImGui::GetStyle().FrameRounding = 4.f;
    ImGui::GetStyle().GrabRounding = 4.f;
  }

  void tick() override {

    frame_number++;

    // don't do anything while waiting for the streams to start
    if (!rs.is_active(RS2_STREAM_DEPTH) ||
        (!cam_calibration.calibrated && !rs.is_active(RS2_STREAM_COLOR))) {
      this_thread::sleep_for(chrono::milliseconds(10));

      show_performance_window = false;
      show_debug_window = false;

      ImVec2 p_size = {250, 46};
      ImGui::SetNextWindowPos(ImVec2{(w.w - p_size.x) / 2.f, (w.h - p_size.y) / 2.f});
      ImGui::SetNextWindowSize(p_size);
      ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(16, 16));
      ImGui::Begin(
          "##no_camera",
          NULL,
          ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoScrollbar);

      const int num_dots = 3;
      const int per_frame_count = 40;

      ImVec2 cursor_pos;
      for (int i = 0; i < num_dots + 1; i++) {
        const int rem = frame_number % (per_frame_count * 4);

        if (rem >= (i * per_frame_count) && rem < ((i + 1) * per_frame_count)) {

          string m = "Looking for depth camera";
          for (int j = 0; j < i; j++) { m += "."; }

          cursor_pos = ImGui::GetCursorPos();
          ImGui::Text(m.c_str());
        }
      }

      ImGui::SameLine();

      ImVec2 help_window_size{580, 92};

      ImGui::SetCursorPos({cursor_pos.x + 200, cursor_pos.y - 2});
      ImGui::SetNextWindowSize(help_window_size);
      if (ImGui::Button(ICON_FA_QUESTION)) { ImGui::OpenPopup("mdl"); }

      ImGui::SetNextWindowPos(ImVec2{(w.w - help_window_size.x) / 2.f, (w.h + p_size.y) / 2.f});
      if (ImGui::BeginPopup("mdl")) {
        ImGui::Text("Trouble getting 3d-beats to connect to your camera?");
        ImGui::Text("Check out the troubleshooting section of the");
        ImGui::SameLine();
        if (ImGui::Button("user manual")) {
          const string cmd = "start https://www.3d-beats.com/help";
          std::system(cmd.c_str());
        }

        ImGui::EndPopup();
      }

      ImGui::End();

      ImGui::PopStyleVar();

      return;
    }

    show_performance_window = true;

#ifdef DEBUG_BUILD
    show_debug_window = true;
#endif

    // reset per frame variables that are set to true by draw_gui function
    last_mouse_live = current_mouse_live;
    current_mouse_live = false;
    last_mouse_click = current_mouse_click;
    current_mouse_click = false;

    // draw all imgui stuff first.. they affect variables in play later, etc.
    draw_gui();

    // Start new timer sequence
    t.clear();

    // when to load intrinsics? can probably do better than every frame
    depth_cam_intrin = rs.intrinsics(RS2_STREAM_DEPTH);

    t.record("wait for camera frame");

    // Copy incoming frame to CUDA/OpenGL texture
    try {
      depth_frame = rs.try_wait(RS2_STREAM_DEPTH, RS2_STREAM_DEPTH, 250);
    } catch (exception e) {

      if (!rs.is_active(RS2_STREAM_DEPTH)) {
        // generally this means the camera was unplugged. just reset everything in this case
        cam_calibration.reset();
        notes_manager.clear();
        return;
      }

      // Timeout! device unplugged??
      printf("error!\n");
      printf(e.what());
      cout << "not arrived in time!!" << endl;
      throw e;
    }

    if (rs.fetch_frame_if_available(RS2_STREAM_COLOR, RS2_FORMAT_RGBA8, &rgba_frame)) {

      rgba_cam_intrin = rs.intrinsics(RS2_STREAM_COLOR);
      depth_to_rgba_extrin = rs.get_extrinsics(RS2_STREAM_DEPTH, RS2_STREAM_COLOR);
      rgba_cam_intrin_set = true;

      current_color_frame_number++;

      if (copy_next_color_frame) {

        if (current_color_frame_number > 5) {
          cam_calibration.active_plane.mesh.tex.cu_copy_from(rgba_frame.get_data(), RGBA_DIMS);
          // d435 has much narrower fov than it's depth image. clamping can look bad, but not much
          // else can be done
          cam_calibration.active_plane.mesh.tex.get().setWrapping(
              Magnum::GL::SamplerWrapping::ClampToEdge);
        }

        if (current_color_frame_number > MIN_COLOR_FRAMES_BEFORE_COPY) {
          copy_next_color_frame = false;
          current_color_frame_number = 0;

          rs.set_desired_streams(
              {{rs2_stream::RS2_STREAM_DEPTH, rs2_format::RS2_FORMAT_Z16, DEPTH_DIMS, DEPTH_FPS}});
        }
      }
    }

    // Copy incoming depth image to GPU texture buffer
    t.record("copy depth img to device");
    depth_img.cu_copy_from(depth_frame.get_data(), DEPTH_DIMS);

// make RGBA image of incoming depth image for debugging
#ifdef DEBUG_BUILD
    t.record("map depth image to RGBA");
    map_depth.run(depth_img, depth_img_rgba);
#endif

    // if camera is calibrated..
    if (cam_calibration.calibrated) {

      // Generate minified positions from depth image, with points transformed to plane coordinate
      // system
      t.record("deproject points");
      deproject_depth.run(
          depth_img,
          // vtxes_filtered_temp & vtxes_filtered act as swap buffers for upcoming filtering
          // operations. just so happens that there are 3 (odd # of) filter operations, so start
          // with 'temp' buffer :)
          vtxes_filtered_temp,
          depth_cam_intrin,
          high_minif,
          cam_calibration.plane.to_plane_coords);

      // remove points that are not included in active plane stencil
      t.record("filter points by plane stencil");
      filter_points.by_plane_stencil(
          vtxes_filtered_temp,
          vtxes_filtered,
          cam_calibration.active_plane.stencil,
          cam_calibration.active_plane.stencil_sampler);

      // remove points that are below threshold of being 'above the plane', as well as points that
      // are too far above top of plane
      t.record("filter stenciled mesh by z value");
      filter_points.by_z_value(
          vtxes_filtered,
          vtxes_filtered_temp,
          FilterPoints::FilterBy::OUTSIDE,
          top_thresh,
          plane_thresh);

      // remove points that are too close to the edge of the viewport border or edge of active
      // plane stencil
      t.record("filter vtxes image by distance from active area border");
      make_simplified_stencil_mesh.filter_vtxes_by_distance_to_border(
          cam_calibration.active_plane.border,
          vtxes_filtered_temp,
          vtxes_filtered,
          depth_cam_intrin,
          cam_calibration.plane.to_plane_coords);

      // copy vtxes image to vtxes buffer used by mesh
      // TODO: can this be avoided? re-interpret texture as buffer?
      t.record("copy vtxes image to vtxes buffer");
      vtxes_filtered.cu_copy_to(generated_filtered_mesh.positions);

      // generate triangles to render the point mesh
      t.record("generate filtered mesh triangles");
      triangle_generator.run(
          vtxes_filtered, generated_filtered_mesh.idxes, &generated_filtered_mesh.num_triangles);

      /*

      t.record("output colors by distance from border");
      MakeSimplifiedStencilMesh::color_vtxes_by_distance_to_border(
        cam_calibration.active_p_stencil.mesh_border,
        generated_filtered_mesh.positions,
        generated_filtered_mesh.colors,
        depth_cam_intrin,
        cam_calibration.plane.to_plane_coords);

        */

      t.record("make minified height map");
      active_p_height_map.generate(
          height_map_resolution,
          generated_filtered_mesh,
          cam_calibration.active_plane.stencil_sampler);

      // is this slow on debug build? should be better on release build..
      t.record("generate mesh groups from height map");
      mesh_groups.make_new_from_height_map(
          active_p_height_map.height_map_cpu,
          active_p_height_map.height_map.get_dims(),
          min_group_size,
          max_num_groups);

      // as debug output of this stage, draw different colors for each group on the mesh
      t.record("color mesh by groups");
      mesh_groups.color_vtxes(
          generated_filtered_mesh.colors,
          generated_filtered_mesh.positions,
          active_p_height_map.height_map_sampler);

      t.record("find bounding boxes for each group");
      mesh_groups.find_groups_bounding_boxes(
          max_num_groups,
          mesh_groups.groups_info_cpu,
          mesh_groups.groups_info,
          mesh_groups.groups_map,
          active_p_height_map.height_map_sampler);

      t.record("gen per-group height map, determine fingertip positions");
      unordered_map<int, vec3> fingertip_positions;

      active_bb_height_maps.generate(
          max_num_groups,
          mesh_groups.groups_map,
          active_p_height_map.height_map_sampler,
          generated_filtered_mesh,
          mesh_groups.groups_info_cpu,
          fingertip_positions);

      hand_positions_manager.put(
          fingertip_positions,
          // callbacks to call when hit is concluded to start, or to stop
          [&](vec2 note_pos, int velocity) { return notes_manager.hit(note_pos, velocity); },
          [&](int note_off_id) { notes_manager.on_note_off(note_off_id); });

      t.record("setup image for drawing");

      // draw to 2nd framebuffer, so we can extract the 3d coord of each fragment as the 2nd color
      // output
      full_fbo.bind();
      Magnum::GL::Renderer::setClearColor(00000000_rgbaf);

      // attach positions buffer to active fbo
      full_fbo.attachTexture(GL::Framebuffer::ColorAttachment{1}, full_fbo_positions.get(), 0);

      full_fbo.clear(
          Magnum::GL::FramebufferClear::Color | Magnum::GL::FramebufferClear::Depth |
          Magnum::GL::FramebufferClear::Stencil);

      t.record("draw active plane");
      cam_calibration.active_plane.render(colored_mesh_cam);

      // unbind positions buffer from active fbo
      full_fbo.detach(GL::Framebuffer::ColorAttachment{1});

      // Inform note system of where mouse is hovering
      t.record("get mouse hover pos");
      get_mouse_hover_pos();

      // also draws ImGui elements which are 'sort of' part of the scene
      t.record("draw notes");
      notes_manager.render(colored_mesh_cam, w.get_dims());

      t.record("draw generated filtered mesh");
      GL::Renderer::setPolygonMode(GL::Renderer::PolygonMode::Line);
      colored_mesh_cam.draw(generated_filtered_mesh);

      t.record("draw bounding box and fingertip positions");
      mesh_groups.render_bounding_boxes(colored_mesh_cam);
      hand_positions_manager.render_positions(colored_mesh_cam);

      GL::defaultFramebuffer.bind();

    } else {
      // If camera is NOT yet calibrated against table..

      // Generate positions from depth image
      t.record("deproject depth");
      deproject_depth.run(depth_img, vtxes, depth_cam_intrin, minif, mat4(1.f));

      // determine flatness of depth image regions
      t.record("fit to planes");
      ivec2 planes_dims;
      const int plane_fit_region_side_length = 48;
      fit_regions_to_planes.run(vtxes, plane_fit_region_side_length, planes, &planes_dims);

      // find largest contiguous plane region
      t.record("find largest contiguous plane region");
      float percent_of_planes_on_largest_contiguous;
      FitRegionsToPlanes::find_largest_contiguous_flat_region(
          planes,
          planes_dims,
          pct_measured_thresh,
          error_thresh,
          &percent_of_planes_on_largest_contiguous,
          &current_plane);

      // can't post plane until the intrinsics for the RGBA cam have been set, for UV mapping
      if (rgba_cam_intrin_set) {
        // send plane of best fit along with error (1 is best, 0 is worst) to calibration
        // mechanism if this function returns true, then calibration has completed. Generate
        cam_calibration.post_plane(
            depth_img,
            current_plane,
            percent_of_planes_on_largest_contiguous,
            depth_cam_intrin,
            rgba_cam_intrin,
            depth_to_rgba_extrin,
            RS_DEPTH_UNITS);

        if (cam_calibration.calibrated) { copy_next_color_frame = true; }
      }

      // copy vtx image to vtxes bugger used by mesh
      t.record("copy vtxes texture to buffer - gpu to gpu");
      vtxes.cu_copy_to(generated_depth_camera_mesh.positions);

      t.record("map flatness to texture!");
      fit_regions_to_planes.to_vtx_colors(
          generated_depth_camera_mesh.colors,
          generated_depth_camera_mesh.positions,
          current_plane.to_plane_coords,
          vtxes.get_dims(),
          plane_thresh);

      t.record("generate triangles");
      triangle_generator.run(
          vtxes, generated_depth_camera_mesh.idxes, &generated_depth_camera_mesh.num_triangles);

      t.record("draw mesh");

      // Draw generated mesh to main screen according to m
      GL::Renderer::setPolygonMode(GL::Renderer::PolygonMode::Line);
      generated_depth_camera_mesh.position.set(current_plane.to_plane_coords);
      colored_mesh_cam.draw(generated_depth_camera_mesh);
    }

    t.record("end");
  }

  // Finds position of 3d fragment mouse is hovering over, according to positions buffer of
  // rendered image
  void get_mouse_hover_pos() {
    vec4 mouse_hover_pos{0, 0, 0, 0};

    // if the current mouse is on-screen, read position from fbo which has positions as 2nd color
    // output
    if (current_mouse_live) {
      // TODO: why is CUDA memcpy of single value to CPU so much faster than OpenGL version? (even
      // after bind/unbind overhead?)
      full_fbo_positions.cu_copy_single_value<vec4>(current_mouse_pos, &mouse_hover_pos);
    }

    if (current_mouse_click && mouse_hover_pos.w > 0) {
      mouseup_callback = notes_manager.click_surface(mouse_hover_pos.xy());
    } else {
      notes_manager.set_pending(mouse_hover_pos.w > 0, mouse_hover_pos.xy());
    }
  }

  void draw_debug_gui() override {

    ImGui::Begin("debuggin!");

    ImGuiTabBarFlags tab_bar_flags = ImGuiTabBarFlags_None;
    if (ImGui::TreeNode("Misc")) {

      const float thresh_range = 200;
      ImGui::SliderFloat("plane threshols", &plane_thresh, -thresh_range, thresh_range / 3);

      ImGui::SliderInt("minif", &minif, 1, 12);
      ImGui::SliderInt("minif2", &high_minif, 1, 12);
      ImGui::SliderFloat("min group size", &min_group_size, 0., 0.1);
      ImGui::SliderFloat("pct measured", &pct_measured_thresh, 0., 1.);
      ImGui::SliderFloat("error", &error_thresh, 0., 75.);

      ImGui::SliderInt("shrink iterations", &active_p_height_map.num_rounds, 0, 16);

      ImGui::TreePop();
    }

    if (ImGui::TreeNode("hand thresholds")) {
      hand_positions_manager.draw_imgui();
      ImGui::TreePop();
    }
    if (ImGui::TreeNode("Timing")) {
      // print duration of each recorded event to imgui
      t.render(
          [&](string event_name, float ms) { ImGui::Text("%s: %f ms", event_name.c_str(), ms); });

      ImGui::Text("--");
      ImGui::TreePop();
    }
    if (ImGui::TreeNode("images")) {
      // put incoming RGBA image into stencil mesh texture

#ifdef DEBUG_BUILD
      ImGui::Text("first per-arm height map");
      for (auto& m : active_bb_height_maps.height_maps_rgba) {
        Engine::ImGuiCtx::Image2(m);
        ImGui::Text("--");
      }

      ImGui::Text("height map");
      Engine::ImGuiCtx::Image2(active_p_height_map.height_map_rgba);

      // rgba representation of raw depth!
      ImGui::Text("raw depth!");
      Engine::ImGuiCtx::Image2(depth_img_rgba);
#endif

      ImGui::Text("rgba in");
      Engine::ImGuiCtx::Image2(cam_calibration.active_plane.mesh.tex);

      // stencil!
      ImGui::Text("stencil!");
      Engine::ImGuiCtx::Image2(cam_calibration.active_plane.stencil_rgba);

      ImGui::TreePop();
    }
    if (ImGui::TreeNode("Camera")) {
      rs.draw_imgui();
      ImGui::TreePop();
    }
    ImGui::End();
  }

  void draw_gui() {

    bool main_window_hovered = false;

    if (cam_calibration.calibrated) {

      auto on_main_click = [&](ivec2 v) { current_mouse_click = true; };
      auto on_main_hover = [&](ivec2 v) {
        current_mouse_live = true;
        current_mouse_pos = v;
      };
      auto on_main_click_up = [&]() {
        mouseup_callback();
        mouseup_callback = [] {};
      };

      // render output framebuffer rendering 3d scene as full-screen window that can't move to the
      // front (i.e basically the same as regular output)
      ImGui::SetNextWindowPos({0, 0});
      ImGui::SetNextWindowSize({(float)w.w, (float)w.h});
      ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, {0, 0});
      ImGui::PushStyleVar(ImGuiStyleVar_WindowBorderSize, 0);
      ImGui::Begin(
          "abc",
          NULL,
          ImGuiWindowFlags_NoBringToFrontOnFocus | ImGuiWindowFlags_NoCollapse |
              ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoScrollbar |
              ImGuiWindowFlags_NoScrollWithMouse | ImGuiWindowFlags_NoTitleBar |
              ImGuiWindowFlags_NoBackground);

      const glm::ivec2 fbo_offset = (ivec2{w.w, w.h} / 2) - (full_fbo_rgba.get_dims() / 2);
      Engine::ImGuiCtx::Image2(
          full_fbo_rgba, true, 2, on_main_click, on_main_hover, on_main_click_up, true, fbo_offset);

      // check to see if the window is hovered - only handle scroll behavior to zoom in/out if
      // mouse is hovering over main image
      main_window_hovered = ImGui::IsWindowHovered();

      ImGui::End();

      ImGui::PopStyleVar();
      ImGui::PopStyleVar();
    }

    // camera GUI on top-right of screen
    const float cam_gui_width = 64;
    const float top_bar_height = 16;
    const float gui_right_padding = 16;
    const float gui_top_padding = 16;
    colored_mesh_cam.position.draw_imgui_pretty(
        main_window_hovered,
        {w.w - (cam_gui_width + gui_right_padding), gui_top_padding},
        &colored_mesh_cam.config.mirror);

    ImGui::SetNextWindowPos({16, 16});
    ImGui::Begin("info", NULL, ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_AlwaysAutoResize);

    ImGui::Text("Connected camera: %s", rs.get_camera_name().c_str());

    notes_manager.midi.draw_imgui_pretty();

    if (!cam_calibration.calibrated) {
      ImGui::Text("Surface calibrating..");
    } else {
      ImGui::Text(u8"Surface calibrated. %1.2f m�", cam_calibration.surface_area);
      ImGui::SameLine();
      if (ImGui::Button(ICON_FA_UNDO)) {
        // clear both notes and calibration - (right now) no way to persist notes across
        // re-calibrations and have them show up in the same places
        cam_calibration.reset();
        notes_manager.clear();

        cam_calibration.active_plane.mesh.tex.cu_copy_from(black_image.data(), RGBA_DIMS);

        // Turn the color camera back on! we will want to take another picture!
        rs.set_desired_streams(
            {{rs2_stream::RS2_STREAM_DEPTH, rs2_format::RS2_FORMAT_Z16, DEPTH_DIMS, DEPTH_FPS},
             {rs2_stream::RS2_STREAM_COLOR, rs2_format::RS2_FORMAT_RGBA8, RGBA_DIMS, RGBA_FPS}});
      }
    }

    if (ImGui::Button("Settings...")) { ImGui::OpenPopup("Settings"); }
    ImGui::SameLine();

    if (ImGui::Button("Help?")) {
      string command = "start https://www.3d-beats.com/help";
      std::system(command.c_str());
    }
    ImGui::SameLine();
    if (ImGui::Button("About")) { ImGui::OpenPopup("About"); }

    if (ImGui::BeginPopupModal("About", NULL, ImGuiWindowFlags_AlwaysAutoResize)) {

      const std::string app_name = APP_NAME;
      const std::string app_version = APP_VERSION;
      const std::string app_title = app_name + " " + app_version;
      ImGui::Text(app_title.c_str());
      ImGui::Text("Check for updated versions at www.3d-beats.com");
      ImGui::Text("Send feedback and bug reports to info@3d-beats.com");
      ImGui::Text("Copyright (c) 2019 Carson Swope");

      ImGui::Text("--");
      ImGui::Text("Thanks to maintainers of these open source libraries:");
      ImGui::Text("  glfw (https://www.glfw.org)");
      ImGui::Text("  magnum (https://magnum.graphics/)");
      ImGui::Text("  Dear, Imgui (github.com/ocornut/imgui)");
      ImGui::Text("  librealsense (github.com/IntelRealSense/librealsense)");
      ImGui::Text("  rtmidi (github.com/thestk/rtmidi)");
      ImGui::Text("  cereal (github.com/USCiLab/cereal)");
      ImGui::Text("  glm (github.com/g-truc/glm)");
      ImGui::Text("  cglm (github.com/recp/cglm)");

      if (ImGui::GetIO().MouseClicked[0] && !ImGui::IsMouseHoveringWindow()) {
        ImGui::CloseCurrentPopup();
      }
      ImGui::EndPopup();
    }

    if (ImGui::BeginPopupModal("Settings", NULL, ImGuiWindowFlags_AlwaysAutoResize)) {

      ImGui::Text("Hand settings:");
      ImGui::SliderFloat("Note on  threshold", &hand_positions_manager.note_on_threshold, 100, 250);
      ImGui::SliderFloat("Note off threshold", &hand_positions_manager.note_off_threshold, 0, 50);
      ImGui::Checkbox("Detect note strengths", &hand_positions_manager.note_velocities_on);

      if (!hand_positions_manager.note_velocities_on) {
        ImGui::PushItemFlag(ImGuiItemFlags_Disabled, true);
        ImGui::PushStyleVar(ImGuiStyleVar_Alpha, ImGui::GetStyle().Alpha * 0.5f);
      }
      ImGui::SliderFloat("Min. note strength", &hand_positions_manager.min_velocity, 0., 1.);

      if (!hand_positions_manager.note_velocities_on) {
        ImGui::PopItemFlag();
        ImGui::PopStyleVar();
      }

      ImGui::Text("calibration settings:");
      ImGui::SliderFloat("Table threshold", &plane_thresh, -150, 20);

      if (ImGui::Button("Reset all")) {
        hand_positions_manager.note_on_threshold = hand_positions_manager.DEFAULT_NOTE_ON_THRESHOLD;
        hand_positions_manager.note_off_threshold =
            hand_positions_manager.DEFAULT_NOTE_OFF_THRESHOLD;
        hand_positions_manager.note_velocities_on = true;
        hand_positions_manager.min_velocity = hand_positions_manager.DEFAULT_MIN_VELOCITY;
        plane_thresh = DEFAULT_PLANE_THRESH;
      }

      ImGui::Text("\nNote: Changes to these settings will be saved automatically");

      if (ImGui::GetIO().MouseClicked[0] && !ImGui::IsMouseHoveringWindow()) {
        ImGui::CloseCurrentPopup();
      }
      ImGui::EndPopup();
    }

    ImGui::End();
  }

  unordered_map<string, std::pair<float*, float>> settings_f;
  unordered_map<string, std::pair<int*, int>> settings_i;
  unordered_map<string, std::pair<string*, string>> settings_s;

  void register_setting_f(const string name, float* val, float default_val) {
    settings_f[name] = {val, default_val};
  }

  void register_setting_s(const string name, string* val, string default_val) {
    settings_s[name] = {val, default_val};
  }

  void register_setting_i(const string name, int* val, int default_val) {
    settings_i[name] = {val, default_val};
  }

  void load_settings() {

    try {
      string settings_text = FileUtil::load_text(FileUtil::cfg_dir + "settings.txt");
      app_settings.load_json(settings_text);
    } catch (FileUtil::FileUtilException e) {
      // that's ok: the file didn't load!
      // TODO: get rid of message in the logs
    }

    for (auto s : settings_f) { *s.second.first = app_settings.get_f(s.first, s.second.second); }
    for (auto s : settings_i) { *s.second.first = app_settings.get_i(s.first, s.second.second); }
    for (auto s : settings_s) { *s.second.first = app_settings.get_s(s.first, s.second.second); }
  }

  void save_settings() {

    for (auto s : settings_f) { app_settings.set_f(s.first, *s.second.first); }
    for (auto s : settings_i) { app_settings.set_i(s.first, *s.second.first); }
    for (auto s : settings_s) { app_settings.set_s(s.first, *s.second.first); }

    string settings_json = app_settings.to_json();
    FileUtil::save_text(FileUtil::cfg_dir + "settings.txt", settings_json);
  }

  void setup_fbo() {

    // Configure framebuffer
    full_fbo_rgba.set_storage(render_dims);
    full_fbo.attachTexture(GL::Framebuffer::ColorAttachment{0}, full_fbo_rgba.get(), 0);
    full_fbo_positions.set_storage(render_dims);
    full_fbo.attachTexture(GL::Framebuffer::ColorAttachment{1}, full_fbo_positions.get(), 0);
    full_fbo.mapForDraw(
        {{0, GL::Framebuffer::ColorAttachment(0)}, {1, GL::Framebuffer::ColorAttachment(1)}});
    full_fbo_depth_stencil.setStorage(RenderbufferFormat::DepthStencil, Vector2i{w.get_dims()});
    full_fbo.attachRenderbuffer(
        GL::Framebuffer::BufferAttachment::DepthStencil, full_fbo_depth_stencil);
    Engine::Gpu::GlUtil::verify_framebuffer_complete(full_fbo);
  }
};
