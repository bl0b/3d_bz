#pragma once

#include <engine/Engine.h>

#include <compute/ComputeLib.hpp>
#include <render/render.hpp>

#include <Components/Midi.hpp>
#include <Components/RsCtxLive.hpp>

#include <Components/RsDataBag.hpp>

#include <Components/BoundingBoxHeightMaps.hpp>
#include <Components/CalibrationState.hpp>
#include <Components/HandPositionsManager.hpp>
#include <Components/NotesManager.hpp>

#include <Components/ActivePlaneHeightMap.hpp>

#include <Components/Plottable.hpp>

#include <Components/Physics.hpp>

#include <atomic>
#include <queue>

#include "Constants.hpp"

using namespace ComputeLib;
using namespace Engine::Gpu;
using namespace PhysicsConversion;

class ClipLoader : public AbstractApp {
public:
  RsDataBag data_bag;

  // compute classes
  MapDepth map_depth;
  DeprojectDepth deproject_depth;
  ConvolveCircle convolve_circle;
  RansacPlane_Pipeline ransac_plane_pipeline;
  RansacPlane_Pipeline ransac_hand_plane_pipeline;
  FilterPoints filter_points;
  GenTriangles gen_triangles;
  CompareDepthImages compare_depth;
  FitRegionsToPlanes fit_regions_to_planes;

  DiffDepthImages diff_depth_images;

  // shader camera classes
  ColoredMeshCamera<Engine::Scene::ArcBallCameraPosition> colored_mesh_cam;

  // OrthoMapShader ortho_cam;
  ShaderInstanced ortho_cam_instanced;
  ShaderInstanced cam_guess_instanced;

  Engine::Scene::ArcBallCameraPosition main_screen_pos;
  ShaderInstanced main_screen_cam;

  mat4 ortho_cam_projection;
  SpriteCamera<Engine::Scene::MatrixPosition> sprite_camera;

  DepthCameraMesh<Engine::Scene::MatrixPosition> gen_mesh;
  InstancedMeshGroup gen_mesh_g;

  InstancedMeshGroup mg_hand, mg_arm;
  mat4 hand_pos_plane_space, joint_pos_plane_space, arm_pos_plane_space;
  vec4 joint_coord_local;
  mat4 joint_pos_local;

  InstancedMeshGroup rectangle_mesh;

  InstancedMeshGroup mesh_group_0;
  InstancedMeshGroup mesh_group_1;
  // OrthoInstancedMesh rectangle_mesh;
  DepthCameraMesh<Engine::Scene::MatrixPosition> ray_mesh;

  DepthCameraMesh<Engine::Scene::MatrixPosition> calc_tform_square;

  Engine::Gpu::Tex<Engine::Gpu::TF::RGBA8> debug_output_image;

  // GPU texture blocks
  Engine::Gpu::Tex<Engine::Gpu::TF::R16UI> object_depth_img, plane_depth_img;
  Engine::Gpu::Tex<Engine::Gpu::TF::RGBA8> object_depth_img_rgba, plane_depth_img_rgba;
  Engine::Gpu::Tex<Engine::Gpu::TF::RGBA8> depth_img_rgba;

  Engine::Gpu::Tex<Engine::Gpu::TF::R16UI> rendered_guess, rendered_guess_mesh_ids;
  Engine::Gpu::Tex<Engine::Gpu::TF::RGBA8> rendered_guess_rgba;

  Engine::Gpu::Tex<Engine::Gpu::TF::R16UI> rectangle_map_binary, rectangle_map_binary_best,
      rectangle_map_binary_best_2;
  Engine::Gpu::Tex<Engine::Gpu::TF::RGBA8> rectangle_map_rgba, rectangle_map_rgba_best,
      rectangle_map_rgba_best_2;

  Engine::Gpu::Tex<Engine::Gpu::TF::R16UI> rectangle_hand_binary, rectangle_hand_depth,
      rectangle_arm_binary, rectangle_arm_depth;
  Engine::Gpu::Tex<Engine::Gpu::TF::RGBA8> rectangle_hand_binary_rgba, rectangle_hand_depth_rgba,
      rectangle_arm_binary_rgba, rectangle_arm_depth_rgba, rectangle_hand_depth_rgba_raw;

  Engine::Gpu::Tex<Engine::Gpu::TF::RGBA32F> hand_vtxes;

  Engine::Gpu::Tex<Engine::Gpu::TF::RGBA32F> vtxes;
  Engine::Gpu::Tex<Engine::Gpu::TF::RGBA32F> vtxes_2;
  Engine::Gpu::Tex<Engine::Gpu::TF::RGBA32F> deprojected_2;

  Engine::Gpu::Tex<Engine::Gpu::TF::R16UI> arm_depth_img_filtered, hand_depth_img_filtered,
      hand_depth_img_rendered, hand_render, arm_render;
  Engine::Gpu::Tex<Engine::Gpu::TF::RGBA8> arm_depth_img_filtered_rgba, rend_render_rgba,
      hand_depth_img_rendered_rgba, arm_render_rgba, hand_depth_img_filtered_rgba;
  Engine::Gpu::Tex<Engine::Gpu::TF::RGBA32F> arm_vtxes_filtered, hand_vtxes_filtered;

  Engine::Gpu::Buff<vec4> convolve_coords;
  unsigned int num_coords_for_convolution;

  Engine::Gpu::Buff<vec4> rendered_costs_out;

  int num_thetas = 24;
  float sq_err_thresh = 1000.f;

  int current_frame = 0;

  // params for plane calibration
  float calibrate_z_threshold = 90.f;
  float live_z_threshold = 30.f;
  int num_iterations = 200;

  //
  int minif_factor = 4;
  int ortho_minif_factor = 4;
  int convolve_minif = 6;
  int minif_divided = 8;

  ivec2 ortho_map_dims{512, 512};

  // minimum maximum image 2d dimension
  const int MAX_IMG_DIM = 8192 / 2;

  rs2_intrinsics object_depth_img_intrinsics;

  PhysicsCtx physics_ctx;
  PhysicsObject physics_hand;
  PhysicsObject physics_arm;

  Engine::Gpu::Buff<ivec4> edge_overlap_coords, nearest_empty_coords, depth_diffs_out;
  Engine::Gpu::Buff<vec4> edge_overlap_positions, nearest_empty_positions;
  Engine::Gpu::Buff<vec4> edge_overlap_forces;

  ~ClipLoader() {}

  ClipLoader(Engine::Gpu::GraphicsCtx& _w) : AbstractApp(_w) {

    debug_output_image.set_storage({800, 600});

    main_screen_cam.cam_projection = glm::perspective(
        45.f,
        (1.f * debug_output_image.get_dims().x) / debug_output_image.get_dims().y,
        10.f,
        50000.f);

    w.on_press(GLFW_KEY_ESCAPE, [&] { w.set_should_close(true); });
    data_bag.from_file("demo_flipper.bag");
    // data_bag.from_file("demo_sequence_1.bag");

    {
      ray_mesh.num_triangles = 1;
      vector<vec4> rmp{{0, 0, 0, 1}, {1, 0, 0, 1}, {0, 0, 0, 1}};
      ray_mesh.positions.set_data(rmp);
      vector<unsigned int> rmi{0, 1, 2};
      ray_mesh.idxes.set_data(rmi);
      vector<vec4> rmc{{.5, .7, 0.9, 1.0}, {0.5, 0.7, 0.9, 1.0}, {0.5, 0.7, 0.9, 1.0}};
      ray_mesh.colors.set_data(rmc);
    }

    {
      rectangle_mesh.meshes.resize(1);
      rectangle_mesh.num_meshes = {1};
      rectangle_mesh.meshes[0].num_triangles = 2;
      vector<vec4> rmp{{-0.5, -0.5, 0, 1}, {0.5, -0.5, 0, 1}, {-0.5, 0.5, 0, 1}, {0.5, 0.5, 0, 1}};
      rectangle_mesh.meshes[0].positions.set_data(rmp);
      vector<unsigned int> rmi{0, 1, 2, 2, 1, 3};
      rectangle_mesh.meshes[0].idxes.set_data(rmi);
      // rectangle_mesh.num_instances = 2;
    }

    // allocate 1 mesh, 1 transform per instance. simplest mesh group
    {
      vector<vec4> mgp{{0, -0.5, 0, 1}, {1, -0.5, 0, 1}, {0, 0.5, 0, 1}, {1, 0.5, 0, 1}};
      vector<unsigned int> mgi{0, 1, 2, 2, 1, 3};
      mesh_group_0.meshes.resize(2);
      mesh_group_0.num_meshes = vector<unsigned int>{1, 1};
      mesh_group_0.meshes[0].num_triangles = 2;
      mesh_group_0.meshes[0].positions.set_data(mgp);
      mesh_group_0.meshes[0].idxes.set_data(mgi);

      // 1 mesh, 2 transforms per instance. slightly more complicated..
      mesh_group_1.meshes.resize(2);
      mesh_group_1.num_meshes = vector<unsigned int>{2, 1};

      mesh_group_1.meshes[0].num_triangles = 2;
      mesh_group_1.meshes[0].positions.set_data(mgp);
      mesh_group_1.meshes[0].idxes.set_data(mgi);
    }

    // generate a circle for the rendering!
    vector<vec4> circle_vtxes;
    vector<unsigned int> circle_idxes;
    const int num_outer_vtxes = 32;
    circle_vtxes.emplace_back(0., 0., 0., 1.);
    for (int i = 0; i < num_outer_vtxes; i++) {
      const float theta = i * M_PI * 2.f / num_outer_vtxes;
      circle_vtxes.emplace_back(cos(theta), sin(theta), 0., 1.);
      circle_idxes.emplace_back(0);
      circle_idxes.emplace_back(i + 1);
      circle_idxes.emplace_back(((i + 1) % num_outer_vtxes) + 1);
    }

    mesh_group_1.meshes[1].positions.set_data(circle_vtxes);
    mesh_group_1.meshes[1].idxes.set_data(circle_idxes);
    mesh_group_1.meshes[1].num_triangles = num_outer_vtxes;

    mesh_group_0.meshes[1].positions.set_data(circle_vtxes);
    mesh_group_0.meshes[1].idxes.set_data(circle_idxes);
    mesh_group_0.meshes[1].num_triangles = num_outer_vtxes;

    {
      vector<vec4> ctsp{
          {-0.5, -0.5, 0., 1.}, {0.5, -0.5, 0., 1.}, {0.5, 0.5, 0., 1.}, {-0.5, 0.5, 0., 1.}};
      vector<unsigned int> ctsi{0, 1, 2, 0, 2, 3};
      vector<vec4> ctsc{{1., 1., 1., 1.}, {1., 1., 1., 1.}, {1., 1., 1., 1.}, {1., 1., 1., 1.}};
      calc_tform_square.num_triangles = 2;
      calc_tform_square.positions.set_data(ctsp);
      calc_tform_square.idxes.set_data(ctsi);
      calc_tform_square.colors.set_data(ctsc);
    }

    gen_mesh_g.meshes.resize(1);
    gen_mesh_g.num_meshes = {1};

    // Defaults for arcball cam
    colored_mesh_cam.position.config.distance = 15000;
    colored_mesh_cam.config.mirror = false;

    // main_screen_pos.config.distance = 15000;
    colored_mesh_cam.position.config.angle_x = M_PI;
    colored_mesh_cam.position.config.angle_y = M_PI * 0.75f;

    colored_mesh_cam.position.min_y = M_PI / 2.f;
    colored_mesh_cam.position.max_y = M_PI;
    // main_screen_pos.mirror = false;
  }

  void tick() override {

    if (!ransac_plane_pipeline.plane_set) {

      auto& depth_img = data_bag.depth_frames[current_frame];
      find_calibration(depth_img);
      snapshot_mesh_and_init_physics();
      current_frame = 36;
    }

    auto& depth_img = data_bag.depth_frames[current_frame];
    rs2_intrinsics depth_img_intrinsics = data_bag.depth_intrinsics;

    deproject_depth.run(
        depth_img,
        deprojected_2,
        data_bag.depth_intrinsics,
        minif_divided,
        ransac_plane_pipeline.p.to_plane_coords);

    filter_points.divide_original(
        depth_img,
        data_bag.depth_intrinsics,
        deprojected_2,
        object_depth_img,
        plane_depth_img,
        ransac_plane_pipeline.ortho_map_points,
        ransac_plane_pipeline.ortho_map_binary,
        ransac_plane_pipeline.ortho_sampler,
        live_z_threshold,
        &object_depth_img_intrinsics);

    // generate debug output
    map_depth.run(depth_img, depth_img_rgba);
    map_depth.run(object_depth_img, object_depth_img_rgba);

    convolve_circle.generate_coords_for_convolution(
        object_depth_img,
        convolve_coords,
        convolve_minif,
        &num_coords_for_convolution,
        object_depth_img_rgba);

    vector<vec4> convolve_coords_cpu(num_coords_for_convolution);
    convolve_coords.cu_copy_to(convolve_coords_cpu.data(), num_coords_for_convolution);

    // step 1 of pipeline:

    const int num_images_1 = num_coords_for_convolution * num_thetas;

    auto get_tform_round_1 = [&](int i, vector<mat4>& tform_list) {
      const int coord_id = i / num_thetas;
      const int theta_id = i % num_thetas;

      auto coord_info = convolve_coords_cpu[coord_id];

      const vec2 coords_xy = coord_info.xy();
      const float theta = (float)(theta_id * 2.f * M_PI) / num_thetas;
      const vec2 scale{250.f, 15.};

      mat4 pos_tform = glm::translate(vec3{coords_xy, 0}) *
                       glm::rotate((float)theta, vec3{0., 0., 1.}) * glm::scale(vec3{scale, 1});
      tform_list.push_back(move(pos_tform));

      tform_list.push_back(move(
          glm::translate(vec3{coords_xy, 0}) * glm::scale(vec3{scale.y / 2, scale.y / 2, 1.})));
    };

    int best_cost_image_id = -1;
    float best_cost;
    iterate_cost_minification(
        num_images_1, mesh_group_0, get_tform_round_1, &best_cost_image_id, &best_cost);

    rectangle_map_binary_best.set_storage(object_depth_img.get_dims());
    draw_best_transforms(best_cost_image_id, mesh_group_0, rectangle_map_binary_best);

    draw_best_transforms(best_cost_image_id, mesh_group_0, plane_depth_img, false);

    /*

    vector<mat4> _best_tform;
    get_tform_round_1(best_cost_image_id, _best_tform);
    mat4 best_tform = _best_tform[0];

    */

    // rectangle_map_binary_best_2.set_storage(object_depth_img.get_dims());
    // draw_best_transforms(best_cost_image_id, mesh_group_1, rectangle_map_binary_best_2);

    map_depth.run(plane_depth_img, plane_depth_img_rgba);

    draw_full_mesh_color_coded(depth_img);

    // colored_mesh_cam.draw(mg_hand);
    // colored_mesh_cam.draw(mg_arm);

    draw_gui();
  }

  void snapshot_mesh_and_init_physics() {
    // reasonably straight looking frame
    const int frame_id = 36;

    auto& depth_img = data_bag.depth_frames[frame_id];
    rs2_intrinsics depth_img_intrinsics = data_bag.depth_intrinsics;

    deproject_depth.run(
        depth_img,
        deprojected_2,
        data_bag.depth_intrinsics,
        minif_divided,
        ransac_plane_pipeline.p.to_plane_coords);

    filter_points.divide_original(
        depth_img,
        data_bag.depth_intrinsics,
        deprojected_2,
        object_depth_img,
        plane_depth_img,
        ransac_plane_pipeline.ortho_map_points,
        ransac_plane_pipeline.ortho_map_binary,
        ransac_plane_pipeline.ortho_sampler,
        live_z_threshold,
        &object_depth_img_intrinsics);

    // initialized debug output, essentially
    map_depth.run(object_depth_img, object_depth_img_rgba);

    // first pick coords to convolve..
    convolve_circle.generate_coords_for_convolution(
        object_depth_img,
        convolve_coords,
        convolve_minif,
        &num_coords_for_convolution,
        object_depth_img_rgba);

    vector<vec4> convolve_coords_cpu(num_coords_for_convolution);
    convolve_coords.cu_copy_to(convolve_coords_cpu.data(), num_coords_for_convolution);

    const int num_images_1 = num_coords_for_convolution * num_thetas;

    const vec2 scale{250.f, 15.};

    // xy/theta
    auto get_xy_theta_round_1 = [&](int i) -> vec3 {
      const int coord_id = i / num_thetas;
      const int theta_id = i % num_thetas;

      auto coord_info = convolve_coords_cpu[coord_id];
      const float theta = (float)(theta_id * 2.f * M_PI) / num_thetas;

      return vec3{coord_info.xy(), theta};
    };

    auto get_tform_round_1 = [&](int i, vector<mat4>& tform_list) {
      auto xy_theta = get_xy_theta_round_1(i);
      const vec2 coords_xy = xy_theta.xy();
      const float theta = xy_theta.z;

      mat4 pos_tform = glm::translate(vec3{coords_xy, 0}) *
                       glm::rotate((float)theta, vec3{0., 0., 1.}) * glm::scale(vec3{scale, 1});
      tform_list.push_back(move(pos_tform));

      tform_list.push_back(move(
          glm::translate(vec3{coords_xy, 0}) * glm::scale(vec3{scale.y / 2, scale.y / 2, 1.})));
    };

    int best_cost_image_id = -1;
    float best_cost;
    iterate_cost_minification(
        num_images_1, mesh_group_0, get_tform_round_1, &best_cost_image_id, &best_cost);

    rectangle_map_binary_best_2.set_storage(object_depth_img.get_dims());
    draw_best_transforms(best_cost_image_id, mesh_group_0, rectangle_map_binary_best_2);

    const float wrist_cutoff = 46.f;

    auto xy_theta = get_xy_theta_round_1(best_cost_image_id);
    vec2 start = xy_theta.xy();
    vec2 long_dir = (glm::rotate(xy_theta.z, vec3{0., 0., 1.}) * vec4{1., 0., 0., 1.}).xy();
    vec2 short_dir = (glm::rotate(xy_theta.z, vec3{0., 0., 1.}) * vec4{0., -1., 0., 1.}).xy();

    // printf("theta dir: %s\n", to_string(theta_dir));
    const float scale_factor = 1.;
    vec2 rect_coord_0 =
        start + (long_dir * -scale_factor * scale.y) + (short_dir * -scale_factor * scale.y);
    vec2 rect_coord_1 = rect_coord_0 + (short_dir * 2.f * scale_factor * scale.y);
    vec2 rect_coord_2 = rect_coord_0 + (long_dir * wrist_cutoff);
    vec2 rect_coord_3 = rect_coord_1 + (long_dir * wrist_cutoff);

    vec2 rect_coord_4 = rect_coord_2 + (long_dir * wrist_cutoff);
    vec2 rect_coord_5 = rect_coord_3 + (long_dir * wrist_cutoff);

    vec2 joint_coord_0 = ((rect_coord_0 + rect_coord_1) / 2.f) + (long_dir * wrist_cutoff);

    vec2 hand_origin = (rect_coord_0 + rect_coord_1 + rect_coord_2 + rect_coord_3) / 4.f;
    vec2 arm_origin = (rect_coord_2 + rect_coord_3 + rect_coord_4 + rect_coord_5) / 4.f;

    InstancedMeshGroup m_p;
    m_p.meshes.resize(1);
    m_p.num_meshes = vector<unsigned int>{1};

    mg_hand.meshes.resize(1);
    mg_hand.num_meshes = {1};
    // mg_hand.num_instances = 1;

    mg_arm.meshes.resize(1);
    mg_arm.num_meshes = {1};
    // mg_arm.num_instances = 1;

    GL::Renderer::setPolygonMode(GL::Renderer::PolygonMode::Fill);

    {

      m_p.meshes[0].num_triangles = 2;
      vector<unsigned int> mpi{0, 1, 2, 1, 2, 3};
      vector<vec4> mpp{{rect_coord_0, 0., 1.},
                       {rect_coord_1, 0., 1.},
                       {rect_coord_2, 0., 1.},
                       {rect_coord_3, 0., 1.},
                       {rect_coord_4, 0., 1.},
                       {rect_coord_5, 0., 1.}};
      m_p.meshes[0].idxes.set_data(mpi);
      m_p.meshes[0].positions.set_data(mpp);

      // m_p.num_instances = 1;
      vector<mat4> id{mat4{1.f}};
      m_p.mesh_tforms.set_data(id);
    }

    rectangle_hand_binary.set_storage(object_depth_img.get_dims());
    rectangle_arm_binary.set_storage(object_depth_img.get_dims());

    ortho_cam_instanced.fbo.bind();

    ortho_cam_instanced.set_target(&rectangle_hand_binary, nullptr, nullptr, nullptr);

    ortho_cam_instanced.fbo.clear(GL::FramebufferClear::Color | GL::FramebufferClear::Depth);

    auto d = rectangle_hand_binary.get_dims();
    ortho_cam_instanced.cam_projection = glm::ortho(0.f, 1.f * d.x, 0.f, 1.f * d.y);

    ortho_cam_instanced.draw_instanced_no_viewport(m_p, 1, 0);

    ortho_cam_instanced.clear_target();

    {
      vector<unsigned int> mpi{2, 3, 4, 3, 4, 5};
      m_p.meshes[0].idxes.set_data(mpi);
    }

    ortho_cam_instanced.set_target(&rectangle_arm_binary, nullptr, nullptr, nullptr);

    ortho_cam_instanced.fbo.clear(GL::FramebufferClear::Color | GL::FramebufferClear::Depth);

    d = rectangle_arm_binary.get_dims();
    ortho_cam_instanced.cam_projection = glm::ortho(0.f, 1.f * d.x, 0.f, 1.f * d.y);

    ortho_cam_instanced.draw_instanced_no_viewport(m_p, 1, 0);

    ortho_cam_instanced.clear_target();

    filter_points.by_stencil(object_depth_img, rectangle_hand_binary, hand_depth_img_filtered);
    filter_points.by_stencil(object_depth_img, rectangle_arm_binary, arm_depth_img_filtered);

    deproject_depth.run(
        hand_depth_img_filtered, hand_vtxes_filtered, object_depth_img_intrinsics, 1, mat4{1.f});
    deproject_depth.run(
        arm_depth_img_filtered, arm_vtxes_filtered, object_depth_img_intrinsics, 1, mat4{1.f});

    gen_triangles.run(
        hand_vtxes_filtered, mg_hand.meshes[0].idxes, &mg_hand.meshes[0].num_triangles);
    gen_triangles.run(arm_vtxes_filtered, mg_arm.meshes[0].idxes, &mg_arm.meshes[0].num_triangles);

    ivec2 origin_hand_i{hand_origin.x, hand_origin.y};
    vec4 origin_hand_pos;
    hand_vtxes_filtered.cu_copy_single_value(origin_hand_i, &origin_hand_pos.x);
    origin_hand_pos = ransac_plane_pipeline.p.to_plane_coords * origin_hand_pos;
    // z position halfway between top of hand surface and table, at given point
    // origin_hand_pos.z /= 2.f;

    // this theta will be slightly distorted, hopefully not too much
    mat4 hand_origin_tform =
        glm::translate(vec3{origin_hand_pos.xyz()}) * glm::rotate(xy_theta.z, vec3{0., 0., 1.});

    {
      vector<mat4> id{mat4{1.f}};
      mg_hand.mesh_tforms.set_data(id);
      mg_arm.mesh_tforms.set_data(id);
    }

    rectangle_hand_depth_rgba_raw.set_storage(object_depth_img.get_dims());
    rectangle_hand_depth.set_storage(object_depth_img.get_dims());
    hand_depth_img_rendered.set_storage(hand_depth_img_filtered.get_dims());
    ortho_cam_instanced.set_target(
        &hand_depth_img_rendered, &rectangle_hand_depth, &rectangle_hand_depth_rgba_raw, nullptr);

    d = rectangle_hand_binary.get_dims();

    auto& i = object_depth_img_intrinsics;
    const mat4 m2 = glm::perspectiveFromIntrinsics(
        {i.fx, i.fy}, {i.ppx, i.ppy}, {i.width, i.height}, {-5000.f, 5000.f});

    ortho_cam_instanced.cam_projection = m2;

    ortho_cam_instanced.draw_instanced_no_viewport(mg_hand, 1, 0);

    ortho_cam_instanced.clear_target();

    // convert vtxes to plane coordinates
    deproject_depth.tform_points(
        hand_vtxes_filtered, hand_vtxes, ransac_plane_pipeline.p.to_plane_coords);

    // hand vtxes have been rendered directly using shader.
    ransac_hand_plane_pipeline.find_calibration(hand_vtxes, 200.f, num_iterations, ortho_map_dims);

    arm_vtxes_filtered.cu_copy_to(mg_arm.meshes[0].positions);
    hand_vtxes.cu_copy_to(mg_hand.meshes[0].positions);

    vector<vec4> hand_colors(mg_hand.meshes[0].positions.size(), vec4{.1, 0.6, 0.2, 1.});
    mg_hand.meshes[0].vtx_colors.set_data(hand_colors);

    vector<vec4> arm_colors(mg_arm.meshes[0].positions.size(), vec4{.1, 0.4, 0.7, 1.});
    mg_arm.meshes[0].vtx_colors.set_data(arm_colors);

    mat4 __hand_pos_plane_space = ransac_hand_plane_pipeline.p.to_plane_coords;

    unsigned short joint_coord_depth;
    object_depth_img.cu_copy_single_value(ivec2{joint_coord_0}, &joint_coord_depth);

    vec4 joint_coord_vtx;
    rs2_deproject_pixel_to_point(
        &joint_coord_vtx.x,
        &object_depth_img_intrinsics,
        &joint_coord_0.x,
        joint_coord_depth * 1.f);

    joint_coord_vtx.w = 1.f;

    vec4 joint_coord_hand_local = ransac_hand_plane_pipeline.p.to_plane_coords *
                                  ransac_plane_pipeline.p.to_plane_coords * joint_coord_vtx;

    const float th = atan2(joint_coord_hand_local.y, joint_coord_hand_local.x);

    mat4 rotate_th = glm::rotate(-th, vec3{0., 0., 1.});
    hand_pos_plane_space = glm::inverse(__hand_pos_plane_space) * glm::inverse(rotate_th);

    vec4 joint_coord_hand_local_corrected = rotate_th * joint_coord_hand_local;

    joint_pos_local = glm::translate(vec3{joint_coord_hand_local_corrected.xy(), 0.});

    joint_pos_plane_space = hand_pos_plane_space * joint_pos_local;

    // bake in as local coords!
    deproject_depth.tform_points(mg_hand.meshes[0].positions, rotate_th * __hand_pos_plane_space);

    // vec2 joint_from_hand_dir = glm::normalize(vec3{joint_coord_hand_local_corrected.xy, 0.f}).xy;

    // just keep it going for the arm origin too!
    arm_pos_plane_space =
        joint_pos_plane_space * glm::translate(vec3{joint_coord_hand_local_corrected.xy(), 0});

    deproject_depth.tform_points(
        mg_arm.meshes[0].positions,
        glm::inverse(arm_pos_plane_space) * ransac_plane_pipeline.p.to_plane_coords);

    // set position transforms!
    const mat4 sc = glm::scale(vec3{50., 50., 1.});
    {
      vector<mat4> origins{
          hand_pos_plane_space * sc, joint_pos_plane_space * sc, arm_pos_plane_space * sc};
      rectangle_mesh.mesh_tforms.set_data(origins);

      vector<mat4> hand_poss{
          hand_pos_plane_space, hand_pos_plane_space, hand_pos_plane_space, hand_pos_plane_space};
      mg_hand.mesh_tforms.set_data(hand_poss);

      vector<mat4> arm_poss{
          arm_pos_plane_space, arm_pos_plane_space, arm_pos_plane_space, arm_pos_plane_space};
      mg_arm.mesh_tforms.set_data(arm_poss);
    }

    physics_ctx.m_dynamics_world->setGravity({0., 0., 0.});
    physics_hand.make({50, 50, 50}, 5., hand_pos_plane_space);
    physics_arm.make({50, 50, 50}, 5., arm_pos_plane_space);

    physics_ctx.m_dynamics_world->addRigidBody(physics_hand.body);
    physics_ctx.m_dynamics_world->addRigidBody(physics_arm.body);

    btGeneric6DofSpring2Constraint* c = new btGeneric6DofSpring2Constraint(
        *physics_hand.body,
        *physics_arm.body,
        toBt(glm::translate(vec3{joint_coord_hand_local_corrected.xy(), 0.f})),
        toBt(glm::translate(vec3{joint_coord_hand_local_corrected.xy() * -1.f, 0.f})));

    c->setAngularLowerLimit({-1., -1.5, -2.});
    c->setAngularUpperLimit({1., 1.5, 2.});

    physics_ctx.m_dynamics_world->addConstraint(c, true);

    physics_hand.body->setActivationState(DISABLE_DEACTIVATION);
    physics_arm.body->setActivationState(DISABLE_DEACTIVATION);

    // inputs:
    // force: vec3 in world space
    // relpos: vec3 pos of force application, relative to object but in world space

    vec3 n = ((glm::inverse(hand_pos_plane_space) * vec4{0., 0., 1., 1.}) -
              (glm::inverse(hand_pos_plane_space) * vec4{0., 0., 0., 1.})) *
             5.f;

    // physics_hand.body->applyForce(
    //    {n.x, n.y, n.z}, {joint_coord_hand_local.x, joint_coord_hand_local.y, 0.});

    {
      vector<mat4> id{mat4{1.f}};
      gen_mesh_g.mesh_tforms.set_data(id);
    }

    map_depth.run(rectangle_hand_depth, rectangle_hand_depth_rgba);
    map_depth.run(hand_depth_img_filtered, hand_depth_img_filtered_rgba);
    map_depth.run(hand_depth_img_rendered, hand_depth_img_rendered_rgba);
    map_depth.run(arm_depth_img_filtered, arm_depth_img_filtered_rgba);
    map_depth.run(rectangle_hand_binary, rectangle_hand_binary_rgba);
    map_depth.run(rectangle_arm_binary, rectangle_arm_binary_rgba);
  }

  void draw_gui() {

    ImGui::Begin("images");

    Engine::ImGuiCtx::Image2(rendered_guess_rgba);
    Engine::ImGuiCtx::Image2(object_depth_img_rgba);

    Engine::ImGuiCtx::Image2(debug_output_image);
    Engine::ImGuiCtx::Image2(rectangle_hand_depth_rgba_raw);
    Engine::ImGuiCtx::Image2(rectangle_hand_depth_rgba);
    Engine::ImGuiCtx::Image2(hand_depth_img_rendered_rgba);
    Engine::ImGuiCtx::Image2(hand_depth_img_filtered_rgba);
    Engine::ImGuiCtx::Image2(arm_depth_img_filtered_rgba);
    Engine::ImGuiCtx::Image2(rectangle_arm_binary_rgba);
    // Engine::ImGuiCtx::Image2(rectangle_map_rgba_best_2);
    Engine::ImGuiCtx::Image2(rectangle_map_rgba_best);
    Engine::ImGuiCtx::Image2(rectangle_map_rgba);
    Engine::ImGuiCtx::Image2(plane_depth_img_rgba);
    Engine::ImGuiCtx::Image2(depth_img_rgba);
    Engine::ImGuiCtx::Image2(ransac_plane_pipeline.ortho_map_binary_rgba);
    ImGui::End();

    ImGui::Begin("hello");

    ImGui::SliderInt("Frame", &current_frame, 0, data_bag.depth_frames.size() - 1);
    if (ImGui::Button("frame--")) { current_frame--; }
    ImGui::SameLine();
    if (ImGui::Button("frame++")) { current_frame++; }

    ImGui::SliderFloat("calib z thresh", &calibrate_z_threshold, 0.f, 500.f);
    ImGui::SliderFloat("live z thresh", &live_z_threshold, 0.f, 500.f);
    ImGui::SliderInt("num iterations", &num_iterations, 1, 256);
    ImGui::SliderInt("ortho minif", &ortho_minif_factor, 1, 32);
    ImGui::SliderInt("minif_divied", &minif_divided, 1, 32);

    // min_ray_length_pct
    ImGui::SliderInt("convolve minif", &convolve_minif, 1, 64);
    ImGui::Text("num coords to convolve: %u", num_coords_for_convolution);

    if (ImGui::Button("reset calibration")) { ransac_plane_pipeline.plane_set = false; }

    ImGui::SliderInt("num thetas", &num_thetas, 1, 64);

    ImGui::End();

    // camera GUI on top-right of screen
    const float cam_gui_width = 64;
    const float top_bar_height = 16;
    const float gui_right_padding = 16;
    const float gui_top_padding = 16;
    colored_mesh_cam.position.draw_imgui_pretty(
        true,
        {w.w - (cam_gui_width + gui_right_padding), gui_top_padding},
        &colored_mesh_cam.config.mirror);
  }

  void draw_debug_gui() override {}

  void draw_best_transforms(
      int tform_id,
      InstancedMeshGroup& ms,
      Engine::Gpu::Tex<Engine::Gpu::TF::R16UI>& draw_tex,
      bool clear = true) {

    // rectangle_map_binary_best.set_storage(object_depth_img.get_dims());
    ortho_cam_instanced.fbo.bind();
    ortho_cam_instanced.set_target(&draw_tex, nullptr, nullptr, nullptr);

    if (clear) {
      GL::Renderer::setClearColor(0x000000_rgbf);
      ortho_cam_instanced.fbo.clear(GL::FramebufferClear::Color | GL::FramebufferClear::Depth);
    }
    GL::Renderer::setPolygonMode(GL::Renderer::PolygonMode::Fill);

    auto& d = draw_tex.get_dims();
    ortho_cam_instanced.cam_projection = glm::ortho(0.f, 1.f * d.x, 0.f, 1.f * d.y);
    ortho_cam_instanced.draw_instanced_no_viewport(ms, 1, tform_id);

    ortho_cam_instanced.clear_target();

    GL::defaultFramebuffer.bind();
  }

  ivec2 current_generated_viewport_dims_2{0, 0};
  Engine::Gpu::Buff<vec4> instance_viewports_2;

  // returns the grid of viewports on the large image
  static ivec2
  make_viewports(ivec2 viewport_dims, ivec2 large_image_dims, Engine::Gpu::Buff<vec4>& vp_buff) {

    ivec2 steps = large_image_dims / viewport_dims;
    const int max_steps = steps.x * steps.y;

    // generate viewports array for every possible viewport in the large image
    vector<vec4> vps(max_steps);
    for (int i = 0; i < max_steps; i++) {
      int step_x = i % steps.x;
      int step_y = i / steps.x;

      vec2 vp_low = {(step_x)*viewport_dims.x, (step_y)*viewport_dims.y};

      vec2 vp_high = {(step_x + 1) * viewport_dims.x, (step_y + 1) * viewport_dims.y};

      vps[i] = {vp_low, vp_high};
    }
    vp_buff.cu_copy_from(vps.data(), max_steps);

    return steps;
  }

  ivec2 current_generated_viewport_dims{0, 0};
  Engine::Gpu::Buff<vec4> instance_viewports;
  void iterate_cost_minification(
      int num_images,
      InstancedMeshGroup& ms,
      function<void(int, vector<mat4>&)> add_tforms,
      int* output_best_cost_image_id,
      float* output_best_cost) {

    vector<vec4> convolve_coords_cpu;
    convolve_coords.cu_copy_to(convolve_coords_cpu);

    rectangle_map_binary.set_storage({MAX_IMG_DIM, MAX_IMG_DIM});

    const int steps_x = MAX_IMG_DIM / object_depth_img.get_dims().x;
    const int steps_y = MAX_IMG_DIM / object_depth_img.get_dims().y;
    const int max_steps = steps_x * steps_y;

    if (current_generated_viewport_dims.x != object_depth_img.get_dims().x &&
        current_generated_viewport_dims.y != object_depth_img.get_dims().y) {

      make_viewports(
          object_depth_img.get_dims(), rectangle_map_binary.get_dims(), instance_viewports);
      current_generated_viewport_dims = object_depth_img.get_dims();
    }

    // generate transforms for every guess value
    vector<mat4> instance_transforms;
    for (int i = 0; i < num_images; i++) {
      add_tforms(i, instance_transforms);
    }
    ms.mesh_tforms.cu_copy_from(instance_transforms.data(), num_images * ms.full_mesh_count());

    const int num_iterations_needed = (num_images / max_steps) + ((num_images % max_steps) ? 1 : 0);

    GL::Renderer::setClearColor(0x000000_rgbf);
    GL::Renderer::setPolygonMode(GL::Renderer::PolygonMode::Fill);

    bool best_cost_found = false;
    float best_cost = 0.f;
    int best_cost_id;

    for (int i = 0; i < num_iterations_needed; i++) {

      const bool last_iteration = num_iterations_needed == (i + 1);
      const int num_images_this_iteration = last_iteration ? (num_images % max_steps) : max_steps;
      const unsigned int instance_offset = i * max_steps;

      ortho_cam_instanced.fbo.bind();
      ortho_cam_instanced.set_target(&rectangle_map_binary, nullptr, nullptr, nullptr);
      ortho_cam_instanced.fbo.clear(GL::FramebufferClear::Color | GL::FramebufferClear::Depth);

      const auto d = object_depth_img.get_dims();
      ortho_cam_instanced.cam_projection = glm::ortho(0.f, 1.f * d.x, 0.f, 1.f * d.y);

      ortho_cam_instanced.draw_instanced_per_viewport(
          ms, num_images_this_iteration, instance_offset, instance_viewports);

      ortho_cam_instanced.clear_target();

      compare_depth.compare_generated_binary_images_to_original_depth(
          object_depth_img,
          plane_depth_img,
          rectangle_map_binary,
          num_images_this_iteration,
          {steps_x, steps_y},
          rendered_costs_out);

      vector<vec4> rendered_costs_cpu;
      rendered_costs_out.cu_copy_to(rendered_costs_cpu);

      for (int j = 0; j < num_images_this_iteration; j++) {
        auto& c = rendered_costs_cpu[j];
        if (!best_cost_found || (c.x > best_cost)) {
          best_cost_found = true;
          best_cost_id = j + int(instance_offset);
          best_cost = c.x;
        }
      }
    }

    if (best_cost_found) {
      *output_best_cost_image_id = best_cost_id;
      *output_best_cost = best_cost;
    }

    GL::defaultFramebuffer.bind();

    map_depth.run(rectangle_map_binary, rectangle_map_rgba);
    map_depth.run(rectangle_map_binary_best, rectangle_map_rgba_best);
  }

  void find_calibration(Engine::Gpu::Tex<Engine::Gpu::TF::R16UI>& depth_img) {
    deproject_depth.run(depth_img, vtxes, data_bag.depth_intrinsics, minif_factor, mat4(1.f));
    ransac_plane_pipeline.find_calibration(
        vtxes, calibrate_z_threshold, num_iterations, ortho_map_dims);
  }

  void draw_full_mesh_color_coded(Engine::Gpu::Tex<Engine::Gpu::TF::R16UI>& depth_img) {

    deproject_depth.run(
        depth_img,
        vtxes_2,
        data_bag.depth_intrinsics,
        ortho_minif_factor,
        ransac_plane_pipeline.p.to_plane_coords);

    vtxes_2.cu_copy_to(gen_mesh_g.meshes[0].positions);

    gen_triangles.run(vtxes_2, gen_mesh_g.meshes[0].idxes, &gen_mesh_g.meshes[0].num_triangles);

    // run physics simulation..
    const float timestep = 0.1f;
    physics_ctx.m_dynamics_world->stepSimulation(timestep, 10);

    mat4 current_hand_tform_plane_space = physics_hand.get_current_tform();
    mat4 current_arm_tform_plane_space = physics_arm.get_current_tform();
    {
      vector<mat4> current_pos_hand{current_hand_tform_plane_space};
      vector<mat4> current_pos_arm{current_arm_tform_plane_space};
      mg_hand.mesh_tforms.set_data(current_pos_hand);
      mg_arm.mesh_tforms.set_data(current_pos_arm);
    }

    {
      const mat4 sc = glm::scale(vec3{50., 50., 1.});
      vector<mat4> rmt{current_hand_tform_plane_space * sc,
                       current_hand_tform_plane_space * joint_pos_local * sc,
                       current_arm_tform_plane_space * sc};
      rectangle_mesh.mesh_tforms.set_data(rmt);
    }

    fit_regions_to_planes.to_vtx_colors_2(
        gen_mesh_g.meshes[0].vtx_colors,
        gen_mesh_g.meshes[0].positions,
        mat4{1.f},
        vtxes_2.get_dims(),
        live_z_threshold,
        ransac_plane_pipeline.ortho_map_points,
        ransac_plane_pipeline.ortho_map_binary,
        ransac_plane_pipeline.ortho_sampler);

    main_screen_cam.fbo.bind();

    GL::Renderer::setClearColor(0x000000_rgbf);
    GL::Renderer::setPolygonMode(GL::Renderer::PolygonMode::Line);
    GL::Renderer::enable(GL::Renderer::Feature::DepthTest);
    GL::Renderer::setDepthFunction(GL::Renderer::DepthFunction::Less);

    main_screen_cam.set_target(nullptr, nullptr, &debug_output_image, nullptr);
    main_screen_cam.fbo.clear(GL::FramebufferClear::Color | GL::FramebufferClear::Depth);

    main_screen_cam.cam_position = colored_mesh_cam.position.transform_inverse();

    main_screen_cam.render_mode = ShaderInstanced::ColorRenderMode::COLORED_VTX;
    main_screen_cam.draw_instanced_no_viewport(gen_mesh_g, 1, 0);
    main_screen_cam.draw_instanced_no_viewport(mg_hand, 1, 0);
    main_screen_cam.draw_instanced_no_viewport(mg_arm, 1, 0);

    main_screen_cam.render_mode = ShaderInstanced::ColorRenderMode::FULL_COLOR;
    main_screen_cam.draw_instanced_no_viewport(rectangle_mesh, 3, 0);

    main_screen_cam.clear_target();

    if (current_generated_viewport_dims_2.x != object_depth_img.get_dims().x * 2 &&
        current_generated_viewport_dims_2.y != object_depth_img.get_dims().y * 2) {
      printf("made second viewports!\n");
      make_viewports(
          object_depth_img.get_dims(), object_depth_img.get_dims() * 2, instance_viewports_2);
      current_generated_viewport_dims_2 = object_depth_img.get_dims() * 2;
    }

    GL::Renderer::setPolygonMode(GL::Renderer::PolygonMode::Fill);

    {
      cam_guess_instanced.cam_position = glm::inverse(ransac_plane_pipeline.p.to_plane_coords);
      auto& i = object_depth_img_intrinsics;
      cam_guess_instanced.cam_projection = glm::perspectiveFromIntrinsics(
          {i.fx, i.fy}, {i.ppx, i.ppy}, {i.width, i.height}, {-5000.f, 5000.f});
    }

    rendered_guess.set_storage(object_depth_img.get_dims());
    cam_guess_instanced.fbo.bind();
    //cam_guess_instanced.uint_render_mode = ShaderInstanced::UintRenderMode::DEPTH;
    cam_guess_instanced.set_target(nullptr, &rendered_guess, nullptr, nullptr);
    cam_guess_instanced.fbo.clear(GL::FramebufferClear::Color | GL::FramebufferClear::Depth);

    cam_guess_instanced.draw_instanced_no_viewport(mg_hand, 1, 0);
    cam_guess_instanced.draw_instanced_no_viewport(mg_arm, 1, 0);

    cam_guess_instanced.clear_target();

    rendered_guess_mesh_ids.set_storage(object_depth_img.get_dims());

    cam_guess_instanced.uint_render_mode = ShaderInstanced::UintRenderMode::MESH_ID;
    cam_guess_instanced.set_target(nullptr, &rendered_guess_mesh_ids, nullptr, nullptr);
    cam_guess_instanced.fbo.clear(GL::FramebufferClear::Color | GL::FramebufferClear::Depth);

   // cam_guess_instanced.mesh_id = 10;
    cam_guess_instanced.draw_instanced_no_viewport(mg_hand, 1, 0);
   // cam_guess_instanced.mesh_id = 20;
    cam_guess_instanced.draw_instanced_no_viewport(mg_arm, 1, 0);

    cam_guess_instanced.clear_target();

    GL::defaultFramebuffer.bind();

    unsigned int num_edge_diffs;
    diff_depth_images.get_edge_diffs(
        plane_depth_img,
        rendered_guess,
        rendered_guess_mesh_ids,
        &num_edge_diffs,
        edge_overlap_coords);

    unsigned int num_depth_diffs;
    diff_depth_images.get_depth_diffs(
        object_depth_img,
        rendered_guess,
        rendered_guess_mesh_ids,
        &num_depth_diffs,
        depth_diffs_out);

    // vector<ivec4> edge_diffs_cpu(num_edge_diffs);
    // edge_overlap_coords.cu_copy_to(edge_diffs_cpu.data(), num_edge_diffs);

    printf("num edge diffs: %u\n", num_edge_diffs);
    printf("num depth diffs: %u\n", num_depth_diffs);

    vector<ivec4> depth_diffs_cpu(num_depth_diffs * 2);
    depth_diffs_out.cu_copy_to(depth_diffs_cpu.data(), num_depth_diffs * 2);

    // edge diff: where both plane and rendered image are appearing.
    // rendered image needs to be pushed away from plane area: search on plane image for nearest
    // non-plane region, push object in that direction!

    diff_depth_images.find_nearest_empty_pixels(
        plane_depth_img, num_edge_diffs, edge_overlap_coords, nearest_empty_coords);

    vector<ivec4> edge_overlap_coords_cpu(num_edge_diffs);
    edge_overlap_coords.cu_copy_to(edge_overlap_coords_cpu.data(), num_edge_diffs);
    vector<ivec4> nearest_empty_cpu(num_edge_diffs);
    nearest_empty_coords.cu_copy_to(nearest_empty_cpu.data(), num_edge_diffs);

    /*
    vector<float> distances;
    for (int i = 0; i < num_edge_diffs; i++) {
      auto& a = edge_overlap_coords_cpu[i].xy;
      auto& b = nearest_empty_cpu[i].xy;
      ivec2 diff = a - b;
      float d = sqrtf(diff.x * diff.x + diff.y * diff.y);
      distances.push_back(d);
    }
    */

    diff_depth_images.convert_depth_img_pairs_to_plane_space_vectors(
        object_depth_img_intrinsics,
        ransac_plane_pipeline.p,
        num_edge_diffs,
        edge_overlap_coords,
        nearest_empty_coords,
        edge_overlap_positions,
        nearest_empty_positions);

    vector<vec4> edge_poss(num_edge_diffs);
    edge_overlap_positions.cu_copy_to(edge_poss.data(), num_edge_diffs);
    vector<vec4> near_poss(num_edge_diffs);
    nearest_empty_positions.cu_copy_to(near_poss.data(), num_edge_diffs);

    vector<int> mesh_ids;
    vector<vec2> force_directions;
    // these are force origins in world space!
    vector<vec2> force_origins;
    // const vec4 cam_origin = ransac_plane_pipele

    vec3 hand_pos = (hand_pos_plane_space * vec4{0., 0., 0., 1.}).xyz();
    vec3 arm_pos = (arm_pos_plane_space * vec4{0., 0., 0., 1.}).xyz();

    physics_hand.body->setAngularVelocity(physics_hand.body->getAngularVelocity() * 0.2);
    physics_hand.body->setLinearVelocity(physics_hand.body->getLinearVelocity() * 0.8);

    physics_arm.body->setAngularVelocity(physics_arm.body->getAngularVelocity() * 0.2);
    physics_arm.body->setLinearVelocity(physics_arm.body->getLinearVelocity() * 0.8);

    for (int i = 0; i < num_depth_diffs; i++) {
      ivec4 a = depth_diffs_cpu[i * 2];
      ivec4 b = depth_diffs_cpu[i * 2 + 1];

      const vec2 coords = {a.x * 1.f, a.y * 1.f};
      const int mesh_id = a.z;
      const float depth1 = b.x * 1.f;
      const float depth2 = b.y * 1.f;

      vec3 pos1;
      vec3 pos2;
      rs2_deproject_pixel_to_point(&pos1.x, &object_depth_img_intrinsics, &coords.x, depth1);
      rs2_deproject_pixel_to_point(&pos2.x, &object_depth_img_intrinsics, &coords.x, depth2);

      vec3 diff_dir = pos1 - pos2;
      vec4 diff_dir_plane_space = ransac_plane_pipeline.p.to_plane_coords * vec4{diff_dir, 1.f};
      vec4 diff_origin_plane_space = ransac_plane_pipeline.p.to_plane_coords * vec4{pos1, 1.f};

      const float scale = 0.002;

      btRigidBody* body;
      vec3 body_pos;
      switch (mesh_id) {
      case 10:
        // HAND!
        body = physics_hand.body;
        body_pos = hand_pos;
        break;
      case 20:
        // ARM!
        body = physics_arm.body;
        body_pos = arm_pos;
        break;
      default:
        printf("error error!\n");
      }

      vec3 origin_rel = diff_origin_plane_space.xyz() - body_pos;
      const auto& o = origin_rel;
      body->applyForce(
          {diff_dir.x * scale, diff_dir.y * scale, diff_dir.z * scale}, {o.x, o.y, o.z});
    }

    for (int i = 0; i < num_edge_diffs; i++) {

      vec4 overlap_ray_dir = edge_poss[i];
      vec4 closest_empty_ray_dir = near_poss[i];

      vec4 overlap_point_on_plane =
          ransac_plane_pipeline.p.to_plane_coords *
          ransac_plane_pipeline.p.intersection_with_line(overlap_ray_dir.xyz(), vec3{0., 0., 0.});
      vec4 closest_empty_point_on_plane = ransac_plane_pipeline.p.to_plane_coords *
                                          ransac_plane_pipeline.p.intersection_with_line(
                                              closest_empty_ray_dir.xyz(), vec3{0., 0., 0.});

      // this is the direction in plane space of the nearest place on the plane that is empty from
      // the point of overlap
      vec2 plane_dir = (closest_empty_point_on_plane - overlap_point_on_plane).xy();

      int mesh_id = edge_overlap_coords_cpu[i].z;
      force_directions.push_back(plane_dir);
      force_origins.push_back(overlap_point_on_plane.xy());
      mesh_ids.push_back(mesh_id);

      const float scale = 0.07;

      if (mesh_id == 10) {
        // HAND!

        vec3 origin_rel_to_hand = overlap_point_on_plane.xyz() - hand_pos;
        const auto& o = origin_rel_to_hand;

        physics_hand.body->applyForce(
            {plane_dir.x * scale, plane_dir.y * scale, 0.}, {o.x, o.y, o.z});
      }

      else if (mesh_id == 20) {
        // ARM!
        vec3 origin_rel_to_arm = overlap_point_on_plane.xyz() - arm_pos;
        const auto& o = origin_rel_to_arm;

        physics_arm.body->applyForce(
            {plane_dir.x * scale, plane_dir.y * scale, 0.}, {o.x, o.y, o.z});
      } else {
        // errr unknonw!!!
        printf("error!!!\n");
      }
    }

    // just for debuggin!
    map_depth.run(rendered_guess, rendered_guess_rgba);
  }
};
