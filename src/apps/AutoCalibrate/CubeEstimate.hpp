#pragma once

#include <engine/Engine.h>

#include <compute/ComputeLib.hpp>
#include <render/render.hpp>

#include <Components/Physics.hpp>

#include <compute/img_compare_physics/TrackableObject.hpp>

#include <thread>
#include <chrono>

using namespace ComputeLib;
using namespace Engine::Gpu;
using namespace PhysicsConversion;
using namespace std;

class CubeEstimate : public AbstractApp {
public:
  TrackableObject* guess_cube;
  TrackableObject* real_cube;

  ShaderInstanced render_cam;

  Engine::Scene::ArcBallCameraPosition preview_cam_position;
  ShaderInstanced preview_cam;

  const ivec2 render_dims{256, 256};
  const ivec2 preview_dims{800, 600};
  rs2_intrinsics render_intrin;

  Tex<TF::R16UI> real_img_in, guess_img, guess_img_mesh_ids;
  Tex<TF::RGBA32F> real_img_vtxes, guess_img_vtxes;
  Tex<TF::RGBA8> preview_out, real_img_in_rgba, guess_img_rgba;

  Buff<ivec4> real_img_edge_pixels, guess_img_edge_pixels;
  Buff<vec4> real_img_edge_normals, guess_img_edge_normals;
  Buff<ivec4> guess_coords_on_real_edge, real_coords_on_guess_edge;
  Buff<vec4> real_edge_diff_impulses_out, guess_edge_diff_impulses_out, impulses_2_out;
  Buff<ivec4> match_coords;

  MapDepth map_depth;
  ImgCompare img_compare;

  int depth_sample_minif = 4;
  float point_diff_force_scale = 0.15;
  float edge_diff_force_scale = 0.25;

  explicit CubeEstimate(Engine::Gpu::GraphicsCtx& _w) : AbstractApp(_w) {

    w.on_press(GLFW_KEY_ESCAPE, [&] { w.set_should_close(true); });

    guess_cube = new TrackableObject();
    guess_cube->init();
    guess_cube->physics_damping = 0.3;

    real_cube = new TrackableObject();
    real_cube->init();
    real_cube->physics_damping = 0.98; // closer to 1: less damped!

    render_intrin.width = render_dims.x;
    render_intrin.height = render_dims.y;
    render_intrin.ppx = render_dims.x / 2.f;
    render_intrin.ppy = render_dims.y / 2.f;
    render_intrin.fx = render_dims.x * 1.2f;
    render_intrin.fy = render_dims.x * 1.2f;

    real_img_in.set_storage(render_dims);
    guess_img.set_storage(render_dims);
    guess_img_mesh_ids.set_storage(render_dims);
    real_img_vtxes.set_storage(render_dims);
    guess_img_vtxes.set_storage(render_dims);

    preview_out.set_storage(preview_dims);

    preview_cam.cam_projection =
        glm::perspective(45.f, (1.f * preview_dims.x) / preview_dims.y, 1.f, 1000.f);
    preview_cam_position.config.distance = 240.f;
    preview_cam_position.config.angle_x = 0.;
    preview_cam_position.config.angle_y = 2.18f; // M_PI / 2.f;
  }

  void tick() override {
    // generate synthetic input..
    draw_real_img_in();

    run_physics_simulation();

    // float frame_diff;
    // img_compare.compute_img_difference(real_img_in, guess_img, &frame_diff);

    draw_preview_img();
    draw_imgui();
  }

  void run_physics_simulation() {

    const float timestep = 0.1f;
    const int max_substeps = 10;

    // necessary??
    real_cube->damp_physics_bodies();
    real_cube->physics_ctx.m_dynamics_world->stepSimulation(timestep, max_substeps);

    // slow existing physics momentum
    guess_cube->damp_physics_bodies();
    draw_guess_img_in();
    generate_all_forces();
    guess_cube->physics_ctx.m_dynamics_world->stepSimulation(timestep, max_substeps);
  }

  void generate_all_forces() {

    guess_cube->write_cube_mesh_positions();
    {
      // generate forces for when real image edge overlaps with current guess image
      unsigned int num_real_img_edge_pixels;
      img_compare.find_edge_pixels(
          real_img_in, &num_real_img_edge_pixels, real_img_edge_pixels, real_img_in_rgba);

      unsigned int num_guess_coords_on_real_edge;
      img_compare.find_overlapping_coords(
          num_real_img_edge_pixels,
          real_img_edge_pixels,
          guess_img,
          &num_guess_coords_on_real_edge,
          guess_coords_on_real_edge,
          &guess_img_mesh_ids);

      if (num_guess_coords_on_real_edge) {

        img_compare.find_edge_line_normals(
            real_img_in,
            num_guess_coords_on_real_edge,
            guess_coords_on_real_edge,
            real_img_edge_normals,
            real_img_in_rgba);

        img_compare.generate_edge_diff_impulses_out(
            num_guess_coords_on_real_edge,
            guess_coords_on_real_edge,
            real_img_edge_normals,
            guess_img_vtxes,
            render_intrin,
            preview_cam_position.transform(),
            guess_cube->m.mesh_tforms,
            real_edge_diff_impulses_out);

        guess_cube->apply_generated_impulses(
            num_guess_coords_on_real_edge, real_edge_diff_impulses_out, -edge_diff_force_scale);
      }
    }

    {
      // generate forces for when current guess image edge overlaps with real image
      unsigned int num_guess_edge_pixels;
      img_compare.find_edge_pixels(
          guess_img,
          &num_guess_edge_pixels,
          guess_img_edge_pixels,
          guess_img_rgba,
          &guess_img_mesh_ids);

      unsigned int num_real_coords_on_guess_edge;
      img_compare.find_overlapping_coords(
          num_guess_edge_pixels,
          guess_img_edge_pixels,
          real_img_in,
          &num_real_coords_on_guess_edge,
          real_coords_on_guess_edge);

      if (num_real_coords_on_guess_edge) {

        img_compare.find_edge_line_normals(
            guess_img,
            num_real_coords_on_guess_edge,
            real_coords_on_guess_edge,
            guess_img_edge_normals,
            guess_img_rgba);

        img_compare.generate_edge_diff_impulses_out(
            num_real_coords_on_guess_edge,
            real_coords_on_guess_edge,
            guess_img_edge_normals,
            real_img_vtxes,
            render_intrin,
            preview_cam_position.transform(),
            guess_cube->m.mesh_tforms,
            guess_edge_diff_impulses_out);

        guess_cube->apply_generated_impulses(
            num_real_coords_on_guess_edge, guess_edge_diff_impulses_out, edge_diff_force_scale);
      }
    }

    unsigned int num_match_points;
    // points: both rasterized, do not match!
    img_compare.get_points_minif(
        real_img_in, guess_img, depth_sample_minif, &num_match_points, match_coords);

    if (num_match_points) {

      img_compare.find_points_diffs_impulses(
          num_match_points,
          match_coords,
          real_img_vtxes,
          guess_img_vtxes,
          guess_img_mesh_ids,
          preview_cam_position.transform(),
          guess_cube->m.mesh_tforms,
          impulses_2_out);

      guess_cube->apply_generated_impulses(
          num_match_points, impulses_2_out, point_diff_force_scale);
    }
  }

  void draw_imgui() {
    ImGui::Begin("preview out");
    Engine::ImGuiCtx::Image2(preview_out);
    Engine::ImGuiCtx::Image2(real_img_in_rgba);
    Engine::ImGuiCtx::Image2(guess_img_rgba);
    ImGui::End();

    ImGui::Begin("sliders!");
    ImGui::Text("preview cam!");
    ImGui::SliderFloat("dist", &preview_cam_position.config.distance, 1., 250.);
    ImGui::SliderFloat("angle x", &preview_cam_position.config.angle_x, -M_PI, M_PI);
    ImGui::SliderFloat("angle y", &preview_cam_position.config.angle_y, M_PI / 2.f, M_PI);
    ImGui::End();

    real_cube->draw_control_ui();
  }

  void draw_preview_img() {
    preview_cam.cam_position = preview_cam_position.transform_inverse();

    vec4 real_preview_color{0., 0.4, 0.9, 1.};
    vec4 guess_preview_color{0.3, 1.0, 0.3, 1.};

    real_cube->draw_preview(preview_cam, preview_out, real_preview_color);
    guess_cube->draw_preview(preview_cam, preview_out, guess_preview_color, false);
  }

  void setup_render_cam() {
    const auto& r = render_intrin;
    render_cam.cam_projection =
        glm::perspectiveFromIntrinsics(
            {r.fx, r.fy}, {r.ppx, r.ppy}, {r.width, r.height}, {1., 1500.}) *
        glm::scale(vec3{1., 1., -1.});
    render_cam.cam_position = preview_cam_position.transform_inverse();
    render_cam.uint_render_mode = ShaderInstanced::UintRenderMode::MESH_ID;
  }

  void draw_guess_img_in() {
    setup_render_cam();
    guess_cube->write_cube_mesh_positions();
    guess_cube->draw_img(render_cam, &guess_img_mesh_ids, guess_img, guess_img_vtxes);
    map_depth.run(guess_img, guess_img_rgba);
  }

  void draw_real_img_in() {
    setup_render_cam();
    real_cube->write_cube_mesh_positions();
    real_cube->draw_img(render_cam, nullptr, real_img_in, real_img_vtxes);
    map_depth.run(real_img_in, real_img_in_rgba);
  }
};
