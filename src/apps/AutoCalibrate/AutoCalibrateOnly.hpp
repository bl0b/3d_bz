#pragma once

#include <engine/Engine.h>

#include <compute/ComputeLib.hpp>

#include <Components/Midi.hpp>
#include <Components/RsCtxLive.hpp>

#include <Components/BoundingBoxHeightMaps.hpp>
#include <Components/CalibrationState.hpp>
#include <Components/HandPositionsManager.hpp>
#include <Components/NotesManager.hpp>

#include <Components/ActivePlaneHeightMap.hpp>

#include <Components/Plottable.hpp>

#include <atomic>
#include <queue>

#include "Constants.hpp"

using namespace ComputeLib;

class AutoCalibrateOnly : public AbstractApp {
public:
  // this is essentially the order of the processing pipeline..
  ComputeLib::MapDepth map_depth;
  ComputeLib::GenTriangles triangle_generator;
  ComputeLib::FitRegionsToPlanes fit_regions_to_planes;

  // classes for processing
  DeprojectDepth deproject_depth;

  // GPU texture blocks
  Engine::Gpu::Tex<Engine::Gpu::TF::R16UI> depth_img;
  Engine::Gpu::Tex<Engine::Gpu::TF::RGBA8> depth_img_rgba;
  Engine::Gpu::Tex<Engine::Gpu::TF::RGBA32F> vtxes;
  Engine::Gpu::Tex<Engine::Gpu::TF::RGBA32F> vtxes_filtered;

  // current generated out plane, GPU buffer storing grid of generated planes of best fit
  Engine::Math::Plane current_plane;
  Engine::Gpu::Buff<FitRegionsToPlanes::PlaneFit> planes;

  // 2nd framebuffer with 2 outputs: color & fragment world positions
  Magnum::GL::Framebuffer full_fbo{GL::defaultFramebuffer.viewport()};
  Engine::Gpu::Tex<Engine::Gpu::TF::RGBA8> full_fbo_rgba;
  Engine::Gpu::Tex<Engine::Gpu::TF::RGBA32F> full_fbo_positions;
  GL::Renderbuffer full_fbo_depth_stencil;

  // Meshes generated from incoming depth images
  DepthCameraMesh<Engine::Scene::MatrixPosition> generated_depth_camera_mesh;
  DepthCameraMesh<Engine::Scene::MatrixPosition> generated_filtered_mesh;

  // Camera for rendering meshes to screen
  ColoredMeshCamera<Engine::Scene::ArcBallCameraPosition> colored_mesh_cam;

  // Realsense context
  RsCtxLive rs;
  rs2_intrinsics depth_cam_intrin;
  rs2::frame depth_frame;

  // timer! TODO: put this class in Engine::Util, just use CPU clock instead of CUDA timers so
  // it can work everywhere
  Engine::Util::Timer t;

  int minif = 1;
  int high_minif = 4;
  float pct_measured_thresh = 0.7;
  float error_thresh = 10.;
  float plane_thresh = -75.;

  ~AutoCalibrateOnly() {}

  AutoCalibrateOnly(Engine::Gpu::GraphicsCtx& _w) : AbstractApp(_w) {

    // configure renderer features..
    GL::Renderer::setDepthFunction(GL::Renderer::DepthFunction::Less);
    Magnum::GL::Renderer::enable(Magnum::GL::Renderer::Feature::Blending);
    Magnum::GL::Renderer::setBlendFunction(
        Magnum::GL::Renderer::BlendFunction::SourceAlpha,
        Magnum::GL::Renderer::BlendFunction::OneMinusSourceAlpha);

    {
      auto colors = vector<vec4>{(const uint64_t)(DEPTH_DIMS.x * DEPTH_DIMS.y), {1, 1, 0, 1}};
      generated_filtered_mesh.colors.set_data(colors);
    }

    // Defaults for arcball cam
    colored_mesh_cam.position.config.angle_y = -2.;
    colored_mesh_cam.position.config.angle_x = 0;
    colored_mesh_cam.position.config.distance = 10000;
    colored_mesh_cam.config.clip_near = 50.;
    colored_mesh_cam.config.clip_far = 50000.;
    colored_mesh_cam.config.mirror = true;

    // Declare desired streams
    // clang-format off
    rs.set_desired_streams({
        {rs2_stream::RS2_STREAM_DEPTH, rs2_format::RS2_FORMAT_Z16, DEPTH_DIMS, DEPTH_FPS} });
    // clang-format on

    // Declare desired configuration values
    rs.set_config_callback([&](RsCtxLive& rs) {
      rs.set_depth_units(RS_DEPTH_UNITS);
      string prod_id = string(rs.dev.get_info(RS2_CAMERA_INFO_NAME));
      bool is_d415 = prod_id.find("415") != string::npos;
      rs.set_disparity_shift(is_d415 ? DISPARITY_SHIFT_D415 : DISPARITY_SHIFT_D435);
    });

    rs.init();

    w.on_press(GLFW_KEY_ESCAPE, [&] { w.set_should_close(true); });

    ImGui::GetStyle().FrameRounding = 4.f;
    ImGui::GetStyle().GrabRounding = 4.f;
  }

  void tick() override {

    // don't do anything while waiting for the streams to start
    if (!rs.is_active(RS2_STREAM_DEPTH)) {
      this_thread::sleep_for(chrono::milliseconds(10));
      return;
    }

    // draw all imgui stuff first.. they affect variables in play later, etc.
    draw_gui();

    // Start new timer sequence
    t.clear();

    // when to load intrinsics? can probably do better than every frame
    depth_cam_intrin = rs.intrinsics(RS2_STREAM_DEPTH);

    t.record("wait for camera frame");

    // Copy incoming frame to CUDA/OpenGL texture
    try {
      depth_frame = rs.try_wait(RS2_STREAM_DEPTH, RS2_STREAM_DEPTH, 250);
    } catch (exception e) {

      if (!rs.is_active(RS2_STREAM_DEPTH)) { return; }

      // Timeout! device unplugged??
      printf("error!\n");
      printf(e.what());
      cout << "not arrived in time!!" << endl;
      throw e;
    }

    // Copy incoming depth image to GPU texture buffer
    t.record("copy depth img to device");
    depth_img.cu_copy_from(depth_frame.get_data(), DEPTH_DIMS);

    // make RGBA image of incoming depth image for debugging
    t.record("map depth image to RGBA");
    map_depth.run(depth_img, depth_img_rgba);

    // Generate positions from depth image
    t.record("deproject depth");
    deproject_depth.run(depth_img, vtxes, depth_cam_intrin, minif, mat4(1.f));

    // determine flatness of depth image regions
    t.record("fit to planes");
    ivec2 planes_dims;
    const int plane_fit_region_side_length = 48;
    fit_regions_to_planes.run(vtxes, plane_fit_region_side_length, planes, &planes_dims);

    // find largest contiguous plane region
    t.record("find largest contiguous plane region");
    float percent_of_planes_on_largest_contiguous;
    FitRegionsToPlanes::find_largest_contiguous_flat_region(
        planes,
        planes_dims,
        pct_measured_thresh,
        error_thresh,
        &percent_of_planes_on_largest_contiguous,
        &current_plane);

    // copy vtx image to vtxes bugger used by mesh
    t.record("copy vtxes texture to buffer - gpu to gpu");
    vtxes.cu_copy_to(generated_depth_camera_mesh.positions);

    t.record("map flatness to texture!");
    fit_regions_to_planes.to_vtx_colors(
        generated_depth_camera_mesh.colors,
        generated_depth_camera_mesh.positions,
        current_plane.to_plane_coords,
        vtxes.get_dims(),
        plane_thresh);

    t.record("generate triangles");
    triangle_generator.run(
        vtxes, generated_depth_camera_mesh.idxes, &generated_depth_camera_mesh.num_triangles);

    t.record("draw mesh");

    // Draw generated mesh to main screen according to m
    GL::Renderer::setPolygonMode(GL::Renderer::PolygonMode::Line);
    generated_depth_camera_mesh.position.set(current_plane.to_plane_coords);
    colored_mesh_cam.draw(generated_depth_camera_mesh);

    t.record("end");
  }

  void draw_debug_gui() override {

    ImGui::Begin("debuggin!");

    ImGuiTabBarFlags tab_bar_flags = ImGuiTabBarFlags_None;
    if (ImGui::TreeNode("Misc")) {

      const float thresh_range = 200;
      ImGui::SliderFloat("plane threshols", &plane_thresh, -thresh_range, thresh_range / 3);

      ImGui::SliderInt("minif", &minif, 1, 12);
      ImGui::SliderInt("minif2", &high_minif, 1, 12);
      ImGui::SliderFloat("pct measured", &pct_measured_thresh, 0., 1.);
      ImGui::SliderFloat("error", &error_thresh, 0., 75.);

      ImGui::TreePop();
    }

    if (ImGui::TreeNode("Timing")) {
      // print duration of each recorded event to imgui
      t.render(
          [&](string event_name, float ms) { ImGui::Text("%s: %f ms", event_name.c_str(), ms); });

      ImGui::Text("--");
      ImGui::TreePop();
    }
    if (ImGui::TreeNode("images")) {

      // rgba representation of raw depth!
      ImGui::Text("raw depth!");
      Engine::ImGuiCtx::Image2(depth_img_rgba);

      ImGui::TreePop();
    }
    if (ImGui::TreeNode("Camera")) {
      rs.draw_imgui();
      ImGui::TreePop();
    }
    ImGui::End();
  }

  void draw_gui() {

    // camera GUI on top-right of screen
    const float cam_gui_width = 64;
    const float top_bar_height = 16;
    const float gui_right_padding = 16;
    const float gui_top_padding = 16;
    colored_mesh_cam.position.draw_imgui_pretty(
        false, {w.w - (cam_gui_width + gui_right_padding), (top_bar_height + gui_top_padding)});

    ImGui::SetNextWindowPos({16, 32});
    ImGui::Begin("info", NULL, ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_AlwaysAutoResize);
    ImGui::Text("Connected camera: %s", rs.get_camera_name().c_str());
    ImGui::Text("Surface calibrating..");

    ImGui::End();
  }
};
