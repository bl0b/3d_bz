#pragma once

// pre-determined disparity values for the desired cameras at the given video dimensions for each
// camera (if the video dimensions are changed away from 848x480, these disparity values will be
// messed up)
// TODO: can calculate this more dynamially, by specifying desired range?
constexpr float RS_DEPTH_UNITS = 0.0001;
constexpr int DISPARITY_SHIFT_D415 = 34;
constexpr int DISPARITY_SHIFT_D435 = 14;

constexpr ivec2 DEPTH_DIMS = {848, 480};
// slower framerate target for debug build
constexpr int DEPTH_FPS =
#ifdef DEBUG_BUILD
    30;
#else
    90;
#endif

constexpr ivec2 RGBA_DIMS = {1280, 720};
// RGBA framerate can be low - really just used to take pictures of desk
constexpr int RGBA_FPS = 15;
