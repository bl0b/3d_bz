#pragma once

#include <engine/Engine.h>

#include <compute/ComputeLib.hpp>
#include <render/render.hpp>

#include <Components/Physics.hpp>

#include <compute/img_compare_physics/TrackableObject.hpp>

#include <thread>
#include <chrono>

#include <random>
#include <queue>

using namespace ComputeLib;
using namespace Engine::Gpu;
using namespace PhysicsConversion;
using namespace std;

class HandSizeFit : public AbstractApp {
public:
  std::default_random_engine _g;

  RsDataBag data_bag;

  InstancedMeshGroup debug_mesh_g;

  InstancedMeshGroup ray_mesh_g;
  InstancedMeshGroup bbox_2d_g;

  InstancedMeshGroup cylinder_mesh_g;

  InstancedMeshGroup box_mesh_g;
  InstancedMeshGroup box_mesh_g_2;

  ShaderInstanced main_screen_cam;
  Engine::Scene::ArcBallCameraPosition main_screen_cam_pos;

  ShaderInstanced ray_mesh_cam;

  ShaderInstanced depth_cam;

  DeprojectDepth deproject_depth;
  GenTriangles gen_triangles;
  FitRegionsToPlanes fit_regions_to_planes;
  RansacPlane_Pipeline ransac_plane;
  FilterPoints filter_points;
  MapDepth map_depth;
  ImgCompare img_compare;

  Tex<TF::R16UI> obj, table_minus_obj, table_full, obj_render;
  Tex<TF::RGBA8> obj_c, table_minus_obj_c, table_full_c, obj_render_c;

  Tex<TF::R16UI> finger_bbox_ids;
  Tex<TF::RGBA8> finger_bbox_ids_c;

  const ivec2 dbg_out_dims{1200, 900};
  Tex<TF::RGBA8> dbg_out_img;

  Tex<TF::RGBA32F> vtxes, obj_vtxes;
  int calibration_minif_factor = 2;
  float calibrate_z_threshold = 100.f;
  float live_z_threshold = 30.f;
  int num_iterations = 200;

  ivec2 ortho_map_dims{512, 512};

  int mesh_render_minif = 1;
  int current_frame;
  int last_processed_frame = -1;
  int iter_num_render = 0;

  int num_compare_to_draw = 100, compare_offset = 0;

  Buff<ivec4> obj_edge_coords;

  vector<vec4> edge_chain_cpu;
  Buff<vec4> edge_chain;

  class KnuckleJoint {
  public:
    vec4 pos;
    vec2 radius;
  };

  class FingerBoundingBox {
  public:
    vec2 img_dims;
    vec2 origin_coords;
    float theta, length, width1, width2;

    void add_tforms_debug(vector<mat4>& t) const {

      const float theta2 = theta + (float)M_PI_2;
      const vec2 orthogonal{cos(theta2), sin(theta2)};
      const vec2 mid_coords = origin_coords + (vec2{cos(theta), sin(theta)} * length);

      // line going through middle
      t.emplace_back(make_tform_matrix(img_dims, origin_coords, mid_coords));

      vec2 pav = origin_coords + (orthogonal * width1);
      vec2 pbv = origin_coords - (orthogonal * width2);
      vec2 pcv = mid_coords + (orthogonal * width1);
      vec2 pdv = mid_coords - (orthogonal * width2);

      // actual bounding box
      t.emplace_back(make_tform_matrix(img_dims, pav, pbv));
      t.emplace_back(make_tform_matrix(img_dims, pcv, pdv));
      t.emplace_back(make_tform_matrix(img_dims, pav, pcv));
      t.emplace_back(make_tform_matrix(img_dims, pbv, pdv));
    }
  };

  struct IdentifiedFinger {
    vector<KnuckleJoint> knuckle_joints;
    FingerBoundingBox box;

    IdentifiedFinger() {}

    IdentifiedFinger(const IdentifiedFinger& f) {
      box = f.box;
      knuckle_joints.resize(f.knuckle_joints.size());
      std::copy(f.knuckle_joints.begin(), f.knuckle_joints.end(), knuckle_joints.begin());
    }

    void add_tforms_raster(vector<mat4>& r) {

      const float t2 = box.theta + (float)M_PI_2;
      vec2 origin = box.origin_coords + (vec2{cos(t2), sin(t2)} * (box.width1 - box.width2) / 2.f);

      r.emplace_back(make_tform_matrix(
          box.img_dims, origin, box.theta, vec2{box.length, box.width1 + box.width2}));
    }

    void add_box_tforms(vector<mat4>& t) { box.add_tforms_debug(t); }

    void add_mesh_tforms(vector<mat4>& cylinder, vector<mat4>& sphere) const {

      vec4 p0 = knuckle_joints[0].pos;
      vec4 p1 = knuckle_joints[1].pos;

      vec2 r0 = knuckle_joints[0].radius;
      vec2 r1 = knuckle_joints[1].radius;

      const vec3 p01_xyz = p1.xyz() - p0.xyz();
      vec2 p01_xy_dir = glm::normalize(vec2{p01_xyz.xy()});
      float p01_xy_length = glm::length(vec2{p01_xyz.xy()});
      float p01_xyz_length = glm::length(p01_xyz);

      float z_theta = atan2(p01_xy_dir.y, p01_xy_dir.x);

      const vec2 p01_z_slope = glm::normalize(vec2{p01_xy_length, p1.z - p0.z});
      const float y_theta = atan2(p01_z_slope.y, p01_z_slope.x);

      mat4 c_tf = glm::translate(vec3{p0.xyz()}) * glm::rotate(z_theta, vec3{0., 0., 1.}) *
                  glm::rotate(-y_theta, vec3{0., 1., 0.}) *
                  glm::scale(vec3{p01_xyz_length, 1., 1.}) * glm::translate(vec3{0.5, 0., 0.});

      // 2 transforms for cylinder!
      cylinder.emplace_back(c_tf * glm::scale(vec3{1, r0}));
      cylinder.emplace_back(c_tf * glm::scale(vec3{1, r1}));

      // 1 transform for sphere!
      mat4 s_tf_0 = glm::translate(vec3{p0.xyz()}) * glm::rotate(z_theta, vec3{0., 0., 1.}) *
                    glm::rotate(-y_theta, vec3{0., 1., 0.}) * glm::scale(vec3{r0.x, r0.x, r0.y});
      mat4 s_tf_1 = glm::translate(vec3{p1.xyz()}) * glm::rotate(z_theta, vec3{0., 0., 1.}) *
                    glm::rotate(-y_theta, vec3{0., 1., 0.}) * glm::scale(vec3{r1.x, r1.x, r1.y});

      sphere.emplace_back(s_tf_0);
      sphere.emplace_back(s_tf_1);
    }
  };

  vector<IdentifiedFinger> fingers;

  const string dfile = "C:\\Users\\Carson\\Documents\\20200321_150544.bag";

  Buff<vec4> vp_buff;
  bool viewports_made = false;

  explicit HandSizeFit(Engine::Gpu::GraphicsCtx& _w) : AbstractApp(_w) {

    // Defaults for arcball cam
    main_screen_cam_pos.config.distance = 15000;
    main_screen_cam_pos.config.angle_x = M_PI;
    main_screen_cam_pos.config.angle_y = M_PI * 0.75f;
    main_screen_cam_pos.min_y = M_PI / 2.f;
    main_screen_cam_pos.max_y = M_PI;

    {
      cylinder_mesh_g.meshes.resize(2);

      Primitives::make_cylinder(cylinder_mesh_g.meshes[0], vec3{1., 2., 2.}, glm::mat4{1.f}, 12);

      Primitives::make_sphere(cylinder_mesh_g.meshes[1], vec3{1., 1., 1.}, glm::mat4{1.f}, 16);
    }

    {
      box_mesh_g.meshes.resize(2);
      Primitives::make_cylinder(box_mesh_g.meshes[0], vec3{1., 2., 2.}, mat4{1.f}, 12);
      Primitives::make_sphere(box_mesh_g.meshes[1], vec3{1., 1., 1.}, mat4{1.f}, 16);
      box_mesh_g.num_meshes = {1, 1};

      box_mesh_g_2.meshes.resize(1);
      Primitives::make_cube(box_mesh_g_2.meshes[0], vec3{1., 1., 1.});
      box_mesh_g_2.num_meshes = {1};
    }

    {
      ray_mesh_g.meshes.resize(1);
      ray_mesh_g.num_meshes = {5};

      ray_mesh_g.meshes[0].num_triangles = 1;
      auto idxs = vector<unsigned int>{0, 1, 2};
      ray_mesh_g.meshes[0].idxes.set_data(idxs);
      auto psns = vector<vec4>{{0., 0., 0., 1.}, {1., 0., 0., 1.}, {0., 0., 0., 1.}};
      ray_mesh_g.meshes[0].positions.set_data(psns);
    }

    {
      bbox_2d_g.meshes.resize(1);
      bbox_2d_g.num_meshes = {1};

      auto& m = bbox_2d_g.meshes[0];
      m.num_triangles = 2;
      auto idxs = vector<unsigned int>{0, 1, 2, 0, 2, 3};
      m.idxes.set_data(idxs);
      auto psns =
          vector<vec4>{{0, -.5, 0., 1.}, {0, .5, 0., 1.}, {1, .5, 0., 1.}, {1, -.5, 0., 1.}};
      m.positions.set_data(psns);
    }

    dbg_out_img.set_storage(dbg_out_dims);

    w.on_press(GLFW_KEY_ESCAPE, [&] { w.set_should_close(true); });

    data_bag.from_file(dfile, true);

    main_screen_cam.cam_projection =
        glm::perspective(45.f, (1.f * dbg_out_dims.x) / dbg_out_dims.y, 10.f, 50000.f);

    // pure NDCs!!
    ray_mesh_cam.cam_projection = mat4{1.f};
    ray_mesh_cam.cam_position = mat4{1.f};

    auto& depth_img = data_bag.depth_frames[0];

    Tex<TF::RGBA32F> _vtxes;
    deproject_depth.run(
        depth_img, _vtxes, data_bag.depth_intrinsics, calibration_minif_factor, mat4{1.f});
    ransac_plane.find_calibration(_vtxes, calibrate_z_threshold, num_iterations, ortho_map_dims);

    rs2_intrinsics obj_intrinsics;
    filter_points.divide_original(
        depth_img,
        data_bag.depth_intrinsics,
        _vtxes,
        obj,
        table_full,
        ransac_plane.ortho_map_points,
        ransac_plane.ortho_map_binary,
        ransac_plane.ortho_sampler,
        calibrate_z_threshold,
        &obj_intrinsics);

    // initialized debug output, essentially
    map_depth.run(table_full, table_full_c);

    current_frame = 70;

    {
      debug_mesh_g.meshes.resize(1);
      debug_mesh_g.num_meshes = {1};
      vector<mat4> _tforms{mat4{1.f}};
      debug_mesh_g.mesh_tforms.set_data(_tforms);
    }
  }

  void tick() override {
    ImGui::Begin("info!");
    ImGui::Text("oooop");
    ImGui::SliderInt("frame", &current_frame, 0, data_bag.depth_frames.size() - 1);
    ImGui::SliderInt("iter number", &iter_num_render, 0, 15);
    ImGui::SliderInt("offset", &compare_offset, 0, 1400);
    if (ImGui::Button("offset+++++")) compare_offset += 5;
    if (ImGui::Button("offset---")) compare_offset -= 5;

    ImGui::SliderInt("num", &num_compare_to_draw, 0, 750);
    ImGui::End();

    ImGui::Begin("imgs");
    Engine::ImGuiCtx::Image2(dbg_out_img);
    ImGui::End();

    ImGui::Begin("other imgs");
    Engine::ImGuiCtx::Image2(obj_c);
    ImGui::Text("rendered:");
    Engine::ImGuiCtx::Image2(obj_render_c);
    Engine::ImGuiCtx::Image2(finger_bbox_ids_c);
    Engine::ImGuiCtx::Image2(table_minus_obj_c);
    Engine::ImGuiCtx::Image2(table_full_c);
    Engine::ImGuiCtx::Image2(ransac_plane.ortho_map_binary_rgba);
    ImGui::End();

    // camera GUI on top-right of screen
    const float cam_gui_width = 64;
    const float top_bar_height = 16;
    const float gui_right_padding = 16;
    const float gui_top_padding = 16;
    main_screen_cam_pos.draw_imgui_pretty(
        true, {w.w - (cam_gui_width + gui_right_padding), gui_top_padding}, nullptr);

    auto& depth_img = data_bag.depth_frames[current_frame];

    deproject_depth.run(
        depth_img,
        vtxes,
        data_bag.depth_intrinsics,
        mesh_render_minif,
        ransac_plane.p.to_plane_coords);

    rs2_intrinsics obj_intrinsics;
    filter_points.divide_original(
        depth_img,
        data_bag.depth_intrinsics,
        vtxes,
        obj,
        table_minus_obj,
        ransac_plane.ortho_map_points,
        ransac_plane.ortho_map_binary,
        ransac_plane.ortho_sampler,
        live_z_threshold,
        &obj_intrinsics);

    deproject_depth.run(
        obj,
        obj_vtxes,
        data_bag.depth_intrinsics,
        mesh_render_minif,
        ransac_plane.p.to_plane_coords);

    map_depth.run(obj, obj_c);
    map_depth.run(table_minus_obj, table_minus_obj_c);

    //    if (true) {
    // TODO: there is something buggy about the edge-finding & edge-chain-generation code!!!!
    // but who cares for now
    if (current_frame != last_processed_frame) {

      unsigned int num_edge_pixels;
      img_compare.find_edge_pixels_2(obj, &num_edge_pixels, obj_edge_coords, obj_c);

      unsigned int longest_group_idx;
      vector<vector<vec4>> edge_coords_groups;
      gen_edge_coords_groups(
          obj.get_dims(), num_edge_pixels, obj_edge_coords, edge_coords_groups, &longest_group_idx);

      edge_chain_cpu = std::move(edge_coords_groups[longest_group_idx]);
      const auto l = edge_chain_cpu.size();
      edge_chain.set_storage(l);
      edge_chain.cu_copy_from(edge_chain_cpu.data(), l);

      const auto& _d = obj.get_dims();
      vector<uint16_t> obj_cpu(_d.x * _d.y);
      obj.cu_copy_to(obj_cpu.data());

      const int walk_steps = 50;
      const int step = 10;
      int current = 0;

      bool section_started = false;
      int section_started_idx;
      vector<ivec2> edge_chain_section_lengths;
      while (current < l) {

        const auto& p0_v = edge_chain_cpu[current % l];
        const auto& p1_v = edge_chain_cpu[(current + walk_steps) % l];
        const auto& p2_v = edge_chain_cpu[(current - walk_steps) % l];
        const auto mid = (p1_v + p2_v) / 2.f;

        const float length1 = glm::length(p0_v - mid);
        const float length2 = glm::length(p1_v - p2_v);

        const float length_threshold_1 = 25.f;
        const float length_threshold_2 = 50.f;

        if (length1 > length_threshold_1 && length2 < length_threshold_2) {
          ivec2 mid_i{mid.x, mid.y};
          const auto mid_val = obj_cpu[mid_i.y * _d.x + mid_i.x];
          const auto mid_val_1 = obj_cpu[(mid_i.y + 1) * _d.x + mid_i.x];
          const auto mid_val_2 = obj_cpu[(mid_i.y + 1) * _d.x + mid_i.x + 1];
          if (mid_val && mid_val_1 && mid_val_2) {
            if (!section_started) {
              section_started = true;
              section_started_idx = current;
            }
          }

        } else {
          if (section_started) {
            section_started = false;
            if (section_started_idx < current - step) {
              edge_chain_section_lengths.emplace_back(ivec2{section_started_idx, current - step});
            }
          }
        }

        current += step;
      }

      fingers.clear();
      for (auto& e : edge_chain_section_lengths) {
        const auto start_idx = e.x;
        const auto end_idx = e.y;

        float best_length_ratio;
        int best_length_ratio_idx;
        for (int i = start_idx; i < end_idx; i++) {

          const auto& p0_v = edge_chain_cpu[i % l];
          const auto& p1_v = edge_chain_cpu[(i + walk_steps) % l];
          const auto& p2_v = edge_chain_cpu[(i - walk_steps) % l];
          const auto mid = (p1_v + p2_v) / 2.f;

          const float length1 = glm::length(p0_v - mid);
          const float length2 = glm::length(p1_v - p2_v);

          const float length_ratio = length1 / length2;

          if (i == start_idx || length_ratio > best_length_ratio) {
            best_length_ratio = length_ratio;
            best_length_ratio_idx = i;
          }
        }

        bool found = false;
        int current_walk = 10;
        int walk_step = 2;
        bool counting = true;
        // int best_walk;
        float best_walk_length;
        float best_theta;

        const vec2 p0_v = edge_chain_cpu[best_length_ratio_idx % l].xy();

        while (counting) {

          const vec2 p1_v = edge_chain_cpu[(best_length_ratio_idx + current_walk) % l].xy();
          const vec2 p2_v = edge_chain_cpu[(best_length_ratio_idx - current_walk) % l].xy();
          const vec2 mid = (p1_v + p2_v) / 2.f;
          const vec2 mid_dir = glm::normalize(mid - p0_v);

          const float theta = atan2(mid_dir.y, mid_dir.x);

          const float length1 = glm::length(p0_v - mid);
          const float length2 = glm::length(p1_v - p2_v);

          const float length_ratio = length1 / length2;
          if (!found || length1 > best_walk_length) {
            best_walk_length = length1;
            best_theta = theta;
            found = true;
            current_walk += walk_step;
          } else {
            counting = false;
          }
        }

        counting = true;
        int start_current_length = 10;
        int current_length = 10;
        int length_step = 2;
        int last_sample_ortho_length;

        const vec2 offset_dir{cos(best_theta), sin(best_theta)};
        const vec2 ortho_offset_dir{cos(best_theta + float(M_PI_2)),
                                    sin(best_theta + float(M_PI_2))};

        int max_length_1 = 0;
        int max_length_2 = 0;

        while (counting) {

          const vec2 start_pos = p0_v + (offset_dir * (float)current_length);

          int count_1 = 0;
          int count_2 = 0;
          bool counting_1 = true;
          bool counting_2 = true;

          while (counting_1 || counting_2) {
            if (counting_1) {
              const vec2 check_pos_v = start_pos + (ortho_offset_dir * (float)count_1);
              const ivec2 check_pos{check_pos_v.x, check_pos_v.y};
              const auto& v = obj_cpu[check_pos.y * _d.x + check_pos.x];
              if (v) {
                count_1++;
              } else {
                counting_1 = false;
              }
            }

            if (counting_2) {
              const vec2 check_pos_v = start_pos - (ortho_offset_dir * (float)count_2);
              const ivec2 check_pos{check_pos_v.x, check_pos_v.y};
              const auto& v = obj_cpu[check_pos.y * _d.x + check_pos.x];
              if (v) {
                count_2++;
              } else {
                counting_2 = false;
              }
            }
          }

          int total_length = count_1 + count_2;

          if (current_length == start_current_length) {
            last_sample_ortho_length = total_length;
            current_length = current_length + 2;
          } else {
            int d_length = abs(last_sample_ortho_length - total_length);
            if (total_length < 2 || d_length > 15) {
              counting = false;
            } else {
              current_length = current_length + 2;
            }
          }

          if (counting) {
            if (count_1 > max_length_1) max_length_1 = count_1;
            if (count_2 > max_length_2) max_length_2 = count_2;
          }
        }

        const vec2 p1_v =
            p0_v + (vec2{cos(best_theta), sin(best_theta)} * float(current_length - length_step));

        const vec2 ortho_dir =
            vec2{cos(best_theta + float(M_PI_2)), sin(best_theta + float(M_PI_2))};

        const vec2 s = p1_v - (ortho_dir * float(max_length_2));

        float min_z = 0.;
        bool v_found = false;
        vec4 min_plane_coord{0., 0., 0., 0.};
        for (int c = 0; c < max_length_1 + max_length_2; c++) {
          const vec2 coord = s + (ortho_dir * (float)c);
          const ivec2 coord_i{coord.x, coord.y};
          const uint16_t v = obj_cpu[coord_i.y * _d.x + coord_i.x];
          if (v) {
            v_found = true;
            vec4 p{0., 0., 0., 1.};
            rs2_deproject_pixel_to_point(&p.x, &obj_intrinsics, &coord.x, float(v));
            p = ransac_plane.p.to_plane_coords * p;
            if (p.z < min_z) {
              min_z = p.z;
              min_plane_coord = p;
            }
          }
        }

        if (v_found) {

          const ivec2 p0_i{p0_v.x, p0_v.y};
          const uint16_t v0 = obj_cpu[p0_i.y * _d.x + p0_i.x];
          vec4 origin_coords_cam{0., 0., 0., 1.};
          rs2_deproject_pixel_to_point(&origin_coords_cam.x, &obj_intrinsics, &p0_v.x, v0 * 1.f);
          vec4 origin_coords_plane = ransac_plane.p.to_plane_coords * origin_coords_cam;

          vec3 origin_to_mid_dir =
              glm::normalize(vec3((min_plane_coord - origin_coords_plane).xyz()));

          origin_coords_plane = origin_coords_plane + vec4{(origin_to_mid_dir * 50.f), 0.f};

          const float o_height = min_plane_coord.z / 2.f;
          min_plane_coord.z = o_height;
          const float r1 = abs(o_height);
          const float r0 = r1 / 2.f;

          IdentifiedFinger f;
          f.knuckle_joints =
              vector<KnuckleJoint>{{origin_coords_plane, vec2{r0}}, {min_plane_coord, vec2{r1}}};

          const float trace_length = (float)(current_length - length_step);
          const float extend_back_length = 5.f; // pixels!

          vec2 p0_back = p0_v - (extend_back_length * vec2{cos(best_theta), sin(best_theta)});

          f.box.img_dims = obj.get_dims();
          f.box.origin_coords = p0_back;
          f.box.length = trace_length + extend_back_length;
          f.box.width1 = (float)(max_length_1);
          f.box.width2 = (float)(max_length_2);
          f.box.theta = best_theta;

          fingers.emplace_back(move(f));
        }
      }

      // generate tforms for debug drawing
      vector<mat4> tforms;
      vector<mat4> bbox_raster_tforms;

      for (auto& f : fingers) {
        f.add_tforms_raster(bbox_raster_tforms);
        f.add_box_tforms(tforms);
      }

      bbox_2d_g.num_meshes[0] = fingers.size();
      ray_mesh_g.mesh_tforms.set_data(tforms);
      bbox_2d_g.mesh_tforms.set_data(bbox_raster_tforms);
      bbox_2d_g.num_meshes[0] = bbox_raster_tforms.size();

      cylinder_mesh_g.num_meshes = {(unsigned int)fingers.size(), (unsigned int)(2 * fingers.size())};

      // for (int zzz = 0; zzz < fingers.size(); zzz++) {
      //   fingers[zzz].knuckle_joints[0].pos.z = -500.f;
      // }

      vector<vector<IdentifiedFinger>> best_fingers_per_iteration(1);
      best_fingers_per_iteration[0].resize(fingers.size());
      std::copy(fingers.begin(), fingers.end(), best_fingers_per_iteration[0].begin());

      vector<function<float*(IdentifiedFinger&)>> free_variable_addresses = {
          [](IdentifiedFinger& f) -> float* { return &f.knuckle_joints[0].pos.x; },
          [](IdentifiedFinger& f) -> float* { return &f.knuckle_joints[0].pos.y; },
          [](IdentifiedFinger& f) -> float* { return &f.knuckle_joints[0].pos.z; },
          [](IdentifiedFinger& f) -> float* { return &f.knuckle_joints[0].radius.x; },
          [](IdentifiedFinger& f) -> float* { return &f.knuckle_joints[0].radius.y; },
          [](IdentifiedFinger& f) -> float* { return &f.knuckle_joints[1].pos.x; },
          [](IdentifiedFinger& f) -> float* { return &f.knuckle_joints[1].pos.y; },
          [](IdentifiedFinger& f) -> float* { return &f.knuckle_joints[1].pos.z; },
          [](IdentifiedFinger& f) -> float* { return &f.knuckle_joints[1].radius.x; },
          [](IdentifiedFinger& f) -> float* { return &f.knuckle_joints[1].radius.y; }};
      // vector<float> test_change = {-50., -10, -2, 2, -10, 50};

      const ivec2 vp_dims{8, 8};
      const ivec2 render_fullsize = vp_dims * obj.get_dims();
      obj_render.set_storage(render_fullsize);
      depth_cam.cam_position = ransac_plane.p.from_plane_coords;
      depth_cam.cam_projection = glm::perspectiveFromIntrinsics(obj_intrinsics, {1., 50000.});

      if (!viewports_made) {
        make_viewports(obj.get_dims(), render_fullsize, vp_buff);
        viewports_made = true;
      }

      {
        // draw rasterized bounding boxes
        ray_mesh_cam.fbo.bind();

        GL::Renderer::setClearColor(0x000000_rgbf);

        finger_bbox_ids.set_storage(obj.get_dims());

        ray_mesh_cam.set_target(&finger_bbox_ids, nullptr, nullptr, nullptr);

        ray_mesh_cam.fbo.clear(GL::FramebufferClear::Color | GL::FramebufferClear::Depth);
        ray_mesh_cam.uint_render_mode = ShaderInstanced::UintRenderMode::MESH_ID;

        GL::Renderer::setPolygonMode(GL::Renderer::PolygonMode::Fill);

        bbox_2d_g.num_meshes[0] = fingers.size();
        ray_mesh_cam.draw_instanced_no_viewport(bbox_2d_g, 1, 0);

        ray_mesh_cam.clear_target();
      }

      for (int aaa = 0; aaa < 15; aaa++) {

        vector<vector<IdentifiedFinger>> tested_fingers;

        {
          vector<IdentifiedFinger> start_fingers(best_fingers_per_iteration[0].size());

          vector<IdentifiedFinger>& s_f =
              best_fingers_per_iteration[best_fingers_per_iteration.size() - 1];
          for (int i = 0; i < start_fingers.size(); i++) {
            start_fingers[i] = IdentifiedFinger(s_f[i]);
          }
          tested_fingers.emplace_back(std::move(start_fingers));
        }

        for (int a = 0; a < 60; a++) {

          // 10 - 3 - 1
          const float std_dev = a > 40 ? 20. : (a > 20 ? 3. : 1.);
          auto n = std::normal_distribution<float>(0., std_dev);

          vector<IdentifiedFinger> test_fingers;
          for (auto& f_orig : tested_fingers[0]) {
            IdentifiedFinger f(f_orig);

            for (auto& fn : free_variable_addresses) {
              float* v_addr = fn(f);
              const auto v_start = *v_addr;
              *v_addr = v_start + n(_g);
            }

            test_fingers.emplace_back(f);
          }
          tested_fingers.emplace_back(move(test_fingers));
        }

        vector<mat4> mesh_tforms_all;
        for (auto& fs : tested_fingers) {
          vector<mat4> c_tf;
          vector<mat4> s_tf;
          for (auto& f : fs) {
            f.add_mesh_tforms(c_tf, s_tf);
          }

          for (auto m : c_tf)
            mesh_tforms_all.emplace_back(m);
          for (auto m : s_tf)
            mesh_tforms_all.emplace_back(m);
        }

        cylinder_mesh_g.mesh_tforms.set_data(mesh_tforms_all);

        GL::Renderer::setPolygonMode(GL::Renderer::PolygonMode::Fill);

        depth_cam.fbo.bind();

        depth_cam.set_target(nullptr, &obj_render, nullptr, nullptr);
        depth_cam.fbo.clear(GL::FramebufferClear::Color | GL::FramebufferClear::Depth);

        depth_cam.draw_instanced_per_viewport(cylinder_mesh_g, 61, 0, vp_buff);

        depth_cam.clear_target();

        GL::defaultFramebuffer.bind();

        vector<float> all_costs;
        img_compare.gen_all_costs(
            obj, finger_bbox_ids, fingers.size(), obj_render, all_costs, vp_buff, 61);

        vector<float> best_costs(fingers.size());
        vector<int> best_costs_idx(fingers.size(), 0);

        for (int a = 0; a < 61; a++) {

          for (int b = 0; b < fingers.size(); b++) {
            const float c = all_costs[(a * fingers.size()) + b];
            if (!a || c < best_costs[b]) {
              best_costs[b] = c;
              best_costs_idx[b] = a;
            }
          }
        }

        vector<IdentifiedFinger> new_best_fingers;
        for (int a = 0; a < fingers.size(); a++) {
          new_best_fingers.emplace_back(IdentifiedFinger(tested_fingers[best_costs_idx[a]][a]));
        }

        best_fingers_per_iteration.emplace_back(std::move(new_best_fingers));
      }

      vector<mat4> best_fingers_tforms;
      for (auto& fs : best_fingers_per_iteration) {
        vector<mat4> c_tf;
        vector<mat4> s_tf;
        for (auto& f : fs) {
          f.add_mesh_tforms(c_tf, s_tf);
        }

        for (auto m : c_tf)
          best_fingers_tforms.emplace_back(m);
        for (auto m : s_tf)
          best_fingers_tforms.emplace_back(m);
      }

      cylinder_mesh_g.mesh_tforms.set_data(best_fingers_tforms);

      {
        // the final best position!
        // lines: ve4{p0.xy, p1.xy}
        vector<vec4> lines;
        vector<vec4> p1_top_points;
        vector<IdentifiedFinger>& best_fingers = *(best_fingers_per_iteration.end() - 1);
        for (auto& f : best_fingers) {
          // focusing on xy coords in plane space!
          lines.emplace_back(vec4{f.knuckle_joints[0].pos.xy(), f.knuckle_joints[1].pos.xy()});

          p1_top_points.emplace_back(
              f.knuckle_joints[1].pos - vec4{0., 0., (f.knuckle_joints[1].radius.y * 0.8f), 0.});
        }

        vec2 p0_sum{0., 0.}, p1_sum{0., 0.};

        for (auto& line : lines) {
          vec2 p0 = line.xy(), p1 = line.zw();
          p0_sum += p0;
          p1_sum += p1;
          printf("p0: %s, p1: %s\n", glm::to_string(p0).c_str(), glm::to_string(p1).c_str());

          // p1_top_points.emplace_back(vec4{})
        }

        vec2 p0_avg = p0_sum / (float)lines.size(), p1_avg = p1_sum / (float)lines.size();
        vec2 dir = p1_avg - p0_avg;

        // pop z point up a little bit!
        vec3 p2 = vec3{p1_avg + (dir * 2.f), -450.f};

        // :) time for some better variable names..
        IdentifiedFinger wrist;
        wrist.knuckle_joints.resize(2);
        wrist.knuckle_joints[0].pos = vec4{p1_avg + (dir * 1.5f), -250.f, 1.f};
        wrist.knuckle_joints[0].radius = vec2{250., 150.};
        wrist.knuckle_joints[1].pos = vec4{p1_avg + (dir * 6.f), -500.f, 1.f};
        wrist.knuckle_joints[1].radius = vec2{250., 150.};

        vector<mat4> cy_mesh, sp_mesh;
        wrist.add_mesh_tforms(cy_mesh, sp_mesh);

        vector<mat4> all_m;
        for (auto& m : cy_mesh)
          all_m.emplace_back(m);
        for (auto& m : sp_mesh)
          all_m.emplace_back(m);

        // vector<mat4> v = {glm::translate(p2) * glm::scale(vec3{100., 100., 100.})};
        box_mesh_g.mesh_tforms.set_data(all_m);

        vector<mat4> cube_points;
        for (auto p : p1_top_points) {
          cube_points.emplace_back(glm::translate(vec3{p.xyz()}) * glm::scale(vec3{20., 20., 20.}));
        }

        box_mesh_g_2.mesh_tforms.set_data(cube_points);

        printf("avg xy plane dir: %s\n", to_string(dir).c_str());

        // obj_cpu[3];
      }

      GL::defaultFramebuffer.bind();

      map_depth.run(finger_bbox_ids, finger_bbox_ids_c);
    }

    obj_vtxes.cu_copy_to(debug_mesh_g.meshes[0].positions);

    gen_triangles.run(
        obj_vtxes, debug_mesh_g.meshes[0].idxes, &debug_mesh_g.meshes[0].num_triangles);

    fit_regions_to_planes.to_vtx_colors_2(
        debug_mesh_g.meshes[0].vtx_colors,
        debug_mesh_g.meshes[0].positions,
        mat4{1.f},
        obj_vtxes.get_dims(),
        live_z_threshold,
        ransac_plane.ortho_map_points,
        ransac_plane.ortho_map_binary,
        ransac_plane.ortho_sampler);

    ray_mesh_cam.fbo.bind();

    GL::Renderer::setClearColor(0x000000_rgbf);
    GL::Renderer::setPolygonMode(GL::Renderer::PolygonMode::Line);
    GL::Renderer::disable(GL::Renderer::Feature::DepthTest);

    ray_mesh_cam.set_target(nullptr, nullptr, &obj_c, nullptr);
    ray_mesh_cam.render_mode = ShaderInstanced::ColorRenderMode::FULL_COLOR;
    ray_mesh_cam.color_out = {1., 1., 1., 1.};

    ray_mesh_g.num_meshes[0] = 5 /* num lines per debug finger */ * fingers.size();
    ray_mesh_cam.draw_instanced_no_viewport(ray_mesh_g, 1, 0);

    ray_mesh_cam.clear_target();

    last_processed_frame = current_frame;

    main_screen_cam.fbo.bind();

    GL::Renderer::enable(GL::Renderer::Feature::DepthTest);
    GL::Renderer::setDepthFunction(GL::Renderer::DepthFunction::Less);

    main_screen_cam.set_target(nullptr, nullptr, &dbg_out_img, nullptr);
    main_screen_cam.fbo.clear(GL::FramebufferClear::Color | GL::FramebufferClear::Depth);

    main_screen_cam.cam_position = main_screen_cam_pos.transform_inverse();

    main_screen_cam.render_mode = ShaderInstanced::ColorRenderMode::COLORED_VTX;
    main_screen_cam.draw_instanced_no_viewport(debug_mesh_g, 1, 0);

    main_screen_cam.render_mode = ShaderInstanced::ColorRenderMode::FULL_COLOR;
    main_screen_cam.color_out = vec4{0., 0.3, 0.9, 1.};
    main_screen_cam.draw_instanced_no_viewport(cylinder_mesh_g, 1, iter_num_render);

    main_screen_cam.color_out = vec4{0.7, 0.4, 0.3, 1.};
    main_screen_cam.draw_instanced_no_viewport(box_mesh_g, 1, 0);
    box_mesh_g_2.num_meshes[0] = 5;
    main_screen_cam.draw_instanced_no_viewport(box_mesh_g_2, 1, 0);
    //    main_screen_cam.draw_instanced_no_viewport(cylinder_mesh_g, 1, 0);

    main_screen_cam.clear_target();

    /*
    map_depth.run(obj_render, obj_render_c);


    ray_mesh_cam.fbo.bind();
    ray_mesh_cam.set_target(nullptr, nullptr, &obj_render_c, nullptr);
    ray_mesh_cam.render_mode = ShaderInstanced::ColorRenderMode::FULL_COLOR;
    ray_mesh_cam.color_out = {1., 1., 1., 1.};


    GL::Renderer::setPolygonMode(GL::Renderer::PolygonMode::Line);
    GL::Renderer::disable(GL::Renderer::Feature::DepthTest);

    ray_mesh_g.num_meshes[0] =
     5 // num lines per debug finger
        * fingers.size();
    for (int i = 0; i < 64; i++) {
      ray_mesh_cam.draw_instanced_per_viewport(ray_mesh_g, 1, 0, vp_buff, i);
    }

    ray_mesh_cam.clear_target();

    */
  }

private:
  static void gen_edge_coords_groups(
      const ivec2& img_dims,
      unsigned int num_edge_pixels,
      Buff<ivec4>& obj_edge_coords,
      vector<vector<vec4>>& edge_coords_groups,
      unsigned int* longest_group_idx) {

    int longest_group_size = 0;

    vector<ivec4> obj_edge_coords_cpu(num_edge_pixels);
    obj_edge_coords.cu_copy_to(obj_edge_coords_cpu.data(), num_edge_pixels);

    const auto get_idx = [img_dims](const ivec2& c) -> int { return c.y * img_dims.x + c.x; };

    vector<bool> seen(img_dims.x * img_dims.y, false);
    vector<int> edge_coords_idx(img_dims.x * img_dims.y, -1);
    for (int i = 0; i < num_edge_pixels; i++) {
      ivec2 c = obj_edge_coords_cpu[i].xy();
      edge_coords_idx[get_idx(c)] = i;
    }

    for (int i = 0; i < num_edge_pixels; i++) {
      std::stack<ivec2> to_follow;
      vector<vec4> current_group;

      bool counting = true;

      int start_i_idx;
      {
        ivec2 c = obj_edge_coords_cpu[i];
        start_i_idx = get_idx(c);
        if (seen[start_i_idx]) continue;
        to_follow.push(c);
      }

      while (!to_follow.empty()) {

        const ivec2 _c = to_follow.top();
        to_follow.pop();
        const int _i_idx = get_idx(_c);

        if (!current_group.empty() && _i_idx == start_i_idx) { counting = false; }

        if (seen[_i_idx]) continue;

        if (counting) current_group.emplace_back(vec4{_c.x, _c.y, 0., 0.});
        seen[_i_idx] = true;

        for (int j = 0; j < 9; j++) {
          if (j == 4) { continue; }
          ivec2 c_test{_c.x - 1 + (j % 3), _c.y - 1 + (j / 3)};
          if (c_test.x > 0 && c_test.y > 0 && c_test.x < img_dims.x && c_test.y < img_dims.y) {
            const int __i_idx = get_idx(c_test);
            const bool is_coord = edge_coords_idx[__i_idx] > -1;
            if (is_coord) {
              if (!seen[__i_idx] || __i_idx == start_i_idx) { to_follow.push(c_test); }
            }
          }
        }
      }

      if (current_group.size() > longest_group_size) {
        longest_group_size = current_group.size();
        *longest_group_idx = edge_coords_groups.size(); // idx before pushing!
      }

      edge_coords_groups.push_back(std::move(current_group));
    }
  }

  static mat4 make_tform_matrix(ivec2 img_dims, ivec2 p0, ivec2 p1) {
    const auto get_ndc = [](ivec2 img_dims, ivec2 p) -> vec2 {
      return vec2{(2.f * p.x / img_dims.x) - 1, (2.f * p.y / img_dims.y) - 1};
    };
    vec2 ndc_p0 = get_ndc(img_dims, p0);
    vec2 ndc_p1 = get_ndc(img_dims, p1);
    vec2 diff = (ndc_p1 - ndc_p0);
    const float theta = atan2(diff.y, diff.x);
    const float scale = glm::length(diff);

    return glm::translate(vec3{ndc_p0.xy(), 0.}) * glm::rotate(theta, vec3{0, 0, 1}) *
           glm::scale(vec3{scale, 1., 1.});
  };

  static mat4 make_tform_matrix(ivec2 img_dims, ivec2 p0, float theta, vec2 scale) {

    mat4 to_ndc = glm::translate(vec3{-1.f, -1.f, 0.}) *
                  glm::scale(vec3{2.f / img_dims.x, 2.f / img_dims.y, 1.});

    return to_ndc * glm::translate(vec3{p0.x, p0.y, 0.}) * glm::rotate(theta, vec3{0., 0., 1.}) *
           glm::scale(vec3{scale, 1.f});
  };

  // returns the grid of viewports on the large image
  static ivec2
  make_viewports(ivec2 viewport_dims, ivec2 large_image_dims, Engine::Gpu::Buff<vec4>& vp_buff) {

    ivec2 steps = large_image_dims / viewport_dims;
    const int max_steps = steps.x * steps.y;

    // generate viewports array for every possible viewport in the large image
    vector<vec4> vps(max_steps);
    for (int i = 0; i < max_steps; i++) {
      int step_x = i % steps.x;
      int step_y = i / steps.x;

      vec2 vp_low = {(step_x)*viewport_dims.x, (step_y)*viewport_dims.y};

      vec2 vp_high = {(step_x + 1) * viewport_dims.x, (step_y + 1) * viewport_dims.y};

      vps[i] = {vp_low, vp_high};
    }
    vp_buff.cu_copy_from(vps.data(), max_steps);

    return steps;
  }
};
