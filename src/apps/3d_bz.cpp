#include <engine/Engine.h>

#include "AutoCalibrate/AutoCalibrateDemo.hpp"

constexpr int WINDOW_W = 1080;
constexpr int WINDOW_H = 720;
// todo: verify that everything actually works on a 4.3 card - ideally 4.1 would work (mac!)
constexpr int _GL_MAJ = 4;
constexpr int _GL_MIN = 3;

const Engine::Gpu::GraphicsCtx::Cfg window_cfg{WINDOW_W, WINDOW_H, "3d-beats", _GL_MAJ, _GL_MIN};

int main(int argc, char** argv) {

  FileUtil::init(argc, argv);

  AppViewer viewer;
  viewer.set_window_config(&window_cfg);

  viewer.register_app(
      "Full system", [](Engine::Gpu::GraphicsCtx& c) { return new AutoCalibrateDemo(c); });

  viewer.run();

  return 0;
}
