#pragma once

#include <engine/Engine.h>

class MeshPrimitives {
public:
  class DebugForce {
  public:
    static void init(MeshObj& o) {
      auto& v = o.get_view();
      v.primitive = GL::MeshPrimitive::Lines;
      v.idxes_count = 2;
      vector<unsigned int> idxes{0, 1};
      v.idxes.set_data(idxes);

      auto& d = o.get_data();
      // By default, force points in all 3 directions.
      // scaling by the force applied will generate the force in the world!
      vector<vec4> vtxes{{-1., -1., -1., 1.}, {0., 0., 0., 1.}};
      d.vtx_set_size = 2;
      d.vtx_set_count = 1;
      d.vtxes.set_data(vtxes);

      const vec4 _black{0., 0., 0., 1.}, _orange{1., 0.92, 0., 1.};
      vector<vec4> colors{_black, _orange};
      d.vtx_colors.set_data(colors);
    }
  };

  class Axis {
  public:
    static void init(MeshObj& o) {

      auto& v = o.get_view();
      v.primitive = GL::MeshPrimitive::Lines;
      v.idxes_count = 6;
      vector<unsigned int> idxes{0, 1, 2, 3, 4, 5};
      v.idxes.set_data(idxes);

      auto& d = o.get_data();
      const vec4 z{0, 0, 0, 1};
      vector<vec4> vtxes{z, {100., 0, 0, 1}, z, {0, 100., 0, 1}, z, {0, 0, 100, 1}};
      d.vtx_set_size = 6;
      d.vtx_set_count = 1;
      d.vtxes.set_data(vtxes);

      const vec4 r{1., 0., 0., 1.}, g{0., 1., 0., 1.}, b{0., 0., 1., 1.};
      vector<vec4> colors{r, r, g, g, b, b};
      d.vtx_colors.set_data(colors);
    }
  };

  class Cube {
  public:
    static string PRIMITIVE_ID() { return "mesh-cube"; }

    static void init(MeshObj& o) {
      vector<vec4> vtxes;
      gen_vtx_set(vtxes, vec3{1., 1., 1.});
      vector<unsigned int> idxes{0, 1, 2, 2, 3, 0, 1, 5, 6, 6, 2, 1, 7, 6, 5, 5, 4, 7,
                                 4, 0, 3, 3, 7, 4, 4, 5, 1, 1, 0, 4, 3, 2, 6, 6, 7, 3};

      auto& d = o.get_data();
      d.vtx_set_count = 1;
      d.vtx_set_size = vtxes.size();
      d.vtxes.set_data(vtxes);

      auto& v = o.get_view();
      v.idxes_count = idxes.size();
      v.idxes.set_data(idxes);
      v.primitive = GL::MeshPrimitive::Triangles;
    }

    static void gen_vtx_set(vector<vec4>& all_vtxes, vec3 s) {
      vector<vec4> vtxes{
          {-0.5, -0.5, 0.5, 1.0},
          {0.5, -0.5, 0.5, 1.0},
          {0.5, 0.5, 0.5, 1.0},
          {-0.5, 0.5, 0.5, 1.0},
          {-0.5, -0.5, -0.5, 1.0},
          {0.5, -0.5, -0.5, 1.0},
          {0.5, 0.5, -0.5, 1.0},
          {-0.5, 0.5, -0.5, 1.0}};
      for (auto& v : vtxes) { all_vtxes.emplace_back(v * vec4{s, 1.f}); }
    }
  };

  class Hull_8 {
  public:
    static string PRIMITIVE_ID() { return "mesh-hull-8"; }

    static void init(MeshObj& o) {
      vector<unsigned int> idxes{0, 1, 2, 2, 3, 0, 1, 5, 6, 6, 2, 1, 7, 6, 5, 5, 4, 7,
                                 4, 0, 3, 3, 7, 4, 4, 5, 1, 1, 0, 4, 3, 2, 6, 6, 7, 3};

      auto& d = o.get_data();
      d.vtx_set_size = 8;

      auto& v = o.get_view();
      v.idxes_count = idxes.size();
      v.idxes.set_data(idxes);
      v.primitive = GL::MeshPrimitive::Triangles;
    }
  };

  class Hull_16 {
  public:
    static string PRIMITIVE_ID() { return "mesh-hull-16"; }

    static void init(MeshObj& o) {

      vector<unsigned int> idxes;
      get_hull_debug_idxes(idxes);

      auto& d = o.get_data();
      d.vtx_set_count = 1;
      d.vtx_set_size = 16;

      auto& v = o.get_view();
      v.idxes_count = idxes.size();
      v.idxes.set_data(idxes);
      v.primitive = GL::MeshPrimitive::Triangles;
    }

  private:
    static void get_hull_debug_idxes(vector<unsigned int>& idxes) {
      // Make idxes for debug mesh!
      const auto ring0 = [&idxes](int x) { idxes.emplace_back(x % 8); };
      const auto ring1 = [&idxes](int x) { idxes.emplace_back(8 + (x % 8)); };
      const auto r_idx = [&ring0, &ring1](pair<bool, int> a) {
        if (a.first) {
          ring1(a.second);
        }
        else {
          ring0(a.second);
        }
      };

      const auto t1 = [&r_idx](pair<bool, int> a, pair<bool, int> b, pair<bool, int> c) {
        r_idx(a);
        r_idx(b);
        r_idx(c);
      };

      for (int i = 0; i < 8; i++) {
        t1({ 0, i }, { 0, i + 1 }, { 1, i });
        t1({ 0, i + 1 }, { 1, i }, { 1, i + 1 });
      }

      /*

      for (int i = 0; i < 2; i++) {
        t1({ i, 0 }, { i, 1 }, { i, 9 });
        t1({ i, 1 }, { i, 8 }, { i, 9 });
        t1({ i, 1 }, { i, 2 }, { i, 8 });
        t1({ i, 2 }, { i, 7 }, { i, 8 });
        t1({ i, 2 }, { i, 3 }, { i, 7 });
        t1({ i, 3 }, { i, 6 }, { i, 7 });
        t1({ i, 3 }, { i, 4 }, { i, 6 });
        t1({ i, 4 }, { i, 5 }, { i, 6 });
      }

      */
    }
  };

  class Hull_20 {
  public:
    static string PRIMITIVE_ID() { return "mesh-hull-20"; }

    static void init(MeshObj& o) {

      vector<unsigned int> idxes;
      get_hull_debug_idxes(idxes);

      auto& d = o.get_data();
      d.vtx_set_count = 1;
      d.vtx_set_size = 20;

      auto& v = o.get_view();
      v.idxes_count = idxes.size();
      v.idxes.set_data(idxes);
      v.primitive = GL::MeshPrimitive::Triangles;
    }

  private:
    static void get_hull_debug_idxes(vector<unsigned int>& idxes) {
      // Make idxes for debug mesh!
      const auto ring0 = [&idxes](int x) { idxes.emplace_back(x % 10); };
      const auto ring1 = [&idxes](int x) { idxes.emplace_back(10 + (x % 10)); };
      const auto r_idx = [&ring0, &ring1](pair<bool, int> a) {
        if (a.first) {
          ring1(a.second);
        } else {
          ring0(a.second);
        }
      };

      const auto t1 = [&r_idx](pair<bool, int> a, pair<bool, int> b, pair<bool, int> c) {
        r_idx(a);
        r_idx(b);
        r_idx(c);
      };

      for (int i = 0; i < 10; i++) {
        t1({0, i}, {0, i + 1}, {1, i});
        t1({0, i + 1}, {1, i}, {1, i + 1});
      }

      for (int i = 0; i < 2; i++) {
        t1({i, 0}, {i, 1}, {i, 9});
        t1({i, 1}, {i, 8}, {i, 9});
        t1({i, 1}, {i, 2}, {i, 8});
        t1({i, 2}, {i, 7}, {i, 8});
        t1({i, 2}, {i, 3}, {i, 7});
        t1({i, 3}, {i, 6}, {i, 7});
        t1({i, 3}, {i, 4}, {i, 6});
        t1({i, 4}, {i, 5}, {i, 6});
      }
    }
  };
};