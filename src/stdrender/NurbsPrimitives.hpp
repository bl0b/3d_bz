#pragma once

#include <random>

class PosScale3D {
public:
  vec3 pos;
  vec2 scale;
};

namespace NurbsPrimitives {

static vec3 get_length_and_thetas(vec3 p0, vec3 p1) {
  vec3 diff = p1 - p0;
  const float l = glm::length(diff);
  const float z_theta = atan2(diff.y, diff.x);
  const float y_theta = atan2(-diff.z, glm::length(vec2{diff.xy()}));
  return vec3{l, y_theta, z_theta};
}

static mat4 get_center_tform(vec3 p0, vec3 p1) {

  vec3 _lyz = get_length_and_thetas(p0, p1);
  const float z_theta = _lyz.z;
  const float y_theta = _lyz.y;
  const vec3 avg_pos = (p0 + p1) / 2.f;

  return glm::translate(avg_pos) * glm::rotate(z_theta, vec3{0., 0., 1.}) *
         glm::rotate(y_theta, vec3{0., 1., 0.});
}

class Cylinder {
public:
  struct Config {
    vec2 scale0;
    vec2 scale1;
    float length;
  };

  static const string PRIMITIVE_ID() { return "cylinder"; };

  static void init(NurbsObj& o, ivec2 eval_dims) {
    o.s.degree_u = 1;
    o.s.degree_v = 3;
    o.s.knots_u = {0, 0, 1, 1};
    o.s.knots_v = {0, 0, 0, 0, 0.5, 0.5, 0.5, 1, 1, 1, 1};

    const ivec2 control_points_dims = {2, 7};
    // degree, knots, dims of control points are only thing that influences precomputed vals!
    o.gen_dims(eval_dims, control_points_dims);
  }

  static inline void
  gen_data(vector<vec4>& cp_sets, vector<mat4>& tforms, vec3 p0, vec3 p1, vec2 r0, vec2 r1) {
    gen_cp_set(cp_sets, r0, r1, glm::length(p0 - p1));
    tforms.emplace_back(get_center_tform(p0, p1));
  }

  static inline void gen_cp_set(vector<vec4>& cp_sets, Config& c) {
    gen_cp_set(cp_sets, c.scale0, c.scale1, c.length);
  }

  static inline void gen_cp_set(vector<vec4>& cp_sets, vec2 r0, vec2 r1, float x_scale) {
    const float ot = 1.f / 3.f; // one-third
    const float l = x_scale * 0.5f;
    // clang-format off
    auto control_points_weighted = vector<vec4>{
        vec4{-l, 0,          r0.x, 1},  vec4{l, 0,          r1.x, 1},
        vec4{-l, r0.y * 2,   r0.x, ot}, vec4{l, r1.y * 2,   r1.x, ot},
        vec4{-l, r0.y * 2,  -r0.x, ot}, vec4{l, r1.y * 2,  -r1.x, ot},
        vec4{-l, 0,         -r0.x, 1},  vec4{l, 0,         -r1.x, 1},
        vec4{-l, -r0.y * 2, -r0.x, ot}, vec4{l, -r1.y * 2, -r1.x, ot},
        vec4{-l, -r0.y * 2,  r0.x, ot}, vec4{l, -r1.y * 2,  r1.x, ot},
        vec4{-l, 0,          r0.x, 1},  vec4{l, 0,          r1.x, 1}
    };
    // clang-format on
    for (auto p : control_points_weighted) cp_sets.emplace_back(p);
  }
};

class HalfSphere {
public:
  static string PRIMITIVE_ID() { return "nurbs-half-sphere"; };

  static void init(NurbsObj& o, ivec2 eval_dims) {
    o.s.degree_u = 3;
    o.s.degree_v = 3;
    o.s.knots_u = {0, 0, 0, 0, 1, 1, 1, 1};
    o.s.knots_v = {0, 0, 0, 0, 1, 1, 1, 1};

    const ivec2 control_points_dims = {4, 4};
    o.gen_dims(eval_dims, control_points_dims);
  }

  static void gen_cp_set(vector<vec4>& cp_sets, vec3 r, mat4 bake_tform = mat4{1.f}) {

    // 2D array of control points using tinynurbs::array2<T> container
    // Example from geometrictools.com/Documentation/NURBSCircleSphere.pdf
    // one third, one ninth
    const float ot = 1.f / 3.f, on = 1.f / 9.f;

    const float r0 = r.x, r1 = r.y, r2 = r.z, r1_2 = r1 * 2.f, r2_4 = r2 * 4.f;

    // Dims: 4x4
    // clang-format off
    auto cp_set = vector<vec4>{
        vec4{0,    0,  r0, 1},  vec4{0,    0,     r0, ot}, vec4{0,     0,     r0, ot}, vec4{0,  0,     r0, 1},
        vec4{0, r1_2,  r0, ot}, vec4{r2_4, r1_2,  r0, on}, vec4{r2_4, -r1_2,  r0, on}, vec4{0, -r1_2,  r0, ot},
        vec4{0, r1_2, -r0, ot}, vec4{r2_4, r1_2, -r0, on}, vec4{r2_4, -r1_2, -r0, on}, vec4{0, -r1_2, -r0, ot},
        vec4{0,    0, -r0, 1},  vec4{0,    0,    -r0, ot}, vec4{0,     0,    -r0, ot}, vec4{0,  0,    -r0, 1}};
    // clang-format on

    for (auto cp : cp_set)
      // map based on tform matrix, without modifying w value (weight!)
      cp_sets.emplace_back(vec4{(bake_tform * glm::vec4{cp.xyz(), 1.f}).xyz(), cp.w});
  }
};

class CappedCylinder {
public:
  static void gen_data(
      vector<vec4>& cylinder_cps,
      vector<mat4>& cylinder_tforms,
      vector<vec4>& hsphere_cps,
      vector<mat4>& hsphere_tforms,
      PosScale3D ps0,
      PosScale3D ps1) {

    const mat4 origin = get_center_tform(ps0.pos, ps1.pos);
    const float l = glm::length(ps0.pos - ps1.pos);

    Cylinder::gen_cp_set(cylinder_cps, ps0.scale, ps1.scale, l);
    cylinder_tforms.emplace_back(origin);

    hsphere_tforms.emplace_back(origin);
    hsphere_tforms.emplace_back(origin);

    NurbsPrimitives::HalfSphere::gen_cp_set(
        hsphere_cps,
        vec3{ps0.scale, (ps0.scale.x + ps0.scale.y) / 2.f},
        glm::translate(vec3{-l / 2.f, 0., 0.}) * glm::scale(vec3{-1, 1, 1}));
    NurbsPrimitives::HalfSphere::gen_cp_set(
        hsphere_cps,
        vec3{ps1.scale, (ps1.scale.x + ps1.scale.y) / 2.f},
        glm::translate(vec3{l / 2.f, 0., 0.}));
  }
};

} // namespace NurbsPrimitives