//! #include "StdCameraBase.vert"

void main() {
    mat4 obj = get_obj_tform();
    int cp_set_offset = get_cp_set_offset();

    // ignore weight
    vec4 cp = vec4(control_pts[cp_set_offset + gl_VertexID].xyz, 1);

    if (color_mode == COLOR_MODE_VTX) {
        vtx_color = vtx_colors[cp_set_offset + gl_VertexID];
    }

    obj_pos_cam_coords = cam * obj * cp;
    vec4 raw_ndc = proj * obj_pos_cam_coords;
    write_gl_position(raw_ndc);

    vtx_obj_id = get_obj_id();
}
