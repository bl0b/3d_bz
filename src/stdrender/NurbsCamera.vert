//! #include "StdCameraBase.vert"

layout(binding = 3) buffer bases_u_buff { float bases_u[]; };
layout(binding = 4) buffer bases_v_buff { float bases_v[]; };
layout(binding = 5) buffer spans_u_buff { uint spans_u[]; };
layout(binding = 6) buffer spans_v_buff { uint spans_v[]; };

uniform ivec2 eval_dims;
uniform ivec2 deg;


vec4 get_control_pt(ivec2 c, int cp_offset) {
    return control_pts[cp_offset + (c.y * control_pts_dims.x + c.x)];
}

vec4 get_homog_control_pt(ivec2 c, int cp_offset) {
    vec4 pt = get_control_pt(c, cp_offset);
    pt.xyz *= pt.w;
    return pt;
}

vec4 get_evaluated_pt() {

    ivec2 uv = { gl_VertexID % (eval_dims.x), gl_VertexID / (eval_dims.x) };

    uint span_u = spans_u[uv.x];
    uint span_v = spans_v[uv.y];

    const int basis_u_offset = (deg.x + 1) * uv.x;
    const int basis_v_offset = (deg.y + 1) * uv.y;

    const int cp_set_offset = get_cp_set_offset();

    vec4 point = vec4(0, 0, 0, 0);
    for (int l = 0; l <= deg.y; l++) {
        vec4 temp = vec4(0, 0, 0, 0);
        for (int k = 0; k <= deg.x; k++) {
            int cx = int(span_u) - deg.x + k;
            int cy = int(span_v) - deg.y + l;
            vec4 pt = get_homog_control_pt(ivec2(cx, cy), cp_set_offset);
            temp += (bases_u[basis_u_offset + k] * pt);
        }
        point += (bases_v[basis_v_offset + l] * temp);
    }

    // de-homogenize!
    point = point / point.w;
    return point;
}

void main() {

    const mat4 obj = get_obj_tform();

    vec4 point = get_evaluated_pt();

    obj_pos_cam_coords = cam * obj * point;
    vec4 raw_ndc = proj * obj_pos_cam_coords;

    // Nurbs does not support per-vtx colors (yet!)
    vtx_color = vec4(1., 1., 1., 1.);

    write_gl_position(raw_ndc);

    vtx_obj_id = get_obj_id();
}
