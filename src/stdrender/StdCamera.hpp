#pragma once

#include <engine/Engine.h>

using namespace Engine::Gpu;
using namespace std;

#include "StdObj.hpp"
#include "StdFramebuffer.hpp"
#include "StdCameraBase.hpp"

class StdCamera : public StdCameraBase<AbstractMeshObj> {
public:
  StdCamera() : StdCameraBase() {
#include <generated/StdCameraBase_vert.hpp>
#include <generated/StdCamera_vert.hpp>
#include <generated/StdCamera_frag.hpp>

    auto vert = Engine::ProgramSource::from_many({StdCameraBase_vert, StdCamera_vert}, "vert");
    auto frag = Engine::ProgramSource::from_many({StdCamera_frag}, "frag");

    load(vert);
    load(frag);
    finish();
  }

  void __set_uniforms(AbstractMeshObj& o) override {}
};
