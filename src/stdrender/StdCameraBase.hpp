#pragma once

#include <engine/Engine.h>

using namespace Engine::Gpu;
using namespace std;

#include "StdObj.hpp"
#include "StdFramebuffer.hpp"

class IterateConfig {
public:
  unsigned int start_idx = 0;
  unsigned int instances_per = 0;
};

class DrawConfig {
public:
  unsigned int num_instances = 1;
  IterateConfig tform_cfg = IterateConfig();
  IterateConfig cp_set_cfg = IterateConfig();
  IterateConfig obj_id_cfg = IterateConfig();

  DrawConfig& num(unsigned int n) {
    num_instances = n;
    return *this;
  }

  DrawConfig& both(IterateConfig cfg) {
    tform_cfg = cfg;
    cp_set_cfg = cfg;
    return *this;
  }

  DrawConfig& tforms(IterateConfig cfg) {
    tform_cfg = cfg;
    return *this;
  }

  DrawConfig& cps(IterateConfig cfg) {
    cp_set_cfg = cfg;
    return *this;
  }

  DrawConfig& obj_ids(IterateConfig cfg) {
    obj_id_cfg = cfg;
    return *this;
  }

  bool draw_viewports = false;
  StdFramebufferSubViewports* vps = nullptr;
  IterateConfig vp_cfg = IterateConfig();

  DrawConfig& viewports(StdFramebufferSubViewports& v, IterateConfig cfg) {
    draw_viewports = true;
    vps = &v;
    vp_cfg = cfg;
    return *this;
  }
};

class DrawSharedCfg {
public:
  // number of worlds to draw
  unsigned int num_worlds = 1;

  unsigned int bodies_per_world = 1;
  unsigned int start_world = 0;
  unsigned int obj_bodies_per_world = 1;

  // one viewport, one world!
  StdFramebufferSubViewports* vps = nullptr;
  unsigned int start_vp = 0;
};

template <class T>
class StdCameraBase : public Engine::Gpu::ShaderBase {
public:
  StdCameraBase() {
    // Compile-time sanity check
    static_assert(
        std::is_base_of<AbstractMeshObj, T>::value,
        "Camera object not derived from AbstractMeshObj");
  }

  // these are currently not instanced!
  mat4 cam_proj = mat4{1.f}, cam_pos = mat4{1.f};

  virtual void __set_uniforms(T& o) = 0;

  // stuff specifically for the fragment shader
  // Color out mode (RGBA8)
  enum ColorMode : unsigned int {
    Full = 1,
    Vtx = 2,
  };
  ColorMode color_mode = ColorMode::Full;
  vec4 color_out = {1., 1., 1., 1.};

  void draw_shared_tforms(T& o, Buff<mat4>& tforms, DrawSharedCfg c) {
    o.get_data().vtxes.bind(2, Magnum::GL::Buffer::Target::ShaderStorage);
    tforms.bind(7, Magnum::GL::Buffer::Target::ShaderStorage);

    o.get_data().body_ids.bind(11, Magnum::GL::Buffer::Target::ShaderStorage);
    set_uniform("shared_tforms_mode", (unsigned int)1);
    set_uniform("start_world", c.start_world);
    set_uniform("bodies_per_world", c.bodies_per_world);
    set_uniform("obj_bodies_per_world", c.obj_bodies_per_world);

    set_uniform("vtx_set_size", o.get_data().vtx_set_size);

    set_uniform("cam", cam_pos);
    set_uniform("proj", cam_proj);

    if (c.vps != nullptr) {
      set_uniform("custom_viewports", true);
      set_uniform("instances_per_viewport", c.obj_bodies_per_world);
      set_uniform("start_viewport_idx", c.start_vp);
      set_uniform("full_img_dims", c.vps->full_dims);
      c.vps->vp_buff.bind(9, Magnum::GL::Buffer::Target::ShaderStorage);
    } else {
      set_uniform("custom_viewports", false);
    }

    // frag shader stuff
    set_uniform("color_mode", color_mode);
    switch (color_mode) {
    case ColorMode::Full:
      set_uniform("c", color_out);
      break;
    case ColorMode::Vtx:
      o.get_data().vtx_colors.bind(10, Magnum::GL::Buffer::Target::ShaderStorage);
      break;
    }

    __set_uniforms(o);

    o.get_view().setup(c.num_worlds * c.obj_bodies_per_world).draw(*this);
  }

  void draw(T& o, DrawConfig draw_cfg = DrawConfig()) {

    o.get_data().vtxes.bind(2, Magnum::GL::Buffer::Target::ShaderStorage);
    o.get_data().tforms.bind(7, Magnum::GL::Buffer::Target::ShaderStorage);

    set_uniform("shared_tforms_mode", (unsigned int)0);

    set_uniform("tform_start_idx", draw_cfg.tform_cfg.start_idx);

    set_uniform("cp_set_start_idx", draw_cfg.cp_set_cfg.start_idx);
    set_uniform("instances_per_cp_set", draw_cfg.cp_set_cfg.instances_per);
    set_uniform("cp_set_mod", o.get_data().vtx_set_count);
    set_uniform("vtx_set_size", o.get_data().vtx_set_size);

    set_uniform("cam", cam_pos);
    set_uniform("proj", cam_proj);

    if (draw_cfg.draw_viewports) {
      set_uniform("custom_viewports", true);
      set_uniform("instances_per_viewport", draw_cfg.vp_cfg.instances_per);
      set_uniform("start_viewport_idx", draw_cfg.vp_cfg.start_idx);
      set_uniform("full_img_dims", draw_cfg.vps->full_dims);

      draw_cfg.vps->vp_buff.bind(9, Magnum::GL::Buffer::Target::ShaderStorage);
    } else {
      set_uniform("custom_viewports", false);
    }

    // frag shader stuff
    set_uniform("color_mode", color_mode);
    switch (color_mode) {
    case ColorMode::Full:
      set_uniform("c", color_out);
      break;
    case ColorMode::Vtx:
      o.get_data().vtx_colors.bind(10, Magnum::GL::Buffer::Target::ShaderStorage);
      break;
    }

    __set_uniforms(o);

    o.get_view().setup(draw_cfg.num_instances).draw(*this);
  }
};
