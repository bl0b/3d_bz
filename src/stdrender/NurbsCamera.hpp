#pragma once

#include <engine/Engine.h>

using namespace Engine::Gpu;
using namespace std;

#include "StdObj.hpp"
#include "StdFramebuffer.hpp"
#include "StdCameraBase.hpp"

class NurbsShader : public StdCameraBase<NurbsObj> {
public:
  NurbsShader() : StdCameraBase() {
#include <generated/StdCameraBase_vert.hpp>
#include <generated/StdCamera_frag.hpp>
#include <generated/NurbsCamera_vert.hpp>

    auto vert = Engine::ProgramSource::from_many({StdCameraBase_vert, NurbsCamera_vert}, "vert");
    auto frag = Engine::ProgramSource::from_many({StdCamera_frag}, "frag");

    load(vert);
    load(frag);
    finish();
  }

  void __set_uniforms(NurbsObj& o) override {
    o.bases_u.bind(3, Magnum::GL::Buffer::Target::ShaderStorage);
    o.bases_v.bind(4, Magnum::GL::Buffer::Target::ShaderStorage);
    o.spans_u.bind(5, Magnum::GL::Buffer::Target::ShaderStorage);
    o.spans_v.bind(6, Magnum::GL::Buffer::Target::ShaderStorage);

    set_uniform("eval_dims", o.eval_dims);
    set_uniform("deg", ivec2{o.s.degree_u, o.s.degree_v});
    set_uniform("control_pts_dims", o.cp_dims);
  }
};
