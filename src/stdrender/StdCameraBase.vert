#version 450 core

// Shared code for use in different (similar) shader programs
uniform mat4 cam;
uniform mat4 proj;

// Code for custom viewports
uniform bool custom_viewports;
uniform uint instances_per_viewport;
uniform uint start_viewport_idx;
layout(binding = 9) buffer viewports_buff { vec4 viewports[]; };
uniform ivec2 full_img_dims;
out vec3 norm_ndc;// ndc after dividing by w

out vec4 obj_pos_cam_coords;

void write_gl_position(vec4 raw_ndc) {
    if (custom_viewports) {
        // correct rendered position to new viewort in NDC coordinates
        norm_ndc = raw_ndc.xyz / raw_ndc.w;
        const uint vp_id = start_viewport_idx + (gl_InstanceID / instances_per_viewport);
        vec4 vp = viewports[vp_id];
        vec2 vp_min = vp.xy, vp_max = vp.zw, vp_size = vp_max - vp_min;
        gl_Position = vec4(((((vp_min + (((norm_ndc.xy + 1.) / 2) * vp_size)) / full_img_dims) * 2) - 1) * raw_ndc.w, raw_ndc.zw);
    } else {
        gl_Position = raw_ndc;
    }
}

uniform uint shared_tforms_mode;
uniform uint start_world;
uniform uint bodies_per_world;
uniform uint obj_bodies_per_world;
// uniform uint vtx_set_size; // declared below!

// Code for handling control points / vtxes
uniform uint vtx_set_size;
uniform ivec2 control_pts_dims;
uniform uint cp_set_start_idx;
uniform uint instances_per_cp_set;
uniform uint cp_set_mod;
layout(binding = 2) buffer control_pts_buff { vec4 control_pts[]; };




// Code for colors!
const uint COLOR_MODE_FULL = 1;
const uint COLOR_MODE_VTX = 2;

uniform uint color_mode;
layout(binding = 10) buffer vtx_colors_buff { vec4 vtx_colors[]; };
out vec4 vtx_color;

out flat uint vtx_obj_id;
layout(binding = 11) buffer vtx_obj_id_buff { uint obj_ids[]; };

// Code for handling per-instance object tform matrices
uniform uint tform_start_idx;
//uniform uint tform_mod;
layout(binding = 7) buffer tforms_buff { mat4 tforms[]; };

mat4 get_obj_tform() {
    if (shared_tforms_mode == 1) {
        uint w_idx = start_world + (gl_InstanceID / obj_bodies_per_world);
        uint local_body_id = obj_ids[gl_InstanceID % obj_bodies_per_world] - 1;
        uint tform_idx = (w_idx * bodies_per_world) + local_body_id;
        return tforms[tform_idx];
    } else {
        uint tform_idx = tform_start_idx + gl_InstanceID;
        return tforms[tform_idx];
    }
}

int get_cp_set_offset() {
    if (shared_tforms_mode == 1) {
        uint cp_set_idx = (start_world * obj_bodies_per_world) + gl_InstanceID;
        return int(cp_set_idx * vtx_set_size);
    } else {
        int cp_set_size = int(vtx_set_size);
        int cp_set_idx = int(cp_set_start_idx) + int(instances_per_cp_set > 0 ? gl_InstanceID / instances_per_cp_set : 0);
        if (cp_set_mod > 0) {
            cp_set_idx = cp_set_idx % int(cp_set_mod);
        }
        return cp_set_idx * cp_set_size;
    }
}

uint get_obj_id() { 
    if (shared_tforms_mode == 1) return obj_ids[gl_InstanceID % obj_bodies_per_world];
    return 0;
}
