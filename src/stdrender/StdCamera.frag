#version 450 core

layout(location = 0) out vec4 rgba_out;
layout(location = 1) out uvec2 uint_out;

const uint COLOR_MODE_FULL = 1;
const uint COLOR_MODE_VTX = 2;

uniform uint color_mode;
uniform vec4 c;
in vec4 vtx_color;

in flat uint vtx_obj_id;

in vec4 obj_pos_cam_coords;

uniform bool custom_viewports;
in vec3 norm_ndc;

void main() {
    if (custom_viewports) {
        if (norm_ndc.x < -1. || norm_ndc.x > 1. || norm_ndc.y < -1. || norm_ndc.y > 1.) {
            discard;
        }
    }

    switch (color_mode) {
        case COLOR_MODE_FULL:
        rgba_out = c;
        break;

        case COLOR_MODE_VTX:
        rgba_out = vtx_color;
        break;
    }

    uint_out = uvec2(
        // r: depth value
        uint(abs(int(obj_pos_cam_coords.z))),
        // g: mesh ID
        vtx_obj_id);
}
