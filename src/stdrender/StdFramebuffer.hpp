#pragma once

class StdFramebuffer {
public:
  enum FragLayout { rgba8 = 0, rg16ui = 1, rgba32f = 2 };

  Magnum::GL::Framebuffer fbo{Magnum::NoCreate};
  Magnum::GL::Renderbuffer depth_stencil;

  StdFramebuffer() {
    fbo = move(GL::Framebuffer({{0, 0}, {0, 0}}));
    fbo.mapForDraw({{FragLayout::rgba8, GL::Framebuffer::ColorAttachment(FragLayout::rgba8)},
                    {FragLayout::rg16ui, GL::Framebuffer::ColorAttachment(FragLayout::rg16ui)},
                    {FragLayout::rgba32f, GL::Framebuffer::ColorAttachment(FragLayout::rgba32f)}});
  }

  class StdTargetConfig {
  public:
    Tex<TF::RG16UI>* tex_rg16ui = nullptr;
    Tex<TF::RGBA8>* tex_rgba8 = nullptr;
    Tex<TF::RGBA32F>* tex_rgba32f = nullptr;

    StdTargetConfig& add(Tex<TF::RG16UI>* t) {
      tex_rg16ui = t;
      return *this;
    }

    StdTargetConfig& add(Tex<TF::RGBA8>* t) {
      tex_rgba8 = t;
      return *this;
    }

    StdTargetConfig& add(Tex<TF::RGBA32F>* t) {
      tex_rgba32f = t;
      return *this;
    }

    void attach_to_fbo(Magnum::GL::Framebuffer& f) {
      if (tex_rgba8 != nullptr) {
        f.attachTexture(GL::Framebuffer::ColorAttachment(FragLayout::rgba8), tex_rgba8->get(), 0);
      }
      if (tex_rg16ui != nullptr) {
        f.attachTexture(GL::Framebuffer::ColorAttachment(FragLayout::rg16ui), tex_rg16ui->get(), 0);
      }
      if (tex_rgba32f != nullptr) {
        f.attachTexture(
            GL::Framebuffer::ColorAttachment(FragLayout::rgba32f), tex_rgba32f->get(), 0);
      }
    }

    ivec2 verify_and_get_dims() {
      vector<ivec2> dims;
      push_dim_if_present(tex_rg16ui, dims);
      push_dim_if_present(tex_rgba8, dims);
      push_dim_if_present(tex_rgba32f, dims);
      if (dims.empty()) {
        printf("no texture provided!");
        throw invalid_argument("no texture provided!");
      }
      ivec2 d_start = dims[0];
      for (int i = 1; i < dims.size(); i++) {
        if (dims[i].x != d_start.x || dims[i].y != d_start.y) {
          printf("dims mismatch!\n");
          throw invalid_argument("dims mismatch");
        }
      }
      return d_start;
    }

    void detach_from_fbo(Magnum::GL::Framebuffer& f) {
      if (tex_rgba8 != nullptr) {
        f.detach(GL::Framebuffer::ColorAttachment(FragLayout::rgba8));
      }
      if (tex_rg16ui != nullptr) {
        f.detach(GL::Framebuffer::ColorAttachment(FragLayout::rg16ui));
      }
      if (tex_rgba32f != nullptr) {
        f.detach(GL::Framebuffer::ColorAttachment(FragLayout::rgba32f));
      }
    }

  private:
    template <TF _tf>
    void push_dim_if_present(Tex<_tf>* t, vector<ivec2>& dims) {
      if (t != nullptr) dims.emplace_back(t->get_dims());
    }
  };

  StdTargetConfig current_cfg;

  void set_target(StdTargetConfig cfg, bool clear_images = false) {
    current_cfg = cfg;
    ivec2 d = cfg.verify_and_get_dims();
    depth_stencil.setStorage(RenderbufferFormat::DepthStencil, {d.x, d.y});
    fbo.setViewport({{0, 0}, {d.x, d.y}});
    fbo.attachRenderbuffer(GL::Framebuffer::BufferAttachment::DepthStencil, depth_stencil);
    cfg.attach_to_fbo(fbo);
    Engine::Gpu::GlUtil::verify_framebuffer_complete(fbo);
    fbo.bind();

    if (clear_images) {
      GL::Renderer::setClearColor(0x000000_rgbf);
      fbo.clear(GL::FramebufferClear::Color | GL::FramebufferClear::Depth);
    }
  }

  void clear_target() {
    current_cfg.detach_from_fbo(fbo);
    GL::defaultFramebuffer.bind();
  }
};

class StdFramebufferSubViewports {

  bool _init = false;
  int num_vps;

public:
  ivec2 vp_dims, full_dims;
  Engine::Gpu::Buff<vec4> vp_buff;

  bool enabled() {
    return _init;
  }

  void init(ivec2 _vp_dims, ivec2 _full_dims) {
    num_vps = make_viewports(_vp_dims, _full_dims, vp_buff);
    vp_dims = _vp_dims;
    full_dims = _full_dims;
    _init = true;
  }

  // returns the grid of viewports on the large image
  static int
  make_viewports(ivec2 viewport_dims, ivec2 large_image_dims, Engine::Gpu::Buff<vec4>& vp_buff) {

    ivec2 steps = large_image_dims / viewport_dims;
    const int max_steps = steps.x * steps.y;

    // generate viewports array for every possible viewport in the large image
    vector<vec4> vps(max_steps);
    for (int i = 0; i < max_steps; i++) {
      int step_x = i % steps.x;
      int step_y = i / steps.x;

      vec2 vp_low = {(step_x)*viewport_dims.x, (step_y)*viewport_dims.y};

      vec2 vp_high = {(step_x + 1) * viewport_dims.x, (step_y + 1) * viewport_dims.y};

      vps[i] = {vp_low, vp_high};
    }
    vp_buff.cu_copy_from(vps.data(), max_steps);

    return max_steps;
  }

};