#pragma once

#include <tinynurbs/tinynurbs.h>

// Most primitive mesh possible!
// Just idxes and count defining a draw invocation
class ObjView {
public:
  GL::Mesh m;
  Buff<unsigned int> idxes;
  unsigned int idxes_count = 0;
  GL::MeshPrimitive primitive = GL::MeshPrimitive::Triangles;

  GL::Mesh& setup(unsigned int n = 1) {
    return m.setPrimitive(primitive)
        .setCount(idxes_count)
        .setIndexBuffer(idxes.get_gl(), 0, MeshIndexType::UnsignedInt)
        .setInstanceCount(n);
  }
};

// Holder of one or many copies of data to be drawn according to idxes/primitive combo of an ObjView
class ObjData {
public:
  Buff<vec4> vtxes;
  unsigned int vtx_set_size = 0, vtx_set_count = 1;

  Buff<mat4> tforms;
  unsigned int tforms_count = 1;

  // Optional body ids buffer. if used, must be parallel with tforms
  Buff<unsigned int> body_ids;

  // Optional colors buffer. if used, must match vtxes
  Buff<vec4> vtx_colors;
//  unsigned int colors_set_size = 0, colors_set_count = 1;

  // convenience method to set tforms buffer to single tform
  void set_tform(mat4 t) {
    vector<mat4> ts = {t};
    set_tforms(ts);
  }

  void set_tforms(vector<mat4>& ts) {
    tforms.set_data(ts);
    tforms_count = ts.size();
  }

};

class AbstractMeshObj {
public:
  virtual ObjData& get_data() = 0;
  virtual ObjView& get_view() = 0;
};

// Simple implementation of mesh obj interface, for when data and view components already exist
// somewhere else, and the mesh needs to come together accordingly
class MeshObjPointers : public AbstractMeshObj {
private:
  ObjData& d;
  ObjView& v;

public:
  explicit MeshObjPointers(ObjData& _d, ObjView& _v) : d(_d), v(_v) {}
  ObjData& get_data() override { return d; }
  ObjView& get_view() override { return v; }
};

class MeshObj : public AbstractMeshObj {
private:
  ObjData d;
  ObjView v;

public:
  ObjData& get_data() override { return d; }
  ObjView& get_view() override { return v; }
};

// Holds precomputed NURBS data.
// This configuration is generated from the degree and knot vector configuration of the surface,
// for a specific evaluation dimension.
// This allows the points to be computed quickly and repeatedly on the GPU given buffers holding
// control points and weights (or just one buffer with homogenized coordinates?)
class NurbsObj : public AbstractMeshObj {
public:
  tinynurbs::RationalSurface3f s;

  // mesh dims for which surface is to be evaluated
  ivec2 eval_dims;
  // dimensions of control points grid
  ivec2 cp_dims;
  // 1d arrays: [num_u/v]
  Buff<unsigned int> spans_u, spans_v;

  // 2d arrays: [num_u/v][degree_u/v + 1]
  Buff<float> bases_u, bases_v;

  // vtxes buffer contains weighted control points
  ObjData data;

  void set_cp_sets(vector<vec4>& cp_sets) {
    data.vtxes.set_data(cp_sets);
    data.vtx_set_count =
        cp_sets.size() / (cp_dims.x * cp_dims.y);
  }

  // 2 idx buffers, 1st for evaluating nurbs control points to draw full object, 2nd to just draw
  // control points.
  ObjView nurbs_view, cp_debug_view;

  ObjData& get_data() override { return data; }

  ObjView& get_view() override { return nurbs_view; }

  MeshObjPointers cp_mesh = MeshObjPointers(data, cp_debug_view);

  NurbsObj() { cp_debug_view.primitive = GL::MeshPrimitive::Lines; }

  void gen_dims(ivec2 _eval_dims, ivec2 _cp_dims) {
    eval_dims = _eval_dims;
    cp_dims = _cp_dims;
    data.vtx_set_size = cp_dims.x * cp_dims.y;

    s.control_points.resize(_cp_dims.x, _cp_dims.y);
    s.weights.resize(_cp_dims.x, _cp_dims.y);

    if (!tinynurbs::surfaceIsValid(s)) { printf("tinynurbs says surface invalid!!! beware!!\n"); }

    vector<unsigned int> __spans_u, __spans_v;
    vector<float> __bases_u, __bases_v;

    for (int u_i = 0; u_i < _eval_dims.x; u_i++) {
      const float u = u_i * 1.f / (_eval_dims.x - 1);
      int span_u = tinynurbs::findSpan(s.degree_u, s.knots_u, u);
      __spans_u.push_back(span_u);

      vector<float> Nu = tinynurbs::bsplineBasis(s.degree_u, span_u, s.knots_u, u);
      for (auto N : Nu)
        __bases_u.push_back(N);
    }

    for (int v_i = 0; v_i < _eval_dims.y; v_i++) {
      const float v = v_i * 1.f / (_eval_dims.y - 1);
      int span_v = tinynurbs::findSpan(s.degree_v, s.knots_v, v);
      __spans_v.push_back(span_v);

      vector<float> Nv = tinynurbs::bsplineBasis(s.degree_v, span_v, s.knots_v, v);
      for (auto N : Nv)
        __bases_v.push_back(N);
    }

    bases_u.set_data(__bases_u);
    bases_v.set_data(__bases_v);
    spans_u.set_data(__spans_u);
    spans_v.set_data(__spans_v);

    vector<unsigned int> __idxes;
    for (int v = 0; v < eval_dims.y - 1; v++) {
      for (int u = 0; u < eval_dims.x - 1; u++) {
        const unsigned int i0 = v * _eval_dims.x + u;
        const unsigned int i1 = i0 + 1;
        const unsigned int i2 = i1 + _eval_dims.x;
        const unsigned int i3 = i2 - 1;
        __idxes.emplace_back(i0);
        __idxes.emplace_back(i1);
        __idxes.emplace_back(i2);
        __idxes.emplace_back(i0);
        __idxes.emplace_back(i2);
        __idxes.emplace_back(i3);
      }
    }

    nurbs_view.idxes.set_data(__idxes);
    nurbs_view.idxes_count = __idxes.size();

    vector<unsigned int> __cp_idxes;
    for (int v = 0; v < cp_dims.y - 1; v++) {
      for (int u = 0; u < cp_dims.x - 1; u++) {
        const unsigned int i0 = v * cp_dims.x + u;
        const unsigned int i1 = i0 + 1;
        const unsigned int i2 = i0 + cp_dims.x;
        const unsigned int i3 = i2 + 1;

        __cp_idxes.emplace_back(i0);
        __cp_idxes.emplace_back(i1);
        __cp_idxes.emplace_back(i0);
        __cp_idxes.emplace_back(i2);

        if (u == cp_dims.x - 2) {
          __cp_idxes.emplace_back(i1);
          __cp_idxes.emplace_back(i3);
        }

        if (v == cp_dims.y - 2) {
          __cp_idxes.emplace_back(i2);
          __cp_idxes.emplace_back(i3);
        }
      }
    }

    cp_debug_view.idxes_count = __cp_idxes.size();
    cp_debug_view.idxes.set_data(__cp_idxes);
  }

};
