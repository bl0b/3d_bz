#version 450 core

// x and y coordinates on original depth image - defines direction of
layout(location = 0) in vec4 pos;
layout(location = 1) in vec4 vtx_color;
// layout(location = 1) in mat4 pos_tform_2;

layout(binding = 3) buffer tf_buff { mat4 tfs[]; };
layout(binding = 4) buffer vp_buff { vec4 vps[]; };

layout(binding = 5) buffer local_tf_map_buff { uint local_tf_map[]; };

uniform mat4 cam_projection;
uniform mat4 cam_position;

uniform float max_depth;

uniform bool viewport_filter;
uniform uint viewport_id_offset;

uniform uint num_local_tforms;
uniform uint tforms_per_instance;
uniform uint meshes_per_instance;
uniform uint instance_start_idx;
uniform uint tform_full_stride;
uniform uint tform_local_offset;

uniform ivec2 full_screen_dims;
out vec2 v_orig_ndc;

out vec4 v_pos_cam_space;
out vec4 v_pos_world_space;
out noperspective float v_z_pos;

out vec4 v_vtx_color;

flat out uint mesh_id;

void main() {

    const uint instance_id = uint(gl_InstanceID) / meshes_per_instance;
    const uint local_id = uint(gl_InstanceID) % meshes_per_instance;


    const uint local_mesh_id = tform_local_offset + (local_id * num_local_tforms);
    uint local_tform_id = local_mesh_id;

    if (num_local_tforms > 1) {
        local_tform_id += local_tf_map[gl_VertexID];
    }

    const uint tform_id = ((instance_id + instance_start_idx) * tform_full_stride) + local_tform_id;

    const vec4 pos_world_space = tfs[tform_id] * pos;
    const vec4 pos_cam_space = cam_position * pos_world_space;
    vec4 start_ndc = cam_projection * pos_cam_space;

    // correct rendered position to new viewort in NDC coordinates
    if (viewport_filter) {

        // does this line cause bugs?
        start_ndc /= start_ndc.w;

        vec4 vp = vps[instance_id + viewport_id_offset];
        vec2 vp_min = vp.xy;
        vec2 vp_max = vp.zw;

        v_orig_ndc = start_ndc.xy;

        vec2 start_out_pixel = (start_ndc.xy + 1.) / 2.;
        vec2 end_out_pixel =
        (vp_min / full_screen_dims) + (start_out_pixel * (vp_max - vp_min) / full_screen_dims);
        gl_Position = vec4((2. * end_out_pixel) - 1, start_ndc.zw);
    } else {
        gl_Position = start_ndc;
    }

    v_pos_world_space = pos_world_space;
    v_pos_cam_space = pos_cam_space;
    v_z_pos = pos_cam_space.z;

    v_vtx_color = vtx_color;

    mesh_id = local_mesh_id;
}
