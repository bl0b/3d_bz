#version 450 core

in vec4 v_pos_cam_space;
in vec4 v_pos_world_space;
in noperspective float v_z_pos;

in vec4 v_vtx_color;

uniform int color_render_mode;
const int COLOR_RENDER_MODE_COLORED_VTX = 1;
const int COLOR_RENDER_MODE_FULL_COLOR = 2;

uniform int uint_render_mode;
const int UINT_RENDER_MODE_BINARY = 1;
const int UINT_RENDER_MODE_MESH_ID = 2;
//uniform uint uint_mesh_id;
flat in uint mesh_id;

uniform bool draw_height;

uniform bool viewport_filter;
in vec2 v_orig_ndc;

uniform vec4 draw_color_out;

layout(location = 0) out uint binary_out;
layout(location = 1) out uint depth_out;
layout(location = 2) out vec4 color_out;
layout(location = 3) out vec4 vtxes_out;

void main() {

  if (viewport_filter) {
    if (v_orig_ndc.x < -1. || v_orig_ndc.x > 1. || v_orig_ndc.y < -1. || v_orig_ndc.y > 1.) {
      discard;
      return;
    }
  }

  switch (uint_render_mode) {
  case UINT_RENDER_MODE_BINARY:
    binary_out = 1;
    break;
  case UINT_RENDER_MODE_MESH_ID:
    // +1 to distinguish from no fragment present!
    binary_out = mesh_id + 1;
    break;
  }

  depth_out = uint(abs(v_z_pos));

  switch (color_render_mode) {
  case COLOR_RENDER_MODE_COLORED_VTX:
    color_out = v_vtx_color;
    break;
  case COLOR_RENDER_MODE_FULL_COLOR:
    color_out = draw_color_out;
    break;
  }
  // color_out = draw_color_out;
  vtxes_out = v_pos_cam_space;
}
