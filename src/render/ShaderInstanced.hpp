#pragma once

#include <engine/Engine.h>

namespace glm {

mat4 ortho(vec3 min, vec3 max) {
  return glm::translate(vec3{-1., -1., -1.}) * glm::scale(2.f / (max - min)) *
         glm::translate(-1.f * min);
}

// https://stackoverflow.com/questions/22064084/how-to-create-perspective-projection-matrix-given-focal-points-and-camera-princ
mat4 perspectiveFromIntrinsics(vec2 f, vec2 pp, vec2 img_dims, vec2 z_range) {
  const float fx = f.x;
  const float fy = f.y;
  const float w = img_dims.x * 1.f;
  const float h = img_dims.y * 1.f;
  const float cx = pp.x;
  const float cy = pp.y;
  const float _far = z_range.y;
  const float zmax = z_range.y;
  const float _near = z_range.x;
  const float zmin = z_range.x;

  /*
  return mat4{2 * fx / w,
              0,
              0,
              0,
              0,
              2 * fy / h,
              0,
              0,
              2 * (cx / w) - 1,
              2 * (cy / h) - 1,
              -(_far + _near) / (_far - _near),
              -1,
              0,
              0,
              -2 * _far * _near / (_far - _near),
              0};
              */

  // BConic answer:
  return mat4{2 * fx / w,
              0,
              0,
              0,
              0,
              // 2 * s / w,
              2 * fy / h,
              0,
              0,
              2 * (cx / w) - 1,
              2 * (cy / h) - 1,
              (zmax + zmin) / (zmax - zmin),
              1,
              0,
              0,
              2 * zmax * zmin / (zmin - zmax),
              0};
}

mat4 perspectiveFromIntrinsics(rs2_intrinsics& i, vec2 z_range) {
  return glm::perspectiveFromIntrinsics(
      vec2{i.fx, i.fy}, vec2{i.ppx, i.ppy}, vec2{i.width, i.height}, z_range);
}

}; // namespace glm

namespace ComputeLib {

// shader for drawing a top-down orthographic projection, looking along the z axis.
// can either render 0/1 for whether a fragment is present at the given pixel, uint for the height
// above 0 each fragment is, or float for real number change from 0.

class InstancedMesh {
private:
  typedef GL::Attribute<0, Vector4> VtxPosition;
  typedef GL::Attribute<1, Vector4> VtxColor;

public:
  GL::Mesh m;
  Engine::Gpu::Buff<vec4> positions;
  Engine::Gpu::Buff<unsigned int> idxes;
  unsigned int num_triangles = 0;

  // may not be populated
  Engine::Gpu::Buff<vec4> vtx_colors;

  // if local tforms is 1, that means that a single transformation matrix applies to all vtxes.
  // if local tforms > 1, must bind buffer of idxes for each vtx, mapping to
  unsigned int num_local_tforms = 1;
  Engine::Gpu::Buff<unsigned int> local_tform_map;

  GL::Mesh& get_mesh() {

    m.setPrimitive(MeshPrimitive::Triangles)
        .setCount(num_triangles * 3)
        .setIndexBuffer(idxes.get_gl(), 0, MeshIndexType::UnsignedInt)
        .addVertexBuffer(positions.get_gl(), 0, VtxPosition{})
        .addVertexBuffer(vtx_colors.get_gl(), 0, VtxColor{});

    return m;
  }
};

// small scene graph that can contain many different positional configurations at once
class InstancedMeshGroup {

public:
  // set of meshes used in group
  vector<InstancedMesh> meshes;

  // num times each mesh appears in a full instance
  vector<unsigned int> num_meshes;

  // transform matrixes defining transforms to be applied
  Engine::Gpu::Buff<mat4> mesh_tforms;

  unsigned int full_mesh_count() {
    unsigned int sum = 0;
    for (int i = 0; i < meshes.size(); i++) {
      sum += num_meshes[i] * meshes[i].num_local_tforms;
    }
    return sum;
  }
};

class ShaderInstanced : public Engine::Gpu::ShaderBase {
public:
  Magnum::GL::Framebuffer fbo{Magnum::NoCreate};
  Magnum::GL::Renderbuffer depth_stencil;
  //{Magnum::NoCreate};

  ivec2 bound_dims{0, 0};

  enum ColorRenderMode { COLORED_VTX = 1, FULL_COLOR = 2 };
  ColorRenderMode render_mode = ColorRenderMode::FULL_COLOR;
  vec4 color_out{1., 1., 1., 1.};

  enum UintRenderMode {
    // 1 or 0! depending on if it is a fragment or not
    BINARY = 1,
    // ID of the unique mesh/transform combo
    MESH_ID = 2
  };
  UintRenderMode uint_render_mode = UintRenderMode::BINARY;
  // unsigned int mesh_id = 0;

  float max_depth = 50000.f;

  mat4 cam_projection{1.f};
  mat4 cam_position{1.f};

  ShaderInstanced() : Engine::Gpu::ShaderBase() {

#include <generated/ShaderInstanced_vert.hpp>
#include <generated/ShaderInstanced_frag.hpp>

    load(ShaderInstanced_vert);
    load(ShaderInstanced_frag);
    finish();

    fbo = move(GL::Framebuffer({{0, 0}, {0, 0}}));
    fbo.mapForDraw({{0, GL::Framebuffer::ColorAttachment(0)},
                    {1, GL::Framebuffer::ColorAttachment(1)},
                    {2, GL::Framebuffer::ColorAttachment(2)},
                    {3, GL::Framebuffer::ColorAttachment(3)}});
  }

  void set_target(
      Engine::Gpu::Tex<Engine::Gpu::TF::R16UI>* binary_tex,
      Engine::Gpu::Tex<Engine::Gpu::TF::R16UI>* depth_tex,
      Engine::Gpu::Tex<Engine::Gpu::TF::RGBA8>* color_tex,
      Engine::Gpu::Tex<Engine::Gpu::TF::RGBA32F>* vtxes_tex) {

    const auto verify_dims_match = [](ivec2& a, ivec2& b) {
      if (a.x != b.x || a.y != b.y) {
        printf("output textures need to match!\n");
        throw std::invalid_argument("--\n");
      }
    };

    bool draw_binary = binary_tex != nullptr;
    bool draw_depth = depth_tex != nullptr;
    bool draw_color = color_tex != nullptr;
    bool draw_vtxes = vtxes_tex != nullptr;

    bool d_set = false;
    ivec2 d;

    if (draw_binary) {
      d = binary_tex->get_dims();
      d_set = true;
    }

    if (draw_depth) {
      ivec2 _d = depth_tex->get_dims();
      if (d_set)
        verify_dims_match(d, _d);
      else {
        d = _d;
        d_set = true;
      }
    }

    if (draw_color) {
      ivec2 _d = color_tex->get_dims();
      if (d_set)
        verify_dims_match(d, _d);
      else {
        d = _d;
        d_set = true;
      }
    }

    if (draw_vtxes) {
      ivec2 _d = vtxes_tex->get_dims();
      if (d_set)
        verify_dims_match(d, _d);
      else {
        d = _d;
        d_set = true;
      }
    }

    if (!d_set) {
      printf("no textures provided!\n");
      throw new std::invalid_argument("---\n");
    }

    bound_dims = d;

    // const auto d = tex.get_dims();
    depth_stencil.setStorage(RenderbufferFormat::DepthStencil, {d.x, d.y});
    fbo.setViewport({{0, 0}, {d.x, d.y}});
    fbo.attachRenderbuffer(GL::Framebuffer::BufferAttachment::DepthStencil, depth_stencil);

    if (draw_binary) {
      fbo.attachTexture(GL::Framebuffer::ColorAttachment(0), binary_tex->get(), 0);
    }
    if (draw_depth) { fbo.attachTexture(GL::Framebuffer::ColorAttachment(1), depth_tex->get(), 0); }
    if (draw_color) { fbo.attachTexture(GL::Framebuffer::ColorAttachment(2), color_tex->get(), 0); }
    if (draw_vtxes) { fbo.attachTexture(GL::Framebuffer::ColorAttachment(3), vtxes_tex->get(), 0); }

    Engine::Gpu::GlUtil::verify_framebuffer_complete(fbo);
  }

  void clear_target() {
    fbo.detach(GL::Framebuffer::ColorAttachment(0));
    fbo.detach(GL::Framebuffer::ColorAttachment(1));
    fbo.detach(GL::Framebuffer::ColorAttachment(2));
    fbo.detach(GL::Framebuffer::ColorAttachment(3));
    bound_dims = {0, 0};
  }

  void verify_bound_tex() {
    if (bound_dims.x == 0 && bound_dims.y == 0) {
      printf("Must bind texture first!\n");
      throw new std::invalid_argument("bind texture");
    }
  }

  void draw_instanced_per_viewport(
      // meshes to draw!
      InstancedMeshGroup& ms,
      // number of instances to draw
      unsigned int num_instances_to_draw,
      // instance idx of the first instance to draw
      unsigned int start_instance_idx,
      // every full instance gets a unique viewport. this should be at least as large as
      // num_instances_to_draw
      Engine::Gpu::Buff<vec4>& viewports,
      // viewport ID to start at..
      unsigned int viewport_start = 0) {

    verify_bound_tex();

    ms.mesh_tforms.bind(3, Magnum::GL::Buffer::Target::ShaderStorage);
    viewports.bind(4, Magnum::GL::Buffer::Target::ShaderStorage);
    set_uniform("viewport_filter", true);
    set_uniform("viewport_id_offset", viewport_start);
    set_uniform("full_screen_dims", bound_dims);

    __draw_instanced(ms, num_instances_to_draw, start_instance_idx);
  }

  void draw_instanced_no_viewport(
      // meshes to draw!
      InstancedMeshGroup& ms,
      // number of instances to draw
      unsigned int num_instances_to_draw,
      // instance idx of the first instance to draw
      unsigned int start_instance_idx) {

    verify_bound_tex();

    ms.mesh_tforms.bind(3, Magnum::GL::Buffer::Target::ShaderStorage);
    set_uniform("viewport_filter", false);

    __draw_instanced(ms, num_instances_to_draw, start_instance_idx);
  }

private:
  void __draw_instanced(
      InstancedMeshGroup& ms, unsigned int num_instances_to_draw, unsigned int start_instance_idx) {

    const unsigned int tform_full_stride = ms.full_mesh_count();

    set_uniform("color_render_mode", render_mode);
    set_uniform("draw_color_out", color_out);

    set_uniform("uint_render_mode", uint_render_mode);

    set_uniform("cam_projection", cam_projection);
    set_uniform("cam_position", cam_position);

    set_uniform("instance_start_idx", start_instance_idx);

    unsigned int current_tform_offset = 0;
    for (int m_id = 0; m_id < ms.meshes.size(); m_id++) {

      unsigned int nm = ms.num_meshes[m_id];
      auto& m = ms.meshes[m_id];

      if (m.num_local_tforms > 1) {
        m.local_tform_map.bind(5, Magnum::GL::Buffer::Target::ShaderStorage);
      }

      set_uniform("meshes_per_instance", nm);
      // set_uniform("tforms_per_instance", nm * m.num_local_tforms);
      set_uniform("tform_full_stride", tform_full_stride);
      set_uniform("tform_local_offset", current_tform_offset);
      set_uniform("num_local_tforms", m.num_local_tforms);

      m.get_mesh().setInstanceCount(nm * num_instances_to_draw).draw(*this);

      current_tform_offset += (nm * m.num_local_tforms);
    }
  }
};

} // namespace ComputeLib