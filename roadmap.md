# Things to do:

## Before declaring a v1:

- Logging!
  - Log to file
  - Make sure exceptions get recorded in log file
  - Configurable logging levels (debug, etc. command line flag)
- Installer / distribution
  - Icons!
- Testing! How many different PC configurations?
  - currently: testing on high-end NVIDIA GPU, low-end AMD GPU, and integrated Intel GPU, all against fairly high-end Intel CPUs.
- Go-to-market plan!! :)

## Maybe

- Allow for notes along a 'spectrum' - midi control change signals
- Optimize depth camera parameters for < 1.5m hand tracking. Have already added 'disparity shift' values, but surely other values exist.
  - built in hand preset? 
- Check lightness / exposure settings when taking RGBA picture, for consistency in photo taken,
- Make entire system depth-units aware, so things can be presented in meters instead of tenths-of-millimeters
- Explanation for when calibration isn't happening correctly  (too far away, too close, etc.)
- Auto tune auto calibration:
  - Gradually relax surface area & error restrictions until a plane is generated
  - Use error of plane to calculate cutoff (~worst error + some const amount)
  - Generate score for calibration - based on error
- Auto tune real-time hand pose detection pipeline:
  - Automatically tune:
    - Detail levels of intermediate computations (minification levels, height-map resolutions, etc).
    - Thresholds (hand size, cutoffs, etc).
  - Using as feedback:
    - Time that each stage is taking to complete
	- Quality of signal - this is currently hardest one to analyze
- recommend to upgrade firmware! (perhaps if the calibrated surfaces have really jagged edges [obvy need picture])

## Later

- Make map of higher/lower sections of active area, so thresholds can be the same on all parts of the active area
- Come up with good system for seed data / image diffs, and produce test output including rendered-to-rgba images, etc.
- Pose detection algorithm
  - Make crap less pants when hands are close together!
  - Perhaps fully different algorithm..
- Some way to persist note configurations, so people don't have to re-set-up their drum pads every time
  - Print out notes on piece of paper, use OCR to scan paper such that instrument layout is auto-loaded.
    All necessary aspects included on paper itself
  - Load saved note configuration (would ideally involve attached projector, projecting notes positions)
- Multiple camera support, to expand active area
- Bundle MIDI loop-back program for windows builds (get commercial license, etc.)
