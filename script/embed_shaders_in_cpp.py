import platform
import os
import os.path
import argparse
from shutil import copyfile

parser = argparse.ArgumentParser(description='Bah')
parser.add_argument('--srcdir', action="store", dest='srcdir', default=0)
parser.add_argument('--bindir', action="store", dest='bindir', default=0)
parser.add_argument('--embed_kernels', action="store", dest='embed_kernels', default=0)
args = parser.parse_args()

embed_kernels = args.embed_kernels == 'ON'


def find_all(a_str, sub):
    start = 0
    while True:
        start = a_str.find(sub, start)
        if start == -1: return
        yield start
        start += len(sub)  # use start += 1 to find overlapping matches


def write_if_different(file, text):
    write_file = True
    if os.path.isfile(file):
        with open(file, 'r') as f_in:
            ex_file_text = f_in.read()
            if (ex_file_text == text):
                write_file = False

    if write_file:
        with open(file, 'w+') as f_out:
            f_out.write(text)


def convert_to_glm(ar):
    if ar == 'float2' or ar == 'float3' or ar == 'float4':
        ar = ar.replace('float', 'glm::vec')
    elif ar == 'int2' or ar == 'int3' or ar == 'int4':
        ar = ar.replace('int', 'glm::ivec')
    elif ar == '_mat4':
        ar = 'glm::mat4'
    elif ar == 'Plane':
        ar = 'Engine::Math::Plane'
    elif ar == 'uint2' or ar == 'uint3' or ar == 'uint4':
        ar = ar.replace('uint', 'glm::uvec')
    # elif ar == 'CompareMode':
    #    ar = 'CompareDepthImages::CompareMode'
    # elif ar == 'FilterBy':
    #    ar = 'FilterPoints::FilterBy'
    # elif ar == 'PlaneFit':
    #    ar = 'FitRegionsToPlanes::PlaneFit'
    return ar


# z: kernel file text
# f: kernel file filename
def get_class_constructor(z, file_path):
    file_path = file_path.replace('\\', '/')

    included_files = []
    for included_file_idx in list(find_all(z, '// #py_include')):
        line_end_idx = z.find('\n', included_file_idx)
        include_line = z[included_file_idx:line_end_idx].strip()
        # strip out the '// # include <' part as well as the last '>'
        include_file = include_line[16:-1]
        included_files.append(include_file)

    orig_filename = file_path.split('/')[-1]
    classname = orig_filename.split('.')[0]
    filename = orig_filename.replace('.', '_') + '_signature.hpp'

    kernel_names = []

    all_fn_cpp = ''
    for kernel_start_idx in list(find_all(z, '__kernel')):

        handled = True

        open_paren_idx = z.find('(', kernel_start_idx)
        close_paren_idx = z.find(')', open_paren_idx)

        kernel_decl = z[kernel_start_idx:open_paren_idx]
        fn_name = kernel_decl.split(' ')[2]

        invocation_dims_args = [
            'int grid, int block',
            'glm::ivec2 grid, glm::ivec2 block',
            'glm::ivec3 grid, glm::ivec3 block'
        ]
        for i in range(3):
            fn_cpp = 'void kernel_' + classname + '_' + fn_name + '(' + invocation_dims_args[i]

            kernel_names.append(fn_name)
            fn_args = [a.strip() for a in z[open_paren_idx + 1:close_paren_idx].split(',')]
            fn_arg_list_with_types = []
            invocation_calls = ['cl_program', 'invoke("' + fn_name + '")', 'dims(grid, block)']
            # fn_arg_list = []
            for fn_arg in fn_args:
                fn_arg_symbols = fn_arg.split(' ')

                # simplest case: normal variables
                if len(fn_arg_symbols) == 2:
                    type1 = convert_to_glm(fn_arg_symbols[0])

                    fn_arg_list_with_types.append(type1 + ' ' + fn_arg_symbols[1])
                    invocation_calls.append('arg(' + fn_arg_symbols[1] + ')')

                # next simplest: buffer!
                elif len(fn_arg_symbols) == 3 and fn_arg_symbols[0] == '__global':
                    # strip the trailing *
                    type1 = convert_to_glm(fn_arg_symbols[1][0:-1])
                    fn_arg_list_with_types.append('Engine::Gpu::Buff<' + type1 + '>& ' + fn_arg_symbols[2])
                    invocation_calls.append('arg(' + fn_arg_symbols[2] + ')')

                elif len(fn_arg_symbols) == 3 and fn_arg_symbols[0] == '__local':
                    type1 = convert_to_glm(fn_arg_symbols[1][0:-1])
                    fn_arg_list_with_types.append('const unsigned int ' + fn_arg_symbols[2])
                    invocation_calls.append('arg_local(sizeof(' + type1 + ') * ' + fn_arg_symbols[2] + ')')

                elif len(fn_arg_symbols) == 4 and (
                        fn_arg_symbols[0] == '__read_only' or fn_arg_symbols[0] == '__write_only'):
                    # third element needs to be comment like this /*TF::R16UI*/
                    type1 = fn_arg_symbols[2][2:-2]
                    fn_arg_list_with_types.append('Engine::Gpu::Tex<' + type1 + '>& ' + fn_arg_symbols[3])
                    invocation_calls.append('arg(' + fn_arg_symbols[3] + ')')



                else:
                    handled = False

            invocation_calls.append('call()')

            if len(fn_arg_list_with_types) > 0:
                fn_cpp = fn_cpp + ', ' + ', '.join(fn_arg_list_with_types)
            fn_cpp = fn_cpp + ') { ' + '.'.join(invocation_calls) + '; }\n'

            if handled:
                all_fn_cpp = all_fn_cpp + fn_cpp

        if not handled:
            print('Failed to auto-generate invocation for kernel ' + classname + '::' + fn_name)

    text = 'public:\n' + classname + '() {\n\n'
    for included_file in included_files:
        text = text + '#include <generated/' + included_file.replace('.', '_') + '.hpp>\n'
    text = text + '#include <generated/' + orig_filename.replace('.', '_') + '.hpp>\n'

    included_files.append(orig_filename.replace('.', '_'))
    text = text + '\ncl_program.init(\n  {' + ', '.join(included_files).replace('.', '_') + '},\n  {"' + '", "'.join(
        kernel_names) + '"});\n}\n'

    text = text + '\nprivate:\n' + all_fn_cpp + '\npublic:\n'

    return (filename, text)


if platform.platform().startswith('Linux'):
    copyfile(args.bindir + '/CMakeFiles/C_GLM.dir/src/engine/Gpu/OpenCL/cglm.c.o',
             args.bindir + '/src/compute/impl/types/cglm.cl')
    # is linux~!

exts = ["vert", "geom", "frag", "cl", "i"]

embed_filenames = ['cglm.cl']
embed_files = [args.bindir + '/src/compute/impl/types/cglm.cl']
embed_relative_files = ['/src/compute/impl/types/cglm.cl']

# look in source directory recursively
for root, dirs, files in os.walk(args.srcdir + '/src'):
    if root.find('third_party') == -1:
        for file in files:
            for ext in exts:
                if (file.endswith('.' + ext)):
                    embed_filenames.append(file)
                    embed_files.append(os.path.join(root, file))
                    embed_relative_files.append(os.path.join(root, file)[(len(args.srcdir) + 1):])

for (file, name, rel_name) in zip(embed_files, embed_filenames, embed_relative_files):
    # print file
    with open(file, 'r') as f:
        file_text = f.read()
        name_underline = name.replace('.', '_')

        suffix = name.split('.')[1]

        rel_name = rel_name.replace('\\', '/')

        MAX_STRING_SIZE = 1000

        if embed_kernels:
            source_string_literal_text = 'Engine::ProgramSource ' + name_underline + ';\n\
                ' + name_underline + '.pre_loaded = true; \n\
                ' + name_underline + '.suffix = "' + suffix + '"; \
                char* __src__' + name_underline + ';\n';
            num_els = len(file_text)
            start = 0
            while start < num_els:
                source_string_literal_text = source_string_literal_text + '__src__' + name_underline + ' = R"V0G0N(' + file_text[
                                                                                                                       start:(
                                                                                                                                   start + MAX_STRING_SIZE)] + ')V0G0N"; \n\
                    ' + name_underline + '.file += std::string(__src__' + name_underline + '); \n\
                    '
                start += MAX_STRING_SIZE

        else:
            source_string_literal_text = 'Engine::ProgramSource ' + name_underline + ';\n\
                ' + name_underline + '.file = "' + rel_name + '"; \n\
                ' + name_underline + '.pre_loaded = false; \n\
                ' + name_underline + '.suffix = "' + suffix + '";'

            # copy original file contents to same relative directory as original but in build directory
            # always write this file out: it isn't involved in the compilation process, so no downside to re-writing every time
            # first make sure the parent directory exists, then write it
            # the CGLM dep is copied to itself, but that doesn't do anything
            mdir = args.bindir + '/' + '/'.join(rel_name.split('/')[0:-1])
            os.makedirs(mdir, exist_ok=True)
            with open(args.bindir + '/' + rel_name, 'w+') as f_out:
                f_out.write(file_text)

        # first: check for existing file. if existing file is identical, don't write, so make it easy for compiler to cache stuff
        gen_file = args.bindir + '/generated/generated/' + name_underline + '.hpp'
        write_if_different(gen_file, source_string_literal_text)

        has_kernels = file_text.find('__kernel') != -1
        if has_kernels:
            class_constructor_name, class_constructor_text = get_class_constructor(file_text, file)
            gen_sig_file = args.bindir + '/generated/generated/' + class_constructor_name
            write_if_different(gen_sig_file, class_constructor_text)

if embed_kernels:
    print('Compute kernels: Embedded in source')
else:
    print('Compute kernels: Copied to build directory')
