	# this script will pull source for all dependencies from github, place them in the third_party directory,
# and pin their versions to the desired versions.
$deps = 
	("https://github.com/glfw/glfw.git", "glfw", "3.2.1"), 
	("https://github.com/mosra/magnum.git", "magnum", "v2019.01", ("b83c4366e707ac5c27803bf8cdb482eec32ed020")),
	("https://github.com/mosra/corrade.git", "corrade", "v2019.01"),
	("https://github.com/mosra/magnum-integration.git", "magnum-integration", "v2019.01"),
	("https://github.com/ocornut/imgui.git", "imgui", "v1.67"),
	("https://github.com/IntelRealSense/librealsense.git", "librealsense", "v2.24.0"),
	("https://github.com/thestk/rtmidi.git", "rtmidi", "v3.0.0"),
	("https://github.com/USCiLab/cereal.git", "cereal", "v1.3.0"),
	("https://github.com/google/googletest.git", "googletest", "release-1.8.1"),
	("https://github.com/g-truc/glm.git", "glm", "0.9.9.5"),
	# lodepng, icon codepoints headers not doing release tags, so just pin to a commit ID
	("https://github.com/lvandeve/lodepng", "lodepng", "", "378b01d18ad181bd383f1dd9e0346950a4b141d1"),
	("https://github.com/juliettef/IconFontCppHeaders.git", "IconFontCppHeaders", "", "ca5b8b48173aeaeb642353f711ebbf31d5a18465"),
	# pull from custom CGLM fork, modified to run as an OpenCL library
	("https://github.com/carsonswope/cglm.git", "cglm", "", "d2a34443e1a8b921e52a1c8e18452ec9f533aa99"),
	("https://github.com/bulletphysics/bullet3.git", "bullet3", "2.89"),
	("https://github.com/pradeep-pyro/tinynurbs.git", "tinynurbs", "", "48a4b61e2d321969cd568852738fbee51a751096")

# root dir
$r = Get-Location
# third-party root dir
$tpr = $r.tostring() + '/src/engine/third_party'

if (-Not (test-path $tpr)) {mkdir $tpr}

$pinned_tag_branch = "pinned_tag";

'Updating the following dependencies:'

Foreach ($i in $deps) {

	cd $tpr

	$repo_url = $i[0]
	$repo_name = $i[1]
	$has_tag = $i[2].length -gt 0
	$tag = "tags/" + $i[2]

	$repo_name

	# clone repo if not already present
	if (-Not (test-path $tpr/$repo_name)) { 
		" Not found, cloning.."
		git clone $repo_url --quiet
		if (-Not (test-path $tpr/$repo_name)) { " failed to clone repo!" } else { " Cloned" }
	}

	if (test-path $tpr/$repo_name) {
	
		cd $tpr/$repo_name

		#update tags list
		git fetch --all --tags --prune --quiet

		#switch back to master
		git checkout master --quiet

		# remove pinned tag branch, if it exists
		if (git rev-parse --verify --quiet $pinned_tag_branch) { 
			(git branch -D $pinned_tag_branch) > $null
		}

		#a tag is specified, pin to tag
		if ($has_tag) {
		
			# make new branch out of provided tag
			git checkout $tag -b $pinned_tag_branch --quiet
			if ($?) { " " + $tag }

			# apply cherry-picks, if any
			if ($i[3]) {
				Foreach ($c in $i[3]) {
					(git cherry-pick $c --strategy-option theirs) > $null
					" cherry-pick: " + $c
				}
			}

		# no tag specified. 4th element is commit ID, just reset hard to that commit ID
		} else {
			git checkout -b $pinned_tag_branch --quiet
			git reset --hard $i[3] --quiet
			" commit " + $i[3]
		}
	}
}

cd $r
