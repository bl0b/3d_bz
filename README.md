# 3d_bz

Turn your desk into a MIDI drumpad!
[3d-beats.com](https://www.3d-beats.com)

## requirements for compilation:

* OpenCL SDK.
  - It shouldn't really matter which OpenCL SDK you use (NVIDIA, Intel, AMD). Builds will be able to target any OpenCL device.
  - I'm using the [CUDA SDK (10.1)](https://developer.nvidia.com/cuda-downloads) which also includes OpenCL dev deps.
* 64-bit Windows 10 system with Visual Studio >= 2017
  - Earlier versions of Windows or Visual Studio might work, as might MinGW or Cygwin however currently only tested with 
    Visual Studio compiler & 64 bit builds.
  - Even though everything is built using Visual Studio, all the build configuration is done via CMake
  - It should be simple enough to configure a linux build. Currently this is low priority, due
    to difficulties configuring low-latency audio on linux.
* Python.
  - Some wrangling of shader/compute programs is done via python scripts (before building, auto-generate string-literal variables in code, etc.).
    I think you will need python 3+ for this as well.

## requirements for use

* [Intel RealSense depth camera](https://www.intelrealsense.com/depth-camera-d415/) 
  - D415 & D435 both work, though the D415 *might* perform better for this use case
  - Must use a USB3 cable and port - USB2 doesn't have enough bandwidth.
* Adjustable camera arm, for mounting camera above table. Something like [this](https://www.amazon.com/dp/B071VR8PWF)
  - Note: RealSense camera uses standard 1/4" camera mount
* Internal MIDI loop-back program
   - [LoopBe1](https://www.nerds.de/en/loopbe1.html) is free and does the job.
   - This wouldn't be necessary for linux (which provides build-in virtual MIDI instrument support)
* DAW of your choice. The instrument is exposed as an internal MIDI device, so any DAW will do.
* Low-latency audio output configuration.
   - The responsiveness of the drumpad can only be as good as your system's output audio latency. For example, 
   using an external USB audio interface as a dedicated sound card, I'm able to set the output audio buffer 
   size to 3.9 ms - which feels fully real-time. However, my integrated audio output jack cannot handle such
   low latency. It needs at least a 15-20 ms buffer, at which point the delay becomes noticeable, especially
   when playing at a high tempo.

## compilation / dev setup

* Fetch/update dependencies using powershell script: `.\update_deps.ps1`
* Open repo in Visual Studio as a CMake project (just open root directory or `CMakeLists.txt` file)
* For optimal performance, select the `x64-Release` build flavor. 
  - `x64-Release-Dist` build config will statically bundle the MSVC runtime & create an installer, so isn't necessary
    for most local development. Download InnoSetup 6.0.2 to create installer files.
* Build and run `3d_bz.exe`
* Check out 3d-beats.com/help for instructions on how to use it
