const get_overview = (v) => `
  test count: ${v['tests']} failures: ${v['failures']} disabled: ${v['disabled']} errors: ${v['errors']} time: ${v['time']}`;

const CompareMode_PCT_MATCH = 1;
const CompareMode_AVG_DIFF = 2;

const get_screendiff_log = (run) => {
    let vals = '';
    const num_screendiffs = +run['screendiff_count'] || 0;


    if (num_screendiffs) {

        vals += '<div class="single-test-run-screendiff-log">'

        //vals += `<div>diffs: ${num_screendiffs}</div>`;

        for (let i = 0; i < num_screendiffs; i++) {

            const img_diff = JSON.parse(run[`screendiff_${i}_obj`]);

            vals += '<div class="img-diff-main">';

            vals += `
                <div class = "img-title">
                    ${img_diff.img_name}
                    <span class ="${img_diff.pass ? 'pass' : 'fail'}">${img_diff.pass ? 'PASS': 'FAIL'}</span>
                </div>`

            if (img_diff.mode == CompareMode_PCT_MATCH) {
                vals += `<div class = "image-propery"> match percentage: `;
            } else if (img_diff.mode == CompareMode_AVG_DIFF) {
                vals += `<div class = "image-propery"> avg diff: `;
            }

            vals += `${img_diff.compare_result.toFixed(3)} (threshold: ${img_diff.pass_threshold.toFixed(3)})</div>`

            vals += `
                <div class ="image-main">
                    <div class ="image-title">expected</div>
                    <img src="${img_diff.expected_img}"/>
                </div>`;

            vals += `
                <div class ="image-main">
                    <div class ="image-title">actual</div>
                    <img src="${img_diff.actual_img}"/>
                </div>`;

            vals += `
                <div class ="image-main">
                    <div class ="image-title">diff</div>
                    <img src="${img_diff.diff_img}"/>
                </div>`;



            vals += '</div>';

        }

        vals += '';

    }

    return vals;
}

const get_failures_log = (failures) => {

    let vals = '';
    vals += `<div class="failures-log">`

    failures.forEach(failure => {

        messages = failure.failure.split('\n');

        // only log failure here if non-image-diff failure. image diff failure gets special treatment
        if (messages[1] != 'Value of: compare_result.assertion()') {
            vals += `<div class="test-fail-message">`

            messages.forEach(msg => {
                vals += msg + ' '
            });

            vals += `</div>`
        }

        


    });

    vals += `</div>`;
    return vals;
}

const get_run_details = (runs) => {
    let vals = '<div class="suite-tests">';
    runs.forEach(run => {

        const failures = run.failures && run.failures.length > 0;

        vals += `
            <div class="single-test-run">
                <div class ="single-test-run-title ${failures ? 'fail' : 'pass'}">
                   ${run['name']}
               </div>
               ${failures ? get_failures_log(run['failures']) : ''}
               
                    ${get_screendiff_log(run)}
            </div>
            `
    })

    vals += '</div>'

    return vals
}

const b = document.getElementsByTagName("body")[0];

console.log(data);

overview = document.createElement('div');

const all_passed = data.failures == 0 && data.errors == 0;

overview.innerHTML = `
    <div class ="overview-title">
        All tests:
        <span class ="${all_passed ? 'pass' : 'fail'}">${all_passed ? 'PASS': 'FAIL'}</span>
        (${data.time})
    </div>`;

b.appendChild(overview);

data['testsuites'].forEach(s => {
    console.log(s);

    const e = document.createElement('div');

    const suite_pass = s.failures == 0 && s.errors == 0;

    e.innerHTML = `
        <div class="suite">
        <div class ="suite-title">
            ${s['name']}
            <span class ="${suite_pass ? 'pass' : 'fail'}">${suite_pass ? 'PASS': 'FAIL'}</span>
            (${s.time})
        </div>

        ${get_run_details(s.testsuite)}
        </div>`;
    b.appendChild(e);

});