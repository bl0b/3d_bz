#include <TestCore.hpp>

#include "PlaneTest.hpp"

int main(int argc, char** argv) {
  FileUtil::init(argc, argv);
  TestCore::Runner runner(argc, argv, "Engine_MathTest");
  return runner.run();
}
