#pragma once

#include <TestCore.hpp>

using namespace Engine::Math;
using namespace glm;

// Test fixture
class PlaneTest : public ::testing::Test {
protected:
  void SetUp() override {}
  void TearDown() override {}
  PlaneTest() {}
  ~PlaneTest() override {}
};

TEST_F(PlaneTest, Plane_calculatesIntersectionWithRay_whenRayDirEqualsNormal) {
  Plane p({0, 0, 1}, {0, 0, 100});
  vec4 pos = p.intersection_with_line({0, 0, 1}, {0, 0, 0});
  assertEqual(pos, {0, 0, 100, 1});
}

TEST_F(PlaneTest, Plane_calculatesIntersectionWithRay_whenRayDirAlmostEqualsNormal) {
  Plane p({0, 0, 1}, {0, 0, 100});
  vec4 pos = p.intersection_with_line(normalize(vec3{0, .1, 10}), {0, 0, 0});
  assertEqual(pos, {0, 1, 100, 1});
}

TEST_F(PlaneTest, Plane_calculatesIntersectionWithRay_whenRayDirEqualsOppositeOfNormal) {
  Plane p({0, 0, 1}, {0, 0, 100});
  vec4 pos = p.intersection_with_line({0, 0, -1}, {0, 0, 0});
  assertEqual(pos, {0, 0, 100, 1});
}

TEST_F(PlaneTest, Plane_failsToCalculateIntersectionWithRay_whenRayDirPerpundicularToNormal) {
  Plane p({0, 0, 1}, {0, 0, 1});
  vec4 pos = p.intersection_with_line({0, 1, 0}, {0, 0, 0});
  assertEqual(pos, {0, 0, 0, 0});

  p.generate(normalize(vec3{0, -1, 1}), {0, 0, 0});
  pos = p.intersection_with_line(normalize(vec3{0, 1, 1}), {0, 0, 0});
  assertEqual(pos, {0, 0, 0, 0});
}