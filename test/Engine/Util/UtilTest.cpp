#include <TestCore.hpp>

// no tests so far..

int main(int argc, char** argv) {
  FileUtil::init(argc, argv);
  TestCore::Runner runner(argc, argv, "Engine_UtilTest");
  return runner.run();
}
