#pragma once

#include <TestCore.hpp>

using namespace std;

extern RsTestData* rs_test_data;

// Test fixture
class DepthCameraTest : public ::testing::Test {
public:
protected:
  TestCore::ImgCompare img_compare;

  DepthCamera<Engine::Scene::MatrixPosition>* depth_camera;

  void SetUp() override {
    auto& d = rs_test_data->data[rs_test_data->DESK_SNAPSHOT];
    depth_camera = new DepthCamera<Engine::Scene::MatrixPosition>(
        d.depth_intrinsics,
        1.0,
        3,
        Magnum::Vector2i{d.depth_intrinsics.width, d.depth_intrinsics.height});
  }

  void TearDown() override { delete depth_camera; }

  DepthCameraTest() {}
  ~DepthCameraTest() override {}
};

// Tests that the Foo::Bar() method does Abc.
TEST_F(DepthCameraTest, Renders) {

  // Load test depth frame, make sure that test frame dimensions match as expected
  auto& frame = rs_test_data->data[rs_test_data->DESK_SNAPSHOT].depth_frames[0];
  EXPECT_EQ(848, frame.get_dims().x);
  EXPECT_EQ(480, frame.get_dims().y);

  Engine::Gpu::Tex<Engine::Gpu::TF::R16UI> out_frame({1, 1});

  DepthCameraMesh<Engine::Scene::MatrixPosition> mesh;

  const string pos_prefix = "assets/test/shaders/compute/DeprojectDepthImage/";
  const string idx_prefix = "assets/test/shaders/compute/FilterDepthImageTriangles/";

  struct RenderedCase {
    // avg difference of deprojected image must be less than this number
    const float max_avg_diff;
    const string filename;
  };

  vector<RenderedCase> cases{{10.1, "desk_snapshot[0]_min_1_scale_1_translate_0"},
                             {20.3, "desk_snapshot[0]_min_2_scale_1_translate_0"},
                             {29.7, "desk_snapshot[0]_min_3_scale_1_translate_0"},
                             {37.6, "desk_snapshot[0]_min_4_scale_1_translate_0"},
                             {46.8, "desk_snapshot[0]_min_5_scale_1_translate_0"},
                             {55.5, "desk_snapshot[0]_min_6_scale_1_translate_0"},
                             {64.7, "desk_snapshot[0]_min_7_scale_1_translate_0"},
                             {63.6, "desk_snapshot[0]_min_8_scale_1_translate_0"}};

  for (int i = 0; i < cases.size(); i++) {

    const string pos_file = FileUtil::exe_dir + pos_prefix + cases[i].filename;
    // vector<Vector4> pos_mem;
    vector<glm::vec4> pos_mem = FileUtil::load<vector<glm::vec4>>(pos_file);
    // FileUtil::load_asset_file(pos_file, pos_mem);
    mesh.positions.set_data(pos_mem);

    const string idx_file = FileUtil::exe_dir + idx_prefix + cases[i].filename;
    vector<uint32_t> idx_mem = FileUtil::load<vector<uint32_t>>(idx_file);
    mesh.idxes.set_data(idx_mem);

    mesh.set_render_mode(DepthCameraMesh<Engine::Scene::MatrixPosition>::RenderMode::ColorVtx);

    mesh.num_triangles = idx_mem.size() / 3;

    out_frame.set_storage({frame.get_dims().x, frame.get_dims().y});
    depth_camera->set_target_texture(out_frame);
    depth_camera->fbo.bind();

    GL::Renderer::setClearColor(0x000000_rgbf);
    GL::Renderer::setPolygonMode(GL::Renderer::PolygonMode::Fill);
    GL::Renderer::disable(GL::Renderer::Feature::FaceCulling);
    depth_camera->fbo.clear(GL::FramebufferClear::Depth | GL::FramebufferClear::Color);

    depth_camera->draw(mesh);

    depth_camera->clear_target_texture();

    GL::defaultFramebuffer.bind();

    img_compare.expect_match(
        "deproject_" + to_string(i),
        frame,
        out_frame,
        ComputeLib::CompareDepthImages::CompareMode::AVG_DIFF,
        cases[i].max_avg_diff);
  }
}
