#include <TestCore.hpp>

#include "DepthCameraTest.hpp"

using namespace std;

TestCore::GpuCtx* gpu_ctx = nullptr;
RsTestData* rs_test_data = nullptr;

int main(int argc, char** argv) {
  FileUtil::init(argc, argv);

  TestCore::Runner runner(
      argc,
      argv,
      "Engine_3dTest",
      {gpu_ctx = new TestCore::GpuCtx(), rs_test_data = new RsTestData()});
  return runner.run();
}
