#pragma once

#include <TestCore.hpp>
#include <compute/ComputeLib.hpp>

// Test fixture
class Gpu_TexTest : public ::testing::Test {};

TEST_F(Gpu_TexTest, TexLoads_vec4) {

  const ivec2 tex_dims = {32, 32};
  vector<vec4> b_original(tex_dims.x * tex_dims.y, vec4{1.0, 2.0, 3.0, 1.0});
  b_original[51] = vec4{9., 5., 0., 1111.};

  Engine::Gpu::Tex<Engine::Gpu::TF::RGBA32F> t(tex_dims, b_original.data());

  vector<vec4> b_copy;
  t.cu_copy_to(b_copy);

  for (int i = 0; i < tex_dims.x * tex_dims.y; i++) {
    assertEqual(b_original[i], b_copy[i]);
  }
}

TEST_F(Gpu_TexTest, TexLoads_uint16) {

  const ivec2 tex_dims = {32, 32};
  vector<uint16_t> b_original(tex_dims.x * tex_dims.y, 420);
  b_original[51] = 69;

  Engine::Gpu::Tex<Engine::Gpu::TF::R16UI> t(tex_dims, b_original.data());

  vector<uint16_t> b_copy;
  t.cu_copy_to(b_copy);

  for (int i = 0; i < tex_dims.x * tex_dims.y; i++) {
    EXPECT_EQ(b_original[i], b_copy[i]);
  }
}
