#pragma once

#include <TestCore.hpp>
#include <compute/ComputeLib.hpp>

// Test fixture
class Gpu_BuffTest : public ::testing::Test {
public:
protected:
  void SetUp() override {}
  void TearDown() override {}
  Gpu_BuffTest() {}
  ~Gpu_BuffTest() override {}
};

TEST_F(Gpu_BuffTest, Bufferloads) {

  // Uint16 doesn't work in openCL!!! ????
  // how to remedy this?
  const int vec_size = 256;
  vector<unsigned int> b_original(vec_size);
  for (uint i = 0; i < vec_size; i++) {
    b_original[i] = 333;
  }

  Engine::Gpu::Buff<unsigned int> b(b_original);

  vector<unsigned int> b2;
  b.cu_copy_to(b2);

  // verify it matches!!
}
