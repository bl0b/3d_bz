#include <TestCore.hpp>

#include "BuffTest.hpp"
#include "TexTest.hpp"

using namespace std;

TestCore::GpuCtx* gpu_ctx = nullptr;

int main(int argc, char** argv) {
  FileUtil::init(argc, argv);
  TestCore::Runner runner(argc, argv, "Engine_GpuTest", {gpu_ctx = new TestCore::GpuCtx()});
  return runner.run();
}
