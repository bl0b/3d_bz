#pragma once

#include <TestCore.hpp>
#include <compute/ComputeLib.hpp>

#include <Components/CalibrationState.hpp>

#include <rosbag/bag.h>
//#include <rosbag/view.h>

extern RsTestData* rs_data;

// Test fixture
class ShapeDetectorTest : public ::testing::Test {

public:
  void ShapeDetectorTest::SetUp() {

    vector<unsigned int> strip_idxes_cpu{0, 1, 2, 1, 2, 3};
    strip.idxes.cu_copy_from(strip_idxes_cpu.data(), strip_idxes_cpu.size());

    vector<vec4> rectangle_vtxes{
        vec4{-0.5, -0.5, 0, 1}, vec4{-0.5, 0.5, 0, 1}, vec4{0.5, -0.5, 0, 1}, vec4{0.5, 0.5, 0, 1}};
    strip.positions.cu_copy_from(rectangle_vtxes.data(), rectangle_vtxes.size());
    strip.num_triangles = 2;

    // strip.position.set(tform);

    GL::Renderer::setClearColor(0x000000_rgbf);
    GL::Renderer::setPolygonMode(GL::Renderer::PolygonMode::Fill);

    ortho_shader.draw_height = true;
  }

protected:
  OrthoMapShader ortho_shader;
  DepthCameraMesh<Engine::Scene::MatrixPosition> strip;
  TestCore::ImgCompare img_compare;

  mat4 get_tform(vec4 origin, vec2 scale, vec3 rot) {
    return glm::translate(origin.xyz()) * glm::rotate(rot.z, vec3{0., 0., 1.}) *
           glm::rotate(rot.y, vec3{0., 1., 0.}) * glm::rotate(rot.x, vec3{1., 0., 0.}) *
           glm::scale(vec3{scale, 1.f});
  }

  void draw_rectangle(
      mat4 tform, Engine::Gpu::Tex<Engine::Gpu::TF::R16UI>& out, OrthoSampler2D& sampler) {
    ortho_shader.fbo.clear(GL::FramebufferClear::Color);
    ortho_shader.set_uniforms(sampler.min_coords, sampler.max_coords, tform);
    ortho_shader.set_target_texture(out);
    strip.get_mesh().draw(ortho_shader);
    ortho_shader.clear_target_texture();
  }
};

TEST_F(ShapeDetectorTest, calibrates_table1) {

  auto& data = rs_data->data[rs_data->DEMO_SEQUENCE_1];

  auto& first_depth_frame = data.depth_frames[100];

  OrthoSampler2D depth_img_sampler;
  const ivec2 dims = first_depth_frame.get_dims();
  depth_img_sampler.img_dims = first_depth_frame.get_dims();
  depth_img_sampler.min_coords = {0.f, 0.};
  depth_img_sampler.max_coords = {1.f * first_depth_frame.get_dims().x,
                                  1.f * first_depth_frame.get_dims().y};

  // vec4 r_origin{40, 100, 200, 1.};
  // vec2 r_scale{350, 30};
  // vec3 r_rot{0.4f, 0.2f, (float)-M_PI_4};
  // mat4 tform = get_tform(r_origin, r_scale, r_rot);
  // draw_rectangle(tform, depth_img, depth_img_sampler);

  ConvolveCircle convolve_circle;

  Engine::Gpu::Buff<vec4> convolve_out;

  /*

  convolve_circle.run(
      first_depth_frame,
      depth_img_sampler,
      convolve_out,
      16,          // num thetas
      32,          // dist out to check in each direction
      16,          // num samples for each direction
      vec2{4, 4}); // step in world-units in each direction

    */

  vector<vec4> convolve_out_cpu;
  convolve_out.cu_copy_to(convolve_out_cpu);

  vector<uint16_t> ex_img(dims.x * dims.y, 0);
  Engine::Gpu::Tex<Engine::Gpu::TF::R16UI> ex_depth_img{dims, ex_img.data()};

  img_compare.expect_match(
      "1",
      ex_depth_img,
      first_depth_frame,
      ComputeLib::CompareDepthImages::CompareMode::EXACT_MATCH,
      0.999);
}
