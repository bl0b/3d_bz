#include <TestCore.hpp>
#include <GpuCtxProducer.hpp>

//#include "CalibrationStateTest.hpp"
#include "ShapeDetectorTest.hpp"

using namespace std;

TestCore::GpuCtx* gpu_ctx = nullptr;
RsTestData* rs_data = nullptr;

int main(int argc, char** argv) {
  FileUtil::init(argc, argv);
  TestCore::Runner runner(
      argc, argv, "ComponentsTest", {gpu_ctx = new TestCore::GpuCtx(), rs_data = new RsTestData()});
  return runner.run();
}
