#pragma once

#include <TestCore.hpp>
#include <compute/ComputeLib.h>

#include <Components/CalibrationState.hpp>

extern RsTestData* rs_data;

// Test fixture
class CalibrationStateTest : public ::testing::Test {
protected:
  ComputeLib::FitRegionsToPlanes fit_regions_to_planes;

  TestCore::ImgCompare img_compare;

  const string EX_STENCIL_PREFIX = "assets/test/Components/Calibration/";

  ComputeLib::DeprojectDepth deproject_depth;
};

struct CalibrationStateTestCase {
  string data_file;
  string ex_stencil_file;

  Engine::Math::Plane ex_plane;
  float ex_surface_area;
};

TEST_F(CalibrationStateTest, calibrates_table1) {

  vector<CalibrationStateTestCase> cases = {
      {
          rs_data->TABLE_1_D415,
          "table_1_d415_expected_stencil",
          Engine::Math::Plane(
              // normal
              {0.443977416, 0.0343131050, 0.895380735},
              // center
              {1290.24207, 121.240807, 6141.44482}),
          0.272061f,
      },
      {
          rs_data->TABLE_1_D435,
          "table_1_d435_expected_stencil",
          Engine::Math::Plane(
              // normal
              {0.0730793402, -0.0374232270, 0.996623755},
              // center
              {-1677.35608, -379.963928, 6157.43896}),
          0.431322f,
      },
  };

  for (auto& test_case : cases) {

    CalibrationState calibration;

    calibration.num_frames_to_sample = 5;

    Engine::Gpu::Tex<Engine::Gpu::TF::RGBA32F> vtxes;
    Engine::Gpu::Buff<ComputeLib::FitRegionsToPlanes::PlaneFit> planes;
    Engine::Math::Plane current_plane;

    const int MINIF = 1;

    const float pct_measured_thresh = 0.7;
    const float error_thresh = 10.;

    auto& test_data = rs_data->data[test_case.data_file];

    auto& depth_intrin = test_data.depth_intrinsics;
    auto& rgba_intrin = test_data.rgba_intrinsics;
    auto& depth_to_rgba_extrin = test_data.depth_to_rgba_extrinsics;

    for (int i = 0; i < 5; i++) {

      // deproject depth..
      auto& depth_img = test_data.depth_frames[i];
      deproject_depth.run(depth_img, vtxes, depth_intrin, MINIF, mat4(1.f));

      // fit to planes..
      ivec2 planes_dims;
      const int plane_fit_region_side_length = 48;
      fit_regions_to_planes.run(vtxes, plane_fit_region_side_length, planes, &planes_dims);

      // find largest contiguous
      float percent_of_planes_on_largest_contiguous;
      FitRegionsToPlanes::find_largest_contiguous_flat_region(
          planes,
          planes_dims,
          pct_measured_thresh,
          error_thresh,
          &percent_of_planes_on_largest_contiguous,
          &current_plane);

      calibration.post_plane(
          depth_img,
          current_plane,
          percent_of_planes_on_largest_contiguous,
          depth_intrin,
          rgba_intrin,
          depth_to_rgba_extrin,
          0.0001);

      // calibration should not occur until the 5th frame (configured above)
      if (i == 4) {
        EXPECT_TRUE(calibration.calibrated);
      } else {
        EXPECT_FALSE(calibration.calibrated);
      }
    }

    vector<uint16_t> stencil_cpu = FileUtil::load<vector<uint16_t>>(
        FileUtil::exe_dir + EX_STENCIL_PREFIX + test_case.ex_stencil_file);

    Engine::Gpu::Tex<Engine::Gpu::TF::R16UI> ex_stencil(
        calibration.active_plane.stencil.get_dims(), stencil_cpu.data());

    img_compare.expect_match(
        test_case.ex_stencil_file,
        ex_stencil,
        calibration.active_plane.stencil,
        ComputeLib::CompareDepthImages::CompareMode::EXACT_MATCH,
        0.999);

    assertEqual(calibration.plane.normal, test_case.ex_plane.normal, F_ERR);
    assertEqual(calibration.plane.center, test_case.ex_plane.center, F_ERR);

    float surface_area = calibration.determine_surface_area();
    assertEqual(vec<1, float>{surface_area}, vec<1, float>{test_case.ex_surface_area}, F_ERR);
  }
}
