#pragma once

#include <TestCore.hpp>
#include <compute/ComputeLib.hpp>

// Test fixture
class MeshGroupsTest : public ::testing::Test {
public:
protected:
  void SetUp() override {}
  void TearDown() override {}
  MeshGroupsTest() {}
  ~MeshGroupsTest() override {}
};

TEST_F(MeshGroupsTest, makes_mesh_groups) {

  ComputeLib::MeshGroups mesh_groups;

  vector<uint32_t> groups_out;

  // approx. min group size of 2.8, so groups 3+ should be part of it :)
  const float min_group_size = 0.015;
  const int max_num_groups = 16;

  const ivec2 height_map_in_dims{21, 9};

  // clang-format off
  vector<uint16_t> height_map_in = {
	0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 3, 3, 0, 0, 0, 3, 2, 4, 7, 7, 0,
	0, 0, 0, 9, 8, 0, 0, 0, 3, 3, 3, 0, 0, 0, 0, 3, 0, 7, 0, 9, 0,
	0, 0, 8, 9, 8, 0, 0, 5, 5, 5, 5, 5, 0, 0, 0, 5, 5, 7, 1, 6, 0,
	0, 0, 0, 0, 0, 0, 0, 5, 0, 0, 0, 5, 0, 0, 0, 0, 0, 4, 0, 0, 0,
	0, 0, 0, 4, 4, 0, 0, 5, 0, 0, 0, 5, 0, 0, 0, 0, 0, 4, 0, 0, 0,
	0, 4, 4, 4, 4, 0, 0, 5, 0, 0, 0, 5, 0, 0, 0, 0, 0, 4, 0, 0, 0,
	0, 4, 4, 0, 4, 0, 0, 5, 0, 9, 0, 5, 0, 8, 0, 0, 0, 3, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 6, 0, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 3, 5, 7, 0, 5, 0, 9, 9, 0, 3, 3, 3, 0, 0,
  };
  // clang-format on

  mesh_groups.make_new_from_height_map(
      height_map_in.data(), height_map_in_dims, min_group_size, max_num_groups);
  mesh_groups.groups_map.cu_copy_to(groups_out);

  // clang-format off
  vector<uint32_t> ex_groups_out = {
	0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 2, 2, 0, 0, 0, 3, 3, 3, 3, 3, 0,
	0, 0, 0, 1, 1, 0, 0, 0, 2, 2, 2, 0, 0, 0, 0, 3, 0, 3, 0, 3, 0,
	0, 0, 1, 1, 1, 0, 0, 2, 2, 2, 2, 2, 0, 0, 0, 3, 3, 3, 3, 3, 0,
	0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 2, 0, 0, 0, 0, 0, 3, 0, 0, 0,
	0, 0, 0, 4, 4, 0, 0, 2, 0, 0, 0, 2, 0, 0, 0, 0, 0, 3, 0, 0, 0,
	0, 4, 4, 4, 4, 0, 0, 2, 0, 0, 0, 2, 0, 0, 0, 0, 0, 3, 0, 0, 0,
	0, 4, 4, 0, 4, 0, 0, 2, 0, 5, 0, 2, 0, 0, 0, 0, 0, 3, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 5, 5, 5, 0, 2, 0, 0, 0, 0, 6, 6, 6, 0, 0,
  };
  // clang-format on

  EXPECT_EQ(groups_out.size(), height_map_in_dims.x * height_map_in_dims.y);
  for (int i = 0; i < groups_out.size(); i++) {
    EXPECT_EQ(groups_out[i], ex_groups_out[i])
        << "groups_out, round 1 @ {" << (i % height_map_in_dims.x) << ","
        << (i / height_map_in_dims.x) << "}";
  }

  // round 2!
  // ever so slightly different
  // clang-format off
  height_map_in = {
	0, 0, 0, 0, 7, 7, 0, 0, 0, 0, 3, 3, 0, 0, 0, 3, 2, 4, 7, 7, 0,
	0, 0, 0, 9, 8, 7, 0, 0, 3, 3, 3, 0, 0, 0, 0, 3, 0, 7, 0, 9, 0,
	0, 0, 8, 9, 8, 0, 0, 5, 5, 5, 5, 5, 0, 0, 0, 5, 5, 7, 1, 6, 0,
	0, 0, 0, 0, 0, 0, 0, 5, 0, 0, 0, 5, 0, 0, 0, 0, 0, 4, 0, 0, 0,
	0, 0, 0, 4, 4, 0, 0, 5, 0, 0, 0, 5, 0, 0, 0, 0, 0, 4, 0, 0, 0,
	0, 4, 4, 4, 4, 0, 0, 5, 0, 0, 0, 5, 0, 0, 0, 0, 0, 4, 0, 0, 0,
	0, 4, 4, 0, 4, 0, 0, 5, 0, 0, 0, 5, 0, 8, 0, 0, 6, 3, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 0, 0, 0, 0, 6, 0, 0, 0, 0,
	0, 0, 2, 2, 6, 9, 0, 0, 0, 0, 0, 5, 0, 9, 9, 0, 3, 3, 3, 0, 0,
  };
  // clang-format on

  mesh_groups.make_new_from_height_map(
      height_map_in.data(), height_map_in_dims, min_group_size, max_num_groups);
  mesh_groups.groups_map.cu_copy_to(groups_out);

  // clang-format off
  ex_groups_out = {
	0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 2, 2, 0, 0, 0, 3, 3, 3, 3, 3, 0,
	0, 0, 0, 1, 1, 1, 0, 0, 2, 2, 2, 0, 0, 0, 0, 3, 0, 3, 0, 3, 0,
	0, 0, 1, 1, 1, 0, 0, 2, 2, 2, 2, 2, 0, 0, 0, 3, 3, 3, 3, 3, 0,
	0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 2, 0, 0, 0, 0, 0, 3, 0, 0, 0,
	0, 0, 0, 4, 4, 0, 0, 2, 0, 0, 0, 2, 0, 0, 0, 0, 0, 3, 0, 0, 0,
	0, 4, 4, 4, 4, 0, 0, 2, 0, 0, 0, 2, 0, 0, 0, 0, 0, 3, 0, 0, 0,
	0, 4, 4, 0, 4, 0, 0, 2, 0, 0, 0, 2, 0, 0, 0, 0, 3, 3, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 3, 0, 0, 0, 0,
	0, 0,13,13,13,13, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 3, 3, 3, 0, 0,
  };
  // clang-format on

  for (int i = 0; i < groups_out.size(); i++) {
    EXPECT_EQ(groups_out[i], ex_groups_out[i])
        << "groups_out, round 2 @ {" << (i % height_map_in_dims.x) << ","
        << (i / height_map_in_dims.x) << "}";
  }
}
