#pragma once

#include <TestCore.hpp>
#include <compute/ComputeLib.hpp>

extern TestCore::GpuCtx* gpu_ctx;

using namespace Engine::Gpu;

using CompareMode = ComputeLib::CompareDepthImages::CompareMode;

class ScreenDiffTest : public ::testing::Test {
protected:
  Tex<TF::R16UI> ex; // expected
  Tex<TF::R16UI> ac; // actual

  TestCore::ImgCompare screen_diff;
};

TEST_F(ScreenDiffTest, DoesScreenDiff1) {

  const ivec2 dims{256, 256};

  vector<uint16_t> ex_data(dims.x * dims.y, 1234);
  memset((void*)(ex_data.data() + 8000), 0, sizeof(uint16_t) * 7000);
  ex.set_data(dims, ex_data.data());

  vector<uint16_t> ac_data(dims.x * dims.y, 1234);
  memset((void*)(ac_data.data() + 10000), 0, sizeof(uint16_t) * 6000);
  ac.set_data(dims, ac_data.data());

  // at least 0.9 must match exactly to pass!
  screen_diff.expect_match("img_1", ex, ac, CompareMode::EXACT_MATCH, 0.90);

  memset((void*)(ac_data.data() + 40000), 0, sizeof(uint16_t) * 5600);
  ac.set_data(dims, ac_data.data());

  // at least 0.85 must match exactly to pass!
  screen_diff.expect_match("img_2", ex, ac, CompareMode::EXACT_MATCH, 0.85);
}

TEST_F(ScreenDiffTest, DoesScreenDiff_differentdims) {

  const ivec2 dims{412, 127};

  vector<uint16_t> ex_data(dims.x * dims.y, 1234);
  memset((void*)(ex_data.data() + 8000), 0, sizeof(uint16_t) * 7000);
  ex.set_data(dims, ex_data.data());

  vector<uint16_t> ac_data(dims.x * dims.y, 1234);
  memset((void*)(ac_data.data() + 10000), 0, sizeof(uint16_t) * 6000);
  ac.set_data(dims, ac_data.data());

  // at least 0.9 must match exactly to pass!
  screen_diff.expect_match("img_1", ex, ac, CompareMode::EXACT_MATCH, 0.90);

  memset((void*)(ac_data.data() + 40000), 0, sizeof(uint16_t) * 5600);
  ac.set_data(dims, ac_data.data());

  // at least 0.9 must match exactly to pass!
  screen_diff.expect_match("img_2", ex, ac, CompareMode::EXACT_MATCH, 0.82);
}

TEST_F(ScreenDiffTest, DoesScreenDiff_avgdiff) {

  const ivec2 dims{128, 128};

  vector<uint16_t> ex_data(dims.x * dims.y, 200);
  vector<uint16_t> ac_data(dims.x * dims.y, 200);
  for (int x = 0; x < dims.x; x++) {
    for (int y = 0; y < dims.y; y++) {

      const int idx = x + y * dims.x;

      if (x < 30 && y < 60) { ex_data[idx] = 0; }

      if (x < 20 && y < 100) { ac_data[idx] = 20; }
    }
  }

  ex.set_data(dims, ex_data.data());
  ac.set_data(dims, ac_data.data());

  screen_diff.expect_match("img_1", ex, ac, CompareMode::AVG_DIFF, 17.6);

  screen_diff.expect_match("img_1_match", ex, ac, CompareMode::EXACT_MATCH, 0.8);

  memset(ex_data.data(), 0, ex_data.size() * sizeof(uint16_t));
  memset(ac_data.data(), 0, ex_data.size() * sizeof(uint16_t));

  for (int x = 0; x < dims.x; x++) {
    for (int y = 0; y < dims.y; y++) {
      const int idx = x + y * dims.x;
      ex_data[idx] = x + 60;
      ac_data[idx] = x + 59;
    }
  }

  ex.set_data(dims, ex_data.data());
  ac.set_data(dims, ac_data.data());

  screen_diff.expect_match("img_2", ex, ac, CompareMode::AVG_DIFF, 2.00);
}