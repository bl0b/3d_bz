#pragma once

#include <TestCore.hpp>
#include <compute/ComputeLib.hpp>

// Test fixture
class DeprojectDepthTest : public ::testing::Test {
public:
protected:
  ComputeLib::DeprojectDepth deproject_depth;

  rs2_intrinsics intrin_4x4;
  rs2_intrinsics intrin_2x2;

  // output 3d points with w
  Engine::Gpu::Tex<Engine::Gpu::TF::RGBA32F> out_points{{1, 1}};
  Engine::Gpu::Tex<Engine::Gpu::TF::R16UI> in_img{{1, 1}};

  void SetUp() override {

    // toy intrinsics for a 4x4 image with principal point in exact center and focal depth of 4
    intrin_4x4.width = 4;
    intrin_4x4.height = 4;
    intrin_4x4.ppx = 1.5;
    intrin_4x4.ppy = 1.5;
    intrin_4x4.fx = 4;
    intrin_4x4.fy = 4;
    intrin_4x4.model = RS2_DISTORTION_BROWN_CONRADY;

    // toy intrinsics for a 2x2 image with principal point in exact center and focal depth of 2
    intrin_2x2.width = 2;
    intrin_2x2.height = 2;
    intrin_2x2.ppx = 0.5;
    intrin_2x2.ppy = 0.5;
    intrin_2x2.fx = 2;
    intrin_2x2.fy = 2;
    intrin_2x2.model = RS2_DISTORTION_BROWN_CONRADY;
  }

  void TearDown() override {}
  DeprojectDepthTest() {}
  ~DeprojectDepthTest() override {}
};

TEST_F(DeprojectDepthTest, Deprojects_4x4_minif_1) {

  // input depth image: all 1s
  vector<uint16_t> img(16, 1);
  in_img.set_data({4, 4}, img.data());

  deproject_depth.run(in_img, out_points, intrin_4x4, 1, mat4(1.f));

  const float c = 0.125;
  // depth values all 1, w values 1. x and y values multiples of constant c
  vector<vec4> expected{{-c * 3, -c * 3, 1., 1.},
                        {-c, -c * 3, 1., 1.},
                        {c, -c * 3, 1., 1.},
                        {c * 3, -c * 3, 1., 1.},
                        {-c * 3, -c, 1., 1.},
                        {-c, -c, 1., 1.},
                        {c, -c, 1., 1.},
                        {c * 3, -c, 1., 1.},
                        {-c * 3, c, 1., 1.},
                        {-c, c, 1., 1.},
                        {c, c, 1., 1.},
                        {c * 3, c, 1., 1.},
                        {-c * 3, c * 3, 1., 1.},
                        {-c, c * 3, 1., 1.},
                        {c, c * 3, 1., 1.},
                        {c * 3, c * 3, 1., 1.}};

  out_points.read<vec4>([&](const vec4* v) {
    for (int i = 0; i < 16; i++) {
      assertEqual(v[i], expected[i]);
    }
  });

  EXPECT_EQ(out_points.get_dims().x, 4);
  EXPECT_EQ(out_points.get_dims().y, 4);
}

TEST_F(DeprojectDepthTest, Deprojects_4x4_minif_2) {

  // input depth image: all 1s
  vector<uint16_t> img(16, 1);
  in_img.set_data({4, 4}, img.data());

  deproject_depth.run(in_img, out_points, intrin_4x4, 2, mat4(1.f));

  const float c = 0.125;
  // depth values all 1, w values 1. x and y values multiples of constant c
  vector<vec4> expected{
      {-c * 3, -c * 3, 1., 1.}, {c, -c * 3, 1., 1.}, {-c * 3, c, 1., 1.}, {c, c, 1., 1.}};

  out_points.read<vec4>([&](const vec4* v) {
    for (int i = 0; i < 4; i++) {
      assertEqual(v[i], expected[i]);
    }
  });

  EXPECT_EQ(out_points.get_dims().x, 2);
  EXPECT_EQ(out_points.get_dims().y, 2);
}

TEST_F(DeprojectDepthTest, Deprojects_2x2_minif_1_some_0s) {

  // input depth image: all 1s
  vector<uint16_t> img(4, 1);
  img[2] = 0;
  img[3] = 0;
  in_img.set_data({2, 2}, img.data());

  deproject_depth.run(in_img, out_points, intrin_2x2, 1, mat4(1.f));

  const float c = 0.25;
  // depth values all 1, w values 1. x and y values multiples of constant c
  vector<vec4> expected{{-c, -c, 1., 1.}, {c, -c, 1., 1.}, {0., 0., 0., 0.}, {0., 0., 0., 0.}};

  out_points.read<vec4>([&](const vec4* v) {
    for (int i = 0; i < 4; i++) {
      assertEqual(v[i], expected[i]);
    }
  });

  EXPECT_EQ(out_points.get_dims().x, 2);
  EXPECT_EQ(out_points.get_dims().y, 2);
}

TEST_F(DeprojectDepthTest, Deprojects_2x2_minif_1_some_0s_apply_transform) {

  // input depth image: half 1s
  vector<uint16_t> img(4, 1);
  img[2] = 0;
  img[3] = 0;
  in_img.set_data({2, 2}, img.data());

  deproject_depth.run(in_img, out_points, intrin_2x2, 1, translate(vec3{0., 2., 10}));

  const float c = 0.25;
  // depth values all 1, w values 1. x and y values multiples of constant c
  vector<vec4> expected{
      {-c, -c + 2, 11., 1.}, {c, -c + 2, 11., 1.}, {0., 0., 0., 0.}, {0., 0., 0., 0.}};

  out_points.read<vec4>([&](const vec4* v) {
    for (int i = 0; i < 4; i++) {
      assertEqual(v[i], expected[i]);
    }
  });

  EXPECT_EQ(out_points.get_dims().x, 2);
  EXPECT_EQ(out_points.get_dims().y, 2);
}
