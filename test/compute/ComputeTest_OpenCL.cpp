#include <TestCore.hpp>
#include <GpuCtxProducer.hpp>

#include <string.h>

// Actual tests to test!

#include "DeprojectDepthTest.hpp"
#include "GenTrianglesTest.hpp"
#include "FitRegionsToPlanesTest.hpp"
#include "ScreenDiffTest.hpp"
#include "MeshGroupsTest.hpp"

TestCore::GpuCtx* gpu_ctx;

int main(int argc, char** argv) {
  FileUtil::init(argc, argv);
  TestCore::Runner runner(argc, argv, "ComputeTest_OpenCL", {gpu_ctx = new TestCore::GpuCtx()});
  return runner.run();
}
