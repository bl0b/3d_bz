#pragma once

#include <TestCore.hpp>
#include <compute/ComputeLib.hpp>

// Test fixture
class FitRegionsToPlanesTest : public ::testing::Test {
public:
protected:
  ComputeLib::FitRegionsToPlanes fit_regions_to_planes;

  void SetUp() override {}
  void TearDown() override {}
  FitRegionsToPlanesTest() {}
  ~FitRegionsToPlanesTest() override {}
};

TEST_F(FitRegionsToPlanesTest, Fits_4x4_sidelength2) {

  // input 3d points
  Engine::Gpu::Tex<Engine::Gpu::TF::RGBA32F> in_points({4, 4});
  // clang-format off
  vector<vec4> in_points_cpu = {
      {0, 0, 1, 1}, {1, 0, 1, 1}, {2, 0, 1, 1}, {3, 0, 1, 1},
      {0, 1, 1, 1}, {1, 1, 1, 1}, {2, 1, 1, 1}, {3, 1, 1, 1}, 
      {0, 2, 1, 1}, {1, 2, 1, 1}, {2, 2, 1, 1}, {3, 2, 1, 1}, 
      {0, 3, 1, 1}, {1, 3, 1, 1}, {2, 3, 1, 1}, {3, 3, 1, 1}};
  // clang-format on
  in_points.set_data({4, 4}, in_points_cpu.data());

  // output planes of best matching
  Engine::Gpu::Buff<ComputeLib::FitRegionsToPlanes::PlaneFit> out_planes;
  out_planes.set_storage(4, Magnum::GL::BufferUsage::DynamicDraw);
  vector<ComputeLib::FitRegionsToPlanes::PlaneFit> out_planes_raw(4);

  ivec2 out_planes_dims;

  fit_regions_to_planes.run(in_points, 2, out_planes, &out_planes_dims);

  out_planes.cu_copy_to(out_planes_raw);

  assertEqual(out_planes_dims, {2, 2});

  assertEqual(out_planes_raw[0].center, {0.5, 0.5, 1, 1});
  assertEqual(out_planes_raw[1].center, {2.5, 0.5, 1, 1});
  assertEqual(out_planes_raw[2].center, {0.5, 2.5, 1, 1});
  assertEqual(out_planes_raw[3].center, {2.5, 2.5, 1, 1});

  // All normals should be positive on the Z axis, 100% pixels used with 0 error
  for (int i = 0; i < 4; i++) {
    assertEqual(out_planes_raw[i].normal, {0, 0, 1, 1});
    EXPECT_FLOAT_EQ(1, out_planes_raw[i].percent_sampled);
    EXPECT_FLOAT_EQ(0, out_planes_raw[i].sum_error);
  }
}

TEST_F(FitRegionsToPlanesTest, Fits_5x5_sidelength4) {

  // input 3d points
  // last column and row should not factor into calculations! (only full planes)
  Engine::Gpu::Tex<Engine::Gpu::TF::RGBA32F> in_points({5, 5});
  // clang-format off
  vector<vec4> in_points_cpu = {
      {0, 0, 1, 1}, {1, 0, 1, 1}, {2, 0, 1, 1}, {3, 0, 1, 1}, {4, 0, 1, 1}, 
      {0, 1, 1, 1}, {1, 1, 1, 1}, {2, 1, 1, 1}, {3, 1, 1, 1}, {4, 1, 1, 1}, 
      {0, 2, 1, 1}, {1, 2, 1, 1}, {2, 2, 1, 1}, {3, 2, 1, 1}, {4, 2, 1, 1}, 
      {0, 3, 1, 1}, {1, 3, 1, 1}, {2, 3, 1, 1}, {3, 3, 1, 1}, {4, 3, 1, 1}, 
      {0, 4, 1, 1}, {1, 4, 1, 1}, {2, 4, 1, 1}, {3, 4, 1, 1}, {4, 4, 1, 1}};
  // clang-format on
  in_points.set_data({5, 5}, in_points_cpu.data());

  // output planes of best matching
  Engine::Gpu::Buff<ComputeLib::FitRegionsToPlanes::PlaneFit> out_planes;
  out_planes.set_storage(1, Magnum::GL::BufferUsage::DynamicDraw);
  vector<ComputeLib::FitRegionsToPlanes::PlaneFit> out_planes_raw(1);

  ivec2 out_planes_dims;
  fit_regions_to_planes.run(in_points, 4, out_planes, &out_planes_dims);

  out_planes.cu_copy_to(out_planes_raw);

  assertEqual(out_planes_dims, {1, 1});
  assertEqual(out_planes_raw[0].center, {1.5, 1.5, 1., 1});
  assertEqual(out_planes_raw[0].normal, {0, 0, 1., 1});
  EXPECT_FLOAT_EQ(1, out_planes_raw[0].percent_sampled);
  EXPECT_FLOAT_EQ(0, out_planes_raw[0].sum_error);
}

TEST_F(FitRegionsToPlanesTest, Fits_4x4_sidelength4_sloped_plane) {

  // input 3d points - 45 degree sloped plane
  Engine::Gpu::Tex<Engine::Gpu::TF::RGBA32F> in_points({4, 4});
  // clang-format off
  vector<vec4> in_points_cpu = {
      {0, 0, 0, 1}, {1, 0, 1, 1}, {2, 0, 2, 1}, {3, 0, 3, 1},
      {0, 1, 0, 1}, {1, 1, 1, 1}, {2, 1, 2, 1}, {3, 1, 3, 1},
      {0, 2, 0, 1}, {1, 2, 1, 1}, {2, 2, 2, 1}, {3, 2, 3, 1},
      {0, 3, 0, 1}, {1, 3, 1, 1}, {2, 3, 2, 1}, {3, 3, 3, 1}};
  // clang-format on
  in_points.set_data({4, 4}, in_points_cpu.data());

  // output planes of best matching
  Engine::Gpu::Buff<ComputeLib::FitRegionsToPlanes::PlaneFit> out_planes;
  out_planes.set_storage(1, Magnum::GL::BufferUsage::DynamicDraw);
  vector<ComputeLib::FitRegionsToPlanes::PlaneFit> out_planes_raw(1);

  ivec2 out_planes_dims;
  fit_regions_to_planes.run(in_points, 4, out_planes, &out_planes_dims);

  out_planes.cu_copy_to(out_planes_raw);

  assertEqual(out_planes_dims, {1, 1});
  assertEqual(out_planes_raw[0].center, {1.5, 1.5, 1.5, 1});
  assertEqual(out_planes_raw[0].normal, {-0.707107, 0, 0.707107, 1}, F_ERR);
  EXPECT_FLOAT_EQ(1, out_planes_raw[0].percent_sampled);
  EXPECT_NEAR(0, out_planes_raw[0].sum_error, F_ERR);
}

TEST_F(FitRegionsToPlanesTest, Fits_8x8_sidelength8_some_missing_some_error) {

  // input 3d points
  // last column and row should not factor into calculations! (only full planes)
  Engine::Gpu::Tex<Engine::Gpu::TF::RGBA32F> in_points({8, 8});
  // clang-format off
  vector<vec4> in_points_cpu = {
      {0, 0, 0, 0},   {0, 0, 0, 0}, {2, 0, 1, 1}, {3, 0, 1, 1}, {4, 0, 1, 1}, {5, 0, 1, 1}, {6, 0, 1, 1}, {7, 0, 1, 1},
      {0, 1, 1.1, 1}, {1, 1, 1, 1}, {2, 1, 1, 1}, {3, 1, 1, 1}, {4, 1, 1, 1}, {5, 1, 1, 1}, {6, 1, 1, 1}, {7, 1, 1, 1},
      {0, 2, 1.1, 1}, {1, 2, 1, 1}, {2, 2, 1, 1}, {3, 2, 1, 1}, {4, 2, 1, 1}, {5, 2, 1, 1}, {6, 2, 1, 1}, {7, 2, 1, 1},
      {0, 3, 1, 1},   {1, 3, 1, 1}, {2, 3, 1, 1}, {3, 3, 1, 1}, {4, 3, 1, 1}, {5, 3, 1, 1}, {6, 3, 1, 1}, {7, 3, 1, 1},
      {0, 4, 1, 1},   {1, 4, 1, 1}, {2, 4, 1, 1}, {3, 4, 1, 1}, {4, 4, 1, 1}, {5, 4, 1, 1}, {6, 4, 1, 1}, {7, 4, 1, 1},
      {0, 5, 1, 1},   {1, 5, 1, 1}, {2, 5, 1, 1}, {3, 5, 1, 1}, {4, 5, 1, 1}, {5, 5, 1, 1}, {6, 5, 1, 1}, {7, 5, 1, 1},
      {0, 6, 1, 1},   {1, 6, 1, 1}, {2, 6, 1, 1}, {3, 6, 1, 1}, {4, 6, 1, 1}, {5, 6, 1, 1}, {6, 6, 1, 1}, {7, 6, 1, 1},
      {0, 7, 1, 1},   {1, 7, 1, 1}, {2, 7, 1, 1}, {3, 7, 1, 1}, {4, 7, 1, 1}, {5, 7, 1, 1}, {6, 7, 1, 1}, {7, 7, 1, 1}};
  // clang-format on
  in_points.set_data({8, 8}, in_points_cpu.data());

  // output planes of best matching
  Engine::Gpu::Buff<ComputeLib::FitRegionsToPlanes::PlaneFit> out_planes;
  out_planes.set_storage(1, Magnum::GL::BufferUsage::DynamicDraw);
  vector<ComputeLib::FitRegionsToPlanes::PlaneFit> out_planes_raw(1);

  ivec2 out_planes_dims;
  fit_regions_to_planes.run(in_points, 8, out_planes, &out_planes_dims);

  out_planes.cu_copy_to(out_planes_raw);

  assertEqual(out_planes_dims, {1, 1});
  assertEqual(out_planes_raw[0].center, {3.59677410, 3.61290312, 1.00322580, 1});
  assertEqual(out_planes_raw[0].normal, {-0.0276710987, -0.0276710987, 0.999234021, 1});
  EXPECT_FLOAT_EQ(0.96875, out_planes_raw[0].percent_sampled);
  EXPECT_FLOAT_EQ(0.0726506338, out_planes_raw[0].sum_error);
}

/*
TEST_F(FitRegionsToPlanesTest, to_vtx_colors_6x6) {

  // input 3d points
  // last column and row should not factor into calculations! (only full planes)
  Engine::Gpu::Buff<vec4> in_points(3 * 6);
  Engine::Gpu::Buff<vec4> out_colors(3 * 6);
  // clang-format off
  vector<vec4> in_points_cpu = {
      {0, 0, 355, 1}, {0, 0, 250, 1}, {0, 0, 250, 1}, {0, 0, 250, 1}, {0, 0, 250, 1}, {0, 0, 250,
1}, {0, 0, 145, 1}, {0, 0, 250, 1}, {0, 0, 250, 1}, {0, 0, 250, 1}, {0, 0, 250, 1}, {0, 0, 250, 1},
      {0, 0, 250, 1}, {0, 0, 250, 1}, {0, 0, 250, 1}, {0, 0, 250, 1}, {0, 0, 250, 1}, {0, 0, 355,
1}};
  // clang-format on
  in_points.cu_copy_from(in_points_cpu.data(), in_points_cpu.size());

  // transform only includes translation... hopefully rotation will still work fine
  mat4 to_surface = glm::translate(glm::vec3{-250, -250, -250});

  const ivec2 vtx_grid_dims{3, 6};

  const float plane_thresh = 100.f;

  fit_regions_to_planes.to_vtx_colors(
      out_colors, in_points, to_surface, vtx_grid_dims, plane_thresh);

  vector<vec4> out_colors_cpu;
  out_colors.cu_copy_to(out_colors_cpu);

  const float above_plane_b_val = 1.;
  const float on_plane_b_val = 0.3;

  const float ab = above_plane_b_val;
  const float on = on_plane_b_val;

  // clang-format off
  vector<float> expected_out_colors_b_values =
      {
          ab, on, on, on, on, on,
          ab, on, on, on, on, on,
          on, on, on, on, on, ab
      };
  // clang-format on

  EXPECT_EQ(out_colors_cpu.size(), 3 * 6);

  for (int i = 0; i < (vtx_grid_dims.x * vtx_grid_dims.y); i++) {

    EXPECT_FLOAT_EQ(out_colors_cpu[i].b, expected_out_colors_b_values[i]);
  }

  // fit_regions_to_planes.to_vtx_colors()
}

*/
