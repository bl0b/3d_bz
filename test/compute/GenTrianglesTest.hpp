#pragma once

#include <TestCore.hpp>
#include <compute/ComputeLib.hpp>

// Test fixture
class GenTrianglesTest : public ::testing::Test {
protected:
  // shorthand for vectors with w value of 1 (to be interpreted as a real point) and
  // w value of 0 (not a point).
  const vec4 v_1{1, 1, 1, 1};
  const vec4 v_0{0, 0, 0, 0};

  ComputeLib::GenTriangles gen_triangles;

  void verify_triangles(
      unsigned int num_triangles,
      vector<unsigned int> actual_triangles,
      vector<unsigned int> expected_triangles) {

    for (int t = 0; t < expected_triangles.size() / 3; t++) {

      unsigned int* ex_t_start = expected_triangles.data() + (t * 3);
      unsigned int ex_t[3] = {ex_t_start[0], ex_t_start[1], ex_t_start[2]};

      bool found = false;
      int t_check = 0;
      while (!found && t_check < num_triangles) {

        unsigned int* ac_t_start = actual_triangles.data() + (t_check * 3);
        unsigned int ac_t[3] = {ac_t_start[0], ac_t_start[1], ac_t_start[2]};

        found = (ac_t[0] == ex_t[0] && ac_t[1] == ex_t[1] && ac_t[2] == ex_t[2]) ||
                (ac_t[0] == ex_t[0] && ac_t[1] == ex_t[2] && ac_t[2] == ex_t[1]) ||
                (ac_t[0] == ex_t[1] && ac_t[1] == ex_t[0] && ac_t[2] == ex_t[2]) ||
                (ac_t[0] == ex_t[1] && ac_t[1] == ex_t[2] && ac_t[2] == ex_t[0]) ||
                (ac_t[0] == ex_t[2] && ac_t[1] == ex_t[0] && ac_t[2] == ex_t[1]) ||
                (ac_t[0] == ex_t[2] && ac_t[1] == ex_t[1] && ac_t[2] == ex_t[0]);

        t_check++;
      }

      EXPECT_TRUE(found) << "Triangle not found in idxes array: " << ex_t[0] << "-" << ex_t[1]
                         << "-" << ex_t[2];
    }
  }
};

TEST_F(GenTrianglesTest, GenTriangles_4x4_allpoints) {

  // clang-format off
  vector<vec4> vtx_img_cpu{
    v_1, v_1, v_1, v_1,
    v_1, v_1, v_1, v_1,
    v_1, v_1, v_1, v_1,
    v_1, v_1, v_1, v_1,
  };
  // clang-format on

  Engine::Gpu::Tex<Engine::Gpu::TF::RGBA32F> vtx_img({4, 4}, vtx_img_cpu.data());

  Engine::Gpu::Buff<unsigned int> triangle_idxes;
  unsigned int num_triangles;

  gen_triangles.run(vtx_img, triangle_idxes, &num_triangles);

  // max num triangles
  EXPECT_EQ(num_triangles, 18);

  vector<unsigned int> triangle_idxes_cpu;
  triangle_idxes.cu_copy_to(triangle_idxes_cpu);

  // triangle_idxes.

  EXPECT_EQ(triangle_idxes_cpu.size(), 54);

  // clang-format off
  vector<unsigned int> expected_idxes{
      0, 4, 5,   1, 5, 6,   2, 6, 7,
      0, 1, 5,   1, 2, 6,   2, 3, 7,

      4, 8, 9,   5, 9,10,   6,10,11,
      4, 5, 9,   5, 6,10,   6, 7,11,

      8,12,13,   9,13,14,  10,14,15,
      8, 9,13,   9,10,14,  10,11,15
  };
  // clang-format on

  verify_triangles(num_triangles, triangle_idxes_cpu, expected_idxes);
}

TEST_F(GenTrianglesTest, GenTriangles_4x4_somePointsMissing) {

  // clang-format off
  vector<vec4> vtx_img_cpu{
    v_1, v_0, v_0, v_1,
    v_1, v_1, v_1, v_1,
    v_1, v_1, v_1, v_1,
    v_1, v_0, v_1, v_1,
  };
  // clang-format on

  Engine::Gpu::Tex<Engine::Gpu::TF::RGBA32F> vtx_img({4, 4}, vtx_img_cpu.data());

  Engine::Gpu::Buff<unsigned int> triangle_idxes;
  unsigned int num_triangles;

  gen_triangles.run(vtx_img, triangle_idxes, &num_triangles);

  // max num triangles
  EXPECT_EQ(num_triangles, 8);

  vector<unsigned int> triangle_idxes_cpu;
  triangle_idxes.cu_copy_to(triangle_idxes_cpu);

  EXPECT_EQ(triangle_idxes_cpu.size(), 54);

  // clang-format off
  vector<unsigned int> expected_idxes{
              


      4, 8, 9,   5, 9,10,   6,10,11,
      4, 5, 9,   5, 6,10,   6, 7,11,

                           10,14,15,
                           10,11,15
  };
  // clang-format on

  verify_triangles(num_triangles, triangle_idxes_cpu, expected_idxes);
}

TEST_F(GenTrianglesTest, GenTriangles_15x10_somePointsMissing) {

  // clang-format off
  vector<vec4> vtx_img_cpu{
    v_0, v_0, v_0, v_0, v_0, v_0, v_0, v_0, v_0, v_0, v_0, v_0, v_0, v_0, v_0,
    v_0, v_0, v_0, v_1, v_1, v_1, v_0, v_0, v_0, v_0, v_0, v_0, v_0, v_0, v_0,
    v_0, v_0, v_0, v_1, v_1, v_1, v_0, v_0, v_0, v_0, v_0, v_0, v_0, v_0, v_0,
    v_0, v_0, v_1, v_1, v_1, v_1, v_0, v_0, v_0, v_1, v_1, v_0, v_0, v_0, v_0,
    v_0, v_0, v_1, v_1, v_0, v_0, v_0, v_0, v_0, v_1, v_0, v_0, v_0, v_0, v_0,
    v_0, v_0, v_0, v_0, v_0, v_0, v_0, v_0, v_1, v_0, v_0, v_0, v_0, v_0, v_0,
    v_0, v_0, v_0, v_0, v_0, v_0, v_0, v_1, v_1, v_0, v_0, v_0, v_0, v_0, v_0,
    v_0, v_0, v_0, v_1, v_1, v_0, v_0, v_1, v_1, v_0, v_0, v_0, v_0, v_0, v_0,
    v_0, v_1, v_0, v_1, v_1, v_0, v_0, v_1, v_0, v_0, v_0, v_0, v_0, v_1, v_1,
    v_0, v_0, v_0, v_0, v_0, v_0, v_0, v_0, v_0, v_0, v_0, v_0, v_0, v_1, v_1,
  };
  // clang-format on

  Engine::Gpu::Tex<Engine::Gpu::TF::RGBA32F> vtx_img({15, 10}, vtx_img_cpu.data());

  Engine::Gpu::Buff<unsigned int> triangle_idxes;
  unsigned int num_triangles;

  gen_triangles.run(vtx_img, triangle_idxes, &num_triangles);

  // max num triangles
  EXPECT_EQ(num_triangles, 16);

  vector<unsigned int> triangle_idxes_cpu;
  triangle_idxes.cu_copy_to(triangle_idxes_cpu);

  // mostly 0s at end!
  EXPECT_EQ(triangle_idxes_cpu.size(), 756);

  // clang-format off
  vector<unsigned int> expected_idxes{
     18, 19, 34,
     19, 20, 35,
     33, 34, 49,
     34, 35, 50,
     47, 48, 63,
     97, 98,113,
    108,109,124,
     18, 34, 33,
     19, 35, 34,
     33, 49, 48,
     34, 50, 49,
     47, 63, 62,
     97,113,112,
    108,124,123,
    133,148,149,
    133,134,149
  };
  // clang-format on

  verify_triangles(num_triangles, triangle_idxes_cpu, expected_idxes);
}
