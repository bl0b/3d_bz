#pragma once

#include <gtest/gtest.h>
#include <Engine/Engine.h>

namespace TestCore {

const static Engine::Gpu::GraphicsCtx::Cfg DEFAULT_CFG = {500, 500, "test ctx", 4, 3};

class GpuCtx : public ::testing::Environment {

public:
  Engine::Gpu::GraphicsCtx* window = nullptr;
  const Engine::Gpu::GraphicsCtx::Cfg cfg;

  explicit GpuCtx(const Engine::Gpu::GraphicsCtx::Cfg _cfg = DEFAULT_CFG) : cfg(_cfg) {
    window = new Engine::Gpu::GraphicsCtx(cfg);
  }

  ~GpuCtx() override { delete window; }
};

} // namespace TestCore