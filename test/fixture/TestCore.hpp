#pragma once

#include <gtest/gtest.h>
#include <Engine/Engine.h>

#include "TestUtil.hpp"
#include "TestAssertions.hpp"
#include "RsTestData.hpp"
#include "GpuCtxProducer.hpp"
#include "ImgCompare/ImgCompare.hpp"

#include <string>

#define F_ERR 0.0001f

namespace TestCore {

class Args {
public:
  explicit Args(int c, char** v, vector<string>& new_args) {
    for (int i = 0; i < c; i++)
      args.push_back(str_cpy(v[i]));
    for (auto s : new_args)
      args.push_back(str_cpy(s.c_str()));
  }

  int size() { return args.size(); }

  char** data() { return args.data(); }

private:
  vector<char*> args;

  char* str_cpy(const char* src) {
    size_t len = (strlen(src) + 1) * sizeof(char);
    char* a = (char*)malloc(len);
    memcpy(a, src, len);
    return a;
  };
};

class Runner {
public:
  const string test_name;

  const string HTML_TEMPLATE_FILE =
      FileUtil::exe_dir + "test_log/log_viewer/test_results.html_template";
  const string RESULTS_JSON = "${RESULTS_JSON}";
  const string HTML_OUT_FILE;
  const string JSON_OUT_FILE;

  explicit Runner(
      int argc, char** argv, const string _test_name, vector<::testing::Environment*> env = {})
      : test_name(_test_name), JSON_OUT_FILE(FileUtil::exe_dir + "test_log/" + test_name + ".json"),
        HTML_OUT_FILE(FileUtil::exe_dir + "test_log/" + test_name + ".html") {

    // test name used to specify --gtest_output flag
    Args new_args(argc, argv, vector<string>{gtest_output_flag(JSON_OUT_FILE)});
    int num_args = new_args.size();
    ::testing::InitGoogleTest(&num_args, new_args.data());

    for (auto p : env)
      ::testing::AddGlobalTestEnvironment(p);
  }

  int run() {
    int test_result = RUN_ALL_TESTS();

    string html_template = FileUtil::load_text(HTML_TEMPLATE_FILE);
    str_replace(html_template, RESULTS_JSON, FileUtil::load_text(JSON_OUT_FILE));
    FileUtil::save_text(HTML_OUT_FILE, html_template);

    return test_result;
  }

private:
  string gtest_output_flag(const string filename) {
    return "--gtest_output=json:" + FileUtil::full_path(filename);
  }

  void str_replace(std::string& str, const std::string& old, const std::string& new_str) {
    std::string::size_type pos = 0u;
    while ((pos = str.find(old, pos)) != std::string::npos) {
      str.replace(pos, old.length(), new_str);
      pos += new_str.length();
    }
  }
};

}; // namespace TestCore
