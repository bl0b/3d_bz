#pragma once

#include <gtest/gtest.h>

#include <Engine/Engine.h>

template <std::size_t size, class T, glm::qualifier q>
void assertEqual(
    const glm::vec<size, T, q>& actual,
    const glm::vec<size, T, q>& expected,
    const T epsilon,
    const std::string& msg = "") {
  // TODO: error message doesn't correctly report origin file/line of error
  std::string s = msg.size() ? msg + "\n" : "Vector comparison failed:\n";
  // glm::vec<size, float, q> eps_vec(0.00001f);
  if (!glm::all(glm::equal(actual, expected, epsilon))) {
    FAIL() << s << " Expected: glm::" << glm::to_string(expected)
           << "\n Actual:   glm::" << glm::to_string(actual);
  }
}

// no epsilon... just use whatever is default
template <std::size_t size, class T, glm::qualifier q>
void assertEqual(
    const glm::vec<size, T, q>& actual,
    const glm::vec<size, T, q>& expected,
    const std::string& msg = "") {
  // TODO: error message doesn't correctly report origin file/line of error
  std::string s = msg.size() ? msg + "\n" : "Vector comparison failed:\n";
  if (!glm::all(glm::equal(actual, expected))) {
    FAIL() << s << " Expected: glm::" << glm::to_string(expected)
           << "\n Actual:   glm::" << glm::to_string(actual);
  }
}