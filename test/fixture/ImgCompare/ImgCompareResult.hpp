#pragma once

#include <TestCore.hpp>

namespace TestCore {

class ImgCompareResult {
public:
  enum DiffImage { EXPECTED, ACTUAL, DIFF };

  const int img_num;
  const string img_name;

  const ComputeLib::CompareDepthImages::CompareMode mode;
  const float thresh;

  explicit ImgCompareResult(
      int _img_num,
      string _img_name,
      ComputeLib::CompareDepthImages::CompareMode _mode,
      const float _thresh)
      : img_num(_img_num), filename_prefix(get_filename_prefix(_img_name)), img_name(_img_name),
        mode(_mode), thresh(_thresh) {}

  float compare_result;
  bool pass;

  // writes result to test output JSON, for consumption by the HTML viewer
  void write_to_output() {
    stringstream json_stream;
    {
      // wrap serialization in own scope, so destructor will close final json bracket
      cereal::JSONOutputArchive archive(json_stream);
      archive(
          cereal::make_nvp("img_num", img_num),
          cereal::make_nvp("img_name", img_name),
          cereal::make_nvp("mode", mode),
          cereal::make_nvp("pass_threshold", thresh),
          cereal::make_nvp("compare_result", compare_result),
          cereal::make_nvp("pass", pass),
          cereal::make_nvp(
              "expected_img", path_from_results_root(ImgCompareResult::DiffImage::EXPECTED)),
          cereal::make_nvp(
              "actual_img", path_from_results_root(ImgCompareResult::DiffImage::ACTUAL)),
          cereal::make_nvp("diff_img", path_from_results_root(ImgCompareResult::DiffImage::DIFF)));
    }

    TestUtil::record("screendiff_" + to_string(img_num) + "_obj", json_stream.str());
    TestUtil::record("screendiff_count", to_string(img_num + 1));
  }

  string path_from_binary_root(DiffImage img) {
    return FileUtil::exe_dir + from_binary_root_prefix + filename_prefix + get(img) + png_suffix;
  }

  string path_from_results_root(DiffImage img) {
    return from_results_root_prefix + filename_prefix + get(img) + png_suffix;
  }

  ::testing::AssertionResult assertion() {

    if (pass) { return ::testing::AssertionSuccess(); }

    switch (mode) {
    case ComputeLib::CompareDepthImages::CompareMode::EXACT_MATCH:
      return ::testing::AssertionFailure()
             << img_name << ": only " << compare_result << "% of pixels match";
    case ComputeLib::CompareDepthImages::CompareMode::AVG_DIFF:
      return ::testing::AssertionFailure() << img_name << ": avg diff is " << compare_result;
      break;
    }
  }

private:
  const string from_binary_root_prefix = "test_log/screen_diffs/";
  const string from_results_root_prefix = "./screen_diffs/";
  const string filename_prefix;
  const string png_suffix = ".png";

  string get(DiffImage img) {
    switch (img) {
    case EXPECTED:
      return "expected";
    case ACTUAL:
      return "actual";
    case DIFF:
      return "diff";
    }
  }

  string get_filename_prefix(string _img_name) {
    return TestUtil::current_suite_name() + "_" + TestUtil::current_test_name() + "_" + img_name +
           "_";
  }
};

} // namespace TestCore
