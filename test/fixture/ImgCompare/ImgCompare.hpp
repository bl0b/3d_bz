#pragma once

#include <TestCore.hpp>
#include <compute/ComputeLib.hpp>

#include "ImgCompareResult.hpp"

using namespace Engine::Gpu;

namespace TestCore {

class ImgCompare {

  ComputeLib::MapDepth map_depth;

public:
  void expect_match(
      const string name,
      Tex<TF::R16UI>& expected,
      Tex<TF::R16UI>& actual,
      ComputeLib::CompareDepthImages::CompareMode mode,
      const float pass_threshold) {

    ImgCompareResult compare_result = compare(name, expected, actual, mode, pass_threshold);
    EXPECT_TRUE(compare_result.assertion());
  }

  ImgCompareResult compare(
      const string name,
      Tex<TF::R16UI>& expected,
      Tex<TF::R16UI>& actual,
      ComputeLib::CompareDepthImages::CompareMode mode,
      const float pass_threshold) {

    vector<ImgCompareResult>& current_results = diffs_by_test[TestUtil::current_test_name()];
    ImgCompareResult new_result(current_results.size(), name, mode, pass_threshold);

    map_depth.run(expected, out_rgba);
    out_rgba.save_png(new_result.path_from_binary_root(ImgCompareResult::DiffImage::EXPECTED));

    map_depth.run(actual, out_rgba);
    out_rgba.save_png(new_result.path_from_binary_root(ImgCompareResult::DiffImage::ACTUAL));

    depth_img_compare.run(expected, actual, out_diff, &new_result.compare_result, mode);
    map_depth.run(out_diff, out_rgba);
    out_rgba.save_png(new_result.path_from_binary_root(ImgCompareResult::DiffImage::DIFF));

    switch (mode) {
    case ComputeLib::CompareDepthImages::CompareMode::EXACT_MATCH:
      new_result.pass = new_result.compare_result >= pass_threshold;
      break;
    case ComputeLib::CompareDepthImages::CompareMode::AVG_DIFF:
      // pct match really avg diff..
      new_result.pass = new_result.compare_result < pass_threshold;
      break;
    }

    new_result.write_to_output();
    current_results.push_back(new_result);
    return new_result;
  }

private:
  // intermediate texture objects & comparator program object
  Tex<TF::RGBA8> out_rgba;
  Tex<TF::R16UI> out_diff;
  ComputeLib::CompareDepthImages depth_img_compare;

  // keeps track of diffs performed by each test
  unordered_map<string, vector<ImgCompareResult>> diffs_by_test;
};

} // namespace TestCore
