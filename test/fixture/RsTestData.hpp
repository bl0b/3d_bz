#pragma once

#include <gtest/gtest.h>
#include <Engine/Engine.h>

#include <Components/RsCtxFile.hpp>
#include <Components/RsDataBag.hpp>

class RsTestData : public ::testing::Environment {
public:
  unordered_map<string, RsDataBag> data;

  const string TABLE_1_D415 = "table_1_d415.bag";
  const string TABLE_1_D435 = "table_1_d435.bag";
  const string DESK_SNAPSHOT = "desk_snapshot.bag";
  const string DEMO_SEQUENCE_1 = "demo_sequence_1.bag";

  const vector<string> data_files{TABLE_1_D415, TABLE_1_D435, DESK_SNAPSHOT, DEMO_SEQUENCE_1};

  int num = 0;

  void SetUp() override {
    for (auto f : data_files) {
      RsDataBag bag;
      data[f].from_file(f);
    }
  }

  void TearDown() override {
    // clear GL textures before GL context is destroyed
    for (auto& d : data) {
      d.second.depth_frames.clear();
      d.second.rgba_frames.clear();
    }
  }

  ~RsTestData() override {}
};
