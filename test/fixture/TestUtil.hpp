#pragma once

#include <TestCore.hpp>

namespace TestCore {

class TestUtil {

public:
  static string current_suite_name() {
    const ::testing::TestInfo* const test_info =
        ::testing::UnitTest::GetInstance()->current_test_info();
    return string(test_info->test_case_name());
  }

  static string current_test_name() {
    const ::testing::TestInfo* const test_info =
        ::testing::UnitTest::GetInstance()->current_test_info();
    return string(test_info->name());
  }

  static void record(const string key, const string val) {
    ::testing::Test::RecordProperty(key, val);
  }
};

} // namespace TestCore
